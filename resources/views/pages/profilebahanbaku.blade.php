@extends('layout.admin.app')

@section('content')

<div class="row">
        <!-- Data Penrusahaan -->
        <div class="col-xxl-12 order-xxl-1">
            @include('pages.widgets.profile.w_data_profile_bahan_baku', ['class' => 'card-stretch gutter-b'])
        </div>
    
        <!-- END -->
    </div>

@endsection