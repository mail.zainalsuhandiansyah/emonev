@extends('layout.blank.app')

@section('styles')
<link href="{{ asset('css/landing/timelinenew.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('css/landing/menunav.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

<div class="card card-custom">
    <div class="card-header">

        <h1 class="card-title align-items-start flex-column" style="width:120px;">
            <img src="{{ asset('media/logos/emonev-new-logo.png') }}" style="width: 160%;">
        </h1>

        @include('landing.nav')

        <div class="card-toolbar">
            <button type="button" onclick="window.location='{{ url("auth/login") }}'" class="btn btn-success font-weight-bolder font-size-h6 px-8 py-4 my-3 mr-3">Login</button>
        </div>

    </div>
</div>
<img src="{{ asset('media/bg/stroke.png') }}">

<div class="container py-8">
    <div class="row">
        <div class="col-lg-12">
            <div class="card card-custom gutter-b">
                <div class="card-header border-0 pt-5">
                    <h2 class="card-title align-items-start flex-column">
                        <span class="card-label font-weight-bold font-size-h4 text-dark-75">Info Covid-19 Indonesia</span>
                    </h2>
                </div>
                <div class="card card-body">
                    <div style="width: 100%">
                        <div style="width: 100%">
                            <iframe width="100%" height="600" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?width=100%25&amp;height=600&amp;hl=en&amp;q=Jl.%20H.%20R.%20Rasuna%20Said%20No.4-9,%20RT.1/RW.2,%20Kuningan,%20Kuningan%20Tim.,%20Kecamatan%20Setiabudi,%20Kota%20Jakarta%20Selatan,%20Daerah%20Khusus%20Ibukota%20Jakarta%2012950,%20Indonesia+()&amp;t=&amp;z=14&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-2">
        </div>
        <div class="col-lg-8">
            <div class="card card-custom gutter-b">
                <div class="card-header border-0 pt-5">
                    <h2 class="card-title align-items-start flex-column">
                        <span class="card-label font-weight-bold font-size-h4 text-dark-75">Contact Us</span>
                    </h2>
                </div>
                <div class="card card-custom">
                    <form method="POST" id="form-contact" action="{{ url('/contact') }}">
                        @csrf
                        <div class="card-body">
                            <div class="form-group">
                                <label for="name">Name <span class="text-danger">*</span></label>
                                <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="{{ old('name') }}" placeholder="Enter Name" />
                            </div>
                            <div class="form-group">
                                <label for="email">Email address <span class="text-danger">*</span></label>
                                <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" value="{{ old('email') }}" placeholder="Enter email" />
                            </div>
                            <div class="form-group">
                                <label for="telp">Phone <span class="text-danger">*</span></label>
                                <input type="text" class="form-control @error('telp') is-invalid @enderror" id="telp" name="telp" value="{{ old('telp') }}" placeholder="Enter Phone" />
                            </div>
                            <div class="form-group mb-1">
                                <label for="address">Address <span class="text-danger">*</span></label>
                                <textarea class="form-control @error('address') is-invalid @enderror" id="address" name="address" value="{{ old('address') }}" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary mr-2">Send</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-2">
        </div>
    </div>



</div>