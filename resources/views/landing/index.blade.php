@extends('layout.blank.app')

@section('styles')
<link href="{{ asset('css/landing/timelinenew2.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('css/landing/menunav.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

<div class="card card-custom">
    <div class="card-header">

        <h1 class="card-title align-items-start flex-column" style="width:120px;">
            <img src="{{ asset('media/logos/emonev-new-logo.png') }}" style="width: 160%;">
        </h1>

        @include('landing.nav')
        <div class="card-toolbar">
            <button type="button" onclick="window.location='{{ route('login') }}'" class="btn btn-success font-weight-bolder font-size-h6 px-8 py-4 my-3 mr-3">Login</button>

        </div>

    </div>
</div>
<img src="{{ asset('media/bg/stroke.png') }}">
<div class="container py-5">
    <div class="row">
        <div class="col-lg-6">
            <div class="card card-custom gutter-b">
                <div class="card-header border-0 pt-5">
                    <h2 class="card-title align-items-start flex-column">
                        <span class="card-label font-weight-bold font-size-h3 text-dark-75">Informasi Industri Farmasi</span>
                    </h2>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="card card-custom gutter-b">
                <div class="card-header border-0 pt-5">
                    <h2 class="card-title align-items-start flex-column">
                        <span class="card-label font-weight-bold font-size-h3 text-dark-75">Informasi Pengembangan Bahan Baku Obat</span>
                    </h2>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3">
            <div class="card card-custom wave wave-animate-slow wave-primary mb-8 mb-lg-0">
                <div class="card-body ">
                    <div class="d-flex align-items-center p-3">
                        <div class="mr-2">
                            <span class="svg-icon svg-icon-primary svg-icon-4x">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24" />
                                        <path d="M13,17.0484323 L13,18 L14,18 C15.1045695,18 16,18.8954305 16,20 L8,20 C8,18.8954305 8.8954305,18 10,18 L11,18 L11,17.0482312 C6.89844817,16.5925472 3.58685702,13.3691811 3.07555009,9.22038742 C3.00799634,8.67224972 3.3975866,8.17313318 3.94572429,8.10557943 C4.49386199,8.03802567 4.99297853,8.42761593 5.06053229,8.97575363 C5.4896663,12.4577884 8.46049164,15.1035129 12.0008191,15.1035129 C15.577644,15.1035129 18.5681939,12.4043008 18.9524872,8.87772126 C19.0123158,8.32868667 19.505897,7.93210686 20.0549316,7.99193546 C20.6039661,8.05176407 21.000546,8.54534521 20.9407173,9.09437981 C20.4824216,13.3000638 17.1471597,16.5885839 13,17.0484323 Z" fill="#000000" fill-rule="nonzero" />
                                        <path d="M12,14 C8.6862915,14 6,11.3137085 6,8 C6,4.6862915 8.6862915,2 12,2 C15.3137085,2 18,4.6862915 18,8 C18,11.3137085 15.3137085,14 12,14 Z M8.81595773,7.80077353 C8.79067542,7.43921955 8.47708263,7.16661749 8.11552864,7.19189981 C7.75397465,7.21718213 7.4813726,7.53077492 7.50665492,7.89232891 C7.62279197,9.55316612 8.39667037,10.8635466 9.79502238,11.7671393 C10.099435,11.9638458 10.5056723,11.8765328 10.7023788,11.5721203 C10.8990854,11.2677077 10.8117724,10.8614704 10.5073598,10.6647638 C9.4559885,9.98538454 8.90327706,9.04949813 8.81595773,7.80077353 Z" fill="#000000" opacity="0.3" />
                                    </g>
                                </svg>
                            </span>
                        </div>
                        <div class="d-flex flex-column">
                            <a href="#" class="text-dark text-hover-primary font-weight-bold font-size-h5 mb-3" style="white-space:nowrap;" data-toggle="modal" data-target="#modaljumlahindustrifarmasi">Jumlah Industri <br /> Farmasi</a>
                            <div class="text-dark-75 font-size-h6">{{$datas['jmlF']}}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="card card-custom wave wave-animate-slow wave-danger mb-8 mb-lg-0">
                <div class="card-body ">
                    <div class="d-flex align-items-center p-3">
                        <div class="mr-2">
                            <span class="svg-icon svg-icon-primary svg-icon-4x">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24" />
                                        <path d="M13,17.0484323 L13,18 L14,18 C15.1045695,18 16,18.8954305 16,20 L8,20 C8,18.8954305 8.8954305,18 10,18 L11,18 L11,17.0482312 C6.89844817,16.5925472 3.58685702,13.3691811 3.07555009,9.22038742 C3.00799634,8.67224972 3.3975866,8.17313318 3.94572429,8.10557943 C4.49386199,8.03802567 4.99297853,8.42761593 5.06053229,8.97575363 C5.4896663,12.4577884 8.46049164,15.1035129 12.0008191,15.1035129 C15.577644,15.1035129 18.5681939,12.4043008 18.9524872,8.87772126 C19.0123158,8.32868667 19.505897,7.93210686 20.0549316,7.99193546 C20.6039661,8.05176407 21.000546,8.54534521 20.9407173,9.09437981 C20.4824216,13.3000638 17.1471597,16.5885839 13,17.0484323 Z" fill="#000000" fill-rule="nonzero" />
                                        <path d="M12,14 C8.6862915,14 6,11.3137085 6,8 C6,4.6862915 8.6862915,2 12,2 C15.3137085,2 18,4.6862915 18,8 C18,11.3137085 15.3137085,14 12,14 Z M8.81595773,7.80077353 C8.79067542,7.43921955 8.47708263,7.16661749 8.11552864,7.19189981 C7.75397465,7.21718213 7.4813726,7.53077492 7.50665492,7.89232891 C7.62279197,9.55316612 8.39667037,10.8635466 9.79502238,11.7671393 C10.099435,11.9638458 10.5056723,11.8765328 10.7023788,11.5721203 C10.8990854,11.2677077 10.8117724,10.8614704 10.5073598,10.6647638 C9.4559885,9.98538454 8.90327706,9.04949813 8.81595773,7.80077353 Z" fill="#000000" opacity="0.3" />
                                    </g>
                                </svg>
                            </span>
                        </div>
                        <div class="d-flex flex-column">
                            <a href="#" class="text-dark text-hover-primary font-weight-bold font-size-h5 mb-3" style="white-space:nowrap;" data-toggle="modal" data-target="#modaljmlFbbo">Jumlah Industri <br /> Farmasi Bahan Obat</a>
                            <div class="text-dark-75 font-size-h6">{{$datas['jmlFBBO']}}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="card card-custom wave wave-animate-slow wave-success mb-8 mb-lg-0">
                <div class="card-body ">
                    <div class="d-flex align-items-center p-3">
                        <div class="mr-2">
                            <span class="svg-icon svg-icon-success svg-icon-4x">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24" />
                                        <polygon fill="#000000" opacity="0.3" points="5 3 19 3 23 8 1 8" />
                                        <polygon fill="#000000" points="23 8 12 20 1 8" />
                                    </g>
                                </svg>
                            </span>
                        </div>
                        <div class="d-flex flex-column">
                            <a href="#" class="text-dark text-hover-primary font-weight-bold font-size-h5 mb-3" style="white-space:nowrap;" data-toggle="modal" data-target="#modaljmlbbo">Jumlah Pengembangan <br /> Bahan Baku Obat</a>
                            <div class="text-dark-75 font-size-h6">{{$datas['jmlBBO']}}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="card card-custom wave wave-animate-slow wave-warning mb-8 mb-lg-0">
                <div class="card-body ">
                    <div class="d-flex align-items-center p-3">
                        <div class="mr-2">
                            <span class="svg-icon svg-icon-primary svg-icon-4x">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24" />
                                        <path d="M13,17.0484323 L13,18 L14,18 C15.1045695,18 16,18.8954305 16,20 L8,20 C8,18.8954305 8.8954305,18 10,18 L11,18 L11,17.0482312 C6.89844817,16.5925472 3.58685702,13.3691811 3.07555009,9.22038742 C3.00799634,8.67224972 3.3975866,8.17313318 3.94572429,8.10557943 C4.49386199,8.03802567 4.99297853,8.42761593 5.06053229,8.97575363 C5.4896663,12.4577884 8.46049164,15.1035129 12.0008191,15.1035129 C15.577644,15.1035129 18.5681939,12.4043008 18.9524872,8.87772126 C19.0123158,8.32868667 19.505897,7.93210686 20.0549316,7.99193546 C20.6039661,8.05176407 21.000546,8.54534521 20.9407173,9.09437981 C20.4824216,13.3000638 17.1471597,16.5885839 13,17.0484323 Z" fill="#000000" fill-rule="nonzero" />
                                        <path d="M12,14 C8.6862915,14 6,11.3137085 6,8 C6,4.6862915 8.6862915,2 12,2 C15.3137085,2 18,4.6862915 18,8 C18,11.3137085 15.3137085,14 12,14 Z M8.81595773,7.80077353 C8.79067542,7.43921955 8.47708263,7.16661749 8.11552864,7.19189981 C7.75397465,7.21718213 7.4813726,7.53077492 7.50665492,7.89232891 C7.62279197,9.55316612 8.39667037,10.8635466 9.79502238,11.7671393 C10.099435,11.9638458 10.5056723,11.8765328 10.7023788,11.5721203 C10.8990854,11.2677077 10.8117724,10.8614704 10.5073598,10.6647638 C9.4559885,9.98538454 8.90327706,9.04949813 8.81595773,7.80077353 Z" fill="#000000" opacity="0.3" />
                                    </g>
                                </svg>
                            </span>
                        </div>
                        <div class="d-flex flex-column">
                            <a href="#" class="text-dark text-hover-primary font-weight-bold font-size-h5 mb-3" style="white-space:nowrap;" data-toggle="modal" data-target="#modaljmlhilirisasi">Hirilisasi BBO <br /> Dalam Negeri</a>
                            <div class="text-dark-75 font-size-h6">{{$datas['jmlHilirisasi']}}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container py-0">
    <div class="row">
        <div class="col-lg-8">
            <div class="card card-custom gutter-b">
                <div class="card-header border-0 pt-5">
                    <h2 class="card-title align-items-start flex-column">
                        <span class="card-label font-weight-bold font-size-h3 text-dark-75">Roadmap Pengembangan Bahan Baku Obat Dalam Negeri</span>
                    </h2>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card card-custom gutter-b">
                <div class="card-header border-0 pt-5">
                    <h2 class="card-title align-items-start flex-column">
                        <span class="card-label font-weight-bold font-size-h3 text-dark-75">Visitor</span>
                    </h2>
                </div>
            </div>
        </div>
    </div>
    <div class="row">

        <div class="col-md-8">
            @include('landing.timelinenew2', ['class' => 'card-stretch gutter-b'])
            
        </div>
        <div class="col-md-4">
                <div class="card card-custom gutter-b">
                    <div class="card-body">
                        <div class="d-flex align-items-center mb-8">
                            <div class="symbol symbol-40 symbol-light-success mr-5">
                                <span class="symbol-label">
                                    <img src="{{ asset('media/svg/avatars/009-boy-4.svg') }}" class="h-75 align-self-end" />
                                </span>
                            </div>
                            <div class="d-flex flex-column flex-grow-1 font-weight-bold font-size-h5">
                                <a href="#" class="text-dark text-hover-primary mb-1 font-size-h4 ">Hari ini</a>
                                <span class="text-muted">{{$visitor->cToday}}</span>
                            </div>
                        </div>
                        <div class="d-flex align-items-center mb-8">
                            <div class="symbol symbol-40 symbol-light-success mr-5">
                                <span class="symbol-label">
                                    <img src="{{ asset('media/svg/avatars/006-girl-3.svg') }}" class="h-75 align-self-end" />
                                </span>
                            </div>
                            <div class="d-flex flex-column flex-grow-1 font-weight-bold font-size-h5">
                                <a href="#" class="text-dark text-hover-primary mb-1 font-size-h4">Kemarin</a>
                                <span class="text-muted">{{$visitor->cYesterday}}</span>
                            </div>
                        </div>
                        <div class="d-flex align-items-center mb-8">
                            <div class="symbol symbol-40 symbol-light-success mr-5">
                                <span class="symbol-label">
                                    <img src="{{ asset('media/svg/avatars/011-boy-5.svg') }}" class="h-75 align-self-end" />
                                </span>
                            </div>
                            <div class="d-flex flex-column flex-grow-1 font-weight-bold font-size-h5">
                                <a href="#" class="text-dark text-hover-primary mb-1 font-size-h4">Bulan Ini</a>
                                <span class="text-muted">{{$visitor->cMonth}}</span>
                            </div>
                        </div>
                        <div class="d-flex align-items-center mb-8">
                            <div class="symbol symbol-40 symbol-light-success mr-5">
                                <span class="symbol-label">
                                    <img src="{{ asset('media/svg/avatars/015-boy-6.svg') }}" class="h-75 align-self-end" />
                                </span>
                            </div>
                            <div class="d-flex flex-column flex-grow-1 font-weight-bold font-size-h5">
                                <a href="#" class="text-dark text-hover-primary mb-1 font-size-h4">Tahun Ini</a>
                                <span class="text-muted">{{$visitor->cYear}}</span>
                            </div>
                        </div>
                        <div class="d-flex align-items-center mb-8">
                            <div class="symbol symbol-40 symbol-light-success mr-5">
                                <span class="symbol-label">
                                    <img src="{{ asset('media/svg/avatars/015-boy-6.svg') }}" class="h-75 align-self-end" />
                                </span>
                            </div>
                            <div class="d-flex flex-column flex-grow-1 font-weight-bold font-size-h5">
                                <a href="#" class="text-dark text-hover-primary mb-1 font-size-h4">Total</a>
                                <span class="text-muted">{{$visitor->count_all}}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    </div>
</div>
<div class="container py-8">
    <div class="row">
        <div id="carouselExampleControls" class="card carousel slide" data-ride="carousel" style="height: 300px;">
            <div class="carousel-inner" style="height: 300px;">
                    
            <!-- <div class="carousel-item active">
            </div> -->
                @php
                    $isFirst = 0;
                @endphp
                @foreach($slider as $img)
                
                
                
                <div class="carousel-item 
                    
                @php
                if($isFirst==0){
                    echo'active';
                }else{
                    echo'';
                }
                @endphp
                " style="width: 1300px !important;">
                
                    <img  class="d-block w-100" src="{{ url('images' .'/'. $img->image) }}" style="height:300px !important;" >
                </div>
                @php
                 $isFirst++
                @endphp
                @endforeach

                <!-- <div class="carousel-item">
                    <img class="d-block w-100" src="{{ asset('media/bg/gambar2.png') }}" alt="Second slide" style="height:400px">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="{{ asset('media/bg/gambar3.png') }}" alt="Third slide" style="height:400px">
                </div> -->
            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-lg-6">
            <div class="card card-custom gutter-b">
                <div class="card-header border-0 pt-7">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label font-weight-bold font-size-h4 text-dark-75">News</span>
                        <span class="text-muted mt-3 font-weight-bold font-size-sm">Get Info Update
                    </h3>
                </div>
                <div class="card-body pt-1">
                    <div class="tab-content mt-5" id="myTabLIist18">
                        <div class="tab-pane fade" id="kt_tab_pane_1_1" role="tabpanel" aria-labelledby="kt_tab_pane_1_1">
                            <div class="form">
                                <div class="d-flex align-items-center pb-9">
                                    <div class="symbol symbol-60 symbol-2by3 flex-shrink-0 mr-4">
                                        <div class="symbol-label" style="background-image: url('assets/media/stock-600x400/img-25.jpg')"></div>
                                    </div>
                                    <div class="d-flex flex-column flex-grow-1">
                                        <a href="#" class="text-dark-75 font-weight-bolder text-hover-primary font-size-lg mb-1">Nike &amp; Blue</a>
                                        <span class="text-dark-50 font-weight-normal font-size-sm">Your website will have long term business should think about those term business</span>
                                    </div>
                                </div>
                                <div class="d-flex align-items-center pb-9">
                                    <div class="symbol symbol-60 symbol-2by3 flex-shrink-0 mr-4">
                                        <div class="symbol-label" style="background-image: url('assets/media/stock-600x400/img-24.jpg')"></div>
                                    </div>
                                    <div class="d-flex flex-column flex-grow-1">
                                        <a href="#" class="text-dark-75 font-weight-bolder text-hover-primary font-size-lg mb-1">Red Boots</a>
                                        <span class="text-dark-50 font-weight-normal font-size-sm">Have long term business objectives. You should think about those long term business</span>
                                    </div>
                                </div>
                                <div class="d-flex align-items-center pb-9">
                                    <div class="symbol symbol-60 symbol-2by3 flex-shrink-0 mr-4">
                                        <div class="symbol-label" style="background-image: url('assets/media/stock-600x400/img-20.jpg')"></div>
                                    </div>
                                    <div class="d-flex flex-column flex-grow-1">
                                        <a href="#" class="text-dark-75 font-weight-bolder font-size-lg text-hover-primary mb-1">Cup &amp; Green</a>
                                        <span class="text-dark-50 font-weight-normal font-size-sm">Your company your website have long term business objectives. You should think about</span>
                                    </div>
                                </div>
                                <div class="d-flex align-items-center pb-6">
                                    <div class="symbol symbol-60 symbol-2by3 flex-shrink-0 mr-4">
                                        <div class="symbol-label" style="background-image: url('assets/media/stock-600x400/img-19.jpg')"></div>
                                    </div>
                                    <div class="d-flex flex-column flex-grow-1">
                                        <a href="#" class="text-dark-75 font-weight-bolder text-hover-primary font-size-lg mb-1">Yellow Background</a>
                                        <span class="text-dark-50 font-weight-normal font-size-sm">Your company will have long term business objectives You should think about those long</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade show active" id="kt_tab_pane_2_2" role="tabpanel" aria-labelledby="kt_tab_pane_2_2">
                            <div class="form">
                                @foreach($article as $key => $value)
                                @php
                                    $url = "/{$value->slug}-{$value->uuid}";
                                    $current_url = url('admin/content'). "/" . $value->id;
                                @endphp
                                <div class="d-flex align-items-center pb-9">
                                    @if($value->image)
                                    <div class="symbol symbol-60 symbol-2by3 flex-shrink-0 mr-4">
                                        <img src="{{ url($value->image_url) }}">
                                    </div>
                                    @else
                                    <div class="symbol symbol-60 symbol-2by3 flex-shrink-0 mr-4">
                                        <div class="symbol-label" style="background-image: url({{ asset('media/stock-600x400/img-20.jpg') }})"></div>
                                    </div>
                                    @endif
                                    <div class="d-flex flex-column flex-grow-1">
                                        <a href="{{$url}}" class="text-dark-75 font-weight-bolder font-size-lg text-hover-primary mb-1">{{$value->title}}</a>
                                        <span class="text-dark-50 font-weight-normal font-size-sm">{{$value->limit_content}}</span>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="card card-custom gutter-b">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="symbol symbol-45 symbol-light mr-5">
                            <span class="symbol-label">
                                <span class="svg-icon svg-icon-lg svg-icon-primary">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <rect x="0" y="0" width="24" height="24" />
                                            <path d="M8,3 L8,3.5 C8,4.32842712 8.67157288,5 9.5,5 L14.5,5 C15.3284271,5 16,4.32842712 16,3.5 L16,3 L18,3 C19.1045695,3 20,3.8954305 20,5 L20,21 C20,22.1045695 19.1045695,23 18,23 L6,23 C4.8954305,23 4,22.1045695 4,21 L4,5 C4,3.8954305 4.8954305,3 6,3 L8,3 Z" fill="#000000" opacity="0.3" />
                                            <path d="M11,2 C11,1.44771525 11.4477153,1 12,1 C12.5522847,1 13,1.44771525 13,2 L14.5,2 C14.7761424,2 15,2.22385763 15,2.5 L15,3.5 C15,3.77614237 14.7761424,4 14.5,4 L9.5,4 C9.22385763,4 9,3.77614237 9,3.5 L9,2.5 C9,2.22385763 9.22385763,2 9.5,2 L11,2 Z" fill="#000000" />
                                            <rect fill="#000000" opacity="0.3" x="7" y="10" width="5" height="2" rx="1" />
                                            <rect fill="#000000" opacity="0.3" x="7" y="14" width="9" height="2" rx="1" />
                                        </g>
                                    </svg>
                                </span>
                            </span>
                        </div>
                        <div class="d-flex flex-column flex-grow-1">
                            <a href="#" class="text-dark-75 text-hover-primary mb-1 font-size-lg font-weight-bolder">Video From Youtube</a>
                        </div>
                    </div>
                    <div class="pt-3">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item rounded" src="https://www.youtube.com/embed/K6rqSJhN1NY" allowfullscreen="allowfullscreen"></iframe>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<img src="{{ asset('media/bg/stroke.png') }}">
<div class="card card-custom">
    <div class="card-body">
        <div class="container py-8">
            <div class="row">
                <div class="col-lg-4">
                    <img src="{{ asset('media/logos/emonev-new-logo.png') }}" style="width: 65%;">
                    <br><br>
                    @foreach($footers as $footer)
                    <!-- <span>Direktorat Jenderal Kefarmasian dan Alat Kesehatan
                        Kementerian Kesehatan RI<br>
                        Gedung Dr. Adhyatma, MPH, Lt. 8 R.817<br>
                        Jl. H.R. Rasuna Said Blok X-5 Kav. 4-9<br>
                        Jakarta Selatan 12950<br>
                        Halo Kemkes ✆ 1500567
                    </span> -->
                        <span>
                            {{ $footer->nama_perusahaan}} <br>
                            {{ $footer->tempat }} <br>
                            {{ $footer->alamat_perusahaan }} <br>
                            {{ $footer->kota }} {{ $footer->kodepos }} <br>
                            {{ $footer->nama_callcenter }} ✆  {{ $footer->notelp }}
                        </span>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
<div class="footer bg-white py-4 d-flex flex-lg-column" id="kt_footer">
    <!--begin::Container-->
    <div class="container-fluid d-flex flex-column flex-md-row align-items-center justify-content-between">
        <!--begin::Copyright-->
        <div class="text-dark order-2 order-md-1">
            <span class="text-muted font-weight-bold mr-2">2021©</span>
            <a href="http://keenthemes.com/metronic" target="_blank" class="text-dark-75 text-hover-primary">Emonev BBO</a>
        </div>
        <!--end::Copyright-->
        <!--begin::Nav-->
        <div class="nav nav-dark">

        </div>
        <!--end::Nav-->
    </div>
</div>

<!-- MODAL POPUP -->
<!-- Jumlah Industri Farmasi -->
<div class="modal fade" id="modaljumlahindustrifarmasi" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Jumlah Industri Farmasi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <table class="table">
                    <!-- <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">First</th>
                            <th scope="col">Last</th>
                            <th scope="col">Status</th>
                        </tr>
                    </thead> -->
                    <tbody>
                        @php
                            $no=1;
                        @endphp
                       @foreach($getjmlFs as $getjmlF)
                        <tr>
                            <th scope="row">{{$no++}}</th>
                            <td>{{$getjmlF->NAMA}}</td>
                        </tr>
                        @endforeach
                       
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- END Jumlah Industri Farmasi  -->
<!-- Jumlah FBBO -->
<div class="modal fade" id="modaljmlFbbo" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Jumlah Industri Farmasi Bahan Baku Obat</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <table class="table">
                    <!-- <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">First</th>
                            <th scope="col">Last</th>
                            <th scope="col">Status</th>
                        </tr>
                    </thead> -->
                    <tbody>
                        @php
                            $no=1;
                        @endphp
                       @foreach($getjmlFBBOs as $getjmlFBBO)
                        <tr>
                            <th scope="row">{{$no++}}</th>
                            <td>{{$getjmlFBBO->NAMA}}</td>
                        </tr>
                        @endforeach
                       
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- END Jumlah FBBO -->
<!-- Jumlah FBBO -->
<div class="modal fade" id="modaljmlbbo" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Jumlah Pengembangan Bahan Baku Obat</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <table class="table">
                    <!-- <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">First</th>
                            <th scope="col">Last</th>
                            <th scope="col">Status</th>
                        </tr>
                    </thead> -->
                    <tbody>
                        @php
                            $no=1;
                        @endphp
                       @foreach($getjmlBBOs as $getjmlBBO)
                        <tr>
                            <th scope="row">{{$no++}}</th>
                            <td>{{$getjmlBBO->zat_active_name}}</td>
                        </tr>
                        @endforeach
                       
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- END Jumlah FBBO -->
<!-- Jumlah hilirisasi -->
<div class="modal fade" id="modaljmlhilirisasi" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Hilirisasi BBO Dalam Negeri</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <table class="table">
                    <!-- <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">First</th>
                            <th scope="col">Last</th>
                            <th scope="col">Status</th>
                        </tr>
                    </thead> -->
                    <tbody>
                        @php
                            $no=1;
                        @endphp
                       @foreach($getjmlhills as $getjmlhill)
                        <tr>
                            <th scope="row">{{$no++}}</th>
                            <td>{{ $getjmlhill->zat_active_name}}</td>
                        </tr>
                        @endforeach
                       
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- END Jumlah hilirisasi -->
<!-- END MODAL POPUP -->
@endsection

@section('scripts')
<script src="{{ asset('js/landing/timelinenew2.js') }}" type="text/javascript"></script>


@endsection
