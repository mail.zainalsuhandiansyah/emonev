<!-- NAV -->
<ul class="menu">
    <li><a href="{{ url('/') }}">Home</a></li>
    @foreach($menu as $key => $value)
    <li><a href="{{$value->menus_path}}">{{$value->menus_name}}</a></li>
    @endforeach
    <li><a href="{{ url('/contact') }}">Contact</a></li>
    <li class="slider"></li>
</ul>
<!-- END NAV -->