<div class="container py-8">
    <div class="row">
        <div class="col-lg-12">
            <div class="card card-custom gutter-b">
                <div class="card-header border-0 pt-5">
                    <h2 class="card-title align-items-start flex-column">
                        <span class="card-label font-weight-bold font-size-h4 text-dark-75">Roadmap Pengembangan Bahan Baku Obat Dalam Negeri</span>
                    </h2>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="card card-custom wave wave-animate-slow wave-success mb-8 mb-lg-0">
                <div class="card-body">
                    <section class="timeline">
                        <ol>
                            <li>
                                <div>
                                    <time>Hingga 2019</time>
                                    <span><b>Bioteknologi</b></span>
                                    <p><a href="">Eritropoietin</a></p>
                                    <span><b>Vaksin</b></span>
                                    <p><a href="">IPV</a></p>
                                    <span><b>Natural</b></span>
                                    <p><a href="">Quinine</a>, Ekstrak Cinnamomum burmanii, Silfamin,
                                    Ekstrak Biji Melinjo, Phylantin, Ekstrak Jahe Merah,
                                    Ekstrak Phaleria macrocarpa, Ekstrak Lumbricus rubellus,
                                    Astaxanthin, Reconyl, Ekstrak Jahe, Ekstrak Jamur Cordyceps,
                                    Ekstrak Kunyit, Ekstrak Temulawak, Ekstrak Bilberry,
                                    Ekstrak Gamat, Ekstrak Gabus, Ekstrak Pegagan, Ekstrak Mengkudu,
                                    Ekstrak Bawang Putih, Ekstrak Adas, Ekstrak Lidah Buaya,
                                    Ekstrak Alang-alang, Ektrak Daun Salam, Ekstrak Daun Kelor,
                                    Ekstrak Keladi Tikus, Ekstrak Daun Katuk, Ekstrak Daun Ungu,
                                    Ekstrak Daun Jati Belanda, Ekstrak Cecendet, Ekstrak Sambiloto,
                                    Ekstrak Seledri, Ekstrak Daun Sirsak, Ekstrak Mikro Alga,
                                    Jahe Emprit, Kulit Buah Manggis
                                    </p>
                                    <span><b>BBO Kimia</b></span>
                                    <p>Ampicillin Natrium, Atorvastatin Ca, Benzil Penisilin Kalium,
                                    Klopidogrel, Entecavir, Guaifenesin, Kloksasilin Natrium Hidrat,
                                    Omeprazol, Parasetamol, Salisinamida, Simvastatin, Sulbaktam Natrium
                                    </p>
                                </div>
                            </li>
                            <li>
                                <div>
                                <time>2020</time>
                                    <span><b>Bioteknologi</b></span>
                                    <p><a href="">Enoksaparin Sodium</a>, Epidermal Growth Factor (EGF),
                                    Somatropin, Stem cell protein (Wound care and cosmetics)</p>
                                    <span><b>Vaksin</b></span>
                                    <p><a href="">nOPV2</a></p>
                                    <span><b>Natural</b></span>
                                    <p><a href="">Ekstrak green coffe</a>, Ekstrak Purple Rice and Brown Rice prototipe,
                                    Ekstrak Kakao dengan High Theobromine, ekstrak daun bungur (corosolic acid),
                                    rumput kipas (flavonoid), ekstrak temu ireng, ekstrak temu putih, ekstrak temu mangga,
                                    ekstrak daun kemangi
                                    </p>
                                    <span><b>BBO Kimia</b></span>
                                    <p>Atapulgit, Efavirenz, Lamivudin, Tenofovir, Zidovudin
                                    </p>
                                </div>
                            </li>
                            <li>
                                <div>
                                <time>2021</time>
                                    <span><b>Bioteknologi</b></span>
                                    <p><a href="">HyFC-EPO</a>, Plasma Fractionation (albumin, Immunogbulin, Faktor VIII),
                                    </p>
                                    <span><b>Vaksin</b></span>
                                    <p><a href="">Typhoid Vi-Conj</a>, Vaksin MR</p>
                                    <span><b>Natural</b></span>
                                    <p><a href="">Ekstrak kapang daun sirsak</a>, Ekstrak Awar-Awar
                                    </p>
                                    <span><b>BBO Kimia</b></span>
                                    <p>Amlodipin, Kandesartan, Iodium Povidon, Niclosamide, Pantoprazol,
                                    Rosuvastatin, Pharma Salt
                                    </p>
                                </div>
                            </li>
                            <li>
                                <div>
                                <time>2022</time>
                                    <span><b>Bioteknologi</b></span>
                                    <p><a href="">Enoksaparin Sodium</a>, Epidermal Growth Factor (EGF),
                                    Somatropin, Stem cell protein (Wound care and cosmetics)</p>
                                    <span><b>Vaksin</b></span>
                                    <p><a href="">Vaksin Hepatitis B</a>, Vaksin Rotavirus, Vaksin Hepatitis A</p>
                                    <span><b>Natural</b></span>
                                    <p><a href="">Ekstrak propolis</a>, Glucosamine, Ekstrak Sukun, Ekstrak Daun jambu biji, Ekstrak Lengkuas
                                    </p>
                                    <span><b>BBO Kimia</b></span>
                                    <p>Bisoprolol, Esomeprazole, Glimepirid, Meloksikam, Rifampisin, Telmisartan, Valsartan
                                    </p>
                                </div>
                            </li>
                            <li>
                                <div>
                                <time>2023</time>
                                    <span><b>Bioteknologi</b></span>
                                    <p><a href="">Insulin</a></p>
                                    <span><b>Vaksin</b></span>
                                    <p><a href="">Vaksin BCG</a></p>
                                    <span><b>Natural</b></span>
                                    <p><a href="">Dihidroartemisinin (DHA)</a>, Ekstrak Gandarusa, Ekstrak Secang, Ekstrak Daun sembung
                                    </p>
                                    <span><b>BBO Kimia</b></span>
                                    <p>Sefadroksil, Sefiksim, Sefoperazon, sefotaksim, Seftazidim, Seftriakson, Hidrotalsit
                                    </p>
                                </div>
                            </li>
                            <li>
                                <div>
                                <time>2024</time>
                                    <span><b>Bioteknologi</b></span>
                                    <p><a href="">Transtuzumab</a>, Bevacizumab</p>
                                    <span><b>Vaksin</b></span>
                                    <p><a href="">nOPV1</a>, nOPV3, Hexavalent</p>
                                    <span><b>Natural</b></span>
                                    <p><a href="">Ekstrak Teripang</a>, Ekstrak Akar Manis
                                    </p>
                                    <span><b>BBO Kimia</b></span>
                                    <p>Amoksisilin, Penisilin G
                                    </p>
                                </div>
                            </li>
                            <li>
                                <div>
                                <time>2025</time>
                                    <span><b>Bioteknologi</b></span>
                                    <p><a href="">Growth Colony Stimulating Factor (GCSF)</a></p>
                                    <span><b>Vaksin</b></span>
                                    <p><a href="">Sabin IPV (Inactivated Polio Vaccine)</a>, DTAP-HB-HiB, New TB Recombinant</p>
                                    <span><b>Natural</b></span>
                                    <p><a href="">Ekstrak Kelembak (Crempamatum)</a>
                                    </p>
                                    <span><b>BBO Kimia</b></span>
                                    <p>
                                    </p>
                                </div>
                            </li>
                            <li></li>
                        </ol>

                        <!-- <div class="arrows">
                                <button class="arrow arrow__prev disabled" disabled>
                                    <img src="{{ asset('media/svg/shapes/left.svg') }}" alt="prev timeline arrow">
                                </button>
                                <button class="arrow arrow__next">
                                    <img src="{{ asset('media/svg/shapes/right.svg') }}" alt="next timeline arrow">
                                </button>
                            </div> -->
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
