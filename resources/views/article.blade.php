@extends('layout.blank.app')

@section('styles')
<link href="{{ asset('css/landing/menunav.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="card card-custom">
    <div class="card-header">

        <h1 class="card-title align-items-start flex-column" style="width:120px;">
            <img src="{{ asset('media/logos/emonev-new-logo.png') }}" style="width: 160%;">
        </h1>

        @include('landing.nav')
        <div class="card-toolbar">
            <button type="button" onclick="window.location='{{ route('login') }}'" class="btn btn-success font-weight-bolder font-size-h6 px-8 py-4 my-3 mr-3">Login</button>

        </div>

    </div>
</div>
<img src="{{ asset('media/bg/stroke.png') }}">

<div class="container py-5">
    <div class="row">
        <div class="col-lg-12">
            <div class="card card-custom gutter-b">
                <div class="card-header border-0 pt-5">
                    <h2 class="card-title align-items-start flex-column">
                        <span class="card-label font-weight-bold font-size-h1 text-dark-75">{{$data->title}}</span>
                    </h2>
                </div>
                <div class="card-body ">
                @if($data->image)
                    <div class="d-flex align-items-center p-3">
                        <div class="row">
                            <div class="col-md-12">
                                <img src="{{ url($data->image_url) }}" style="width: 100%;"/>        
                            </div>
                               
                        </div>
                        
                    </div>
                @endif
                </div>
                <div class="card-body ">
                    <div class="d-flex align-items-center p-3">
                        
                        
                        <div class="row">
                            <div class="col-md-12">
                                {!! $data->description !!}
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
   
</div>
<img src="{{ asset('media/bg/stroke.png') }}">
<div class="card card-custom">
    <div class="card-body">
        <div class="container py-8">
            <div class="row">
                <div class="col-lg-4">
                    <img src="{{ asset('media/logos/emonev-new-logo.png') }}" style="width: 65%;">
                    <br><br>
                    @foreach($footers as $footer)
                    <!-- <span>Direktorat Jenderal Kefarmasian dan Alat Kesehatan
                        Kementerian Kesehatan RI<br>
                        Gedung Dr. Adhyatma, MPH, Lt. 8 R.817<br>
                        Jl. H.R. Rasuna Said Blok X-5 Kav. 4-9<br>
                        Jakarta Selatan 12950<br>
                        Halo Kemkes ✆ 1500567
                    </span> -->
                        <span>
                            {{ $footer->nama_perusahaan}} <br>
                            {{ $footer->tempat }} <br>
                            {{ $footer->alamat_perusahaan }} <br>
                            {{ $footer->kota }} {{ $footer->kodepos }} <br>
                            {{ $footer->nama_callcenter }} ✆  {{ $footer->notelp }}
                        </span>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
<div class="footer bg-white py-4 d-flex flex-lg-column" id="kt_footer">
    <!--begin::Container-->
    <div class="container-fluid d-flex flex-column flex-md-row align-items-center justify-content-between">
        <!--begin::Copyright-->
        <div class="text-dark order-2 order-md-1">
            <span class="text-muted font-weight-bold mr-2">2021©</span>
            <a href="http://keenthemes.com/metronic" target="_blank" class="text-dark-75 text-hover-primary">Emonev BBO</a>
        </div>
        <!--end::Copyright-->
        <!--begin::Nav-->
        <div class="nav nav-dark">

        </div>
        <!--end::Nav-->
    </div>
</div>
@endsection

@section('scripts')
@endsection