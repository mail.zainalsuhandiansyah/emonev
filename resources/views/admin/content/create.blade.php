@extends('layout.admin.app')

@section('styles')
<link href="{{ asset('css/summernote/summernote.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <!--begin::Card-->
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">Create Article</h3>
                </div>
                <form method="POST" id="form-editor" action="{{ url('/admin/content') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                        <div class=row>
                            <div class="form-group col-md-5">
                                <label for="image">Image</label>
                                <div></div>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input @error('image') is-invalid @enderror" id="image" name="image" value="{{ old('image') }}" />
                                    <label class="custom-file-label" for="image">Choose file</label>
                                </div>
                            </div>
                            <div class="form-group col-md-7">
                                <label for="title">Title <span class="text-danger">*</span></label>
                                <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title" value="{{ old('title') }}" placeholder="Enter Title" />
                            </div>
                        </div>
                        <div class="form-group">
                            <textarea class="summernote @error('description') is-invalid @enderror" id="description" name="description">{{ old('description') }}</textarea>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary mr-2">Submit</button>
                        <a href="{{ url('admin/content') }}">
                            <button type="button" class="btn btn-secondary" aria-label="Close">Cancel</button>
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('js/pages/crud/forms/editors/summernote.js') }}"></script>
<script>
    var KTSummernoteDemo = function() {
        var demos = function() {
            $('#description').summernote({
                height: 150
            });
        }
        return {
            init: function() {
                demos();
            }
        };
    }();
    jQuery(document).ready(function() {
        KTSummernoteDemo.init();
    });
</script>
@endsection