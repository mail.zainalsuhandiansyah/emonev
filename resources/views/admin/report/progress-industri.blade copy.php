<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <title>Laporan Progress Bahan Baku Obat</title>
</head>

<body>

    <div class="row">

    </div>


    <div class="row">
        <div class="col-lg-12">
            <table class="table table-sm table-bordered">
                <thead class="text-center">
                    <tr>
                        <th>#</th>
                        <th>Produk</th>
                        <?php $title_array = array(); ?>
                        @foreach ($tahapan as $a => $item)
                        <th>
                            {{$item->roadmap_title}}
                        </th>
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                    @foreach ($reporting as $b => $item_reporting)
                        <tr>
                            <td></td>
                            <td>{{ $item_reporting->zat_active_name }}</td>
                            @foreach ($tahapan as $a => $item)
                            <td class="text-center">
                                @foreach ($progress as $c => $item_progress)
                                    @if($item_reporting->reporting_id == $item_progress->reporting_id && $item_progress->zat_active_id == $item_reporting->zat_active_id && $item->roadmap_title == $item_progress->roadmap_title)
                                        @if($item_progress->status_id == 3)
                                            <img src="{{ base_path() .'/public/media/img/Checklist.png' }}" width="2%">
                                        @endif
                                    @endif
                                @endforeach
                            </td>
                            @endforeach
                        </tr>

                    @endforeach
                </tbody>
            </table>

        </div>
    </div>

</body>

</html>
