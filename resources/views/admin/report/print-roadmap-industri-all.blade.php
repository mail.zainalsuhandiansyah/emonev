<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <title>Roadmap Bahan Baku All</title>
</head>

<body>

    <div class="row">
        <div class="col-lg-12 text-center">
            <strong>Roadmap Bahan Baku Indonesia</strong><br>
        </div>
    </div>

    <table class="table table-sm table-bordered" style="margin:5px;">
        <thead class="thead-dark text-center">
            <tr>
                <th>No.</th>
                <th>Tahun</th>
            @foreach ($GroupZat as $key => $item)
                <th>{{ $item->zat_group_description }}</th>
            @endforeach
            </tr>
        </thead>
        <tbody>
            <?php $x=1; ?>
            @foreach ($tahun as $keyYear => $itemYear)

                <tr>
                    <td class="text-center">{{ $x }}</td>
                    <td class="text-center">{{ $itemYear->year_name }}</td>
                    @foreach ($GroupZat as $keyGroup => $itemGroup)
                    <td>
                        @isset($roadmap[$itemYear->year_id][$itemGroup->zat_group_id])
                        <?php $y=1; ?>
                            @foreach ($roadmap[$itemYear->year_id][$itemGroup->zat_group_id] as $keyX => $itemRoadmap)
                            {{ $y }}.{{ $itemRoadmap['zat_active_name'] }} ({{ $itemRoadmap['userName'] }}) <br>
                            <?php $y++; ?>
                            @endforeach

                        @endisset


                    </td>
                    @endforeach
                </tr>
                <?php $x++; ?>
            @endforeach

        </tbody>
    </table>

</body>

</html>
