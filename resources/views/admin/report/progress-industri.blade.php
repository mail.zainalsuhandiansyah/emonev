<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <title>Laporan Progress Bahan Baku Obat</title>
</head>

<body>

    <div class="row">
        <div class="col-lg-12 text-center">
            <strong>Progres Pengembangan BBO Dalam Negeri</strong><br>
            <strong>{{  strtoupper($perusahaan->company_name) }}</strong>
        </div>
    </div>
    @foreach ($tahun2 as $keyYear => $itemYear)
    <table class="table table-bordered" style="margin:4px">
        <thead class="text-center thead-dark">
            <tr>
                <th rowspan="2">#</th>
                <th rowspan="2" class="text-middle">Produk</th>

                <th colspan="4">Tahun : {{ $itemYear->yearName }}</th>

            </tr>
            <tr>

                @foreach ($triwulan as $keyTw => $itemTw)
                    <th>{{ $itemTw->triwulan_description }}</th>
                @endforeach
            </tr>
        </thead>
        <tbody>
            <?php $x = 1; ?>
            @foreach ($produk as $keyProduk => $itemProduk)
                @if ($itemProduk->year_id == $itemYear->yearID)
                <tr>
                    <td>{{ $x }}</td>
                    <td>{{ $itemProduk->zat_active_name }}</td>
                    @foreach ($triwulan as $key => $itemTw)
                    <td id="isi[{{$itemProduk->zat_active_id}}][{{$itemYear->yearID}}][{{ $itemTw->triwulan_id }}]">
                        @isset($progress[$itemProduk->zat_active_id][$itemYear->yearID][$itemTw->triwulan_id])
                            @foreach ($progress[$itemProduk->zat_active_id][$itemYear->yearID][$itemTw->triwulan_id] as $keyx => $itemX)
                                @if ($itemX['statusID'] == 3)
                                {{ $itemX['roadmapTitle'] }}
                                @endif

                            @endforeach
                        @endisset
                    </td>
                    @endforeach
                </tr>
                <?php $x++; ?>
                @else
                <?php $x = 1; ?>
                @endif
            @endforeach
        </tbody>
    </table>
    @endforeach
</body>

</html>
