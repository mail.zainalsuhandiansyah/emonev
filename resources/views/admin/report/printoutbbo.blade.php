<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <title>Laporan</title>
    </head>
    <style type="text/css">
        #footer {
            position: fixed;
            right: 0px;
            bottom: 10px;
            text-align: center;
            border-top: 1px solid black;
        }

        #footer .page:after {
            content: counter(page, decimal);
        }

        @page {
            margin: 20px 30px 40px 50px;
        }
    </style>
    <body>
        <div class="text-center">
            <strong><p>Laporan Perkembangan Rencana Produksi<br>
            Bahan Baku Obat Dalam Negeri<br>
            PT. BIOFARMA </p></strong>
        </div>
        <br>
        <div class="pull-right small">
            Pelaporan : Hingga 2019 
        </div>
        <div class="clearfix"></div>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th class="text-center">No.</th>    
                    <th class="text-center">Nama Zat Aktif</th>
                    <th class="text-center">Capaian</th>
                    <th class="text-center">Fornas/Non Fornas</th>
                    <th class="text-center">Kategori</th>
                    <th class="text-center">Tahun</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>IPV</td>
                    <td></td>
                    <td>FORNAS</td>
                    <td>Vaksin</td>
                    <td>2019</td>
                </tr>
            </tbody>
        </table>
        <div class="pull-left small">
            <p>DiCetak oleh Subdit Kemandirian obat bahan baku sediaan farmasi <br>
                Direktorat Produksi dan Distribusi Kefarmasian
            </p>
        </div>
        <div id="footer">
            <p class="page">Page </p>
        </div>
    </body>
</html>


