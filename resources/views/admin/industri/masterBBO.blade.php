@extends('layout.admin.app')

@section('styles')

@endsection

@section('content')

<div class="card card-custom gutter-b">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">Master Bahan Baku Obat
                <span class="d-block text-muted pt-2 font-size-sm"> Master Bahan Baku Obat</span>
            </h3>
        </div>
        <div class="card-toolbar">
            <!--begin::Button-->
            <a href="{{url('admin/masterBBO')}}" class="btn btn-primary font-weight-bolder">
                <i class="fa fa-sync"></i> Pembaharui Data
            </a>
            <!--end::Button-->
        </div>
    </div>
    <div class="card-body">
        <!--begin: Search Form-->
        <!--begin::Search Form-->
        <div class="mb-7">
            <div class="row align-items-center">
                <div class="col-lg-4 col-xl-4">
                    <div class="row align-items-center">
                        <div class="col-md-12 my-12 my-md-0">
                            <div class="input-icon">
                                <input type="text" class="form-control" placeholder="Search..." id="kt_datatable_search_query" />
                                <span>
                                    <i class="flaticon2-search-1 text-muted"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
                    <a href="javascript:;" id="button-search" class="btn btn-light-primary px-6 font-weight-bold">Search</a>
                </div>
            </div>
        </div>
        <!--end::Search Form-->
        <!--end: Search Form-->
        <!--begin: Datatable-->
        <div class="datatable datatable-bordered datatable-head-custom" id="kt_datatable"></div>
        <!--end: Datatable-->
    </div>
</div>

@endsection

@section('scripts')
<!-- <script src="{{ asset('js/pages/crud/ktdatatable/base/data-bahan-baku.js') }}"></script> -->
<script>
    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var dataJSON = {!! json_encode($list->toArray()) !!};

        console.log(dataJSON);

        var datatable = $('#kt_datatable').KTDatatable({
            // datasource definition
            processing: true,
            serverSide: true,
            data: {
                type: 'local',
                source: dataJSON,
                pageSize: 10,
            },
            // layout definition
            layout: {
                scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
                // height: 450, // datatable's body's fixed height
                footer: false, // display/hide footer
            },

            // column sorting
            sortable: true,

            pagination: true,

            search: {
                input: $('#kt_datatable_search_query'),
                key: 'generalSearch',
            },

            // columns definition
            columns: [{
                field: 'OBAT_ID',
                title: '#',
                sortable: false,
                width: 20,
                type: 'number',
                selector: true,
                textAlign: 'center',
            },{
                field: 'CAS_NUMBER',
                title: 'CAS NUMBER',
            },{
                field: 'NAMA_BAHAN',
                title: 'NAMA BAHAN BAKU OBAT',
            }],
        });

        $('#kt_datatable_search_status').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Status');
        });

        $('#kt_datatable_search_type').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Type');
        });

        $("#button-search").on('click', function(){
            datatable.search();
        })

        $('#kt_datatable_search_status, #kt_datatable_search_type').selectpicker();

    });
</script>

<script>
    $(document).ajaxStart(function(){
        $.blockUI({ message: '<div style="padding:5px 0;">Please wait...</div>' ,css: { backgroundColor: '#fff', color: '#000', fontSize: '12px'} })
    }).ajaxStop($.unblockUI);
</script>
@endsection