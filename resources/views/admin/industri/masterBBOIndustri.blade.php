@extends('layout.admin.app')

@section('styles')
    <style>
        .nav.nav-pills .show > .nav-link, .nav.nav-pills .nav-link.active {
            color: #ffffff;
            background-color: #3699ff30 !important;
            transition: color 0.15s ease, background-color 0.15s ease, border-color 0.15s ease, box-shadow 0.15s ease;
            text-align:center;
        }
    </style>
@endsection

@section('content')

<div class="card card-custom gutter-b" id="portlet_grid">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">Master Bahan Baku yang Dikembangkan
                <span class="d-block text-muted pt-2 font-size-sm">Master Bahan Baku yang Dikembangkan</span>
            </h3>
        </div>
        <div class="card-toolbar">
            <!--begin::Button-->
            <a href="#" class="btn btn-success font-weight-bolder" id="button-add">
                <i class="fa fa-plus"></i> Add Data
            </a>
            &nbsp;&nbsp;
            <!-- <a href="#" class="btn btn-primary font-weight-bolder" data-toggle="modal" data-target="#modalAdd">
                <i class="fa fa-plus"></i> Add Bahan Baku Obat
            </a> -->

            <!--end::Button-->
        </div>
    </div>
    <div class="card-body">
        <!--begin: Search Form-->
        <!--begin::Search Form-->
        <div class="mb-7">
            <div class="row align-items-center">
                <div class="col-lg-4 col-xl-4">
                    <div class="row align-items-center">
                        <div class="col-md-12 my-12 my-md-0">
                            <div class="input-icon">
                                <input type="text" class="form-control" placeholder="Search..." id="kt_datatable_search_query" />
                                <span>
                                    <i class="flaticon2-search-1 text-muted"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
                    <a href="javascript:;" id="button-search" class="btn btn-light-primary px-6 font-weight-bold">Search</a>
                </div>
            </div>
        </div>
        <!--end::Search Form-->
        <!--end: Search Form-->
        <div class="mt-10 mb-5 collapse" id="kt_datatable_group_action_form">
            <div class="d-flex align-items-center">
                <div class="font-weight-bold text-danger mr-3">Selected
                <span id="kt_datatable_selected_records">0</span>records:</div>
                <div class="dropdown mr-2">
                    <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">Update status</button>
                    <div class="dropdown-menu dropdown-menu-sm">
                        <ul class="nav nav-hover flex-column">
                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <span class="nav-text">Pending</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <span class="nav-text">Delivered</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <span class="nav-text">Canceled</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <button class="btn btn-sm btn-danger mr-2" type="button" id="kt_datatable_delete_all">Delete All</button>
                <button class="btn btn-sm btn-success" type="button" data-toggle="modal" data-target="#kt_datatable_fetch_modal">Fetch Selected Records</button>
            </div>
        </div>


        <div class="modal fade" id="kt_datatable_fetch_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel1">Selected Records</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i aria-hidden="true" class="ki ki-close"></i>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="scroll" data-scroll="true" data-height="200">
                            <ul id="kt_datatable_fetch_display"></ul>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <!--begin: Datatable-->
        <div class="datatable datatable-bordered datatable-head-custom" id="kt_datatable"></div>
        <!--end: Datatable-->
    </div>
</div>

@include('admin.industri.action.createbbo')
@include('admin.industri.action.createroadmap')

@endsection
@section('scripts')
<script src="{{ asset('js/wizard/jquery.bootstrap.wizard.js') }}"></script>
<script type="text/javascript">

    var portletGrid = $("#portlet_grid");
    var portletRoadmap = $("#portlet_roadmap");
    var portletHilirisasi = $("#portlet_hilirisasi");

    var formRoadmap = $("#form_roadmap",portletRoadmap);
    var formHilirisasi = $("#form_hilirisasi",portletHilirisasi);

    var modalAddBBO = $("#modalAddBBO");
    var formAddBBO = $("#form_add_bbo",modalAddBBO);

    var datatable;

    var Grid = function () {
        return {
            init: function(){

                datatable = $('#kt_datatable').KTDatatable({
                    // datasource definition
                    data: {
                        type: 'remote',
                        source: {
                            read: {
                                url: 'listBBOIndustri',
                                method:'POST',
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                }
                            },
                        },
                        pageSize: 10,
                        serverPaging: true,
                        serverFiltering: true,
                        serverSorting: true,
                    },

                    // layout definition
                    layout: {
                        scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
                        // height: 450, // datatable's body's fixed height
                        footer: false, // display/hide footer
                    },

                    // column sorting
                    sortable: true,

                    pagination: true,

                    search: {
                        input: $('#kt_datatable_search_query'),
                        key: 'generalSearch',
                    },

                    // columns definition
                    columns: [{
                        field: 'material_id',
                        title: '#',
                        sortable: false,
                        width: 20,
                        type: 'number',
                        selector: true,
                        textAlign: 'center',
                    },{
                        field: 'zat_active_id',
                        title: 'Cas Number',
                        width:100,
                        visible: true,
                    },
                    {
                        field: 'zat_active_name',
                        title: 'Nama Bahan Baku Obat',
                    },
                    {
                        field: 'zat_group_description',
                        title: 'Golongan Bahan Baku Obat',
                    },
                    {
                        field: 'Actions',
                        title: 'Actions',
                        sortable: false,
                        width: 125,
                        overflow: 'visible',
                        autoHide: false,
                        hidden:true,
                        template: function(response) {
                            console.log('response.zat_active_id ,',response.zat_active_id)
                            return `<a href="javascript:;" style="font-size:9px;padding:4px !important;" class="btn btn-sm btn-warning" title="Delete" id="button-roadmap" data-name="`+response.zat_active_name+`" data-zat_active_id="`+response.zat_active_id+`" data-id="`+response.material_id+`" data-token="{{ csrf_token() }}">
                                        Roadmap
                                    </a>
                                    <a href="javascript:;" class="btn btn-sm btn-clean btn-icon" title="Delete" id="button-delete" data-id="`+response.material_id+`" data-token="{{ csrf_token() }}">
                                        <span class="fa fa-trash">
                                        </span>
                                    </a>`;
                        },
                    }],
                });

                $('#kt_datatable_search_status').on('change', function() {
                    datatable.search($(this).val().toLowerCase(), 'Status');
                });

                $('#kt_datatable_search_type').on('change', function() {
                    datatable.search($(this).val().toLowerCase(), 'Type');
                });

                $('#kt_datatable_search_status, #kt_datatable_search_type').selectpicker();

                $("#cancel",formAddBBO).on('click',function(){
                    Grid.closeModal();
                    Grid.reloadGrid();
                });

                Grid.clickDelete();
                Grid.clickRoadmap();

            },
            closeModal:function(){
                modalAddBBO.modal('hide');
            },
            reloadGrid:function(){
                datatable.reload();
            },
            reloadWindow:function(){
                location.reload();
            },
            clickDelete:function(){
                $('#kt_datatable').on("click", "#button-delete", function() {
                    var id = $(this).data('id');
                    var token = $(this).data("token");

                    Swal.fire({
                        title: 'Are you sure?',
                        text: "To deleted this data !",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, delete it!'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            $.ajaxSetup({
                                headers: {
                                    'X-CSRF-TOKEN': token
                                }
                            });
                            $.ajax({
                                url: "masterBBOIndustri/"+id,
                                type: 'DELETE', // replaced from put
                                dataType: "JSON",
                                // data: {
                                //     "id": id // method and token not needed in data
                                // },
                                success: function (response){
                                    Swal.fire({
                                        title: 'Data has been deleted !',
                                        showDenyButton: false,
                                        showCancelButton: false,
                                        confirmButtonText: `OK`,
                                        icon:'success'
                                    }).then((result) => {
                                        /* Read more about isConfirmed, isDenied below */
                                        if (result.isConfirmed) {
                                            Grid.reloadGrid();
                                            // Grid.reloadWindow();
                                        } else if (result.isDenied) {
                                            Swal.fire('Error!', '', 'info')
                                        }
                                    })
                                },
                                error: function(response) {
                                    console.log('response ',response)
                                    if(response.status===419){
                                        // Swal.fire('Warning ! ','Login Expired ! Please Relogin ','warning');
                                        window.location.href = "{{ route('login')}}";
                                    }else{
                                        Swal.fire('Warning ! ','An error occurred, please try again later','warning');
                                    }
                                }
                            });
                        }
                    })
                })
            },
            clickRoadmap:function(){
                $('#kt_datatable').on("click", "#button-roadmap", function() {
                    var id = $(this).data('id');
                    var name = $(this).data('name');
                    var zatActiveId = $(this).data('zat_active_id');
                    var token = $(this).data("token");
                    console.log('zatActiveId ',zatActiveId)
                    $("#materialId",portletHilirisasi).val(id);
                    $("#zatActiveId",portletHilirisasi).val(zatActiveId);
                    $("#materialName",portletHilirisasi).val(name);
                    $("#namaBBO",portletHilirisasi).html(name);
                    Grid.openPortlet(portletHilirisasi,portletGrid);
                });
            },
            openPortlet:function(target,ele){
                ele.hide();
                target.show();
            },
            closePortlet:function(target,ele){
                target.show();
                ele.hide();
                Grid.reloadGrid();
            },
            resetForm:function(){
                formRoadmap.trigger('reset');
            }
        };
    }();

    var MasterBBO = function(){

        var portletGrid = $("#portlet_grid");
        var modalAddBBO = $("#modalAddBBO");
        var formAddBBO = $("#form_add_bbo",modalAddBBO);

        var btncreate = $('#button-create',formAddBBO);
        var btnsubmit = $("#submit",formAddBBO);

        var datatableBBO = $('#kt_datatable_bbo',formAddBBO);

        return {
            init: function() {
                // grid bbo

                datatableBBO.KTDatatable({
                    data: {
                        type: 'remote',
                        source: {
                            read: {
                                url: 'listZatActive',
                                method:'POST',
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                }
                            },
                        },
                        pageSize: 10,
                        serverPaging: true,
                        serverFiltering: true,
                        serverSorting: true,
                    },

                    // layout definition
                    layout: {
                        scroll: false, // enable/disable datatable scroll both horizontal and
                        footer: false // display/hide footer
                    },
                    extensions : {
                    // boolean or object (extension options)
                        checkbox: true,
                    },
                    // layout definition
                    layout: {
                        scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
                        // height: 450, // datatable's body's fixed height
                        footer: false, // display/hide footer
                    },

                    // column sorting
                    sortable: true,

                    pagination: true,

                    search: {
                        input: $('#kt_datatable_search_query',formAddBBO),
                        key: 'generalSearch',
                    },

                    // columns definition
                    columns: [{
                        field: 'zat_active_id',
                        title: '#',
                        sortable: false,
                        width: 30,
                        type: 'number',
                        selector: true,
                        textAlign: 'left',
                    },
                    {
                        field: 'zat_active_name',
                        width: 350,
                        title: 'Nama Bahan Baku Obat',
                        textAlign: 'left',
                    }],
                });

                $('#kt_datatable_search_status, #kt_datatable_search_type',formAddBBO).selectpicker();

                datatableBBO.on(
                    'datatable-on-check datatable-on-uncheck',
                    function(e) {
                        console.log('ttter');
                        // datatable.checkbox() access to extension methods
                        var ids = datatableBBO.checkbox().getSelectedId();
                        var count = ids.length;

                        console.log(' count ',count)

                        $('#kt_datatable_selected_records',formAddBBO).html(count);
                        if (count > 0) {
                            $('#kt_datatable_group_action_form',formAddBBO).collapse('show');
                        } else {
                            $('#kt_datatable_group_action_form',formAddBBO).collapse('hide');
                        }
                    });

                // datatableBBO.on(
                //     'datatable-on-check datatable-on-uncheck',
                //     function(e) {
                //         var checkedNodes = datatableBBO.rows('.datatable-row-active').nodes();
                //         var count = checkedNodes.length;
                //         console.log('count ',count)
                //         $('#kt_datatable_selected_records',formAddBBO).html(count);
                //         if (count > 0) {
                //             $('#kt_datatable_group_action_form',formAddBBO).collapse('show');
                //         } else {
                //             $('#kt_datatable_group_action_form',formAddBBO).collapse('hide');
                //         }
                //     });

                btncreate.on('click', this.submitcreate);
                formAddBBO.on('submit', this.processbbo);

                $('#kt_datatable_search_status',formAddBBO).on('change', function() {
                    var keyword = $(this).val();
                    var param = 'Status';
                    MasterBBO.search(keyword, param);
                });

                $('#kt_datatable_search_type',formAddBBO).on('change', function() {
                    var keyword = $(this).val();
                    var param = 'Type';
                    MasterBBO.search(keyword, param);
                });

                $("#button-add",portletGrid).on('click',function(){
                    MasterBBO.openModal();
                })

            },
            search:function(keyword,param){
                datatableBBO.search(keyword.toLowerCase(), param);
            },
            reloadWindow:function(){
                window.reload();
            },
            closeModal:function(){
                formAddBBO.trigger('reset');
                modalAddBBO.modal('hide');
            },
            openModal:function(){
                modalAddBBO.modal('show');
                datatableBBO.reload();
            },
            resetForm:function(){
                formAddBBO.trigger('reset');
            },
            reloadGrid:function(){
                datatableBBO.reload();
                $('#kt_datatable_selected_records',formAddBBO).html(0);
            },
            submitcreate:function(){
                var ids = datatableBBO.checkbox().getSelectedId();
                var c = [];
                for (var i = 0; i < ids.length; i++) {
                    c.push(ids[i]);
                }
                console.log('c ',c)
                console.log('c ',c.length)
                if(c.length>0){
                    $('#obatid',formAddBBO).val(c.join("','"));
                    btnsubmit.trigger('click');
                }else{
                    Swal.fire('Warning !','Tidak ada data yang dipilih !','warning');
                }
            },
            processbbo:function(e){
                e.preventDefault();
                /*Ajax Request Header setup*/
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                var obatid = $("#obatid",formAddBBO).val();
                if(obatid==''){
                    Swal.fire('Warning !','Please Select Bahan Baku','warning');
                }else{
                    btnsubmit.html('Sending..');
                    btnsubmit.attr('disabled',true);

                    var zatActiveId = $("#obatid",formAddBBO).val();

                    /* Submit form data using ajax*/
                    $.ajax({
                        url:'masterBBOIndustri',
                        method: 'POST',
                        data: formAddBBO.serialize(),
                        success: function(response){
                            btnsubmit.html('Saved');
                            btnsubmit.attr('disabled',false);
                            var data = JSON.parse(response);
                            console.log('response ',JSON.parse(response))
                            console.log('response ',response.success)
                            console.log('data ',data.message)

                            if(data.success===true){
                                // Swal.fire('Success','Data has been saved !','success');
                                Swal.fire({
                                    title: 'Data has been saved !',
                                    html:data.existmessage,
                                    showDenyButton: false,
                                    showCancelButton: false,
                                    confirmButtonText: `OK`,
                                    icon:'success'
                                }).then((result) => {
                                    /* Read more about isConfirmed, isDenied below */
                                    if (result.isConfirmed) {
                                        Grid.reloadGrid();
                                        MasterBBO.closeModal();
                                    } else if (result.isDenied) {
                                        Swal.fire('Changes are not saved', '', 'info')
                                    }
                                })
                            }else{
                                Swal.fire({
                                    icon: 'warning',
                                    text: data.message
                                })
                            }
                        },error:function(response){
                            console.log('response ',response)
                            if(response.status===419){
                                // Swal.fire('Warning ! ','Login Expired ! Please Relogin ','warning');
                                window.location.href = "{{ route('login')}}";
                            }else{
                                Swal.fire('Warning ! ','An error occurred, please try again later','warning');
                            }
                            btnsubmit.html('Saved');
                            btnsubmit.attr('disabled',false);
                        }
                    });
                }
            }
        }
    }();

    var currentYearName=0;
    var currentYearId=0;

    var Hilirisasi = function () {
        var portletGrid = $("#portlet_grid");
        var portletRoadmap = $("#portlet_roadmap");
        var portletHilirisasi = $("#portlet_hilirisasi");
        var formHilirisasi = $("#form_hilirisasi",portletHilirisasi);
        var formRoadmap = $("#form_roadmap",portletRoadmap);

        var start_year = $("#start_year",formRoadmap);
        var end_year = $("#end_year",formRoadmap);
        var count_step = $("#count_step",formRoadmap);
        var current_yearname = $("#current_yearname",formRoadmap);
        var current_yearid = $("#current_yearid",formRoadmap);
        var zat_active_id = $("#zat_active_id",formRoadmap);

        return {
            init:function(){
                $("#button-back",portletHilirisasi).on('click',function(){
                    console.log('tterserrr');
                    Grid.closePortlet(portletGrid,portletHilirisasi);
                    Grid.reloadGrid();
                })

                $("#button-continue",portletHilirisasi).on('click',function(){
                    zatactiveid = $("#zatActiveId",formHilirisasi).val();
                    yearFrom = $("#year_id_from",formHilirisasi).val();
                    yearThru = $("#year_id_thru",formHilirisasi).val();
                    Hilirisasi.generateHilirisasi(zatactiveid,yearFrom,yearThru);
                })
            },
            generateHilirisasi:function(zatactiveid,yearfrom,yearthru){
                $.ajax({
                    url: "<?php echo route('generatehilirisasi') ?>/" + zatactiveid + "/" + yearfrom+"/"+yearthru ,
                    method: 'GET',
                    dataType: 'json',
                    success: function(response) {

                        materialName = $("#materialName",formHilirisasi).val();

                        $("#zat_active_name",formRoadmap).val(materialName);
                        $("#materialName",formRoadmap).html(materialName);

                        start_year.val(yearfrom);
                        end_year.val(yearthru);
                        count_step.val(response.count_step);
                        current_yearname.val(response.current_yearname);
                        current_yearid.val(response.current_yearid);
                        zat_active_id.val(zatactiveid);

                        currentYearName = response.current_yearname;
                        currentYearId = response.current_yearid;

                        $(".stepHeader",formRoadmap).html(response.stepHeader);
                        $(".tab-content",formRoadmap).html(response.stepBody);

                        Roadmap.createRoadmap(currentYearName,currentYearId);

                        setTimeout(function(){
                            Grid.openPortlet(portletRoadmap,portletHilirisasi);
                            Hilirisasi.reloadWizard();
                            console.log('hitung counting ')
                        },400);
                    },
                    error: function(xhr) {
                        console.log(xhr.responseText);
                        if(xhr.status===419){
                            window.location.href = "{{ route('login')}}";
                        }else{
                            Swal.fire('Warning !', 'An error occurred, please try again later', 'warning');
                        }

                    }
                });
            },
            reloadWizard:function(){
                $('.form-wizard').bootstrapWizard({
                    'tabClass': 'nav nav-pills',
                    'nextSelector': '.btn-next',
                    'previousSelector': '.btn-previous',

                    onNext: function(tab, navigation, index) {
                        current = index + 1;

                        countStep = current +1;
                        curnYearName = parseFloat(current_yearname.val())+1;
                        curnYearId = parseFloat(current_yearid.val())+1;
                        current_yearname.val(curnYearName);
                        current_yearid.val(curnYearId);

                        currentYearName = curnYearName;
                        currentYearId = curnYearId;
                        Roadmap.createRoadmap(currentYearName,currentYearId);

                        console.log('count_step ',count_step.val())
                        console.log('current ',current)

                        // cust = $("#year"+yearname+"[data-id='" + idx + "']");
                        // var table_uraian = $("#table_"+twAlias+''+yearname,cust,"#"+twAlias+''+yearname);

                        // if(uraian==''){
                        //     Swal.fire('Warning!','Uraian is required ','warning');
                        // }else{
                        //     var html = '';
                        //         html += `<tr>
                        //                     <td>`+uraian+`
                        //                         <textarea style="display: none;" rows="5" cols="5" class="form-control" id="uraian" name="uraian_`+yearname+''+twAlias+`[]">`+uraian+`</textarea>
                        //                         <textarea style="display: none;" rows="5" cols="5" class="form-control" id="tahapUraian`+yearname+''+twAlias+`" name="tahapUraian`+yearname+''+twAlias+`[]">`+twTahap+`</textarea>'
                        // return false;
                    },
                    onPrevious: function(tab, navigation, index) {
                        var current = index + 1;

                        countStep = current +1;
                        curnYearName = parseFloat(current_yearname.val())-1;
                        curnYearId = parseFloat(current_yearid.val())-1;
                        current_yearname.val(curnYearName);
                        current_yearid.val(curnYearId);

                        currentYearName = curnYearName;
                        currentYearId = curnYearId;
                        Roadmap.createRoadmap(currentYearName,currentYearId);
                    },

                    onInit : function(tab, navigation, index){

                        //check number of tabs and fill the entire row
                        var $total = navigation.find('li').length;
                        $width = 100/$total;
                        var $wizard = navigation.closest('.form-wizard');

                        $display_width = $(document).width();

                        if($display_width < 600 && $total > 3){
                            $width = 50;
                        }

                        navigation.find('li').css('width',$width + '%');
                        $first_li = navigation.find('li:first-child a').html();
                        $moving_div = $('<div class="moving-tab">' + $first_li + '</div>');
                        $('.form-wizard .wizard-navigation').append($moving_div);
                        refreshAnimation($wizard, index);
                        $('.moving-tab').css('transition','transform 0s');
                    },

                    onTabClick : function(tab, navigation, index){

                        return true;
                    },

                    onTabShow: function(tab, navigation, index) {
                        var $total = navigation.find('li').length;
                        var $current = index+1;

                        var $wizard = navigation.closest('.form-wizard');

                        // If it's the last tab then hide the last button and show the finish instead
                        if($current >= $total) {
                            $($wizard).find('.btn-next').hide();
                            $($wizard).find('.btn-finish').show();
                        } else {
                            $($wizard).find('.btn-next').show();
                            $($wizard).find('.btn-finish').hide();
                        }

                        button_text = navigation.find('li:nth-child(' + $current + ') a').html();

                        setTimeout(function(){
                            $('.moving-tab').text(button_text);
                        }, 150);

                        var checkbox = $('.footer-checkbox');

                        if( !index == 0 ){
                            $(checkbox).css({
                                'opacity':'0',
                                'visibility':'hidden',
                                'position':'absolute'
                            });
                        } else {
                            $(checkbox).css({
                                'opacity':'1',
                                'visibility':'visible'
                            });
                        }

                        refreshAnimation($wizard, index);
                    }
                });

                function refreshAnimation($wizard, index){
                    total_steps = $wizard.find('li').length;
                    move_distance = $wizard.width() / total_steps;
                    step_width = move_distance;
                    move_distance *= index;

                    $wizard.find('.moving-tab').css('width', step_width);
                    $('.moving-tab').css({
                        'transform':'translate3d(' + move_distance + 'px, 0, 0)',
                        'transition': 'all 0.3s ease-out'

                    });
                }
            }
        }

    }();

    var dataRoadmap = [];


    var Roadmap = function () {
        var portletGrid = $("#portlet_grid");
        var portletRoadmap = $("#portlet_roadmap");
        var portletHilirisasi = $("#portlet_hilirisasi");
        var formRoadmap = $("#form_roadmap",portletRoadmap);

        var modalAddUraian = $("#modalAddUraian");
        var formUraian = $("#form-uraian",modalAddUraian);

        var btnAddUraian = $("#submit",modalAddUraian);
        var btnResetUraian = $("#reset",modalAddUraian);

        var start_year = $("#start_year",formRoadmap);
        var end_year = $("#end_year",formRoadmap);
        var count_step = $("#count_step",formRoadmap);
        var current_yearname = $("#current_yearname",formRoadmap);
        var current_yearid = $("#current_yearid",formRoadmap);
        return {
            init:function(){
                $("#button-back",portletRoadmap).on('click',function(){
                    Grid.closePortlet(portletGrid,portletHilirisasi);
                    Hilirisasi.reloadWizard();
                    Grid.reloadWindow();
                })

                //definisikan button tambah berdasarkan triwulan ID
                //triwulan 1
                formRoadmap.on("click", "#button-add-uraiantw1", function() {
                    var twAlias = $(this).data('id');
                    var idx = $(this).data('idx');
                    var yearname = $(this).data('yearname');
                    var twTahap = $("#tahapUraian"+twAlias+''+yearname,formRoadmap);
                    console.log(twTahap.val(),' :twTahap');
                    if(twTahap.val()==='' || twTahap.val()===undefined){
                        twTahap.val(1);
                        tahapCount = 1;
                    }else{
                        var count = parseFloat(twTahap.val())+1;
                        tahapCount = count;
                        twTahap.val(count)
                    }

                    $("#twAlias",formUraian).val(twAlias);
                    $("#twTahap",formUraian).val(tahapCount);
                    $("#idx",formUraian).val(idx);
                    $("#yearname",formUraian).val(yearname);
                    modalAddUraian.modal('show');
                });

                //triwulan 2
                formRoadmap.on("click", "#button-add-uraiantw2", function() {
                    var twAlias = $(this).data('id');
                    var idx = $(this).data('idx');
                    var yearname = $(this).data('yearname');
                    var twTahap = $("#tahapUraian"+twAlias+''+yearname,formRoadmap);
                    console.log('tahaptw2 ',twTahap)
                    console.log('twAlias ',twAlias)
                    console.log('yearname ',yearname)
                    if(twTahap.val()==='' || twTahap.val()===undefined){
                        twTahap.val(1);
                        tahapCount = 1;
                    }else{
                        var count = parseFloat(twTahap.val())+1;
                        tahapCount = count;
                        twTahap.val(count)
                    }

                    $("#twAlias",formUraian).val(twAlias);
                    $("#twTahap",formUraian).val(tahapCount);
                    $("#idx",formUraian).val(idx);
                    $("#yearname",formUraian).val(yearname);
                    modalAddUraian.modal('show');
                });

                //triwulan 3
                formRoadmap.on("click", "#button-add-uraiantw3", function() {
                    var twAlias = $(this).data('id');
                    var idx = $(this).data('idx');
                    var yearname = $(this).data('yearname');
                    var twTahap = $("#tahapUraian"+twAlias+''+yearname,formRoadmap);
                    console.log('tahaptw1 ',twTahap)
                    if(twTahap.val()==='' || twTahap.val()===undefined){
                        twTahap.val(1);
                        tahapCount = 1;
                    }else{
                        var count = parseFloat(twTahap.val())+1;
                        tahapCount = count;
                        twTahap.val(count)
                    }

                    $("#twAlias",formUraian).val(twAlias);
                    $("#twTahap",formUraian).val(tahapCount);
                    $("#idx",formUraian).val(idx);
                    $("#yearname",formUraian).val(yearname);
                    modalAddUraian.modal('show');
                });

                //triwulan 4
                formRoadmap.on("click", "#button-add-uraiantw4", function() {
                    var twAlias = $(this).data('id');
                    var idx = $(this).data('idx');
                    var yearname = $(this).data('yearname');
                    var twTahap = $("#tahapUraian"+twAlias+''+yearname,formRoadmap);
                    console.log('tahaptw1 ',twTahap)
                    if(twTahap.val()==='' || twTahap.val()===undefined){
                        twTahap.val(1);
                        tahapCount = 1;
                    }else{
                        var count = parseFloat(twTahap.val())+1;
                        tahapCount = count;
                        twTahap.val(count)
                    }

                    $("#twAlias",formUraian).val(twAlias);
                    $("#twTahap",formUraian).val(tahapCount);
                    $("#idx",formUraian).val(idx);
                    $("#yearname",formUraian).val(yearname);
                    modalAddUraian.modal('show');
                });

                //end definisi button

                //proses untuk menambahkan tahap pengembangan
                btnAddUraian.on('click',function(){
                    var uraian = $("#uraian",modalAddUraian).val();
                    var twAlias = $("#twAlias",formUraian).val();
                    var twTahap = $("#twTahap",formUraian).val();
                    var idx = $("#idx",formUraian).val();
                    var yearname = $("#yearname",formUraian).val();

                    cust = $("#year"+yearname+"[data-id='" + idx + "']")

                    var table_uraian = $("#table_"+twAlias+''+yearname,cust,"#"+twAlias+''+yearname);

                    if(uraian==''){
                        Swal.fire('Warning!','Uraian is required ','warning');
                    }else{
                        var html = '';
                            html += `<tr>
                                        <td>`+uraian+`
                                            <textarea style="display: none;" rows="5" cols="5" class="form-control" id="uraian" name="uraian_`+yearname+''+twAlias+`[]">`+uraian+`</textarea>
                                            <textarea style="display: none;" rows="5" cols="5" class="form-control" id="tahapUraian`+yearname+''+twAlias+`" name="tahapUraian`+yearname+''+twAlias+`[]">`+twTahap+`</textarea>
                                            <table id="table-detail-uraian-`+yearname+''+twAlias+`" style="width:100%;border:none !important;" data-idx="`+idx+`">
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </td>
                                        <td align="center">
                                            <a href="javascript:;" class="btn btn-sm btn-success" data-id="`+twAlias+`" data-idx="`+idx+`" data-yearname="`+yearname+`" id="btn-detail`+twAlias+``+yearname+`" style="font-size:9px;padding:4px !important;"><i class="fa fa-list"></i> Detail</a>
                                        </td>
                                    </tr>`;
                        table_uraian.find('#tbody_uraian'+twAlias+''+yearname,table_uraian).append(html);
                        btnResetUraian.trigger('click');
                    }
                })

                btnResetUraian.on('click',function(){
                    modalAddUraian.modal('hide');
                })

                //end proses untuk menambahkan tahap pengembangan

                //proses submit
                formRoadmap.on('submit',this.processRoadmap);
            },
            createRoadmap:function(yearName,yearId){
                //proses untuk menambahkan detail tahap pengembangan

                // triwulan 1
                cust = $("#year"+yearName+"[data-id='" + yearId + "']")
                var table_uraian = $("#table_tw12020",cust,"#tw1");
                $("#button-contoh",portletRoadmap).on('click',function(){
                    console.log('cust ',cust)
                })

                $(document).on("click", "#btn-detailtw1"+yearName+"[data-idx='"+yearId+"'],#tw1"+yearName+",#table_tw1"+yearName+"", function() {
                    var twAlias = $(this).data('id');
                    console.log('twAlias ',twAlias)
                    var obj = $(this);
                    table_detail_uraian = obj.closest('tr').find("#table-detail-uraian-"+yearName+''+twAlias, obj);
                    console.log('')
                    tahapUraiantw1 = obj.closest('tr').find("#tahapUraian"+yearName+''+twAlias, obj).val();

                    html = ``;
                    html +=`<tr>
                                <td style="width:100%;border:none !important;">
                                    <input type="text" placeholder="Enter detail" class="form-control form-control-sm" name="detail_uraian`+twAlias+yearName+tahapUraiantw1+`[]" id="detail_uraian"/>
                                </td>
                            </tr>`;

                    obj.closest('tr').find("#table-detail-uraian-"+yearName+''+twAlias, obj).append(html);
                })

                // triwulan 2
                // var table_uraian = $("#table_tw2",formRoadmap,"#tw2");
                // table_uraian.on("click", "#btn-detail", function() {
                $(document).on("click", "#btn-detailtw2"+yearName+"[data-idx='"+yearId+"'],#tw2"+yearName+",#table_tw2"+yearName+"", function() {
                    var twAlias = $(this).data('id');
                    console.log('twAlias ',twAlias)
                    var obj = $(this);
                    console.log('obj ',obj)
                    table_detail_uraian = obj.closest('tr').find("#table-detail-uraian-"+yearName+''+twAlias, obj);

                    tahapUraiantw2 = obj.closest('tr').find("#tahapUraian"+yearName+''+twAlias, obj).val();

                    html = ``;
                    html +=`<tr>
                                <td style="width:100%;border:none !important;">
                                    <input type="text" placeholder="Enter detail" class="form-control form-control-sm" name="detail_uraian`+twAlias+yearName+tahapUraiantw2+`[]" id="detail_uraian"/>
                                </td>
                            </tr>`;

                    obj.closest('tr').find("#table-detail-uraian-"+yearName+''+twAlias, obj).append(html);
                })

                // triwulan 3
                // var table_uraian = $("#table_tw3",formRoadmap,"#tw3");
                // table_uraian.on("click", "#btn-detail", function() {
                $(document).on("click", "#btn-detailtw3"+yearName+"[data-idx='"+yearId+"'],#tw3"+yearName+",#table_tw3"+yearName+"", function() {
                    var twAlias = $(this).data('id');
                    console.log('twAlias ',twAlias)
                    var obj = $(this);
                    table_detail_uraian = obj.closest('tr').find("#table-detail-uraian-"+yearName+''+twAlias, obj);

                    tahapUraiantw3 = obj.closest('tr').find("#tahapUraian"+yearName+''+twAlias, obj).val();

                    html = ``;
                    html +=`<tr>
                                <td style="width:100%;border:none !important;">
                                    <input type="text" placeholder="Enter detail" class="form-control form-control-sm" name="detail_uraian`+twAlias+yearName+tahapUraiantw3+`[]" id="detail_uraian"/>
                                </td>
                            </tr>`;

                    obj.closest('tr').find("#table-detail-uraian-"+yearName+''+twAlias, obj).append(html);
                })

                // triwulan 4
                // var table_uraian = $("#table_tw4",formRoadmap,"#tw4");
                // table_uraian.on("click", "#btn-detail", function() {
                $(document).on("click", "#btn-detailtw4"+yearName+"[data-idx='"+yearId+"'],#tw4"+yearName+",#table_tw4"+yearName+"", function() {
                    var twAlias = $(this).data('id');
                    console.log('twAlias ',twAlias)
                    var obj = $(this);
                    table_detail_uraian = obj.closest('tr').find("#table-detail-uraian-"+yearName+''+twAlias, obj);

                    tahapUraiantw4 = obj.closest('tr').find("#tahapUraian"+yearName+''+twAlias, obj).val();

                    html = ``;
                    html +=`<tr>
                                <td style="width:100%;border:none !important;">
                                    <input type="text" placeholder="Enter detail" class="form-control form-control-sm" name="detail_uraian`+twAlias+yearName+tahapUraiantw4+`[]" id="detail_uraian"/>
                                </td>
                            </tr>`;

                    obj.closest('tr').find("#table-detail-uraian-"+yearName+''+twAlias, obj).append(html);
                })

                //end proses untuk menambahkan detail tahap pengembangan

                $("#cancel",formRoadmap).on('click',function(){
                    Hilirisasi.reloadWizard();
                    // var table_uraian = $("#table_tw1",formRoadmap,"#tw1");
                    // table_uraian.find('#tbody_uraian',table_uraian).html('');
                    // var table_uraian2 = $("#table_tw2",formRoadmap,"#tw2");
                    // table_uraian2.find('#tbody_uraian',table_uraian2).html('');
                    // var table_uraian3 = $("#table_tw3",formRoadmap,"#tw3");
                    // table_uraian3.find('#tbody_uraian',table_uraian3).html('');
                    // var table_uraian4 = $("#table_tw4",formRoadmap,"#tw4");
                    // table_uraian4.find('#tbody_uraian',table_uraian4).html('');

                    formRoadmap.trigger('reset');
                    Grid.closePortlet(portletGrid,portletRoadmap);
                })
            },
            processRoadmap:function(e){
                e.preventDefault();
                /*Ajax Request Header setup*/
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url:'roadmap',
                    method: 'POST',
                    data: formRoadmap.serialize(),
                    success: function(response){
                        var data = JSON.parse(response);
                        console.log('response ',JSON.parse(response))
                        console.log('response ',response.success)
                        console.log('data ',data.message)

                        if(data.success===true){
                            // Swal.fire('Success','Data has been saved !','success');
                            Swal.fire({
                                title: 'Data has been saved !',
                                showDenyButton: false,
                                showCancelButton: false,
                                confirmButtonText: `OK`,
                                icon:'success'
                            }).then((result) => {
                                /* Read more about isConfirmed, isDenied below */
                                if (result.isConfirmed) {
                                    Grid.reloadGrid();
                                    Grid.reloadWindow();
                                } else if (result.isDenied) {
                                    Swal.fire('Changes are not saved', '', 'info')
                                }
                            })
                            formRoadmap.trigger('reset'); 
                            Grid.closePortlet(portletGrid,portletRoadmap);
                        }else{
                            Swal.fire({
                                icon: 'warning',
                                text: data.message
                            })
                        }
                    },error:function(response){
                        console.log('response ',response)
                        if(response.status===419){
                            // Swal.fire('Warning ! ','Login Expired ! Please Relogin ','warning');
                            window.location.href = "{{ route('login')}}";
                        }else{
                            Swal.fire('Warning ! ','An error occurred, please try again later','warning');
                        }
                    }
                });
            }
        }
    }();

    // jQuery(document).ready(function () {
    //     WizardRoadmap.init();
    // });

    jQuery(document).ready(function () {

        Grid.init();
        MasterBBO.init();
        Hilirisasi.init();
        Roadmap.init();
        //event add
    });
</script>
<script>
    $(document).ajaxStart(function(){
        $.blockUI({ message: '<div style="padding:5px 0;">Please wait...</div>' ,css: { backgroundColor: '#fff', color: '#000', fontSize: '12px'} })
    }).ajaxStop($.unblockUI);
</script>
@endsection
