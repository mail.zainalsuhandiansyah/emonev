@extends('layout.admin.app')

@section('styles')

@endsection

@section('content')

<div class="card card-custom gutter-b">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">Registrasi Industri
                <span class="d-block text-muted pt-2 font-size-sm"> Registrasi Industri</span>
            </h3>
        </div>
        <div class="card-toolbar">
            <!--begin::Button-->
            <a href="#" class="btn btn-success font-weight-bolder" data-toggle="modal" data-target="#modalAdd">
                <i class="fa fa-plus"></i> Add Data
            </a>
            <!--end::Button-->
        </div>
    </div>
    <div class="card-body">
        <!--begin: Search Form-->
        <!--begin::Search Form-->
        <div class="mb-7">
            <div class="row align-items-center">
                <div class="col-lg-4 col-xl-4">
                    <div class="row align-items-center">
                        <div class="col-md-12 my-12 my-md-0">
                            <div class="input-icon">
                                <input type="text" class="form-control" placeholder="Search..." id="kt_datatable_search_query" />
                                <span>
                                    <i class="flaticon2-search-1 text-muted"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
                    <a href="javascript:;" id="button-search" class="btn btn-light-primary px-6 font-weight-bold">Search</a>
                </div>
            </div>
        </div>
        <!--end::Search Form-->
        <!--end: Search Form-->
        <!--begin: Datatable-->
        <div class="datatable datatable-bordered datatable-head-custom" id="kt_datatable"></div>
        <!--end: Datatable-->
    </div>
</div>


<div class="modal fade" id="modalAdd" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Registrasi Industri</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <form class="form" id="form_add" enctype="multipart/form-data" method="post" role="form">
                    <div class="alert alert-success d-none" id="msg_div">
                        <span id="res_message"></span>
                    </div>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="card-body">
                        <div class="form-group">
                            <label>Nama Perusahaan:</label>
                            <input type="text" id="company_name" name="company_name" class="form-control" placeholder="Enter Name"/>
                        </div>
                        <div class="form-group">
                            <label>NPWP:</label>
                            <input type="text" id="company_npwp" name="company_npwp" class="form-control" placeholder="Enter NPWP"/>
                        </div>
                        <div class="form-group">
                            <label>User Elicensing:</label>
                            <input type="text" id="company_users_elic" name="company_users_elic" class="form-control" placeholder="Enter user"/>
                        </div>
                        <div class="form-group">
                            <label>Password:</label>
                            <input type="password" id="company_password" name="company_password" class="form-control" placeholder="enter password"/>
                        </div>
                        <div class="form-group">
                            <label>Confirm Password:</label>
                            <input type="password" id="company_password_confirm" name="company_password_confirm" class="form-control" placeholder="enter confirm password"/>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary mr-2" id="submit"><i class="fa fa-save"></i> Save</button>
                        <button type="reset" class="btn btn-secondary" id="cancel"><i class="fa fa-arrow-left"></i> Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modalEdit" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Data Industri</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <form class="form" id="form_edit" enctype="multipart/form-data" method="post" role="form">
                    @csrf
                    <div class="alert alert-success d-none" id="msg_div">
                        <span id="res_message"></span>
                    </div>
                    <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" id="company_id" name="company_id"/>
                    <div class="card-body">
                        <div class="form-group">
                            <label>Nama Perusahaan:</label>
                            <input type="text" id="company_name" name="company_name" class="form-control" placeholder="Enter Name"/>
                        </div>
                        <div class="form-group">
                            <label>NPWP:</label>
                            <input type="text" id="company_npwp" name="company_npwp" class="form-control" placeholder="Enter NPWP"/>
                        </div>
                        <div class="form-group">
                            <label>Alamat:</label>
                            <textarea rows="3" cols="3" class="form-control" name="company_address" id="company_address"></textarea>
                        </div>
                        <div class="form-group">
                            <label>No. Telpon :</label>
                            <input type="text" id="company_phone" name="company_phone" class="form-control" placeholder="0"/>
                        </div>
                        <div class="form-group">
                            <label>Fax :</label>
                            <input type="text" id="company_fax" name="company_fax" class="form-control" placeholder="Enter Fax"/>
                        </div>
                        <div class="form-group">
                            <label>Permodalan :</label>
                            <input type="text" id="company_capital" name="company_capital" class="form-control" placeholder=""/>
                        </div>
                        <div class="form-group">
                            <label>Jenis Industri :</label>
                            <input type="text" id="company_type" name="company_type" class="form-control" placeholder=""/>
                        </div>
                        <h5>Data Penanggung Jawab</h5>
                        <div class="form-group">
                            <label>Nama Penanggung Jawab :</label>
                            <input type="text" id="company_pic" name="company_pic" class="form-control" placeholder=""/>
                        </div>
                        <div class="form-group">
                            <label>Jabatan :</label>
                            <input type="text" id="company_pic_position" name="company_pic_position" class="form-control" placeholder=""/>
                        </div>
                        <div class="form-group">
                            <label>No Telpon :</label>
                            <input type="text" id="company_pic_phone" name="company_pic_phone" class="form-control" placeholder=""/>
                        </div>
                        <div class="form-group">
                            <label>Email :</label>
                            <input type="text" id="company_pic_email" name="company_pic_email" class="form-control" placeholder=""/>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary mr-2" id="submit"><i class="fa fa-save"></i> Save</button>
                        <button type="reset" class="btn btn-secondary" id="cancel"><i class="fa fa-arrow-left"></i> Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<!-- <script src="{{ asset('js/pages/crud/ktdatatable/base/data-bahan-baku.js') }}"></script> -->
<script>
    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var modalAdd = $("#modalAdd");
        var formAdd = $("#form_add",modalAdd);

        var modalEdit = $("#modalEdit");
        var formEdit = $("#form_edit",modalEdit);

        var dataJSON = {!! json_encode($list->toArray()) !!};

        console.log(dataJSON);

        var datatable = $('#kt_datatable').KTDatatable({
            // datasource definition
            processing: true,
            serverSide: true,
            data: {
                type: 'local',
                source: dataJSON,
                pageSize: 10,
            },
            // layout definition
            layout: {
                scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
                // height: 450, // datatable's body's fixed height
                footer: false, // display/hide footer
            },

            // column sorting
            sortable: true,

            pagination: true,

            search: {
                input: $('#kt_datatable_search_query'),
                key: 'generalSearch',
            },

            // columns definition
            columns: [{
                field: 'company_id',
                title: '#',
                sortable: false,
                width: 20,
                type: 'number',
                selector: true,
                textAlign: 'center',
            }, {
                field: 'company_users_elic',
                title: 'User Elic',
            },{
                field: 'company_name',
                title: 'Company Name',
            },{
                field: 'company_address',
                title: 'Company Address',
            },{
                field: 'Actions',
                title: 'Actions',
                sortable: false,
                width: 125,
                overflow: 'visible',
                autoHide: false,
                template: function(response) {
                    // r = JSON.parse(response)
                    console.log('response ',response.company_id)
                    return `<a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-2" title="Edit" data-id="`+response.company_id+`" id="button-edit" data-token="{{ csrf_token() }}">
	                            <span class="svg-icon svg-icon-md">
	                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
	                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
	                                        <rect x="0" y="0" width="24" height="24"/>
	                                        <path d="M8,17.9148182 L8,5.96685884 C8,5.56391781 8.16211443,5.17792052 8.44982609,4.89581508 L10.965708,2.42895648 C11.5426798,1.86322723 12.4640974,1.85620921 13.0496196,2.41308426 L15.5337377,4.77566479 C15.8314604,5.0588212 16,5.45170806 16,5.86258077 L16,17.9148182 C16,18.7432453 15.3284271,19.4148182 14.5,19.4148182 L9.5,19.4148182 C8.67157288,19.4148182 8,18.7432453 8,17.9148182 Z" fill="#000000" fill-rule="nonzero"\ transform="translate(12.000000, 10.707409) rotate(-135.000000) translate(-12.000000, -10.707409) "/>
	                                        <rect fill="#000000" opacity="0.3" x="5" y="20" width="15" height="2" rx="1"/>
	                                    </g>
	                                </svg>
	                            </span>
							</a>
							<a href="javascript:;" class="btn btn-sm btn-clean btn-icon" title="Delete" id="button-delete" data-id="`+response.company_id+`" data-token="{{ csrf_token() }}">
	                            <span class="svg-icon svg-icon-md">
	                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
	                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
	                                        <rect x="0" y="0" width="24" height="24"/>
	                                        <path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"/>
	                                        <path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"/>
	                                    </g>
	                                </svg>
	                            </span>
							</a>
						`;
                },
            }],
        });

        $('#kt_datatable_search_status').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Status');
        });

        $('#kt_datatable_search_type').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Type');
        });

        $("#button-search").on('click', function(){
            datatable.search();
        })

        $('#kt_datatable_search_status, #kt_datatable_search_type').selectpicker();


        //event add
        $('#submit',formAdd).click(function(e){
            e.preventDefault();
            /*Ajax Request Header setup*/
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#submit',formAdd).html('Sending..');
            $('#submit',formAdd).attr('disabled',true);
            
            /* Submit form data using ajax*/
            $.ajax({
                url:'registrasi-industri',
                method: 'POST',
                data: formAdd.serialize(),
                success: function(response){
                    $('#submit',formAdd).html('Saved');
                    $('#submit',formAdd).attr('disabled',false);
                    var data = JSON.parse(response);
                    console.log('response ',JSON.parse(response))
                    console.log('response ',response.success)
                    console.log('data ',data.message)

                    if(data.success===true){
                        // Swal.fire('Success','Data has been saved !','success');
                        Swal.fire({
                            title: 'Data has been saved !',
                            showDenyButton: false,
                            showCancelButton: false,
                            confirmButtonText: `OK`,
                            icon:'success'
                        }).then((result) => {
                            /* Read more about isConfirmed, isDenied below */
                            if (result.isConfirmed) {
                                location.reload();
                            } else if (result.isDenied) {
                                Swal.fire('Changes are not saved', '', 'info')
                            }
                        })
                        formAdd.trigger('reset'); 
                        modalAdd.modal('hide');
                    }else{
                        Swal.fire({
                            icon: 'warning',
                            text: data.message
                        })
                    }
                },error:function(response){
                    $('#submit',formAdd).html('Saved');
                    $('#submit',formAdd).attr('disabled',false);
                    Swal.fire('Warning ! ','An error occurred, please try again later','warning');
                }
            });
        });

        $("#cancel",formAdd).on('click',function(){
            formAdd.trigger('reset'); 
            modalAdd.modal('hide'); 
        });
        //end event add

        //event delete 
        $('#kt_datatable').on("click", "#button-delete", function() {
            var id = $(this).data('id');
            var token = $(this).data("token");

            Swal.fire({
                title: 'Are you sure?',
                text: "To deleted this data !",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': token
                        }
                    });
                    $.ajax({
                        url: "registrasi-industri/"+id,
                        type: 'DELETE', // replaced from put
                        dataType: "JSON",
                        success: function (response)
                        {
                            Swal.fire({
                                title: 'Data has been Deleted !',
                                showDenyButton: false,
                                showCancelButton: false,
                                confirmButtonText: `OK`,
                                icon:'success'
                            }).then((result) => {
                                /* Read more about isConfirmed, isDenied below */
                                if (result.isConfirmed) {
                                    location.reload();
                                } else if (result.isDenied) {
                                    Swal.fire('Changes are not saved', '', 'info')
                                }
                            })
                        },
                        error: function(xhr) {
                            console.log(xhr.responseText); // this line will save you tons of hours while debugging
                            // do something here because of error
                        }
                    });
                }
            })
        })

        //end event delete

        //event edit

        $('#kt_datatable').on("click", "#button-edit", function() {
            var id = $(this).data('id');
            var token = $(this).data("token");

            $.ajax({
                url: "registrasi-industri/"+id,
                type: 'GET',
                // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
                data: {
                    id : id,
                    _token:'{{ csrf_token() }}'
                },
                dataType: 'json',
                success: function(response){
                    console.log(response,'response')

                    $("#company_id",formEdit).val(response.company_id);
                    $("#company_name",formEdit).val(response.company_name);
                    $("#company_npwp",formEdit).val(response.company_npwp);
                    $("#company_address",formEdit).val(response.company_address);
                    $("#company_phone",formEdit).val(response.company_phone);
                    $("#company_fax",formEdit).val(response.company_fax);
                    $("#company_capital",formEdit).val(response.company_capital);
                    $("#company_type",formEdit).val(response.company_type);
                    $("#company_pic",formEdit).val(response.company_pic);
                    $("#company_pic_position",formEdit).val(response.company_pic_position);
                    $("#company_pic_phone",formEdit).val(response.company_pic_phone);
                    $("#company_pic_email",formEdit).val(response.company_pic_email);

                    modalEdit.modal('show');
                }, error: function(xhr) {
                    Swal.fire('Warning !','An error occurred, please try again later','warning');
                    console.log(xhr.responseText); // this line will save you tons of hours while debugging
                    // do something here because of error
                }
            })
        })

        $('#submit',formEdit).click(function(e){
            e.preventDefault();
            $('#submit',formEdit).html('Sending..');
            $('#submit',formEdit).attr('disable',true);
            
            company_id = $("#company_id",formEdit).val();
            token  = $("#_token",formEdit).val();
            console.log(token,'token ')
            /* Submit form data using ajax*/
            $.ajax({
                url: "registrasi-industri/edit",
                type: 'PATCH',
                // data : {
                //     data :formEdit.serialize(),
                //     _token:'{{ csrf_token() }}'
                // },
                data: formEdit.serialize(),
                success: function(response){
                    $('#submit',formEdit).html('Saved');
                    $('#submit',formEdit).attr('disable',false);
                    var data = JSON.parse(response);
                    console.log('response ',JSON.parse(response))
                    console.log('response ',response.success)
                    console.log('data ',data.message)

                    if(data.success===true){
                        Swal.fire({
                            title: 'Data has been updated !',
                            showDenyButton: false,
                            showCancelButton: false,
                            confirmButtonText: `OK`,
                            icon:'success'
                        }).then((result) => {
                            /* Read more about isConfirmed, isDenied below */
                            if (result.isConfirmed) {
                                location.reload();
                            } else if (result.isDenied) {
                                Swal.fire('Changes are not saved', '', 'info')
                            }
                        })
                        formEdit.trigger('reset'); 
                        modalEdit.modal('hide');
                        setTimeout(function(){
                            location.reload();
                        },500);
                    }else{
                        Swal.fire({
                            icon: 'warning',
                            text: data.message
                        })
                    }
                },error:function(response){
                    Swal.fire('Warning !','An error occurred, please try again later','warning');
                    console.log(xhr.responseText); // this line will save you tons of hours while debugging
                }
            });
        });

        $("#cancel",formEdit).on('click',function(){
            formEdit.trigger('reset'); 
            modalEdit.modal('hide'); 
        });
        //end event EDIT

        $("#company_password_confirm,#company_password",modalAdd).on('change',function(){
            var confirmPassword = $("#company_password_confirm",modalAdd);
            var Password = $("#company_password",modalAdd);

            if(confirmPassword.val()!='' || password.val()!=''){
                if(confirmPassword.val()!=Password.val()){
                    Swal.fire({
                        icon: 'warning',
                        text: 'Password Tidak Sesuai!'
                    })

                    confirmPassword.val('');
                }
            }
            
        })
    });
</script>
<script>
    $(document).ajaxStart(function(){
        $.blockUI({ message: '<div style="padding:5px 0;">Please wait...</div>' ,css: { backgroundColor: '#fff', color: '#000', fontSize: '12px'} })
    }).ajaxStop($.unblockUI);
</script>
@endsection