
<!-- modalAddBBO -->
<div class="modal fade" id="modalAddBBO" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <form class="form" id="form_add_bbo" enctype="multipart/form-data" method="post" role="form">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="obatid" id="obatid">

                    @csrf
                    <div class="card-body">
                        <div class="mb-7">
                            <div class="row align-items-center">
                                <div class="col-lg-4 col-xl-4">
                                    <div class="row align-items-center">
                                        <div class="col-md-12 my-12 my-md-0">
                                            <div class="input-icon">
                                                <input type="text" class="form-control" placeholder="Search..." id="kt_datatable_search_query" />
                                                <span>
                                                    <i class="flaticon2-search-1 text-muted"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
                                    <a href="javascript:;" id="button-search" class="btn btn-light-primary px-6 font-weight-bold">Search</a>
                                </div>
                            </div>
                        </div>
                        <span style="margin-bottom:10px;color:red;"> *) Silahkan pilih bahan baku obat untuk menambahkan data </span>
                        <!--end: Search Form-->
                        <div class="mt-10 mb-5 collapse" id="kt_datatable_group_action_form">
                            <div class="d-flex align-items-center">
                                <div class="font-weight-bold text-danger mr-3">Selected
                                <span id="kt_datatable_selected_records"> 0 </span> records:</div>
                                <button class="btn btn-sm btn-success" type="button" id="button-create">Create Bahan Baku Obat</button>
                            </div>
                        </div>

                        <!--begin: Datatable-->
                        <div class="datatable datatable-bordered datatable-head-custom" id="kt_datatable_bbo"></div>
                        <!--end: Datatable-->
                    </div>
                    <div class="card-footer">
                        <button type="submit" style="display:none;" class="btn btn-primary mr-2" id="submit"><i class="fa fa-save"></i> Save</button>
                        <button type="reset" class="btn btn-secondary" id="cancel"><i class="fa fa-arrow-left"></i> Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>