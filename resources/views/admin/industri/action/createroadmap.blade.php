
<!-- Portlet Hilirisasi -->
<div class="card card-custom gutter-b" id="portlet_hilirisasi" style="display:none;">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">Target Hilirisasi
                <span class="d-block text-muted pt-2 font-size-sm">Perkembangan Bahan Baku</span>
            </h3>
        </div>
        <div class="card-toolbar">
            <!--begin::Button-->
            
            <!--end::Button-->
        </div>
    </div>
    <div class="card-body">
        <!--begin: Search Form-->
        <!--begin::Search Form-->
        <div class="row">
            <div class="col-md-8">
                <form class="form" id="form_hilirisasi" enctype="multipart/form-data" method="post" role="form">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="card-body">
                        <input type="hidden" name="materialId" id="materialId">
                        <input type="hidden" name="zatActiveId" id="zatActiveId">
                        <input type="hidden" name="materialName" id="materialName">
                        <h3 id="namaBBO"></h3>
                        <br>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">Tahun</label>
                            <div class="col-6">
                                <div class="input-group">
                                    <select class="form-control" name="year_id_from" id="year_id_from">
                                        @foreach ($tahun as $th)
                                            <option value="{{$th->year_id}}">{{$th->year_name}}</option>
                                        @endforeach
                                    </select>
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">sd</span>
                                    </div>
                                    <select class="form-control" name="year_id_thru" id="year_id_thru">
                                        @foreach ($tahun as $th)
                                            <option value="{{$th->year_id}}">{{$th->year_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <a href="javascript:;" id="button-back" class="btn btn-danger btn-sm font-weight-bolder">
                            <i class="fa fa-arrow-left"></i> Back
                        </a>
                        <button type="button" id="button-continue" class="btn btn-primary mr-2 btn-sm float-right">Continue <i class="fa fa-arrow-right"></i> </button>
                    </div>
                </form>
            </div>
            <div class="col-md-4">
                tter
            </div>
        </div>
        <!--end::Search Form-->
    </div>
</div>
<!-- end Portlet Hilirisasi -->

<!-- Portlet Roadmap -->
<div class="card card-custom gutter-b" id="portlet_roadmap" style="display:none;">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">Roadmap
                <span class="d-block text-muted pt-2 font-size-sm">Perkembangan Bahan Baku</span>
            </h3>
        </div>
        <div class="card-toolbar">
            <!--begin::Button-->
            <a href="javascript:;" id="button-back" class="btn btn-danger font-weight-bolder">
                <i class="fa fa-arrow-left"></i> Back
            </a>
            <!--end::Button-->
        </div>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="card-body form-wizard">
                <form class="form" id="form_roadmap" enctype="multipart/form-data" method="post" role="form">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="start_year" id="start_year"/>
                    <input type="hidden" name="end_year" id="end_year"/>
                    <input type="hidden" name="count_step" id="count_step"/>
                    <input type="hidden" name="current_yearname" id="current_yearname"/>
                    <input type="hidden" name="current_yearid" id="current_yearid"/>
                    <input type="hidden" name="zat_active_id" id="zat_active_id"/>
                    <input type="hidden" name="zat_active_name" id="zat_active_name"/>


                    <div class="form-group">
                        <div class="col-md-8">
                            <table class="table">
                                <tr>
                                    <td style="border:none;font-size:16px;font-weight:bold;" width="200">Bahan Baku Obat </td>
                                    <td style="border:none;font-size:16px;font-weight:bold;" align="center" width="10">:</td>
                                    <td style="border:none;font-size:16px;font-weight:bold;"><span id="materialName"></span></td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="example-preview">
                        <ul class="nav nav-pills nav-justified steps stepHeader">
                        </ul>
                    </div>

                    <div class="tab-content stepBody" style="margin-top:40px;">
                    </div>
                    <hr>
                    <div class="wizard-footer height-wizard">
                        <button type="button" class="btn btn-next btn-primary btn-sm float-right"> Continue <i class="fa fa-arrow-right"></i></button>
                        <button type="submit" class="btn btn-finish btn-success btn-sm float-right"><i class="fa fa-save"></i>Finish</button>
                        <button type="button" class="btn btn-previous btn-default btn-sm float-left"><i class="fa fa-arrow-left"></i> Previous</button>
                        <div class="clearfix"></div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
<!-- end Portlet Roadmap -->

<div class="modal fade" id="modalAddUraian" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Tahap Pengembangan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <form id="form-uraian">
                    <input type="hidden" name="idx" id="idx">
                    <input type="hidden" name="yearname" id="yearname">
                    <input type="hidden" name="twAlias" id="twAlias">
                    <input type="hidden" name="twTahap" id="twTahap">
                    <div class="card-body">
                        <div class="form-group">
                            <label class="control-label col-md-2">Uraian:</label>
                            <textarea rows="5" cols="5" class="form-control" id="uraian" name="uraian"></textarea>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="button" class="btn btn-primary mr-2" id="submit"><i class="fa fa-save"></i> Simpan</button>
                        <button type="reset" class="btn btn-secondary" id="reset"><i class="fa fa-close"></i> Batal</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>