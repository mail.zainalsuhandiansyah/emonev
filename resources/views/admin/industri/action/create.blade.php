<div class="modal fade" id="modalAdd" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <form class="form" id="form_add" enctype="multipart/form-data" method="post" role="form">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label>Nama:</label>
                            <input type="text" class="form-control" required="" id="zat_active_name" name="zat_active_name" placeholder="Enter Nama"/>
                        </div>
                        <div class="form-group">
                            <label>Jenis Bahan Baku:</label>
                            <select class="form-control" name="zat_group_id" id="zat_group_id">
                                <option value="">- Please Select -</option>
                                @foreach ($jenisbb as $bb)
                                    <option value="{{$bb->zat_group_id}}">{{$bb->zat_group_description}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Fornas:</label>
                            <select class="form-control" name="zat_active_fornas" id="zat_active_fornas">
                                <option value="">- Please Select -</option>
                                <option value="0">Non Fornas</option>
                                <option value="1">Fornas</option>
                            </select>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary mr-2" id="submit"><i class="fa fa-save"></i> Save</button>
                        <button type="reset" class="btn btn-secondary" id="cancel"><i class="fa fa-arrow-left"></i> Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>