@extends('layout.admin.app')

@section('styles')

@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <!--begin::Card-->
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">Create hirilisasi</h3>
                </div>
                <form method="POST" id="form-editor" action="{{ url('/admin/hirilisasi') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                        <div class=row>
                            <!-- <div class="col-md-6">
                                <div class="form-group">
                                    <label>Tahun</label>
                                    <input type="text" class="form-control" required="" id="year_name" name="year_name" placeholder=""/>
                                </div> -->
                                <!-- <div class="form-group">
                                    <label>Alamat Perusahaan</label>
                                    <input type="text" class="form-control" required="" id="alamat_perusahaan" name="alamat_perusahaan" placeholder=""/>
                                </div>
                                <div class="form-group">
                                    <label>Tempat</label>
                                    <input type="text" class="form-control" required="" id="tempat" name="tempat" placeholder=""/>
                                </div>
                                <div class="form-group">
                                    <label>Kota</label>
                                    <input type="text" class="form-control" required="" id="kota" name="kota" placeholder=""/>
                                </div> -->
                            <!-- </div> -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Zat Active</label>
                                    <!-- <input type="text" class="form-control" required="" id="year_active" name="year_active" placeholder=""/> -->
                                    <select class="form-select form-control" aria-label="Default select example" name="zat_active_id" id="zat_active_id" >
                                        <option selected>-Pilih-</option>
                                        @foreach($getzats as $getzat)
                                            <option value="{{ $getzat->zat_active_id }}" id="zat_active_id" name="zat_active_id">{{ $getzat->zat_active_name }}</option>
                                        @endforeach
                                        <!-- <option value="1">One</option>
                                        <option value="2">Two</option>
                                        <option value="3">Three</option> -->
                                    </select>
                                </div>
                                <!-- <div class="form-group">
                                    <label>Nama Call Center</label>
                                    <input type="text" class="form-control" required="" id="nama_callcenter" name="nama_callcenter" placeholder=""/>
                                </div>
                                <div class="form-group">
                                    <label>No Telp</label>
                                    <input type="text" class="form-control" required="" id="notelp" name="notelp" placeholder=""/>
                                </div>
                                <div class="form-group">
                                    <label for="image">Image</label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input @error('image') is-invalid @enderror" id="image" name="image" value="{{ old('image') }}" />
                                        <label class="custom-file-label" for="image">Choose file</label>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary mr-2">Submit</button>
                        <button type="reset" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection