<div class="col-md-4">
    <div class="card card-custom gutter-b">
        <div class="card-body">
            <div class="d-flex align-items-center mb-8">
                <div class="symbol symbol-40 symbol-light-success mr-5">
                    <span class="symbol-label">
                        <img src="{{ asset('media/svg/avatars/009-boy-4.svg') }}" class="h-75 align-self-end" />
                    </span>
                </div>
                <div class="d-flex flex-column flex-grow-1 font-weight-bold font-size-h5">
                    <a href="#" class="text-dark text-hover-primary mb-1 font-size-h4 ">Hari ini</a>
                    <span class="text-muted">{{$visitor->cToday}}</span>
                </div>
            </div>
            <div class="d-flex align-items-center mb-8">
                <div class="symbol symbol-40 symbol-light-success mr-5">
                    <span class="symbol-label">
                        <img src="{{ asset('media/svg/avatars/006-girl-3.svg') }}" class="h-75 align-self-end" />
                    </span>
                </div>
                <div class="d-flex flex-column flex-grow-1 font-weight-bold font-size-h5">
                    <a href="#" class="text-dark text-hover-primary mb-1 font-size-h4">Kemarin</a>
                    <span class="text-muted">{{$visitor->cYesterday}}</span>
                </div>
            </div>
            <div class="d-flex align-items-center mb-8">
                <div class="symbol symbol-40 symbol-light-success mr-5">
                    <span class="symbol-label">
                        <img src="{{ asset('media/svg/avatars/011-boy-5.svg') }}" class="h-75 align-self-end" />
                    </span>
                </div>
                <div class="d-flex flex-column flex-grow-1 font-weight-bold font-size-h5">
                    <a href="#" class="text-dark text-hover-primary mb-1 font-size-h4">Bulan Ini</a>
                    <span class="text-muted">{{$visitor->cMonth}}</span>
                </div>
            </div>
            <div class="d-flex align-items-center mb-8">
                <div class="symbol symbol-40 symbol-light-success mr-5">
                    <span class="symbol-label">
                        <img src="{{ asset('media/svg/avatars/015-boy-6.svg') }}" class="h-75 align-self-end" />
                    </span>
                </div>
                <div class="d-flex flex-column flex-grow-1 font-weight-bold font-size-h5">
                    <a href="#" class="text-dark text-hover-primary mb-1 font-size-h4">Tahun Ini</a>
                    <span class="text-muted">{{$visitor->cYear}}</span>
                </div>
            </div>
            <div class="d-flex align-items-center mb-8">
                <div class="symbol symbol-40 symbol-light-success mr-5">
                    <span class="symbol-label">
                        <img src="{{ asset('media/svg/avatars/015-boy-6.svg') }}" class="h-75 align-self-end" />
                    </span>
                </div>
                <div class="d-flex flex-column flex-grow-1 font-weight-bold font-size-h5">
                    <a href="#" class="text-dark text-hover-primary mb-1 font-size-h4">Total</a>
                    <span class="text-muted">{{$visitor->count_all}}</span>
                </div>
            </div>
        </div>
    </div>
</div>