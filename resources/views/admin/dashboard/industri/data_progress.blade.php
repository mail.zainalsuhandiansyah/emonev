<div class="card">
    <div class="col-lg-12">
        <div class="float-left">
            <h2 class="card-title align-items-start flex-column">
                <span class="card-label font-weight-bold font-size-h4 text-dark-75">Data Progress Pengembangan Bahan Baku</span>
            </h2>
        </div>

        <div class="float-right">
            <a href="/admin/pelaporan-industri-cetak2/{{ Auth::user()->users_id }}" class="btn btn-success btn-sm" target="_blank"> Cetak</a>
        </div>
        <div class="float-none"></div>
    </div>
    <div class="col-lg-12">
        <ul class="nav nav-light-primary nav-pills nav-fill" id="myTab3" role="tablist">
            {{-- Begin tab tahun --}}
            <?php $x=1; ?>
            @foreach ($tahun2 as $key => $item_year)
                <li class="nav-item">
                    <a class="nav-link {{ ($x == 1 ? 'active' :'') }}" id="home-tab-3" data-toggle="tab" href="#tabYear-{{ $item_year->yearID }}">
                        <span class="nav-icon">
                            <i class="flaticon-calendar-2"></i>
                        </span>
                        <span class="nav-text font-size-h4 font-size-lg-h2"> <strong>{{ $item_year->yearName }}</strong></span>
                    </a>
                </li>
            <?php $x++; ?>
            @endforeach
        </ul>
        <div class="tab-content mt-5" id="myTabContent3">
            <?php $x=1; ?>
            @foreach ($tahun2 as $key => $item_year)
            <div class="tab-pane fade {{ ($x == 1 ? 'show active' :'') }} " id="tabYear-{{ $item_year->yearID }}" role="tabpanel" aria-labelledby="home-tab-3">
                @include('admin.dashboard.industri.table_progress')
            </div>
            <?php $x++; ?>
            @endforeach
        </div>
    </div>
</div>

