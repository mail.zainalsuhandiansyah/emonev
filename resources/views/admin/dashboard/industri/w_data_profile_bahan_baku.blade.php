{{-- List Widget 1 --}}

<div class="card card-custom {{ @$class }}">
    {{-- Header --}}

    
    <div class="card-header border-1 pt-5">
    
        <!-- <h3 class="card-title align-items-start flex-column">
        </h3> -->
        <div class="card-label font-weight-border text-dark">
        
            <button class="btn btn-primary">Show Data</button>
            <button class="btn btn-success">Add Data</button>
            <button class="btn btn-primary">Tambah Bahan Baku Baru</button>
        </div>


        <div class="card-toolbar">
            <div class="form-group mr-sm-2" >
                <button class="btn btn-default" type="button">Sort & Filter</button>
            </div>
            <div class="form-group mr-sm-2">
                <form class="form-inline my-2 my-lg-0">
                    <input class="form-control" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                </form>
            </div>

            <div class="form-group ">
                <select class="form-control" id="exampleFormControlSelect1">
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
                </select>
            </div>
        </div>
    </div>

    {{-- Body --}}
    <div class="card-body pt-3 pb-0">
        
        <table class="table table-hover">
         
            <thead>
                <tr>
                    <th class="bs-checkbox " style="width: 36px; " data-field="state" tabindex="0">
                        <div class="th-inner ">
                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                            <input name="btSelectAll" type="checkbox">
                            <span></span></label>
                        </div>
                        <div class="fht-cell"></div>
                    </th>
                    <th>Zat Aktif</th>
                    <th>Action</th>
                </tr>
                
            </thead>
            <tbody>
                <tr>
                    <td class="bs-checkbox " style="width: 36px; " data-field="state" tabindex="0">
                        <div class="th-inner ">
                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                            <input name="btSelectAll" type="checkbox">
                            <span></span></label>
                        </div>
                        <div class="fht-cell"></div>
                    </td>
                    <td>Enoksaparin Sodium</td>
                    <td>
                        BUTTON
                    </td>
                </tr>
                <tr>
                    <td class="bs-checkbox " style="width: 36px; " data-field="state" tabindex="0">
                        <div class="th-inner ">
                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                            <input name="btSelectAll" type="checkbox">
                            <span></span></label>
                        </div>
                        <div class="fht-cell"></div>
                    </td>
                    <td>Plasma Fractionation (albumin, Immunogbulin, Faktor VIII</td>
                    <td>
                        BUTTON
                    </td>
                </tr>
                <tr>
                    <td class="bs-checkbox " style="width: 36px; " data-field="state" tabindex="0">
                        <div class="th-inner ">
                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                            <input name="btSelectAll" type="checkbox">
                            <span></span></label>
                        </div>
                        <div class="fht-cell"></div>
                    </td>
                    <td>Trastuzumab</td>
                    <td>
                        BUTTON
                    </td>
                </tr>
                <tr>
                    <td class="bs-checkbox " style="width: 36px; " data-field="state" tabindex="0">
                        <div class="th-inner ">
                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                            <input name="btSelectAll" type="checkbox">
                            <span></span></label>
                        </div>
                        <div class="fht-cell"></div>
                    </td>
                    <td>Vaksin HB (Hepatitis-B)</td>
                    <td>
                        BUTTON
                    </td>
                </tr>
                <tr>
                    <td class="bs-checkbox " style="width: 36px; " data-field="state" tabindex="0">
                        <div class="th-inner ">
                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                            <input name="btSelectAll" type="checkbox">
                            <span></span></label>
                        </div>
                        <div class="fht-cell"></div>
                    </td>
                    <td>Vaksin Rotavirus</td>
                    <td>
                        BUTTON
                    </td>
                </tr>
                <tr>
                    <td class="bs-checkbox " style="width: 36px; " data-field="state" tabindex="0">
                        <div class="th-inner ">
                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                            <input name="btSelectAll" type="checkbox">
                            <span></span></label>
                        </div>
                        <div class="fht-cell"></div>
                    </td>
                    <td>IPV</td>
                    <td>
                        BUTTON
                    </td>
                </tr>
                <tr>
                    <td class="bs-checkbox " style="width: 36px; " data-field="state" tabindex="0">
                        <div class="th-inner ">
                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                            <input name="btSelectAll" type="checkbox">
                            <span></span></label>
                        </div>
                        <div class="fht-cell"></div>
                    </td>
                    <td>nOPV2</td>
                    <td>
                        BUTTON
                    </td>
                </tr>
                <tr>
                    <td class="bs-checkbox " style="width: 36px; " data-field="state" tabindex="0">
                        <div class="th-inner ">
                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                            <input name="btSelectAll" type="checkbox">
                            <span></span></label>
                        </div>
                        <div class="fht-cell"></div>
                    </td>
                    <td>Vaksin MR</td>
                    <td>
                        BUTTON
                    </td>
                </tr>
                <tr>
                    <td class="bs-checkbox " style="width: 36px; " data-field="state" tabindex="0">
                        <div class="th-inner ">
                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                            <input name="btSelectAll" type="checkbox">
                            <span></span></label>
                        </div>
                        <div class="fht-cell"></div>
                    </td>
                    <td>Typhoid Vi-Conj</td>
                    <td>
                        BUTTON
                    </td>
                </tr>
                <tr>
                    <td class="bs-checkbox " style="width: 36px; " data-field="state" tabindex="0">
                        <div class="th-inner ">
                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                            <input name="btSelectAll" type="checkbox">
                            <span></span></label>
                        </div>
                        <div class="fht-cell"></div>
                    </td>
                    <td>Vaksin BCG</td>
                    <td>
                        BUTTON
                    </td>
                </tr>
                <tr>
                    <td class="bs-checkbox " style="width: 36px; " data-field="state" tabindex="0">
                        <div class="th-inner ">
                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                            <input name="btSelectAll" type="checkbox">
                            <span></span></label>
                        </div>
                        <div class="fht-cell"></div>
                    </td>
                    <td>Hexavalent</td>
                    <td>
                        BUTTON
                    </td>
                </tr>
                <tr>
                    <td class="bs-checkbox " style="width: 36px; " data-field="state" tabindex="0">
                        <div class="th-inner ">
                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                            <input name="btSelectAll" type="checkbox">
                            <span></span></label>
                        </div>
                        <div class="fht-cell"></div>
                    </td>
                    <td>nOPV1</td>
                    <td>
                        BUTTON
                    </td>
                </tr>
                <tr>
                    <td class="bs-checkbox " style="width: 36px; " data-field="state" tabindex="0">
                        <div class="th-inner ">
                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                            <input name="btSelectAll" type="checkbox">
                            <span></span></label>
                        </div>
                        <div class="fht-cell"></div>
                    </td>
                    <td>nOPV</td>
                    <td>
                        BUTTON
                    </td>
                </tr>
            </tbody>

        </table>
        
    </div>
</div>
