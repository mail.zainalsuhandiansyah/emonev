<div class="body-wrap">
    <section class="cd-horizontal-timeline" style="background-color:white;">
        <div class="timeline">
            <div class="events-wrapper">
                <div class="events">
                    <ol style="list-style-type: none;">

                    <?php $x=1; foreach ($roadmap['year'] as $key) {?>
                        <li><a href="#0" class="{{ ($x == 1 ? 'selected' :'') }}" data-date="01/01/{{$key->year}}">{{$key->year}}</a></li>
                    <?php $x++;} ?>
                    </ol>

                    <span class="filling-line" aria-hidden="true"></span>
                </div> <!-- .events -->
            </div> <!-- .events-wrapper -->

            <ul class="cd-timeline-navigation" style="list-style-type: none;">
                <li><a href="#0" class="prev inactive">Prev</a></li>
                <li>
                    <a href="#0" class="next"> <i class="far fa-angle-double-right"></i></a>
                </li>
            </ul> <!-- .cd-timeline-navigation -->
        </div> <!-- .timeline -->

        <div class="events-content">
        <ol style="list-style-type: none;">
            <?php $j=1; foreach ($roadmap['year'] as $key) {?>
                <li class="{{ ($j == 1 ? 'selected' :'') }}" data-date="01/01/{{$key->year}}">
                    <div class="content">
                        <div class="accordion accordion-solid accordion-panel accordion-svg-toggle" id="accordionExample20">
                            <?php $k=1; ?>
                            @foreach ($roadmap['group'] as $jen)
                                <div class="card">
                                    <div class="card-header" id="headingOne8">
                                        <div class="card-title card-color {{ ($k == 1 ? '' :'collapsed') }}" data-toggle="collapse" data-target="#collapse{{$jen->zat_group_id}}">
                                            <div class="card-label">{{$jen->zat_group_description}}</div>
                                            <span class="svg-icon">

                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <polygon points="0 0 24 0 24 24 0 24" />
                                                        <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero" />
                                                        <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999)" />
                                                    </g>
                                                </svg>

                                            </span>
                                        </div>
                                    </div>
                                    <div id="collapse{{$jen->zat_group_id}}" class="collapse {{ ($k == 1 ? 'show' :'') }}" data-parent="#accordionExample20">
                                        <div class="card-body">
                                            <p class="font-weight-bold font-size-h6 text-dark-75">
                                                @foreach ($roadmap['detail'] as $det)
                                                    @if($det->year_id==$key->year_id && $det->zat_group_id == $jen->zat_group_id)
                                                        {{$det->zat_active_name}};
                                                    @endif
                                                @endforeach
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <?php $k++; ?>
                            @endforeach
                        </div>
                    </div>
                </li>
            <?php $j++; } ?>
        </ol>
    </div>
        </li>
        </ol>
</div> <!-- .events-content -->
</section>
<div></div>
