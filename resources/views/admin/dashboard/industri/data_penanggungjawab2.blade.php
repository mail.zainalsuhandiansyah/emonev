@foreach($company as $key => $value)
<div class="col-xl-12">
    <div class="card card-custom card-stretch gutter-b">
        <div class="card-body pt-7">
            <div class="d-flex align-items-center">
                <span class="bullet bullet-bar bg-success align-self-stretch"></span>
                <label class="checkbox checkbox-lg checkbox-light-success checkbox-inline flex-shrink-0 m-0 mx-4">
                    <input type="checkbox" name="select" value="1" />
                    <span></span>
                </label>
                <div class="d-flex flex-column flex-grow-1">
                    <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-lg mb-1">{{$value->company_users_elic}}</a>
                    <span class="text-muted font-weight-bold">Kode User</span>
                </div>
            </div>
            <div class="d-flex align-items-center mt-10">
                <div class="d-flex align-items-center">
                    <span class="bullet bullet-bar bg-success align-self-stretch"></span>
                    <label class="checkbox checkbox-lg checkbox-light-success checkbox-inline flex-shrink-0 m-0 mx-4">
                        <input type="checkbox" name="select" value="1" />
                        <span></span>
                    </label>
                    <div class="d-flex flex-column flex-grow-1">
                        <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-lg mb-1">{{$value->company_pic}}</a>
                        <span class="text-muted font-weight-bold">Nama</span>
                    </div>
                </div>
            </div>
            <div class="d-flex align-items-center mt-10">
                <div class="d-flex align-items-center">
                    <span class="bullet bullet-bar bg-success align-self-stretch"></span>
                    <label class="checkbox checkbox-lg checkbox-light-success checkbox-inline flex-shrink-0 m-0 mx-4">
                        <input type="checkbox" name="select" value="1" />
                        <span></span>
                    </label>
                    <div class="d-flex flex-column flex-grow-1">
                        <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-lg mb-1"></a>
                        <span class="text-muted font-weight-bold">Last Login</span>
                    </div>
                </div>
            </div>
            <div class="d-flex align-items-center mt-10">
                <div class="d-flex align-items-center">
                    <span class="bullet bullet-bar bg-success align-self-stretch"></span>
                    <label class="checkbox checkbox-lg checkbox-light-success checkbox-inline flex-shrink-0 m-0 mx-4">
                        <input type="checkbox" name="select" value="1" />
                        <span></span>
                    </label>
                    <div class="d-flex flex-column flex-grow-1">
                        <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-lg mb-1"></a>
                        <span class="text-muted font-weight-bold">Alamat IP</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach