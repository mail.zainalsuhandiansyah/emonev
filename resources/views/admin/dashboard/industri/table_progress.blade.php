<table class="table table-all table-bordered">
    <thead class="table-success text-center">
        <tr>
            <th>No.</th>
            <th>Produk</th>
            @foreach ($triwulan as $key => $itemTw)
                <th>{{ $itemTw->triwulan_description }}</th>
            @endforeach
        </tr>
    </thead>
    <tbody>
        <?php $x = 1; ?>
        @foreach ($produk as $keyProduk => $itemProduk)

                @if ($itemProduk->year_id == $item_year->yearID)
                <tr>
                    <td>{{ $x }}</td>
                    <td>{{ $itemProduk->zat_active_name }}</td>
                    @foreach ($triwulan as $key => $itemTw)
                    <td id="isi[{{$itemProduk->zat_active_id}}][{{$item_year->yearID}}][{{ $itemTw->triwulan_id }}]">
                        @isset($progress[$itemProduk->zat_active_id][$item_year->yearID][$itemTw->triwulan_id])
                            @foreach ($progress[$itemProduk->zat_active_id][$item_year->yearID][$itemTw->triwulan_id] as $keyx => $itemX)
                                @if ($itemX['statusID'] == 3)
                                {{ $itemX['roadmapTitle'] }}
                                @endif

                            @endforeach
                        @endisset
                    </td>
                    @endforeach
                </tr>
                <?php $x++; ?>
                @else
                <?php $x = 1; ?>
                @endif


        @endforeach
    </tbody>
</table>

