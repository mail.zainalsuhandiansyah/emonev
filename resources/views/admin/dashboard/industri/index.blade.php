@extends('layout.admin.app')

@section('styles')
<link href="{{ asset('css/landing/timelinenew2.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

<div class="row ">
    <div class="col-xl-12">
        <div class="card card-custom gutter-b">
            <div class="card-header border-0">
                <h2 class="card-title align-items-start flex-column">
                    <span class="card-label font-weight-bold font-size-h4 text-dark-75">Roadmap Pengembangan Bahan Baku</span>
                </h2>
            </div>
        </div>
    </div>
    <div class="col-xl-12 ">
        @include('admin.dashboard.industri.timeline_rencana2', ['class' => 'card-stretch gutter-b'])
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        @include('admin.dashboard.industri.data_progress', ['class' => 'card-stretch gutter-b'])
    </div>
    &nbsp;
</div>


<div class="row mt-5">
    <div class="row col-md-6">
        <div class="col-sm-12">
            <div class="card card-custom gutter-b">
                <div class="card-header border-0 pt-6">
                    <h2 class="card-title align-items-start flex-column">
                        <span class="card-label font-weight-bold font-size-h4 text-dark-75">Data Industri</span>
                    </h2>
                </div>
            </div>
            @include('admin.dashboard.industri.data_perusahaan2')
        </div>
    </div>
    <div class="row col-md-6">
        <div class="col-sm-12">
            <div class="card card-custom gutter-b">
                <div class="card-header border-0 pt-6">
                    <h2 class="card-title align-items-start flex-column">
                        <span class="card-label font-weight-bold font-size-h4 text-dark-75">Data Penanggung Jawab</span>
                    </h2>
                </div>
            </div>
            @include('admin.dashboard.industri.data_penanggungjawab2')
        </div>   
    </div>
</div>




<div class="row mt-5">
    <!-- <div class="col-lg-6 col-xxl-4 order-1 order-xxl-2">
        <div class="card card-custom card-stretch gutter-b">
            <div class="card-header border-0">
                <h3 class="card-title font-weight-bolder text-dark">Last Login</h3>
            </div>
            <div class="card-body pt-2">
                <div class="d-flex align-items-center mb-10">
                    <div class="symbol symbol-40 symbol-light-success mr-5">
                        <span class="symbol-label">
                            <img src="{{ asset('media/svg/avatars/009-boy-4.svg') }}" class="h-75 align-self-end" />
                        </span>
                    </div>
                    <div class="d-flex flex-column flex-grow-1 font-weight-bold">
                        <a href="#" class="text-dark text-hover-primary mb-1 font-size-lg">Ricky Hunt</a>
                        <span class="text-muted">Location, Date, Source IP</span>
                    </div>
                </div>
                <div class="d-flex align-items-center mb-10">
                    <div class="symbol symbol-40 symbol-light-success mr-5">
                        <span class="symbol-label">
                            <img src="{{ asset('media/svg/avatars/006-girl-3.svg') }}" class="h-75 align-self-end" />
                        </span>
                    </div>
                    <div class="d-flex flex-column flex-grow-1 font-weight-bold">
                        <a href="#" class="text-dark text-hover-primary mb-1 font-size-lg">Anne Clarc</a>
                        <span class="text-muted">Location, Date, Source IP</span>
                    </div>
                </div>
                <div class="d-flex align-items-center mb-10">
                    <div class="symbol symbol-40 symbol-light-success mr-5">
                        <span class="symbol-label">
                            <img src="{{ asset('media/svg/avatars/011-boy-5.svg') }}" class="h-75 align-self-end" />
                        </span>
                    </div>
                    <div class="d-flex flex-column flex-grow-1 font-weight-bold">
                        <a href="#" class="text-dark text-hover-primary mb-1 font-size-lg">Kristaps Zumman</a>
                        <span class="text-muted">Location, Date, Source IP</span>
                    </div>
                </div>
                <div class="d-flex align-items-center mb-10">
                    <div class="symbol symbol-40 symbol-light-success mr-5">
                        <span class="symbol-label">
                            <img src="{{ asset('media/svg/avatars/015-boy-6.svg') }}" class="h-75 align-self-end" />
                        </span>
                    </div>
                    <div class="d-flex flex-column flex-grow-1 font-weight-bold">
                        <a href="#" class="text-dark text-hover-primary mb-1 font-size-lg">Ricky Hunt</a>
                        <span class="text-muted">Location, Date, Source IP</span>
                    </div>
                </div>
            </div>
        </div>
    </div> -->

    @include('admin.dashboard.evaluator.notification', ['class' => 'card-stretch gutter-b'])
</div>


@endsection

@section('scripts')
<script src="{{ asset('js/landing/timelinenew2.js') }}" type="text/javascript"></script>
@endsection
