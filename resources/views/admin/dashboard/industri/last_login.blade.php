{{-- List Widget 3 --}}

<div class="card card-custom {{ @$class }}">
    {{-- Header --}}
    <div class="card-header border-0">
        <h3 class="card-title font-weight-bolder text-dark">Last Login</h3>
    </div>

    {{-- Body --}}
    <div class="card-body pt-2">
        {{-- Item --}}
        <div class="d-flex align-items-center mb-10">
            {{-- Symbol --}}
            <div class="symbol symbol-40 symbol-light-success mr-5">
                <span class="symbol-label">
                    <img src="{{ asset('media/svg/avatars/009-boy-4.svg') }}" class="h-75 align-self-end"/>
                </span>
            </div>

            {{-- Text --}}
            <div class="d-flex flex-column flex-grow-1 font-weight-bold">
                <a href="#" class="text-dark text-hover-primary mb-1 font-size-lg">Ricky Hunt</a>
                <span class="text-muted">Location, Date, Source IP</span>
            </div>

            {{-- Dropdown --}}
        </div>

        {{-- Item --}}
        <div class="d-flex align-items-center mb-10">
            {{-- Symbol --}}
            <div class="symbol symbol-40 symbol-light-success mr-5">
                <span class="symbol-label">
                    <img src="{{ asset('media/svg/avatars/006-girl-3.svg') }}" class="h-75 align-self-end"/>
                </span>
            </div>

            {{-- Text --}}
            <div class="d-flex flex-column flex-grow-1 font-weight-bold">
                <a href="#" class="text-dark text-hover-primary mb-1 font-size-lg">Anne Clarc</a>
                <span class="text-muted">Location, Date, Source IP</span>
            </div>
        </div>

        {{-- Item --}}
        <div class="d-flex align-items-center mb-10">
            {{-- Symbol --}}
            <div class="symbol symbol-40 symbol-light-success mr-5">
                <span class="symbol-label">
                    <img src="{{ asset('media/svg/avatars/011-boy-5.svg') }}" class="h-75 align-self-end"/>
                </span>
            </div>

            {{-- Text --}}
            <div class="d-flex flex-column flex-grow-1 font-weight-bold">
                <a href="#" class="text-dark text-hover-primary mb-1 font-size-lg">Kristaps Zumman</a>
                <span class="text-muted">Location, Date, Source IP</span>
            </div>
        </div>

        {{-- Item --}}
        <div class="d-flex align-items-center mb-10">
            {{-- Symbol --}}
            <div class="symbol symbol-40 symbol-light-success mr-5">
                <span class="symbol-label">
                    <img src="{{ asset('media/svg/avatars/015-boy-6.svg') }}" class="h-75 align-self-end"/>
                </span>
            </div>

            {{-- Text --}}
            <div class="d-flex flex-column flex-grow-1 font-weight-bold">
                <a href="#" class="text-dark text-hover-primary mb-1 font-size-lg">Ricky Hunt</a>
                <span class="text-muted">Location, Date, Source IP</span>
            </div>
        </div>
    </div>
</div>
