<div class="body-wrap">
	<!-- # component starts -->
	<div class="pres-timeline" id="this-timeline">
		<!-- ###   -->
		<!--   <div class="periods-section"> -->
		<div class="periods-container">
			<section class="period-single" period="period1">
				<!-- <h4 class="year">181x-181x</h4> -->
				<!-- <h2 class="title">1 Lorem ipsum dolor sit amet, consectetur adipisicing.</h2> -->
				<!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium doloremque, laboriosam officia facere eligendi quam reiciendis, rem explicabo dolores tenetur libero minus, facilis quibusdam. Consectetur amet beatae fuga, architecto magnam.</p> -->
			</section>
			<section class="period-single" period="period2">
				<!-- <h4 class="year">182x-182x</h4> -->
				<!-- <h2 class="title">2 Lorem ipsum dolor sit amet, consectetur adipisicing.</h2> -->
				<!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium doloremque, laboriosam officia facere eligendi quam reiciendis, rem explicabo dolores tenetur libero minus, facilis quibusdam. Consectetur amet beatae fuga, architecto magnam.</p> -->
				<!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium doloremque, laboriosam officia facere eligendi quam reiciendis, rem explicabo dolores tenetur libero minus, facilis quibusdam. Consectetur amet beatae fuga, architecto magnam.</p> -->
			</section>
			<section class="period-single" period="period3">
				<!-- <h4 class="year">183x-183x</h4> -->
				<!-- <h2 class="title">3 Lorem ipsum dolor sit amet, consectetur adipisicing.</h2> -->
				<!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium doloremque, laboriosam officia facere eligendi quam reiciendis, rem explicabo dolores tenetur libero minus, facilis quibusdam. Consectetur amet beatae fuga, architecto magnam.</p> -->
			</section>
			<section class="period-single" period="period4">
				<!-- <h4 class="year">183x-183x</h4> -->
				<!-- <h2 class="title">4 Lorem ipsum dolor sit amet, consectetur adipisicing.</h2> -->
				<!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium doloremque, laboriosam officia facere eligendi quam reiciendis, rem explicabo dolores tenetur libero minus, facilis quibusdam. Consectetur amet beatae fuga, architecto magnam.</p> -->
			</section>
			<section class="period-single" period="period5">
				<!-- <h4 class="year">183x-183x</h4> -->
				<!-- <h2 class="title">5 Lorem ipsum dolor sit amet, consectetur adipisicing.</h2> -->
				<!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium doloremque, laboriosam officia facere eligendi quam reiciendis, rem explicabo dolores tenetur libero minus, facilis quibusdam. Consectetur amet beatae fuga, architecto magnam.</p> -->
			</section>
			<section class="period-single" period="period6">
				<!-- <h4 class="year">183x-183x</h4> -->
				<!-- <h2 class="title">6 Lorem ipsum dolor sit amet, consectetur adipisicing.</h2> -->
				<!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium doloremque, laboriosam officia facere eligendi quam reiciendis, rem explicabo dolores tenetur libero minus, facilis quibusdam. Consectetur amet beatae fuga, architecto magnam.</p> -->
			</section>
			<section class="period-single" period="period7">
				<!-- <h4 class="year">183x-183x</h4> -->
				<!-- <h2 class="title">7 Lorem ipsum dolor sit amet, consectetur adipisicing.</h2> -->
				<!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium doloremque, laboriosam officia facere eligendi quam reiciendis, rem explicabo dolores tenetur libero minus, facilis quibusdam. Consectetur amet beatae fuga, architecto magnam.</p> -->
			</section>
			<div class="btn-back"></div>
			<div class="btn-next"></div>
		</div>
		<!--   </div> -->
		<!-- ### -->
		<!--   <div class="timeline-section"> -->
		<div class="timeline-container">
			<!--     # timeline graphic place holder - fill with js -->
			<div class="timeline"></div>

			<div class="btn-back"><svg width="30" height="30" viewBox="0 0 30 30" xmlns="http://www.w3.org/2000/svg">
					<path fill="none" d="M0 0h30v30H0z" />
					<path fill="#D8D8D8" fill-rule="evenodd" d="M11.828 15l7.89-7.89-2.83-2.828L6.283 14.89l.11.11-.11.11L16.89 25.72l2.828-2.83" />
				</svg></div>
			<div class="btn-next"><svg width="30" height="30" viewBox="0 0 30 30" xmlns="http://www.w3.org/2000/svg">
					<path fill="none" d="M0 0h30v30H0z" />
					<path fill="#D8D8D8" fill-rule="evenodd" d="M18.172 14.718l-7.89-7.89L13.112 4l10.606 10.607-.11.11.11.11-10.608 10.61-2.828-2.83 7.89-7.89" />
				</svg></div>
		</div>
		<!--   </div> -->
		<!-- ### -->
		<!--   <div class="cards-section"> -->
		<div class="cards-container cont" style="height: 500px !important;">
			<section class="card-single active" period="period1">
				<h4 class="year">Hingga 2019</h4>
				<!-- <h2 class="title">Lorem ipsum dolor sit amet.</h2> -->
				<div class="content">
					<div class="accordion accordion-solid accordion-panel accordion-svg-toggle" id="accordionExample19">
						<div class="card">
							<div class="card-header" id="headingOne8">
								<div class="card-title card-color" data-toggle="collapse" data-target="#collapseOne19">
									<div class="card-label">Bioteknologi</div>
									<span class="svg-icon">
										<!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-right.svg-->
										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												<polygon points="0 0 24 0 24 24 0 24" />
												<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero" />
												<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999)" />
											</g>
										</svg>
										<!--end::Svg Icon-->
									</span>
								</div>
							</div>
							<div id="collapseOne19" class="collapse show" data-parent="#accordionExample19">
								<div class="card-body">
									{{-- <table class="table">
										<tr>
											<td>
											Eritropoietin
											</td>
										</tr>
									</table> --}}
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header" id="headingTwo8">
								<div class="card-title card-color collapsed" data-toggle="collapse" data-target="#collapseTwo19">
									<div class="card-label">Vaksin</div>
									<span class="svg-icon">
										<!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-right.svg-->
										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												<polygon points="0 0 24 0 24 24 0 24" />
												<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero" />
												<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999)" />
											</g>
										</svg>
										<!--end::Svg Icon-->
									</span>
								</div>
							</div>
							<div id="collapseTwo19" class="collapse" data-parent="#accordionExample19">
								<div class="card-body">
									{{-- <table class="table">
										<tbody>
											<tr>
												<td>
													IPV
												</td>
											</tr>
										</tbody>
									</table> --}}
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header" id="headingThree8">
								<div class="card-title card-color collapsed" data-toggle="collapse" data-target="#collapseThree19">
									<div class="card-label">Natural</div>
									<span class="svg-icon">
										<!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-right.svg-->
										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												<polygon points="0 0 24 0 24 24 0 24" />
												<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero" />
												<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999)" />
											</g>
										</svg>
										<!--end::Svg Icon-->
									</span>
								</div>
							</div>
							<div id="collapseThree19" class="collapse" data-parent="#accordionExample19">
								<div class="card-body">
									{{-- <table class="table">
										<tbody>
											<tr>
												<td>Quinine </td>
												<td>kstrak Cinnamomum burmanii</td>
												<td>Silfamin</td>
												<td>Ekstrak Biji Melinjo</td>
												<td>Phylantin</td>
												<td>Ekstrak Jahe Merah</td>
											</tr>
											<tr>
												<td>Ekstrak Phaleria macrocarpa</td>
												<td>Ekstrak Lumbricus rubellus</td>
												<td>Astaxanthin </td>
												<td>Reconyl </td>
												<td>Ekstrak Jahe </td>
												<td>Ekstrak Jamur Cordyceps</td>
											</tr>
											<tr>
												<td>Ekstrak Kunyit</td>
												<td>Ekstrak Temulawak</td>
												<td>Ekstrak Bilberry</td>
												<td>Ekstrak Gamat </td>
												<td>Ekstrak Gabus</td>
												<td>Ekstrak Pegagan</td>
											</tr>
											<tr>
												<td>Ekstrak Mengkudu</td>
												<td>Ekstrak Bawang Putih </td>
												<td>Ekstrak Adas</td>
												<td>Ekstrak Lidah Buaya</td>
												<td>Ekstrak Alang-alang</td>
												<td>Ekstrak Daun Salam Ekstrak Daun Kelor</td>
											</tr>
											<tr>
												<td>Ekstrak Keladi Tikus </td>
												<td>Ekstrak Daun Katuk</td>
												<td>Ekstrak Daun Ungu</td>
												<td>Ekstrak Daun Jati Belanda</td>
												<td>Ekstrak Cecendet </td>
												<td>Ekstrak Sambiloto</td>
											</tr>
											<tr>
												<td>Ekstrak Seledri</td>
												<td>Ekstrak Daun Sirsak</td>
												<td>Ekstrak Mikro Alga</td>
												<td>Jahe Emprit </td>
												<td>Kulit Buah Manggis</td>
											</tr>
										</tbody>
									</table> --}}
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header" id="headingTwo8">
								<div class="card-title card-color collapsed" data-toggle="collapse" data-target="#collapseTwobbo19">
									<div class="card-label">BBO Kimia</div>
									<span class="svg-icon">
										<!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-right.svg-->
										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												<polygon points="0 0 24 0 24 24 0 24" />
												<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero" />
												<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999)" />
											</g>
										</svg>
										<!--end::Svg Icon-->
									</span>
								</div>
							</div>
							<div id="collapseTwobbo19" class="collapse" data-parent="#accordionExample19">
								<div class="card-body">
									{{-- <table class="table">
										<tbody>
											<tr>
												<td>Ampicillin Natrium </td>
												<td>Atorvastatin Ca </td>
												<td>Benzil Penisilin Kalium</td>
											</tr>
											<tr>
												<td>Klopidogrel </td>
												<td>Entecavir </td>
												<td>Guaifenesin </td>
											</tr>
											<tr>
												<td>Kloksasilin Natrium Hidrat</td>
												<td>Omeprazol</td>
												<td>Parasetamol </td>

											</tr>
											<tr>
												<td>Salisinamida </td>
												<td>Simvastatin </td>
												<td>Sulbaktam Natrium</td>
											</tr>
										</tbody>
									</table> --}}
								</div>
							</div>
						</div>
					</div>
					<!--end::Accordion-->
				</div>
			</section>
			<section class="card-single" period="period2">
				<h4 class="year">2020</h4>
				<!-- <h2 class="title">Lorem ipsum dolor sit amet.</h2> -->
				<div class="content">
					<div class="accordion accordion-solid accordion-panel accordion-svg-toggle" id="accordionExample20">
						<div class="card">
							<div class="card-header" id="headingOne8">
								<div class="card-title card-color" data-toggle="collapse" data-target="#collapseOnebio20">
									<div class="card-label">Bioteknologi</div>
									<span class="svg-icon">

										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												<polygon points="0 0 24 0 24 24 0 24" />
												<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero" />
												<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999)" />
											</g>
										</svg>

									</span>
								</div>
							</div>
							<div id="collapseOnebio20" class="collapse show" data-parent="#accordionExample20">
								<div class="card-body">
									{{-- <table class="table">
										<tr>
											<td>Enoksaparin Sodium</td>
											<td>Epidermal Growth Factor (EGF),</td>
											<td>Somatropin, </td>
											<td>Stem cell protein (Wound care and cosmetics)</td>
										</tr>
									</table> --}}
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header" id="headingTwo8">
								<div class="card-title card-color collapsed" data-toggle="collapse" data-target="#collapseTwovaksin20">
									<div class="card-label">Vaksin</div>
									<span class="svg-icon">

										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												<polygon points="0 0 24 0 24 24 0 24" />
												<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero" />
												<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999)" />
											</g>
										</svg>

									</span>
								</div>
							</div>
							<div id="collapseTwovaksin20" class="collapse" data-parent="#accordionExample20">
								<div class="card-body">
									{{-- <table class="table">
										<tbody>
											<tr>
												<td>nOPV2</td>
											</tr>
										</tbody>
									</table> --}}
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header" id="headingThree8">
								<div class="card-title card-color collapsed" data-toggle="collapse" data-target="#collapseThreenatural20">
									<div class="card-label">Natural</div>
									<span class="svg-icon">

										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												<polygon points="0 0 24 0 24 24 0 24" />
												<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero" />
												<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999)" />
											</g>
										</svg>

									</span>
								</div>
							</div>
							<div id="collapseThreenatural20" class="collapse" data-parent="#accordionExample20">
								<div class="card-body">
									{{-- <table class="table">
										<tbody>
											<tr>
												<td>Ekstrak green coffe,</td>
												<td>Ekstrak Purple Rice and Brown Rice prototipe,</td>
												<td>Ekstrak Kakao dengan High Theobromine, </td>
											</tr>
											<tr>
												<td>ekstrak daun bungur (corosolic acid), </td>
												<td>rumput kipas (flavonoid), </td>
												<td>ekstrak temu ireng, </td>

											</tr>
											<tr>
												<td>ekstrak temu putih, </td>
												<td>ekstrak temu mangga,</td>
												<td>ekstrak daun kemangi</td>


											</tr>
										</tbody>
									</table> --}}
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header" id="headingTwo8">
								<div class="card-title card-color collapsed" data-toggle="collapse" data-target="#collapseFourbbo20">
									<div class="card-label">BBO Kimia</div>
									<span class="svg-icon">

										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												<polygon points="0 0 24 0 24 24 0 24" />
												<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero" />
												<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999)" />
											</g>
										</svg>

									</span>
								</div>
							</div>
							<div id="collapseFourbbo20" class="collapse" data-parent="#accordionExample20">
								<div class="card-body">
									{{-- <table class="table">
										<tbody>
											<tr>
												<td>Atapulgit,</td>
												<td>Efavirenz,</td>
												<td>Lamivudin,</td>
												<td>Tenofovir,</td>
												<td>Zidovudin</td>
											</tr>
										</tbody>
									</table> --}}
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="card-single" period="period3">
				<h4 class="year">2021</h4>
				<!-- <h2 class="title">Lorem ipsum dolor sit amet.</h2> -->
				<div class="content">
					<div class="accordion accordion-solid accordion-panel accordion-svg-toggle" id="accordionExample21">
						<div class="card">
							<div class="card-header" id="headingOne8">
								<div class="card-title card-color" data-toggle="collapse" data-target="#collapseOnebio21">
									<div class="card-label">Bioteknologi</div>
									<span class="svg-icon">

										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												<polygon points="0 0 24 0 24 24 0 24" />
												<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero" />
												<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999)" />
											</g>
										</svg>

									</span>
								</div>
							</div>
							<div id="collapseOnebio21" class="collapse show" data-parent="#accordionExample21">
								<div class="card-body">
									{{-- <table class="table">
										<tr>
										<td>HyFC-EPO Plasma Fractionation (albumin, Immunogbulin, Faktor VIII)</td>
										</tr>
									</table> --}}
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header" id="headingTwo8">
								<div class="card-title card-color collapsed" data-toggle="collapse" data-target="#collapseTwovaksin21">
									<div class="card-label">Vaksin</div>
									<span class="svg-icon">

										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												<polygon points="0 0 24 0 24 24 0 24" />
												<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero" />
												<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999)" />
											</g>
										</svg>

									</span>
								</div>
							</div>
							<div id="collapseTwovaksin21" class="collapse" data-parent="#accordionExample21">
								<div class="card-body">
									{{-- <table class="table">
										<tbody>
											<tr>
											<td>Typhoid Vi-Conj Vaksin MR</td>
											</tr>
										</tbody>
									</table> --}}
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header" id="headingThree8">
								<div class="card-title card-color collapsed" data-toggle="collapse" data-target="#collapseThreenatural21">
									<div class="card-label">Natural</div>
									<span class="svg-icon">

										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												<polygon points="0 0 24 0 24 24 0 24" />
												<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero" />
												<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999)" />
											</g>
										</svg>

									</span>
								</div>
							</div>
							<div id="collapseThreenatural21" class="collapse" data-parent="#accordionExample21">
								<div class="card-body">
									{{-- <table class="table">
										<tbody>
											<tr>
											<td>Ekstrak kapang daun sirsak Ekstrak Awar-Awar</td>


											</tr>
										</tbody>
									</table> --}}
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header" id="headingTwo8">
								<div class="card-title card-color collapsed" data-toggle="collapse" data-target="#collapseFourbbo21">
									<div class="card-label">BBO Kimia</div>
									<span class="svg-icon">

										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												<polygon points="0 0 24 0 24 24 0 24" />
												<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero" />
												<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999)" />
											</g>
										</svg>

									</span>
								</div>
							</div>
							<div id="collapseFourbbo21" class="collapse" data-parent="#accordionExample21">
								<div class="card-body">
									{{-- <table class="table">
										<tbody>
											<tr>
												<td>Amlodipin, </td>
												<td>Kandesartan, </td>
												<td>Iodium Povidon, </td>
											</tr>
											<tr>
												<td>Niclosamide, </td>
												<td>Pantoprazol,</td>
												<td>Rosuvastatin, </td>
												<td>Pharma Salt</td>
											</tr>
										</tbody>
									</table> --}}
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="card-single" period="period4">
				<h4 class="year">2022</h4>
				<!-- <h2 class="title">Lorem ipsum dolor sit amet.</h2> -->
				<div class="content">
					<div class="accordion accordion-solid accordion-panel accordion-svg-toggle" id="accordionExample22">
						<div class="card">
							<div class="card-header" id="headingOne8">
								<div class="card-title card-color" data-toggle="collapse" data-target="#collapseOnebio22">
									<div class="card-label">Bioteknologi</div>
									<span class="svg-icon">

										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												<polygon points="0 0 24 0 24 24 0 24" />
												<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero" />
												<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999)" />
											</g>
										</svg>

									</span>
								</div>
							</div>
							<div id="collapseOnebio22" class="collapse show" data-parent="#accordionExample22">
								<div class="card-body">
									{{-- <table class="table">
										<tr>
											<td>Enoksaparin Sodium, </td>
											<td>Epidermal Growth Factor (EGF),</td>
											<td>Somatropin, </td>
											<td>Stem cell protein (Wound care and cosmetics)</td>
										</tr>
									</table> --}}
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header" id="headingTwo8">
								<div class="card-title card-color collapsed" data-toggle="collapse" data-target="#collapseTwovaksin22">
									<div class="card-label">Vaksin</div>
									<span class="svg-icon">

										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												<polygon points="0 0 24 0 24 24 0 24" />
												<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero" />
												<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999)" />
											</g>
										</svg>

									</span>
								</div>
							</div>
							<div id="collapseTwovaksin22" class="collapse" data-parent="#accordionExample22">
								<div class="card-body">
									{{-- <table class="table">
										<tbody>
										<tr>
											<td>Vaksin Hepatitis B, </td>
											<td>Vaksin Rotavirus, </td>
											<td>Vaksin Hepatitis A</td>
										</tr>
										</tbody>
									</table> --}}
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header" id="headingThree8">
								<div class="card-title card-color collapsed" data-toggle="collapse" data-target="#collapseThreenatural22">
									<div class="card-label">Natural</div>
									<span class="svg-icon">

										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												<polygon points="0 0 24 0 24 24 0 24" />
												<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero" />
												<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999)" />
											</g>
										</svg>

									</span>
								</div>
							</div>
							<div id="collapseThreenatural22" class="collapse" data-parent="#accordionExample22">
								<div class="card-body">
									{{-- <table class="table">
										<tbody>
											<tr>
												<td>Ekstrak propolis, </td>
												<td>Glucosamine,</td>
												<td>Ekstrak Sukun, </td>
												<td>Ekstrak Daun jambu biji, </td>
												<td>Ekstrak Lengkuas</td>
											</tr>
										</tbody>
									</table> --}}
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header" id="headingTwo8">
								<div class="card-title card-color collapsed" data-toggle="collapse" data-target="#collapseFourbbo22">
									<div class="card-label">BBO Kimia</div>
									<span class="svg-icon">

										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												<polygon points="0 0 24 0 24 24 0 24" />
												<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero" />
												<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999)" />
											</g>
										</svg>

									</span>
								</div>
							</div>
							<div id="collapseFourbbo22" class="collapse" data-parent="#accordionExample22">
								<div class="card-body">
									{{-- <table class="table">
										<tbody>
											<tr>
												<td>Bisoprolol, </td>
												<td>Esomeprazole, </td>
												<td>Glimepirid, </td>
												<td>Meloksikam, </td>

											</tr>
											<tr>
												<td>Rifampisin,</td>
												<td>Telmisartan, </td>
												<td>Valsartan</td>
											</tr>
										</tbody>
									</table> --}}
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="card-single" period="period5">
				<h4 class="year">2023</h4>
				<!-- <h2 class="title">Lorem ipsum dolor sit amet.</h2> -->
				<div class="content">
					<div class="accordion accordion-solid accordion-panel accordion-svg-toggle" id="accordionExample23">
						<div class="card">
							<div class="card-header" id="headingOne8">
								<div class="card-title card-color" data-toggle="collapse" data-target="#collapseOnebio23">
									<div class="card-label">Bioteknologi</div>
									<span class="svg-icon">

										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												<polygon points="0 0 24 0 24 24 0 24" />
												<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero" />
												<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999)" />
											</g>
										</svg>

									</span>
								</div>
							</div>
							<div id="collapseOnebio23" class="collapse show" data-parent="#accordionExample23">
								{{-- <div class="card-body">
									<table class="table">
										<tr>
										<td>Insulin</td>
										</tr>
									</table>
								</div> --}}
							</div>
						</div>
						<div class="card">
							<div class="card-header" id="headingTwo8">
								<div class="card-title card-color collapsed" data-toggle="collapse" data-target="#collapseTwovaksin23">
									<div class="card-label">Vaksin</div>
									<span class="svg-icon">

										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												<polygon points="0 0 24 0 24 24 0 24" />
												<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero" />
												<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999)" />
											</g>
										</svg>

									</span>
								</div>
							</div>
							<div id="collapseTwovaksin23" class="collapse" data-parent="#accordionExample23">
								<div class="card-body">
									{{-- <table class="table">
										<tbody>
										<tr>
										<td>Vaksin BCG</td>
										</tr>
										</tbody>
									</table> --}}
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header" id="headingThree8">
								<div class="card-title card-color collapsed" data-toggle="collapse" data-target="#collapseThreenatural23">
									<div class="card-label">Natural</div>
									<span class="svg-icon">

										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												<polygon points="0 0 24 0 24 24 0 24" />
												<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero" />
												<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999)" />
											</g>
										</svg>

									</span>
								</div>
							</div>
							<div id="collapseThreenatural23" class="collapse" data-parent="#accordionExample23">
								<div class="card-body">
									{{-- <table class="table">
										<tbody>
											<tr>
												<td>Dihidroartemisinin (DHA),</td>
												<td>Ekstrak Gandarusa, </td>
												<td>Ekstrak Secang,</td>
												<td>Ekstrak Daun sembung</td>
											</tr>
										</tbody>
									</table> --}}
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header" id="headingTwo8">
								<div class="card-title card-color collapsed" data-toggle="collapse" data-target="#collapseFourbbo23">
									<div class="card-label">BBO Kimia</div>
									<span class="svg-icon">

										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												<polygon points="0 0 24 0 24 24 0 24" />
												<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero" />
												<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999)" />
											</g>
										</svg>

									</span>
								</div>
							</div>
							<div id="collapseFourbbo23" class="collapse" data-parent="#accordionExample23">
								<div class="card-body">
									{{-- <table class="table">
										<tbody>
											<tr>
												<td>Sefadroksil, </td>
												<td>Sefiksim, </td>
												<td>Sefoperazon, </td>
												<td>Hidrotalsit</td>
											</tr>
											<tr>
												<td>sefotaksim, </td>
												<td>Seftazidim, </td>
												<td>Seftriakson, </td>
											</tr>
										</tbody>
									</table> --}}
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="card-single" period="period6">
				<h4 class="year">2024</h4>
				<!-- <h2 class="title">Lorem ipsum dolor sit amet.</h2> -->
				<div class="content">
					<div class="accordion accordion-solid accordion-panel accordion-svg-toggle" id="accordionExample24">
						<div class="card">
							<div class="card-header" id="headingOne8">
								<div class="card-title card-color" data-toggle="collapse" data-target="#collapseOnebio24">
									<div class="card-label">Bioteknologi</div>
									<span class="svg-icon">

										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												<polygon points="0 0 24 0 24 24 0 24" />
												<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero" />
												<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999)" />
											</g>
										</svg>

									</span>
								</div>
							</div>
							<div id="collapseOnebio24" class="collapse show" data-parent="#accordionExample24">
								<div class="card-body">
									{{-- <table class="table">
										<tr>
											<td>Transtuzumab</td>
											<td>Bevacizumab</td>
										</tr>
									</table> --}}
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header" id="headingTwo8">
								<div class="card-title card-color collapsed" data-toggle="collapse" data-target="#collapseTwovaksin24">
									<div class="card-label">Vaksin</div>
									<span class="svg-icon">

										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												<polygon points="0 0 24 0 24 24 0 24" />
												<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero" />
												<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999)" />
											</g>
										</svg>

									</span>
								</div>
							</div>
							<div id="collapseTwovaksin24" class="collapse" data-parent="#accordionExample24">
								<div class="card-body">
									{{-- <table class="table">
										<tbody>
											<tr>
												<td>nOPV1</td>
												<td>nOPV3</td>
												<td>Hexavalent</td>
											</tr>
										</tbody>
									</table> --}}
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header" id="headingThree8">
								<div class="card-title card-color collapsed" data-toggle="collapse" data-target="#collapseThreenatural24">
									<div class="card-label">Natural</div>
									<span class="svg-icon">

										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												<polygon points="0 0 24 0 24 24 0 24" />
												<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero" />
												<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999)" />
											</g>
										</svg>

									</span>
								</div>
							</div>
							<div id="collapseThreenatural24" class="collapse" data-parent="#accordionExample24">
								<div class="card-body">
									{{-- <table class="table">
										<tbody>
											<tr>
												<td>Ekstrak Teripang</td>
												<td>Ekstrak Akar Manis </td>
											</tr>
										</tbody>
									</table> --}}
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header" id="headingTwo8">
								<div class="card-title card-color collapsed" data-toggle="collapse" data-target="#collapseFourbbo24">
									<div class="card-label">BBO Kimia</div>
									<span class="svg-icon">

										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												<polygon points="0 0 24 0 24 24 0 24" />
												<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero" />
												<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999)" />
											</g>
										</svg>

									</span>
								</div>
							</div>
							<div id="collapseFourbbo24" class="collapse" data-parent="#accordionExample24">
								<div class="card-body">
									{{-- <table class="table">
										<tbody>
											<tr>
												<td>Amoksisilin, </td>
												<td>Penisilin G </td>
											</tr>
										</tbody>
									</table> --}}
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="card-single" period="period7">
				<h4 class="year">2025</h4>
				<!-- <h2 class="title">Lorem ipsum dolor sit amet.</h2> -->
				<div class="content">
				<div class="accordion accordion-solid accordion-panel accordion-svg-toggle" id="accordionExample25">
						<div class="card">
							<div class="card-header" id="headingOne8">
								<div class="card-title card-color" data-toggle="collapse" data-target="#collapseOnebio25">
									<div class="card-label">Bioteknologi</div>
									<span class="svg-icon">

										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												<polygon points="0 0 24 0 24 24 0 24" />
												<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero" />
												<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999)" />
											</g>
										</svg>

									</span>
								</div>
							</div>
							<div id="collapseOnebio25" class="collapse show" data-parent="#accordionExample25">
								<div class="card-body">
									{{-- <table class="table">
										<tr>
											<td>Growth Colony Stimulating Factor (GCSF)</td>
										</tr>
									</table> --}}
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header" id="headingTwo8">
								<div class="card-title card-color collapsed" data-toggle="collapse" data-target="#collapseTwovaksin25">
									<div class="card-label">Vaksin</div>
									<span class="svg-icon">

										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												<polygon points="0 0 24 0 24 24 0 24" />
												<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero" />
												<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999)" />
											</g>
										</svg>

									</span>
								</div>
							</div>
							<div id="collapseTwovaksin25" class="collapse" data-parent="#accordionExample25">
								<div class="card-body">
									{{-- <table class="table">
										<tbody>
											<tr>
												<td>Sabin IPV (Inactivated Polio Vaccine)</td>
												<td>DTAP-HB-HiB</td>
												<td>New TB Recombinant</td>
											</tr>
										</tbody>
									</table> --}}
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header" id="headingThree8">
								<div class="card-title card-color collapsed" data-toggle="collapse" data-target="#collapseThreenatural25">
									<div class="card-label">Natural</div>
									<span class="svg-icon">

										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												<polygon points="0 0 24 0 24 24 0 24" />
												<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero" />
												<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999)" />
											</g>
										</svg>

									</span>
								</div>
							</div>
							<div id="collapseThreenatural25" class="collapse" data-parent="#accordionExample25">
								<div class="card-body">
									{{-- <table class="table">
										<tbody>
											<tr>
												<td>Ekstrak Kelembak (Crempamatum)</td>
											</tr>
										</tbody>
									</table> --}}
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header" id="headingTwo8">
								<div class="card-title card-color collapsed" data-toggle="collapse" data-target="#collapseFourbbo25">
									<div class="card-label">BBO Kimia</div>
									<span class="svg-icon">

										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												<polygon points="0 0 24 0 24 24 0 24" />
												<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero" />
												<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999)" />
											</g>
										</svg>

									</span>
								</div>
							</div>
							<div id="collapseFourbbo25" class="collapse" data-parent="#accordionExample25">
								<div class="card-body">
									{{-- <table class="table">
										<tbody>
										<tr>
											<td>Ekstrak Kelembak (Crempamatum)</td>
										</tr>
										</tbody>
									</table> --}}
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
		<!--   </div> -->
		<!-- ###   -->
	</div>
	<!-- # component ends -->
</div>
