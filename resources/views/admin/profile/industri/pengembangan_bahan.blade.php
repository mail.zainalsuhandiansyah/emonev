{{-- List Widget 1 --}}
<style>
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
    }

    /* Firefox */
    input[type=number] {
    -moz-appearance: textfield;
    }
</style>
<div class="card {{ @$class }}">
    {{-- Header --}}
    <div class="card-header border-1 pt-5 pb-0">
        <h3 class="card-title align-items-start flex-column">
            <span class="card-label font-weight-bolder text-dark">PENGEMBANGAN BAHAN BAKU</span>
        </h3>
    </div>

    {{-- Body --}}
    <div class="card-body pt-3 pb-0">
        
        {{-- Item --}}
        <!-- LIST -->
        @foreach ($listBBO as $bb)

            <div class="d-flex align-items-center">
                {{-- Symbol --}}
                <div class="symbol symbol-40 symbol-light-info mr-5">
                <i class="fa fa-genderless text-primary icon-xl "></i>
                </div>

                {{-- Text --}}
                <div class="d-flex flex-column font-weight-bold">
                    <a href="/admin/masterBBOIndustri" class="text-dark text-hover-primary mb-1 font-size-lg">{{$bb->zat_active_name}}</a>
                </div>
            </div>
        @endforeach
        <!-- END LIST -->
        
    </div>
</div>


<!-- card-custom -->