@extends('layout.admin.app')

@section('styles')

@endsection

@section('content')

<div class="card card-custom gutter-b">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">Data Bahan Baku
                <span class="d-block text-muted pt-2 font-size-sm">Bahan Baku Aktif</span>
            </h3>
        </div>
        <div class="card-toolbar">
            <!--begin::Button-->
            <a href="#" class="btn btn-success font-weight-bolder" id="btn_refresh">
                <i class="fa fa-spinner"></i> Pembaharui Data
            </a>
            &nbsp;&nbsp;
            <a href="#" class="btn btn-success font-weight-bolder" data-toggle="modal" data-target="#modalTambahData">
                <i class="fa fa-plus"></i> Tambah Data
            </a>
            &nbsp;&nbsp;
            <a href="javascript:;" class="btn btn-primary font-weight-bolder" data-toggle="modal" data-target="#modalTambahBahanBaku">
                <i class="fa fa-plus"></i>
                Tambah Bahan Baku
            </a>
            <!--end::Button-->
        </div>
    </div>
    <div class="card-body">
        <!--begin: Search Form-->
        <!--begin::Search Form-->
        <div class="mb-7">
            <div class="row align-items-center">
                <div class="col-lg-9 col-xl-8">
                    <div class="row align-items-center">
                        <div class="col-md-4 my-2 my-md-0">
                            <div class="input-icon">
                                <input type="text" class="form-control" placeholder="Search..." id="kt_datatable_search_query" />
                                <span>
                                    <i class="flaticon2-search-1 text-muted"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-4 my-2 my-md-0">
                            <div class="d-flex align-items-center">
                                <label class="mr-3 mb-0 d-none d-md-block">Status:</label>
                                <select class="form-control" id="kt_datatable_search_status">
                                    <option value="">All</option>
                                    <option value="1">Pending</option>
                                    <option value="2">Delivered</option>
                                    <option value="3">Canceled</option>
                                    <option value="4">Success</option>
                                    <option value="5">Info</option>
                                    <option value="6">Danger</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4 my-2 my-md-0">
                            <div class="d-flex align-items-center">
                                <label class="mr-3 mb-0 d-none d-md-block">Type:</label>
                                <select class="form-control" id="kt_datatable_search_type">
                                    <option value="">All</option>
                                    <option value="1">Online</option>
                                    <option value="2">Retail</option>
                                    <option value="3">Direct</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
                    <a href="#" class="btn btn-light-primary px-6 font-weight-bold">Search</a>
                </div>
            </div>
        </div>
        <!--end::Search Form-->
        <!--end: Search Form-->
        <!--begin: Datatable-->
        <div class="datatable datatable-bordered datatable-head-custom" id="kt_datatable"></div>
        <!--end: Datatable-->
    </div>
</div>


<!-- Modal-->
<div class="modal fade" id="modalTambahBahanBaku" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Zat Aktif</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
            <form class="form">
                <div class="card-body">
                    <div class="form-group">
                        <label>Nama:</label>
                        <input type="text" class="form-control" placeholder="Enter name"/>
                        
                    </div>
                    <div class="form-group">
                        <label>Jenis Bahan Baku</label>
                        <select class="form-control">
                            <option value="">- Silahkan Pilih -</option>
                            <option value="">Bioteknologi</option>
                            <option value="">Natural</option>
                            <option value="">Obat Kimia</option>
                            <option value="">Vaksin</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Fornas</label>
                        <select class="form-control">
                            <option value="">- Silahkan Pilih -</option>
                            <option value="">Non-fornas</option>
                            <option value="">Fornas</option>
                        </select>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="reset" class="btn btn-primary mr-2">Simpan</button>
                    <button type="reset" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Cancel</button>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalTambahData" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Data Bahan Baku</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
            <form class="form">
                <div class="card-body">
                    <div class="form-group">
                        <label>Zat Aktif:</label>
                        <select class="form-control">
                            <option value="">- Silahkan Pilih -</option>
                            <option value="">Enoksaparin Sodium</option>
                            <option value="">Trastuzumab</option>
                            <option value="">Bevacizumab Drug Substance (DS)</option>
                            <option value="">Vaksin HB (Hepatitis-B)</option>
                            <option value="">Vaksin Rotavirus</option>
                            <option value="">IPV</option>
                        </select>
                        
                    </div>
                </div>
                <div class="card-footer">
                    <button type="reset" class="btn btn-primary mr-2">Simpan</button>
                    <button type="reset" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Cancel</button>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalDetailData" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detail Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <form class="form">
                    <div class="card-body">
                        <ul>
                            <li>Enok Saparin Sodium</li>
                            <li>Bentuk Kimia</li>
                        </ul>
                    </div>
                    <div class="card-footer">
                        <button type="reset" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<!-- <script src="{{ asset('js/pages/crud/ktdatatable/base/data-bahan-baku.js') }}"></script> -->
<script>
    $(function () {

        $("#btn_refresh").click(function () {

            setTimeout(function () {
                swal.fire({
                    title: "Good job!",
                    text: "Pembaharuan Berhasil !",
                    icon: "success",
                });
            },400);
        })

        var dataJSONArray = JSON.parse('[{"RecordID":1,"kodeBahanBaku":"BPM-0229921-1","zatAktif":"Enoksaparin Sodium","Actions":null},\n' +
        '{"RecordID":1,"kodeBahanBaku":"BPM-0229921-8","zatAktif":"Bevacizumab Drug Substance (DS)","Actions":null},\n' +
        '{"RecordID":1,"kodeBahanBaku":"BPM-0229921-9","zatAktif":"Trastuzumab","Actions":null},\n' +
        '{"RecordID":1,"kodeBahanBaku":"BPM-0229921-7","zatAktif":"Vaksin Rotavirus","Actions":null},\n' +
        '{"RecordID":1,"kodeBahanBaku":"BPM-0229921-2","zatAktif":"nOPV2","Actions":null},\n' +
        '{"RecordID":1,"kodeBahanBaku":"BPM-0229921-4","zatAktif":"Vaksin MR","Actions":null},\n' +
        '{"RecordID":2,"kodeBahanBaku":"BPM-0229921-3","zatAktif":"Plasma Fractionation (albumin, Immunogbulin, Faktor VIII)","Actions":null}]');

        var datatable = $('#kt_datatable').KTDatatable({
            // datasource definition
            data: {
                type: 'local',
                source: dataJSONArray,
                pageSize: 10,
            },

            // layout definition
            layout: {
                scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
                // height: 450, // datatable's body's fixed height
                footer: false, // display/hide footer
            },

            // column sorting
            sortable: true,

            pagination: true,

            search: {
                input: $('#kt_datatable_search_query'),
                key: 'generalSearch',
            },

            // columns definition
            columns: [{
                field: 'RecordID',
                title: '#',
                sortable: false,
                width: 20,
                type: 'number',
                selector: true,
                textAlign: 'center',
            }, {
                field: 'kodeBahanBaku',
                title: 'Kode Bahan Baku',
            },{
                field: 'zatAktif',
                title: 'Zat Aktif',
            }, {
                field: 'Actions',
                title: 'Actions',
                sortable: false,
                width: 125,
                overflow: 'visible',
                autoHide: false,
                template: function() {
                    return `<a href="javascript:;" class="btn btn-sm btn-clean btn-icon" title="Detail Data" data-toggle="modal" data-target="#modalDetailData">
	                            <i class="fa fa-eye"></i>
							</a>
						`;
                },
            }],
        });

        $('#kt_datatable_search_status').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Status');
        });

        $('#kt_datatable_search_type').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Type');
        });

        $('#kt_datatable_search_status, #kt_datatable_search_type').selectpicker();
  });
</script>
@endsection