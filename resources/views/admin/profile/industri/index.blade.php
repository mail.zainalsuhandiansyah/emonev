@extends('layout.admin.app')

@section('content')

<div class="row">
        <!-- Data Penrusahaan -->
        <div class="col-xxl-12  order-xxl-1">
            @include('admin.profile.industri.profile_perusahaan', ['class' => 'card-stretch gutter-b'])
        </div>
    
        <!-- END -->

        <!-- Data Penanggung Jawab Laporan -->
        
        <div class="col-xxl-6 order-2 col-xxl-4 mt-5">
            @include('admin.profile.industri.pengembangan_bahan', ['class' => 'card-stretch gutter-b'])
        </div>
            
        <!-- END -->
    </div>

@endsection