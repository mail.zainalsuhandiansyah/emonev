@extends('layout.admin.app')

@section('styles')

@endsection

@section('content')

<div class="card card-custom gutter-b">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">Setting Slider
                <span class="d-block text-muted pt-2 font-size-sm">Setting Slider</span>
            </h3>
        </div>
        <div class="card-toolbar">
            <!--begin::Button-->
            <a href="#" class="btn btn-success font-weight-bolder" data-toggle="modal" data-target="#modalAdd">
                <i class="fa fa-plus"></i> Add Data
            </a>
            <!--end::Button-->
        </div>
    </div>
    <div class="card-body">
        <!--begin: Search Form-->
        <!--begin::Search Form-->
        <div class="mb-7">
            <div class="row align-items-center">
                <div class="col-lg-4 col-xl-4">
                    <div class="row align-items-center">
                        <div class="col-md-12 my-12 my-md-0">
                            <div class="input-icon">
                                <input type="text" class="form-control" placeholder="Search..." id="kt_datatable_search_query" />
                                <span>
                                    <i class="flaticon2-search-1 text-muted"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
                    <a href="javascript:;" id="button-search" class="btn btn-light-primary px-6 font-weight-bold">Search</a>
                </div>
            </div>
        </div>
        <!--end::Search Form-->
        <!--end: Search Form-->
        <!--begin: Datatable-->
        <div class="datatable datatable-bordered datatable-head-custom" id="kt_datatable"></div>
        <!--end: Datatable-->
    </div>
</div>

<div class="modal fade" id="modalAdd" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <form class="form" id="form_add" enctype="multipart/form-data" method="post" role="form">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label>Judul</label>
                            <input type="text" class="form-control" required="" id="judul" name="judul" placeholder="Enter judul"/>
                        </div>
                        <!-- <div class="form-group">
                            <label>Image</label>
                            <input type="text" class="form-control" required="" id="judul" name="judul" placeholder="Enter judul"/>
                        </div> -->
                        <!-- <div class="form-group">
                            <label>Image</label>
                            <div></div>
                            <div class="custom-file">
                            
                            <input type="file" class="custom-file-input" name="image" id="image" class="form-control">
                            <label class="custom-file-label" for="image">Choose file</label>
                            </div>
                        </div> -->

                        <div class="form-group">
                            <label><strong>Image : </strong></label>
                            <!-- <input type="file" name="image" id="image" class="form-control"> -->
                            <input id="image" type="file" name="image" accept="image/*" onchange="readURL(this);">
                            <input type="hidden" name="hidden_image" id="hidden_image">
                        </div>
                        
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary mr-2" id="submit"><i class="fa fa-save"></i> Save</button>
                        <button type="reset" class="btn btn-secondary" id="cancel"><i class="fa fa-arrow-left"></i> Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalEdit" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <form action="">
                
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
    $(function () {

        var modalAdd = $("#modalAdd");
        var formAdd = $("#form_add",modalAdd);

        var modalEdit = $("#modalEdit");
        var formEdit = $("#form_edit",modalEdit);

        var data = {!! json_encode($list->toArray()) !!};

        console.log(data);
        

        var datatable = $('#kt_datatable').KTDatatable({
            // datasource definition
            data: {
                type: 'local',
                source: data,
                pageSize: 10,
            },

            // layout definition
            layout: {
                scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
                // height: 450, // datatable's body's fixed height
                footer: false, // display/hide footer
            },

            // column sorting
            sortable: true,

            pagination: true,

            search: {
                input: $('#kt_datatable_search_query'),
                key: 'generalSearch',
            },

            // columns definition
            columns: [{
                field: 'id_slider',
                title: '#',
                sortable: false,
                width: 20,
                type: 'number',
                selector: true,
                textAlign: 'center',
            },
            {
                field: 'judul',
                title: 'Judul',
            },
            {
                field: 'image',
                title: 'Image',
            },
            
            {
                field: 'Actions',
                title: 'Actions',
                sortable: false,
                width: 125,
                overflow: 'visible',
                autoHide: false,
                template: function(response) {
                    return `<a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-2" title="Edit" data-id="`+response.id_slider+`" id="button-edit" data-token="{{ csrf_token() }}">
	                            <span class="svg-icon svg-icon-md">
	                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
	                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
	                                        <rect x="0" y="0" width="24" height="24"/>
	                                        <path d="M8,17.9148182 L8,5.96685884 C8,5.56391781 8.16211443,5.17792052 8.44982609,4.89581508 L10.965708,2.42895648 C11.5426798,1.86322723 12.4640974,1.85620921 13.0496196,2.41308426 L15.5337377,4.77566479 C15.8314604,5.0588212 16,5.45170806 16,5.86258077 L16,17.9148182 C16,18.7432453 15.3284271,19.4148182 14.5,19.4148182 L9.5,19.4148182 C8.67157288,19.4148182 8,18.7432453 8,17.9148182 Z" fill="#000000" fill-rule="nonzero"\ transform="translate(12.000000, 10.707409) rotate(-135.000000) translate(-12.000000, -10.707409) "/>
	                                        <rect fill="#000000" opacity="0.3" x="5" y="20" width="15" height="2" rx="1"/>
	                                    </g>
	                                </svg>
	                            </span>
							</a>
							<a href="javascript:;" class="btn btn-sm btn-clean btn-icon" title="Delete" id="button-delete" data-id="`+response.id_slider+`" data-token="{{ csrf_token() }}">
	                            <span class="svg-icon svg-icon-md">
	                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
	                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
	                                        <rect x="0" y="0" width="24" height="24"/>
	                                        <path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"/>
	                                        <path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"/>
	                                    </g>
	                                </svg>
	                            </span>
							</a>`;
                },
            }],
        });

        $('#kt_datatable_search_status').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Status');
        });

        $('#kt_datatable_search_type').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Type');
        });

        $('#kt_datatable_search_status, #kt_datatable_search_type').selectpicker();


        //event add 

        $('#submit',formAdd).click(function(e){
            e.preventDefault();
            /*Ajax Request Header setup*/
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#submit',formAdd).html('Sending..');
            $('#submit',formAdd).attr('disabled',true);
            
            /* Submit form data using ajax*/
            $.ajax({
                url:'{{route('store.image')}}',
                method: 'POST',
                cache:false,
                data: formAdd.serialize(),
                success: function(response){
                    $('#submit',formAdd).html('Saved');
                    $('#submit',formAdd).attr('disabled',false);
                    var data = JSON.parse(response);
                    console.log('response ',JSON.parse(response))
                    console.log('response ',response.success)
                    console.log('data ',data.message)

                    if(data.success===true){
                        // Swal.fire('Success','Data has been saved !','success');
                        Swal.fire({
                            title: 'Data has been saved !',
                            showDenyButton: false,
                            showCancelButton: false,
                            confirmButtonText: `OK`,
                            icon:'success'
                        }).then((result) => {
                            /* Read more about isConfirmed, isDenied below */
                            if (result.isConfirmed) {
                                location.reload();
                            } else if (result.isDenied) {
                                Swal.fire('Changes are not saved', '', 'info')
                            }
                        })
                        formAdd.trigger('reset'); 
                        modalAdd.modal('hide');
                    }else{
                        Swal.fire({
                            icon: 'warning',
                            text: data.message
                        })
                    }
                },error:function(response){
                    $('#submit',formAdd).html('Saved');
                    $('#submit',formAdd).attr('disabled',false);
                    Swal.fire('Warning ! ','An error occurred, please try again later','warning');
                }
            });
        });

        // console.log("formAdd", formAdd);

        $("#cancel",formAdd).on('click',function(){
            formAdd.trigger('reset'); 
            modalAdd.modal('hide'); 
        });

        //end event add 

        //event delete 
        //  $('#kt_datatable').on("click", "#button-delete", function() {
        //     var id = $(this).data('id');
        //     var token = $(this).data("token");

        //     Swal.fire({
        //         title: 'Are you sure?',
        //         text: "To deleted this data !",
        //         icon: 'warning',
        //         showCancelButton: true,
        //         confirmButtonColor: '#3085d6',
        //         cancelButtonColor: '#d33',
        //         confirmButtonText: 'Yes, delete it!'
        //     }).then((result) => {
        //         if (result.isConfirmed) {
        //             $.ajaxSetup({
        //                 headers: {
        //                     'X-CSRF-TOKEN': token
        //                 }
        //             });
        //             $.ajax({
        //                 url: "zat-active/"+id,
        //                 type: 'DELETE', // replaced from put
        //                 dataType: "JSON",
        //                 // data: {
        //                 //     "id": id // method and token not needed in data
        //                 // },
        //                 success: function (response){
        //                     Swal.fire('success','Data has been deleted !');// see the reponse sent
        //                     location.reload();
        //                 },
        //                 error: function(xhr) {
        //                     console.log(xhr.responseText); // this line will save you tons of hours while debugging
        //                     // do something here because of error
        //                 }
        //             });
        //         }
        //     })
        // })

        //end event delete

        //event edit

        // $('#kt_datatable').on("click", "#button-edit", function() {
        //     var id = $(this).data('id');
        //     var token = $(this).data("token");

        //     $.ajax({
        //         url: "zat-active/"+id,
        //         type: 'GET',
        //         // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
        //         data: {
        //             id : id,
        //             _token:'{{ csrf_token() }}'
        //         },
        //         dataType: 'json',
        //         success: function(response){
        //             console.log(response,'response')

        //             $("#zat_active_id",formEdit).val(response.zat_active_id);
        //             $("#zat_active_name",formEdit).val(response.zat_active_name);
        //             $("#zat_group_id",formEdit).val(response.zat_group_id);
        //             $("#zat_active_fornas",formEdit).val(response.zat_active_fornas);
        //             modalEdit.modal('show');
        //         }, error: function(xhr) {
        //             Swal.fire('Warning !','An error occurred, please try again later','warning');
        //             console.log(xhr.responseText); // this line will save you tons of hours while debugging
        //             // do something here because of error
        //         }
        //     })
        // })

        // $('#submit',formEdit).click(function(e){
        //     e.preventDefault();
        //     $('#submit',formEdit).html('Sending..');
        //     $('#submit',formEdit).attr('disable',true);
            
        //     zat_active_id = $("#zat_active_id",formEdit).val();
        //     token  = $("#_token",formEdit).val();
        //     console.log(token,'token ')
        //     /* Submit form data using ajax*/
        //     $.ajax({
        //         url: "zat-active/edit",
        //         type: 'PATCH',
        //         // data : {
        //         //     data :formEdit.serialize(),
        //         //     _token:'{{ csrf_token() }}'
        //         // },
        //         data: formEdit.serialize(),
        //         success: function(response){
        //             $('#submit',formEdit).html('Saved');
        //             $('#submit',formEdit).attr('disable',false);
        //             var data = JSON.parse(response);
        //             console.log('response ',JSON.parse(response))
        //             console.log('response ',response.success)
        //             console.log('data ',data.message)

        //             if(data.success===true){
        //                 Swal.fire({
        //                     title: 'Data has been updated !',
        //                     showDenyButton: false,
        //                     showCancelButton: false,
        //                     confirmButtonText: `OK`,
        //                     icon:'success'
        //                 }).then((result) => {
        //                     /* Read more about isConfirmed, isDenied below */
        //                     if (result.isConfirmed) {
        //                         location.reload();
        //                     } else if (result.isDenied) {
        //                         Swal.fire('Changes are not saved', '', 'info')
        //                     }
        //                 })
        //                 formEdit.trigger('reset'); 
        //                 modalEdit.modal('hide');
        //             }else{
        //                 Swal.fire({
        //                     icon: 'warning',
        //                     text: data.message
        //                 })
        //             }
        //         },error:function(response){
        //             Swal.fire('Warning !','An error occurred, please try again later','warning');
        //             console.log(xhr.responseText); // this line will save you tons of hours while debugging
        //         }
        //     });
        // });

        // $("#cancel",formEdit).on('click',function(){
        //     formEdit.trigger('reset'); 
        //     modalEdit.modal('hide'); 
        // });
        //end event EDIT
    });
</script>
@endsection