@extends('layout.admin.app')

@section('styles')

@endsection

@section('content')

<div class="card card-custom gutter-b" id="portlet_grid">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">Master Bahan Baku yang Dikembangkan
                <span class="d-block text-muted pt-2 font-size-sm">Master Bahan Baku yang Dikembangkan</span>
            </h3>
        </div>
        <div class="card-toolbar">
            <!--begin::Button-->
            <a href="javascript:;" class="btn btn-primary font-weight-bolder" id="button-add">
                <i class="fa fa-plus"></i> Add Data Bahan Baku
            </a>
            &nbsp;&nbsp;
            <a href="#" class="btn btn-success font-weight-bolder" id="button-add-elic">
                <i class="fa fa-plus"></i> Add Data
            </a>
            <!--end::Button-->
        </div>
    </div>
    <div class="card-body">
        <!--begin: Search Form-->
        <!--begin::Search Form-->
        <div class="mb-7">
            <div class="row align-items-center">
                <div class="col-lg-4 col-xl-4">
                    <div class="row align-items-center">
                        <div class="col-md-12 my-12 my-md-0">
                            <div class="input-icon">
                                <input type="text" class="form-control" placeholder="Search..." id="kt_datatable_search_query" />
                                <span>
                                    <i class="flaticon2-search-1 text-muted"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
                    <a href="javascript:;" id="button-search" class="btn btn-light-primary px-6 font-weight-bold">Search</a>
                </div>
            </div>
        </div>
        <!--end::Search Form-->
        <!--end: Search Form-->
        <!--begin: Datatable-->
        <div class="datatable datatable-bordered datatable-head-custom" id="kt_datatable"></div>
        <!--end: Datatable-->
    </div>
</div>

<div class="modal fade" id="modalAdd" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <form class="form" id="form_add" enctype="multipart/form-data" method="post" role="form">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label>Nama:</label>
                            <input type="text" class="form-control" required="" id="zat_active_name" name="zat_active_name" placeholder="Enter Nama"/>
                        </div>
                        <div class="form-group">
                            <label>Jenis Bahan Baku:</label>
                            <select class="form-control" name="zat_group_id" id="zat_group_id">
                                <option value="">- Please Select -</option>
                                @foreach ($jenisbb as $bb)
                                    <option value="{{$bb->zat_group_id}}">{{$bb->zat_group_description}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Fornas:</label>
                            <select class="form-control" name="zat_active_fornas" id="zat_active_fornas">
                                <option value="">- Please Select -</option>
                                <option value="0">Non Fornas</option>
                                <option value="1">Fornas</option>
                            </select>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary mr-2" id="submit"><i class="fa fa-save"></i> Save</button>
                        <button type="reset" class="btn btn-secondary" id="cancel"><i class="fa fa-arrow-left"></i> Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalEdit" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <form class="form" id="form_edit" action="{{ url('/privileges') }}" enctype="multipart/form-data" method="post" role="form">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="zat_active_id" id="zat_active_id">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label>Nama:</label>
                            <input type="text" class="form-control" required="" id="zat_active_name" name="zat_active_name" placeholder="Enter Nama"/>
                        </div>
                        <div class="form-group">
                            <label>Jenis Bahan Baku:</label>
                            <select class="form-control" name="zat_group_id" id="zat_group_id">
                                <option value="">- Please Select -</option>
                                @foreach ($jenisbb as $bb)
                                    <option value="{{$bb->zat_group_id}}">{{$bb->zat_group_description}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Fornas:</label>
                            <select class="form-control" name="zat_active_fornas" id="zat_active_fornas">
                                <option value="">- Please Select -</option>
                                <option value="0">Non Fornas</option>
                                <option value="1">Fornas</option>
                            </select>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary mr-2" id="submit"><i class="fa fa-save"></i> Save</button>
                        <button type="reset" class="btn btn-secondary" id="cancel"><i class="fa fa-arrow-left"></i> Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- modalAddBBO -->
<div class="modal fade" id="modalAddBBO" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <form class="form" id="form_add_bbo" enctype="multipart/form-data" method="post" role="form">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="obatid" id="obatid">

                    @csrf
                    <div class="card-body">
                        <div class="mb-7">
                            <div class="row align-items-center">
                                <div class="col-lg-4 col-xl-4">
                                    <div class="row align-items-center">
                                        <div class="col-md-12 my-12 my-md-0">
                                            <div class="input-icon">
                                                <input type="text" class="form-control" placeholder="Search..." id="kt_datatable_search_query" />
                                                <span>
                                                    <i class="flaticon2-search-1 text-muted"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
                                    <a href="javascript:;" id="button-search" class="btn btn-light-primary px-6 font-weight-bold">Search</a>
                                </div>
                            </div>
                        </div>
                        <span style="margin-bottom:10px;color:red;"> *) Silahkan pilih bahan baku obat untuk menambahkan data </span>
                        <!--end: Search Form-->
                        <div class="mt-10 mb-5 collapse" id="kt_datatable_group_action_form">
                            <div class="d-flex align-items-center">
                                <div class="font-weight-bold text-danger mr-3">Selected
                                <span id="kt_datatable_selected_records"> 0 </span> records:</div>
                                <button class="btn btn-sm btn-success" type="button" id="button-create">Create Bahan Baku Obat</button>
                            </div>
                        </div>

                        <!--begin: Datatable-->
                        <div class="datatable datatable-bordered datatable-head-custom" id="kt_datatable_bbo"></div>
                        <!--end: Datatable-->
                    </div>
                    <div class="card-footer">
                        <button type="submit" style="display:none;" class="btn btn-primary mr-2" id="submit"><i class="fa fa-save"></i> Save</button>
                        <button type="reset" class="btn btn-secondary" id="cancel"><i class="fa fa-arrow-left"></i> Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>

    var Grid = function(){
        var portletGrid = $("#portlet_grid");
        var modalAdd = $("#modalAdd");
        var formAdd = $("#form_add",modalAdd);
        var modalEdit = $("#modalEdit");
        var formEdit = $("#form_edit",modalEdit);
        

        var datatable = $('#kt_datatable',portletGrid);
        return {
            init :function(){

                datatable.KTDatatable({

                    data: {
                        type: 'remote',
                        source: {
                            read: {
                                url: 'listZatActive',
                                method:'POST',
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                }
                            },
                        },
                        pageSize: 10,
                        serverPaging: true,
                        serverFiltering: true,
                        serverSorting: true,
                    },

                    // layout definition
                    layout: {
                        scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
                        // height: 450, // datatable's body's fixed height
                        footer: false, // display/hide footer
                    },

                    // column sorting
                    sortable: true,

                    pagination: true,

                    search: {
                        input: $('#kt_datatable_search_query'),
                        key: 'generalSearch',
                    },

                    // columns definition
                    columns: [{
                        field: 'zat_active_id',
                        title: '#',
                        sortable: false,
                        width: 20,
                        type: 'number',
                        selector: true,
                        textAlign: 'center',
                    },
                    {
                        field: 'zat_active_name',
                        title: 'Nama Bahan Baku Obat',
                    },
                    {
                        field: 'zat_group_description',
                        title: 'Jenis Bahan Baku Obat',
                    },
                    {
                        field: 'zat_active_fornas',
                        title: 'Fornas',
                        template:function(response){
                            ket = response.zat_active_fornas;
                            if(ket=='0'){
                                ket = 'Non-Fornas';
                            }else{
                                ket = 'Fornas';
                            }

                            return ket;
                        }
                    },
                    {
                        field: 'Actions',
                        title: 'Actions',
                        sortable: false,
                        width: 125,
                        overflow: 'visible',
                        autoHide: false,
                        template: function(response) {
                            return `<a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-2" title="Edit" data-id="`+response.zat_active_id+`" id="button-edit" data-token="{{ csrf_token() }}">
                                        <span class="svg-icon svg-icon-md">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"/>
                                                    <path d="M8,17.9148182 L8,5.96685884 C8,5.56391781 8.16211443,5.17792052 8.44982609,4.89581508 L10.965708,2.42895648 C11.5426798,1.86322723 12.4640974,1.85620921 13.0496196,2.41308426 L15.5337377,4.77566479 C15.8314604,5.0588212 16,5.45170806 16,5.86258077 L16,17.9148182 C16,18.7432453 15.3284271,19.4148182 14.5,19.4148182 L9.5,19.4148182 C8.67157288,19.4148182 8,18.7432453 8,17.9148182 Z" fill="#000000" fill-rule="nonzero"\ transform="translate(12.000000, 10.707409) rotate(-135.000000) translate(-12.000000, -10.707409) "/>
                                                    <rect fill="#000000" opacity="0.3" x="5" y="20" width="15" height="2" rx="1"/>
                                                </g>
                                            </svg>
                                        </span>
                                    </a>
                                    <a href="javascript:;" class="btn btn-sm btn-clean btn-icon" title="Delete" id="button-delete" data-id="`+response.zat_active_id+`" data-token="{{ csrf_token() }}">
                                        <span class="svg-icon svg-icon-md">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"/>
                                                    <path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"/>
                                                    <path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"/>
                                                </g>
                                            </svg>
                                        </span>
                                    </a>`;
                        },
                    }],
                });

                $('#button-search',portletGrid).on('click', function() {
                    Grid.reloadGrid();
                });
                $('#kt_datatable_search_status, #kt_datatable_search_type').selectpicker();

                Grid.clickEdit();
                Grid.clickDelete();
            },
            reloadGrid:function(){
                datatable.reload();
            },
            openModal:function(ele){
                ele.modal('show');
            },
            closeModal:function(ele){
                ele.modal('hide');
            },
            clickEdit:function(){
                datatable.on("click", "#button-edit", function() {
                    var id = $(this).data('id');
                    var token = $(this).data("token");

                    $.ajax({
                        url: "zat-active/"+id,
                        type: 'GET',
                        data: {
                            id : id,
                            _token:'{{ csrf_token() }}'
                        },
                        dataType: 'json',
                        success: function(response){
                            console.log(response,'response')

                            $("#zat_active_id",formEdit).val(response.zat_active_id);
                            $("#zat_active_name",formEdit).val(response.zat_active_name);
                            $("#zat_group_id",formEdit).val(response.zat_group_id);
                            $("#zat_active_fornas",formEdit).val(response.zat_active_fornas);
                            Grid.openModal(modalEdit);
                        }, error: function(response) {
                            if(response.status===419){
                                window.location.href = "{{ route('login')}}";   
                            }else{
                                Swal.fire('Warning ! ','An error occurred, please try again later','warning');
                            }
                        }
                    })
                })
            },
            clickDelete:function(){
                datatable.on("click", "#button-delete", function() {
                    var id = $(this).data('id');
                    var token = $(this).data("token");

                    Swal.fire({
                        title: 'Are you sure?',
                        text: "To deleted this data !",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, delete it!'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            $.ajaxSetup({
                                headers: {
                                    'X-CSRF-TOKEN': token
                                }
                            });
                            $.ajax({
                                url: "zat-active/"+id,
                                type: 'DELETE', // replaced from put
                                dataType: "JSON",
                                success: function (response){
                                    Swal.fire('success','Data has been deleted !');// see the reponse sent
                                    Grid.reloadGrid();
                                },
                                error: function(xhr) {
                                    if(response.status===419){
                                        window.location.href = "{{ route('login')}}";   
                                    }else{
                                        Swal.fire('Warning ! ','An error occurred, please try again later','warning');
                                    }
                                }
                            });
                        }
                    })
                })
            }
        };
    }();

    var AddBBO = function(){
        var portletGrid = $("#portlet_grid");
        var modalAdd = $("#modalAdd");
        var formAdd = $("#form_add",modalAdd);
        var btnsubmit = $("#submit",formAdd);
        var validation;

        return {
            init:function(){
                $("#button-add",portletGrid).on('click',function(){
                    console.log('sster');
                    formAdd.trigger('reset');
                    Grid.openModal(modalAdd);
                })

                $("#cancel",formAdd).on('click',function(){
                    formAdd.trigger('reset');
                    Grid.closeModal(modalAdd);
                })

                btnsubmit.on('click',this.process);
                formAdd.on('submit',this.doprocess);

            },
            process:function(e){
                e.preventDefault();
                validation = FormValidation.formValidation(
					KTUtil.getById('form_add'), {
						fields: {
							zat_active_name: {
								validators: {
									notEmpty: {
										message: 'Zat Active Name is required'
									}
								}
							},
						},
						plugins: {
							trigger: new FormValidation.plugins.Trigger(),
							submitButton: new FormValidation.plugins.SubmitButton(),
							bootstrap: new FormValidation.plugins.Bootstrap()
						}
					}
				);

				validation.validate().then(function(status) {
                    if (status == 'Valid') {
                        formAdd.trigger('submit');
                    }else{
                        swal.fire({
                            text: "Sorry, looks like there are some errors detected, please try again.",
                            icon: "error",
                            buttonsStyling: false,
                            confirmButtonText: "Ok, got it!",
                            customClass: {
                                confirmButton: "btn font-weight-bold btn-light-primary"
                            }
                        }).then(function() {
                            KTUtil.scrollTop();
                        });
                    }
                });
            },
            doprocess:function(e){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                btnsubmit.html('Sending..');
                btnsubmit.attr('disabled',true);
                
                /* Submit form data using ajax*/
                $.ajax({
                    url:'zat-active',
                    method: 'POST',
                    data: formAdd.serialize(),
                    success: function(response){
                        btnsubmit.html('Saved');
                        btnsubmit.attr('disabled',false);
                        var data = JSON.parse(response);
                        console.log('response ',JSON.parse(response))
                        console.log('response ',response.success)
                        console.log('data ',data.message)

                        if(data.success===true){
                            Swal.fire({
                                title: 'Data has been saved !',
                                showDenyButton: false,
                                showCancelButton: false,
                                confirmButtonText: `OK`,
                                icon:'success'
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    Grid.reloadGrid();
                                    Grid.closeModal(modalEdit);
                                } else if (result.isDenied) {
                                    Swal.fire('Changes are not saved', '', 'info')
                                }
                            })
                            formAdd.trigger('reset'); 
                            modalAdd.modal('hide');
                        }else{
                            Swal.fire({
                                icon: 'warning',
                                text: data.message
                            })
                        }
                    },error:function(response){
                        console.log('response ',response)
                        if(response.status===419){
                            window.location.href = "{{ route('login')}}";   
                        }else{
                            Swal.fire('Warning ! ','An error occurred, please try again later','warning');
                        }
                        btnsubmit.html('Saved');
                        btnsubmit.attr('disabled',false);
                    }
                });
            }
        };
    }();

    var EditBBO = function(){
        var portletGrid = $("#portlet_grid");
        var modalEdit = $("#modalEdit");
        var formEdit = $("#form_edit",modalEdit);
        var btnsubmit = $("#submit",formEdit);
        var validation;
        return {
            init:function(){
                $("#cancel",formEdit).on('click',function(){
                    formEdit.trigger('reset');
                    Grid.closeModal(modalEdit)
                })

                btnsubmit.on('click',this.process);
                formEdit.on('submit', this.doprocess);
            },
            process:function(e){
                e.preventDefault();
                validation = FormValidation.formValidation(
					KTUtil.getById('form_edit'), {
						fields: {
							zat_active_name: {
								validators: {
									notEmpty: {
										message: 'Zat Active Name is required'
									}
								}
							},
						},
						plugins: {
							trigger: new FormValidation.plugins.Trigger(),
							submitButton: new FormValidation.plugins.SubmitButton(),
							bootstrap: new FormValidation.plugins.Bootstrap()
						}
					}
				);

				validation.validate().then(function(status) {
                    if (status == 'Valid') {
                        formEdit.trigger('submit');
                    }else{
                        swal.fire({
                            text: "Sorry, looks like there are some errors detected, please try again.",
                            icon: "error",
                            buttonsStyling: false,
                            confirmButtonText: "Ok, got it!",
                            customClass: {
                                confirmButton: "btn font-weight-bold btn-light-primary"
                            }
                        }).then(function() {
                            KTUtil.scrollTop();
                        });
                    }
                });
            },
            doprocess:function(e){
                e.preventDefault();
                btnsubmit.html('Sending..');
                btnsubmit.attr('disable',true);
                
                zat_active_id = $("#zat_active_id",formEdit).val();
                token  = $("#_token",formEdit).val();

                /* Submit form data using ajax*/
                $.ajax({
                    url: "zat-active/edit",
                    type: 'PATCH',
                    data: formEdit.serialize(),
                    success: function(response){
                        btnsubmit.html('Saved');
                        btnsubmit.attr('disable',false);
                        var data = JSON.parse(response);
                        console.log('response ',JSON.parse(response))
                        console.log('response ',response.success)
                        console.log('data ',data.message)

                        if(data.success===true){
                            Swal.fire({
                                title: 'Data has been updated !',
                                showDenyButton: false,
                                showCancelButton: false,
                                confirmButtonText: `OK`,
                                icon:'success'
                            }).then((result) => {
                                /* Read more about isConfirmed, isDenied below */
                                if (result.isConfirmed) {
                                    Grid.reloadGrid();
                                    Grid.closeModal(modalEdit);
                                } else if (result.isDenied) {
                                    Swal.fire('Changes are not saved', '', 'info')
                                }
                            })
                            formEdit.trigger('reset'); 
                            modalEdit.modal('hide');
                        }else{
                            Swal.fire({
                                icon: 'warning',
                                text: data.message
                            })
                        }
                    },error:function(response){
                        console.log('response ',response)
                        if(response.status===419){
                            window.location.href = "{{ route('login')}}";   
                        }else{
                            Swal.fire('Warning ! ','An error occurred, please try again later','warning');
                        }
                        btnsubmit.html('Saved');
                        btnsubmit.attr('disabled',false);
                    }
                });
            }
        };
    }();

    var MasterBBO = function(){
        var portletGrid = $("#portlet_grid");
        var modalAddBBO = $("#modalAddBBO");
        var formAddBBO = $("#form_add_bbo",modalAddBBO);

        var btncreate = $('#button-create',formAddBBO);
        var btnsubmit = $("#submit",formAddBBO);

        var datatableBBO = $('#kt_datatable_bbo',formAddBBO);

        return {
            init: function() {
                // grid bbo

                datatableBBO.KTDatatable({
                    data: {
                        type: 'remote',
                        source: {
                            read: {
                                url: 'listBBO',
                                method:'POST',
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                }
                            },
                        },
                        pageSize: 10,
                        serverPaging: true,
                        serverFiltering: true,
                        serverSorting: true,
                    },

                    // layout definition
                    layout: {
                        scroll: false, // enable/disable datatable scroll both horizontal and
                        footer: false // display/hide footer
                    },
                    extensions : {
                    // boolean or object (extension options)
                        checkbox: true,
                    },
                    // layout definition
                    layout: {
                        scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
                        // height: 450, // datatable's body's fixed height
                        footer: false, // display/hide footer
                    },

                    // column sorting
                    sortable: true,

                    pagination: true,

                    search: {
                        input: $('#kt_datatable_search_query',formAddBBO),
                        key: 'generalSearch',
                    },

                    // columns definition
                    columns: [{
                        field: 'OBAT_ID',
                        title: '#',
                        sortable: false,
                        width: 30,
                        type: 'number',
                        selector: true,
                        textAlign: 'left',
                    },
                    {
                        field: 'NAMA_BAHAN',
                        width: 350,
                        title: 'Nama Bahan Baku Obat',
                        textAlign: 'left',
                    }],
                });

                $('#kt_datatable_search_status, #kt_datatable_search_type',formAddBBO).selectpicker();

                datatableBBO.on(
                    'datatable-on-click-checkbox',
                    function(e) {
                        // datatable.checkbox() access to extension methods
                        var ids = datatableBBO.checkbox().getSelectedId();
                        var count = ids.length;

                        $('#kt_datatable_selected_records',formAddBBO).html(count);
                        if (count > 0) {
                            $('#kt_datatable_group_action_form',formAddBBO).collapse('show');
                        } else {
                            $('#kt_datatable_group_action_form',formAddBBO).collapse('hide');
                        }
                    });

                btncreate.on('click', this.submitcreate);
                formAddBBO.on('submit', this.processbbo);

                $('#kt_datatable_search_status',formAddBBO).on('change', function() {
                    var keyword = $(this).val();
                    var param = 'Status';
                    MasterBBO.search(keyword, param);
                });

                $('#kt_datatable_search_type',formAddBBO).on('change', function() {
                    var keyword = $(this).val();
                    var param = 'Type';
                    MasterBBO.search(keyword, param);
                });

                $("#button-add-elic",portletGrid).on('click',function(){
                    MasterBBO.openModal();
                })

            },
            search:function(keyword,param){
                datatableBBO.search(keyword.toLowerCase(), param);
            },
            closeModal:function(){
                formAddBBO.trigger('reset'); 
                modalAddBBO.modal('hide'); 
            },
            openModal:function(){
                modalAddBBO.modal('show'); 
                datatableBBO.reload();
            },
            resetForm:function(){
                formAddBBO.trigger('reset'); 
            },
            reloadGrid:function(){
                datatableBBO.reload();
                $('#kt_datatable_selected_records',formAddBBO).html(0);
            },
            submitcreate:function(){
                var ids = datatableBBO.checkbox().getSelectedId();
                var c = [];
                for (var i = 0; i < ids.length; i++) {
                    c.push(ids[i]);
                }
                console.log('c ',c)
                console.log('c ',c.length)
                if(c.length>0){
                    $('#obatid',formAddBBO).val(c.join("','"));
                    btnsubmit.trigger('click');
                }else{
                    Swal.fire('Warning !','Tidak ada data yang dipilih !','warning');
                }
            },
            processbbo:function(e){
                e.preventDefault();
                /*Ajax Request Header setup*/
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                var obatid = $("#obatid",formAddBBO).val();
                if(obatid==''){
                    Swal.fire('Warning !','Please Select Bahan Baku','warning');
                }else{
                    btnsubmit.html('Sending..');
                    btnsubmit.attr('disabled',true);

                    var zatActiveId = $("#obatid",formAddBBO).val();

                    /* Submit form data using ajax*/
                    $.ajax({
                        url:'masterBBO',
                        method: 'POST',
                        data: formAddBBO.serialize(),
                        success: function(response){
                            btnsubmit.html('Saved');
                            btnsubmit.attr('disabled',false);
                            var data = JSON.parse(response);
                            console.log('response ',JSON.parse(response))
                            console.log('response ',response.success)
                            console.log('data ',data.message)

                            if(data.success===true){
                                // Swal.fire('Success','Data has been saved !','success');
                                Swal.fire({
                                    title: 'Data has been saved !',
                                    showDenyButton: false,
                                    showCancelButton: false,
                                    confirmButtonText: `OK`,
                                    icon:'success'
                                }).then((result) => {
                                    /* Read more about isConfirmed, isDenied below */
                                    if (result.isConfirmed) {
                                        Grid.reloadGrid();
                                        MasterBBO.closeModal();
                                    } else if (result.isDenied) {
                                        Swal.fire('Changes are not saved', '', 'info')
                                    }
                                })
                                formAddBBO.trigger('reset'); 
                                modalAdd.modal('hide');
                            }else{
                                Swal.fire({
                                    icon: 'warning',
                                    text: data.message
                                })
                            }
                        },error:function(response){
                            console.log('response ',response)
                            if(response.status===419){
                                window.location.href = "{{ route('login')}}";   
                            }else{
                                Swal.fire('Warning ! ','An error occurred, please try again later','warning');
                            }
                            btnsubmit.html('Saved');
                            btnsubmit.attr('disabled',false);
                        }
                    });
                }
            }
        }
    }();

    $(function () {
        Grid.init();     
        AddBBO.init();   
        EditBBO.init();
        MasterBBO.init();
    });
</script>
@endsection