@extends('layout.admin.app')

@section('styles')

@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <!--begin::Card-->
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">Edit Footer</h3>
                </div>
                <form method="POST" id="form-editor" action="{{ url('admin/settahun', ['id' => $data->id]) }}" enctype="multipart/form-data">
                    @method('PUT')
                    @csrf
                    <div class="card-body">
                        <div class=row>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Tahun</label>
                                    <input type="text" class="form-control" required="" id="year_name" name="year_name" placeholder="" value="{{ $data->year_name }}"/>
                                </div>
                                <!-- <div class="form-group">
                                    <label>Alamat Perusahaan</label>
                                    <input type="text" class="form-control" required="" id="alamat_perusahaan" name="alamat_perusahaan" placeholder="" value="{{ $data->alamat_perusahaan }}" />
                                </div>
                                <div class="form-group">
                                    <label>Tempat</label>
                                    <input type="text" class="form-control" required="" id="tempat" name="tempat" placeholder="" value="{{ $data->tempat }}"/>
                                </div>
                                <div class="form-group">
                                    <label>Kota</label>
                                    <input type="text" class="form-control" required="" id="kota" name="kota" placeholder="" value="{{ $data->kota }}"/>
                                </div> -->
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Status</label>
                                    <input type="text" class="form-control" required="" id="year_active" name="year_active" placeholder="" value="{{ $data->year_active }}"/>
                                </div>
                                <!-- <div class="form-group">
                                    <label>Nama Call Center</label>
                                    <input type="text" class="form-control" required="" id="nama_callcenter" name="nama_callcenter" placeholder="" value="{{ $data->nama_callcenter }}"/>
                                </div>
                                <div class="form-group">
                                    <label>No Telp</label>
                                    <input type="text" class="form-control" required="" id="notelp" name="notelp" placeholder="" value="{{ $data->notelp }}"/>
                                </div>
                                <div class="form-group">
                                    <label for="image">Image</label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input @error('image') is-invalid @enderror" id="image" name="image" value="{{ old('image') }}" />
                                        <label class="custom-file-label" for="image">Choose file</label>
                                        <div>{{ $data->image }}</div>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary mr-2">Submit</button>
                        <a href="{{ url('admin/settahun') }}">
                            <button type="button" class="btn btn-secondary" aria-label="Close">Cancel</button>
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection