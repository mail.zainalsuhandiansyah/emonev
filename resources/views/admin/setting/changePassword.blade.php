@extends('layout.admin.app')

@section('styles')
@endsection

@section('content')

<!-- Portlet Change Password -->
<div class="card card-custom gutter-b" id="portlet_change">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">Change Password
                <span class="d-block text-muted pt-2 font-size-sm">My Profile</span>
            </h3>
        </div>
        <div class="card-toolbar">
        </div>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <form class="form" id="form_change_password" enctype="multipart/form-data" method="post" role="form">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="card-body">
                        <br>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">New Password</label>
                            <div class="col-6">
                                <input type="password" name="new_password" id="new_password" class="form-control" placeholder="Enter new password">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">Confirm New Password</label>
                            <div class="col-6">
                                <input type="password" name="confirm_new_password" id="confirm_new_password" class="form-control" placeholder="Enter confirm new password">
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="button" id="button-submit" class="btn btn-primary mr-2 btn-sm float-right"><i class="fa fa-save"></i>Submit </button>
                    </div>
                </form>
            </div>
        </div>
        <!--end::Search Form-->
    </div>
</div>
<!-- end Portlet Hilirisasi -->

@endsection
@section('scripts')

<script type="text/javascript">

    var portletChange = $("#portlet_change");
    var formChange = $("#form_change_password",portletChange);
    var btnSubmit = $("#button-submit",formChange);

    var Change = function() {
        var _change;

        var _handleChangePassword = function() {

            var validation;

            // Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
            validation = FormValidation.formValidation(
                KTUtil.getById('form_change_password'), {
                    fields: {
                        new_password: {
                            validators: {
                                notEmpty: {
                                    message: 'New Password is required'
                                }
                            }
                        },
                        confirm_new_password: {
                            validators: {
                                notEmpty: {
                                    message: 'Confirm New Password is required'
                                }
                            }
                        }
                    },
                    plugins: {
                        trigger: new FormValidation.plugins.Trigger(),
                        submitButton: new FormValidation.plugins.SubmitButton(),
                        bootstrap: new FormValidation.plugins.Bootstrap()
                    }
                }
            );

            btnSubmit.on('click', function(e) {
                e.preventDefault();

                validation.validate().then(function(status) {
                    if (status == 'Valid') {
                        btnSubmit.html('<i class="fas fa-spinner fa-pulse"></i>');
                        $("#form_change_password :input").attr("readonly", true);
                        $.ajax({
                                url: 'changePassword',
                                method: 'POST',
                                data: $("#form_change_password").serialize()
                            }).done(function(response) {
                                var resp = JSON.parse(response);
                                if (resp.success == true) {
                                    swal.fire({
                                    	text: "Change Password Successfully !",
                                    	icon: "success",
                                    	buttonsStyling: false,
                                    	confirmButtonText: "Ok, got it!",
                                    	customClass: {
                                    		confirmButton: "btn font-weight-bold btn-light-primary"
                                    	}
                                    }).then(function() {
                                        window.location.href = "{{ route('logout')}}";
                                        $("#form_change_password :input").attr("readonly", false);
                                        btnSubmit.html('<i class="fa fa-save"></i> Submit');
                                        $('#form_change_password')[0].reset();
                                    });
                                } else {
                                    swal.fire({
                                        text: 'Opps, something wrong please try again or contact administrator',
                                        icon: "error",
                                        buttonsStyling: false,
                                        confirmButtonText: "Ok, got it!",
                                        customClass: {
                                            confirmButton: "btn font-weight-bold btn-light-primary"
                                        }
                                    }).then(function() {
                                        KTUtil.scrollTop();
                                    });
                                    
                                    $('#responseDiv').removeClass('alert-success').addClass('alert-danger').show();
                                    $('#message').html("Opps, something wrong please try again or contact administrator");
                                    $("#form_change_password :input").attr("readonly", false);
                                    btnSubmit.html('<i class="fa fa-save"></i> Submit');
                                    $('#form_change_password')[0].reset();
                                }
                            })
                            .fail(function(response) {
                                $('#message').html("Opps, something wrong please try again or contact administrator");
                                $('#form_change_password')[0].reset();
                                $("#form_change_password :input").attr("readonly", false);
                                btnSubmit.html('<i class="fa fa-save"></i> Submit');
                            });
                    } else {
                        swal.fire({
                            text: "Sorry, looks like there are some errors detected, please try again.",
                            icon: "error",
                            buttonsStyling: false,
                            confirmButtonText: "Ok, got it!",
                            customClass: {
                                confirmButton: "btn font-weight-bold btn-light-primary"
                            }
                        }).then(function() {
                            KTUtil.scrollTop();
                        });
                    }
                });
            });
        }
        // Public Functions
        return {
            // public functions
            init: function() {
                _login = portletChange;

                _handleChangePassword();

                var newPassword = $("#new_password",formChange);
                var CnewPassword = $("#confirm_new_password",formChange);

                $("#new_password,#confirm_new_password",formChange).on('change',function() {
                    if((CnewPassword.val()!='' && newPassword.val()!='') && (CnewPassword.val()!=newPassword.val())){
                        Swal.fire('Warning !','Confirm Password tidak sesuai !','warning');
                        CnewPassword.val('');
                    }
                });

            }
        };
    }();

    jQuery(document).ready(function () {
        Change.init();
    });
</script>
<script>
    $(document).ajaxStart(function(){
        $.blockUI({ message: '<div style="padding:5px 0;">Please wait...</div>' ,css: { backgroundColor: '#fff', color: '#000', fontSize: '12px'} })
    }).ajaxStop($.unblockUI);
</script>
@endsection