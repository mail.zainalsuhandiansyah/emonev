@extends('layout.admin.app')

@section('styles')

@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <!--begin::Card-->
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">Edit Slider</h3>
                </div>
                <form method="POST" id="form-editor" action="{{ url('admin/setslider', ['id' => $data->id]) }}" enctype="multipart/form-data">
                    @method('PUT')
                    @csrf
                    <div class="card-body">
                        <div class=row>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Judul</label>
                                    <input type="text" class="form-control" required="" id="judul" name="judul" placeholder="" value="{{ $data->judul }}" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="image">Image</label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input @error('image') is-invalid @enderror" id="image" name="image" value="{{ old('image') }}" />
                                        <label class="custom-file-label" for="image">Choose file</label>
                                        <div>{{ $data->image }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary mr-2">Submit</button>
                        <a href="{{ url('admin/setfooter') }}">
                            <button type="button" class="btn btn-secondary" aria-label="Close">Cancel</button>
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection