@extends('layout.admin.app')

@section('styles')
<link href=" {{ asset('css/pages/wizard/wizard-4.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

<div class="card">
    {{-- <div class="card-header">
        <h3 class="card-title">
            Data Master

        </h3>
    </div> --}}
 <!--begin::Form-->

        <input type="hidden" name="reportingID" id="reportingID" value="{{ $reportingID }}">
        <div class="card-body">
            <div class="form-group row">
               <h5>Data Master  :</h5>&nbsp;<span class="label label-xl label-success label-pill label-inline mr-2"><h2>{{ $zatActiveName->zat_active_name }}</h2></span>

            </div>
        </div>

</div>
<!-- Data Detail Reporting -->
<br>
<div class="card">
    <div class="card-header">
        <h3 class="card-title">
            Data Detail
        </h3>
    </div>
 <!--begin::Form-->

        <div class="card-body">
            @include('admin.pelaporan.industri.detail.detail')
        </div>

</div>


@endsection

@section('scripts')
<script src="{{ asset('js/pages/custom/wizard/wizard-4.js') }}"></script>

@endsection
