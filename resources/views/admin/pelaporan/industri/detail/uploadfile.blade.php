<div class="modal fade" id="form-upload-{{ $itemRsd->reporting_subdetail_id }}" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
           <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            {{csrf_field()}}
            <div class="modal-content">
                <div class="modal-header">
                    Upload Files
                </div>

                <div class="modal-body">
                    <form method="post" action="{{ route('simpan.file') }}" enctype="multipart/form-data" class="form-upload">
                        {{csrf_field()}}
                          <div class="input-group hdtuto control-group lst increment" >
                            <input type="file" name="filenames[]" class="myfrm form-control">
                            <div class="input-group-btn">
                              <button class="btn btn-success" type="button"><i class="fldemo glyphicon glyphicon-plus"></i>Add</button>
                            </div>
                          </div>
                          <div class="clone" style="display: none">
                            <div class="hdtuto control-group lst input-group" style="margin-top:10px">
                                <input type="hidden" name="reporting_id" value="{{$itemRsd->reporting_id}}" />
                                <input type="hidden" name="reporting_detail_id" value="{{$itemRsd->reporting_detail_id}}" />
                                <input type="hidden" name="reporting_subdetail_id" value="{{$itemRsd->reporting_subdetail_id}}" />

                              <input type="file" name="filenames[]" class="myfrm form-control">
                              <div class="input-group-btn">
                                <button class="btn btn-danger" type="button" onclick="RemoveItem()"><i class="fldemo glyphicon glyphicon-remove"></i> Remove</button>
                              </div>
                            </div>
                          </div>


                          <button type="submit" class="btn btn-success" style="margin-top:10px">Submit</button>


                      </form>


                </div>
            </div>
           </div>
</div>
@section('scripts')

<script type="text/javascript">
    $(document).ready(function() {
      $(".btn-success").click(function(){
          var lsthmtl = $(".clone").html();
          $(".increment").after(lsthmtl);
      });

    });

    function RemoveItem(){
        alert('a');
      }
</script>
@endsection
