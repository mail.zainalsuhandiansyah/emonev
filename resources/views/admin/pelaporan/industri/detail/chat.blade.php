<form id="chatform[{{ $itemRsd->reporting_subdetail_id }}]" name="chatform">
    <input type="hidden" name="reporting_id" value="{{$itemRsd->reporting_id}}" />
    <input type="hidden" name="reporting_detail_id" value="{{$itemRsd->reporting_detail_id}}" />
    <input type="hidden" name="reporting_subdetail_id" value="{{$itemRsd->reporting_subdetail_id}}" />
    <input type="hidden" name="_token" id="_token[{{$itemRsd->reporting_subdetail_id}}]" value="{{ csrf_token() }}" />
<div class="card card-custom">
    <!--begin::Header-->

    <!--end::Header-->
    <!--begin::Body-->
    <div class="card-body">
        <!--begin::Scroll-->
        <div class="scroll scroll-pull" data-height="375" data-mobile-height="300">
            <!--begin::Messages-->
            <div class="messages">
                @foreach ($qna as $key_qna => $item_qna)
                @if ( $item_qna->reporting_detail_id ==  $itemRdetail->reporting_detail_id &&  $item_qna->reporting_subdetail_id ==  $itemRsd->reporting_subdetail_id)
                @if ($item_qna->qna_flag == 1)
                    <!--begin::Message In-->
                    <div class="d-flex flex-column mb-5 align-items-start">
                        <div class="d-flex align-items-center">
                            <span class="symbol symbol-35 symbol-light-success">
                                <span class="symbol-label font-size-h5 font-weight-bold">
                                    Q
                                </span>
                            </span>
                            <div>
                                <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">{{ $item_qna->fullname }}</a>
                                <span class="text-muted font-size-sm">{{ \Carbon\Carbon::parse($item_qna->created_at)->diffForHumans() }}</span>
                            </div>
                        </div>
                        <div class="mt-2 rounded p-5 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-400px">
                            {{ $item_qna->qna_description }}
                        </div>
                    </div>
                    <!--end::Message In-->
                    @else
                    <!--begin::Message Out-->
                    <div class="d-flex flex-column mb-5 align-items-end">
                        <div class="d-flex align-items-center">
                            <div>
                                <span class="text-muted font-size-sm">{{ \Carbon\Carbon::parse($item_qna->created_at)->diffForHumans() }}</span>
                                <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">{{ $item_qna->fullname }}</a>
                            </div>
                            <div class="symbol symbol-circle symbol-40 ml-3">
                                <span class="symbol-label font-size-h5 font-weight-bold">
                                    A
                                </span>
                            </div>
                        </div>
                        <div class="mt-2 rounded p-5 bg-light-primary text-dark-50 font-weight-bold font-size-lg text-right max-w-400px">
                            {{ $item_qna->qna_description }}
                        </div>
                    </div>
                    <!--end::Message Out-->
                @endif
                @endif
                @endforeach


            </div>
            <!--end::Messages-->
        </div>
        <!--end::Scroll-->
    </div>
    <!--end::Body-->
    <!--begin::Footer-->
    @if ( $itemRsd->reporting_detail_id ==  $itemRdetail->reporting_detail_id )
    <div class="card-footer align-items-center">
        <!--begin::Compose-->
        <textarea class="form-control border-0 p-0" rows="2" cols="5" name="isichat[{{ $itemRsd->reporting_subdetail_id }}]" id="isichat[{{ $itemRsd->reporting_subdetail_id }}]" placeholder="Type a message"></textarea>
        <div class="d-flex align-items-center justify-content-between mt-5">
            <div class="mr-3">
                <a href="javascript:;"  class="btn btn-success btn-md text-uppercase font-weight-bold chat-send py-2 px-6" data-toggle="modal" data-target="#form-upload-{{ $itemRsd->reporting_subdetail_id }}">Attach</a>
            </div>
            <div>
                <a href="javascript:;" class="btn btn-primary btn-md text-uppercase font-weight-bold chat-send py-2 px-6" id="{{ $itemRsd->reporting_subdetail_id }}" onclick="SendChat(this)">Send</a>

            </div>
        </div>
        <!--begin::Compose-->
    </div>
    @endif
    <!--end::Footer-->
</div>
</form>
