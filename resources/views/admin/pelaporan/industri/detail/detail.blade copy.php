<form action="{{ Route('simpan.jawaban') }}" method="post" enctype="multipart/form-data" id='form-jawaban' >
    @csrf
    <ul class="nav nav-light-primary nav-pills nav-fill" id="myTab3" role="tablist">
        {{-- Begin tab tahun --}}
        <?php $x=1; ?>
        @foreach ($target as $key => $item_target)
            <li class="nav-item">
                <a class="nav-link {{ ($x == 1 ? 'active' :'') }}" id="home-tab-3" data-toggle="tab" href="#tabYear-{{ $item_target->year_id }}">
                    <span class="nav-icon">
                        <i class="flaticon-calendar-2"></i>
                    </span>
                    <span class="nav-text font-size-h4 font-size-lg-h2">{{ $item_target->year_name }}</span>
                </a>
            </li>
        <?php $x++; ?>
        @endforeach
    </ul>
    <div class="tab-content mt-5" id="myTabContent3">
        <?php $x=1; ?>
        @foreach ($target as $key => $item_target)
            <div class="tab-pane fade {{ ($x == 1 ? 'show active' :'') }} " id="tabYear-{{ $item_target->year_id }}" role="tabpanel" aria-labelledby="home-tab-3">
                <ul class="nav nav-light-info nav-pills nav-fill" id="tabTw" role="tablist">
                        <?php $i=1; ?>
                    @foreach ($triwulan as $keyTw => $itemTw)
                        <li class="nav-item">
                            <a class="nav-link {{ ($i == 1 ? 'active' :'') }}" id="tabTw_{{ $item_target->year_id }}_{{ $itemTw->triwulan_id }}" data-toggle="tab" href="#tabTw-{{ $item_target->year_id }}-{{ $itemTw->triwulan_id }}">
                                <span class="nav-icon">
                                    <i class="flaticon-calendar-3"></i>
                                </span>
                                <span class="nav-text font-size-h4 font-size-lg-h2">{{ $itemTw->triwulan_description }}</span>
                            </a>
                        </li>
                        <?php $i++; ?>
                    @endforeach
                </ul>
                <div class="tab-content mt-5" id="myContentTw">
                    <?php $i=1; ?>
                    @foreach ($triwulan as $keyTw => $itemTw)
                        <div class="tab-pane fade {{ ($i == 1 ? 'show active' :'') }}" id="tabTw-{{ $item_target->year_id }}-{{ $itemTw->triwulan_id }}" role="tabpanel" aria-labelledby="home-tab-3">
                            @foreach ($reportingDetail as $keyRdetail => $itemRdetail)
                                    @if ($itemRdetail->year_id == $item_target->year_id && $itemRdetail->triwulan_id == $itemTw->triwulan_id )
                                        <div class="form-group">
                                            <span class="label label-xl label-warning label-pill label-inline mr-2">Tahapan :</span>
                                            <span class="label label-xl label-dark label-pill label-inline mr-2">{{ $itemRdetail->roadmap_title }}</span>

                                        </div>
                                        <div class="form-group">
                                            <table class="table table-bordered table-checkable" id="kt_datatable">
                                                <thead>
                                                    <tr>
                                                        <th width="2%">No.</th>
                                                        <th colspan=2 width="90%">Uraian</th>
                                                        <th width="8%">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="table_{{ $itemRdetail->reporting_detail_id }}_{{$itemRdetail->year_id}}_{{ $itemRdetail->triwulan_id }}">
                                                    <?php $xx=1; ?>
                                                    @foreach ($reportingSubDetail as $keyRSd => $itemRsd)
                                                        @if ( $itemRsd->reporting_detail_id ==  $itemRdetail->reporting_detail_id )

                                                            <tr>
                                                                <td>{{ $xx }}</td>
                                                                <td colspan="2">{{ $itemRsd->roadmap_description }}</td>
                                                                <td>
                                                                    {{-- <a href="javascript:void(0)" class="btn btn-sm font-weight-bold btn-warning btnAdd" id='{{$itemRsd->reporting_subdetail_id}}_{{ $itemRdetail->reporting_detail_id }}_{{$itemRdetail->year_id}}_{{ $itemRdetail->triwulan_id }}' name='{{ $itemRdetail->reporting_detail_id }}_{{$itemRdetail->year_id}}_{{ $itemRdetail->triwulan_id }}' onclick="handleClick(this);">
                                                                        <i class="la la-plus"></i>
                                                                            Add
                                                                    </a> --}}
                                                                </td>

                                                            </tr>

                                                        @endif
                                                        <?php $xx++;$y=1; ?>
                                                        @foreach ($qna as $key_qna => $item_qna)
                                                            @if ( $item_qna->reporting_detail_id ==  $itemRdetail->reporting_detail_id &&  $item_qna->reporting_subdetail_id ==  $itemRsd->reporting_subdetail_id)

                                                                <tr  class='qna_{{ $item_qna->reporting_qna_id }}'>
                                                                    <td></td>
                                                                    <td width="5%">
                                                                        @if ( $item_qna->qna_flag == 1)
                                                                        <span class="label label-xl label-primary label-pill label-inline mr-2">Pertanyaan:</span>
                                                                        @else
                                                                        <span class="label label-xl label-primary label-pill label-inline mr-2">Jawaban:</span>
                                                                        @endif

                                                                    </td>
                                                                    <td width="75%">{{ $item_qna->qna_description }}</td>
                                                                    <td width="20%">
                                                                        @if($item_qna->qna == 'Pertanyaan')
                                                                        <a href="javascript:;" class="btn btn-light-success" id="qna_{{ $item_qna->reporting_qna_id }}" name="qna_{{ $item_qna->reporting_qna_id }}" onclick="addAnswer(this)">
                                                                            <i class="flaticon2-chat-1"></i> Jawab
                                                                        </a>
                                                                        <a href="javascript:;" class="btn btn-light-success" id="qna_{{ $item_qna->reporting_qna_id }}" name="qna_{{ $item_qna->reporting_qna_id }}" onclick="addFile(this)">
                                                                            <i class="flaticon2-chat-1"></i> File
                                                                        </a>
                                                                        @else

                                                                        @endif
                                                                    </td>
                                                                </tr>

                                                            @endif
                                                        @endforeach
                                                    @endforeach

                                                </tbody>
                                            </table>
                                        </div>
                                    @endif

                                @endforeach
                        </div>
                        <?php $i++; ?>
                    @endforeach
                </div>
            </div>
        <?php $x++; ?>
        @endforeach

        {{-- Button save & back --}}
    <div class="float-right">
        <a href="{{ route('listReport') }}" class="btn btn-danger">
            <i class='flaticon2-back'> </i> Back
        </a>
        <button class='btn btn-success' type='submit' value='submit'>
            <i class='flaticon2-check-mark'> </i> Save
        </button>
    </div>
    {{-- End Button Save & Back --}}

    </div>
</form>
@section('scripts')
<script type="text/javascript">
    function addAnswer(obj) {
        console.log(obj.name);

        var newrow = "<tr><td></td>";
        newrow += "<td><span class='label label-xl label-primary label-pill label-inline mr-2'>Jawaban:</span></td>";
        newrow += "<td><input type='hidden' name='input_id["+obj.id+"]'  value='"+obj.id+"'/><textarea class='form-control form-control-solid' name='jawaban["+obj.id+"]' rows='3'></textarea></td>";
        newrow += "<td></tr>";

        $(newrow).insertAfter($('table tr.'+obj.name+':last'));
    }

    function addFile(obj) {
        console.log(obj.name);

        var newrow = "<tr><td></td>";
        newrow += "<td><span class='label label-xl label-primary label-pill label-inline mr-2'>Jawaban:</span></td>";
        newrow += "<td><div class='form-group'><label>File Browser</label><div></div><div class='custom-file'><input type='file' class='custom-file-input' id='customFile'/><label class='custom-file-label' for='customFile'>Choose file</label></div></div></td>";
        newrow += "<td></tr>";

        $(newrow).insertAfter($('table tr.'+obj.name+':last'));
    }

</script>
@endsection
