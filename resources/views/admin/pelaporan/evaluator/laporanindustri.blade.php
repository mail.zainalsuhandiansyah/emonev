@extends('layout.admin.app')

@section('styles')

@endsection

@section('content')

<div class="card card-custom gutter-b">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">Cetak Pencapaian Per Industri
                <span class="d-block text-muted pt-2 font-size-sm">Bahan Baku Aktif</span>
            </h3>
        </div>
        <div class="card-toolbar">
            <!--begin::Dropdown-->
            <div class="dropdown dropdown-inline mr-2">
                <button type="button" class="btn btn-light-primary font-weight-bolder dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="svg-icon svg-icon-md">
                        <!--begin::Svg Icon | path:assets/media/svg/icons/Design/PenAndRuller.svg-->
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24" />
                                <path d="M3,16 L5,16 C5.55228475,16 6,15.5522847 6,15 C6,14.4477153 5.55228475,14 5,14 L3,14 L3,12 L5,12 C5.55228475,12 6,11.5522847 6,11 C6,10.4477153 5.55228475,10 5,10 L3,10 L3,8 L5,8 C5.55228475,8 6,7.55228475 6,7 C6,6.44771525 5.55228475,6 5,6 L3,6 L3,4 C3,3.44771525 3.44771525,3 4,3 L10,3 C10.5522847,3 11,3.44771525 11,4 L11,19 C11,19.5522847 10.5522847,20 10,20 L4,20 C3.44771525,20 3,19.5522847 3,19 L3,16 Z" fill="#000000" opacity="0.3" />
                                <path d="M16,3 L19,3 C20.1045695,3 21,3.8954305 21,5 L21,15.2485298 C21,15.7329761 20.8241635,16.200956 20.5051534,16.565539 L17.8762883,19.5699562 C17.6944473,19.7777745 17.378566,19.7988332 17.1707477,19.6169922 C17.1540423,19.602375 17.1383289,19.5866616 17.1237117,19.5699562 L14.4948466,16.565539 C14.1758365,16.200956 14,15.7329761 14,15.2485298 L14,5 C14,3.8954305 14.8954305,3 16,3 Z" fill="#000000" />
                            </g>
                        </svg>
                        <!--end::Svg Icon-->
                    </span>Export</button>
                <!--begin::Dropdown Menu-->
                <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                    <!--begin::Navigation-->
                    <ul class="navi flex-column navi-hover py-2">
                        <li class="navi-header font-weight-bolder text-uppercase font-size-sm text-primary pb-2">Choose an option:</li>
                        <li class="navi-item">
                            <a href="#" class="navi-link">
                                <span class="navi-icon">
                                    <i class="la la-print"></i>
                                </span>
                                <span class="navi-text">Print</span>
                            </a>
                        </li>
                        <li class="navi-item">
                            <a href="#" class="navi-link">
                                <span class="navi-icon">
                                    <i class="la la-copy"></i>
                                </span>
                                <span class="navi-text">Copy</span>
                            </a>
                        </li>
                        <li class="navi-item">
                            <a href="#" class="navi-link">
                                <span class="navi-icon">
                                    <i class="la la-file-excel-o"></i>
                                </span>
                                <span class="navi-text">Excel</span>
                            </a>
                        </li>
                        <li class="navi-item">
                            <a href="#" class="navi-link">
                                <span class="navi-icon">
                                    <i class="la la-file-text-o"></i>
                                </span>
                                <span class="navi-text">CSV</span>
                            </a>
                        </li>
                        <li class="navi-item">
                            <a href="#" class="navi-link">
                                <span class="navi-icon">
                                    <i class="la la-file-pdf-o"></i>
                                </span>
                                <span class="navi-text">PDF</span>
                            </a>
                        </li>
                    </ul>
                    <!--end::Navigation-->
                </div>
                <!--end::Dropdown Menu-->
            </div>
            <!--end::Dropdown-->
            <!--begin::Button-->
            <!-- <a href="#" class="btn btn-success font-weight-bolder" data-toggle="modal" data-target="#modalTambahData">
                <i class="fa fa-plus"></i> Tambah Data
            </a>
            &nbsp;&nbsp;
            <a href="javascript:;" class="btn btn-primary font-weight-bolder" data-toggle="modal" data-target="#modalTambahBahanBaku">
                <i class="fa fa-plus"></i>
                Tambah Bahan Baku
            </a> -->
            <!--end::Button-->
        </div>
    </div>
    <div class="card-body">
        <!--begin: Search Form-->
        <!--begin::Search Form-->
        <div class="mb-7">
            <div class="row align-items-center">
                <div class="col-lg-9 col-xl-8">
                    <div class="row align-items-center">
                        <div class="col-md-4 my-2 my-md-0">
                            <div class="input-icon">
                                <input type="text" class="form-control" placeholder="Search..." id="kt_datatable_search_query" />
                                <span>
                                    <i class="flaticon2-search-1 text-muted"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-4 my-2 my-md-0">
                            <div class="d-flex align-items-center">
                                <label class="mr-3 mb-0 d-none d-md-block">Status:</label>
                                <select class="form-control" id="kt_datatable_search_status">
                                    <option value="">All</option>
                                    <option value="1">Pending</option>
                                    <option value="2">Delivered</option>
                                    <option value="3">Canceled</option>
                                    <option value="4">Success</option>
                                    <option value="5">Info</option>
                                    <option value="6">Danger</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4 my-2 my-md-0">
                            <div class="d-flex align-items-center">
                                <label class="mr-3 mb-0 d-none d-md-block">Type:</label>
                                <select class="form-control" id="kt_datatable_search_type">
                                    <option value="">All</option>
                                    <option value="1">Online</option>
                                    <option value="2">Retail</option>
                                    <option value="3">Direct</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
                    <a href="#" class="btn btn-light-primary px-6 font-weight-bold">Search</a>
                </div>
            </div>
        </div>
        <!--end::Search Form-->
        <!--end: Search Form-->
        <!--begin: Datatable-->
        <div class="datatable datatable-bordered datatable-head-custom" id="kt_datatable"></div>
        <!--end: Datatable-->
    </div>
</div>


<!-- Modal-->
<div class="modal fade" id="modalTambahBahanBaku" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Zat Aktif</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
            <form class="form">
                <div class="card-body">
                    <div class="form-group">
                        <label>Nama:</label>
                        <input type="text" class="form-control" placeholder="Enter name"/>

                    </div>
                    <div class="form-group">
                        <label>Jenis Bahan Baku</label>
                        <select class="form-control">
                            <option value="">- Silahkan Pilih -</option>
                            <option value="">Bioteknologi</option>
                            <option value="">Natural</option>
                            <option value="">Obat Kimia</option>
                            <option value="">Vaksin</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Fornas</label>
                        <select class="form-control">
                            <option value="">- Silahkan Pilih -</option>
                            <option value="">Non-fornas</option>
                            <option value="">Fornas</option>
                        </select>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="reset" class="btn btn-primary mr-2">Simpan</button>
                    <button type="reset" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Cancel</button>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalTambahData" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Data Bahan Baku</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
            <form class="form">
                <div class="card-body">
                    <div class="form-group">
                        <label>Zat Aktif:</label>
                        <select class="form-control">
                            <option value="">- Silahkan Pilih -</option>
                            <option value="">Enoksaparin Sodium</option>
                            <option value="">Trastuzumab</option>
                            <option value="">Bevacizumab Drug Substance (DS)</option>
                            <option value="">Vaksin HB (Hepatitis-B)</option>
                            <option value="">Vaksin Rotavirus</option>
                            <option value="">IPV</option>
                        </select>

                    </div>
                </div>
                <div class="card-footer">
                    <button type="reset" class="btn btn-primary mr-2">Simpan</button>
                    <button type="reset" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Cancel</button>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalDetailData" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detail Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <form class="form">
                    <div class="card-body">
                        <ul>
                            <li>Enok Saparin Sodium</li>
                            <li>Bentuk Kimia</li>
                        </ul>
                    </div>
                    <div class="card-footer">
                        <button type="reset" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<!-- <script src="{{ asset('js/pages/crud/ktdatatable/base/data-bahan-baku.js') }}"></script> -->
<script>
    $(function () {
        var dataJSONArray = JSON.parse('[{"RecordID":1,"NamaPerusahaan":"DAEWOONG INFION","Actions":null},\n' +
        '{"RecordID":2 ,"NamaPerusahaan":"MEIJI INDONESIAN PHARMACEUTICAL INDUSTRIES ","Actions":null},\n' +
        '{"RecordID":3 ,"NamaPerusahaan":"RIASIMA ABADI FARMA ","Actions":null},\n' +
        '{"RecordID":4 ,"NamaPerusahaan":"KALBIO GLOBAL MEDIKA ","Actions":null},\n' +
        '{"RecordID":5 ,"NamaPerusahaan":"MERSIFARMA TIRMAKU MERCUSANA ","Actions":null},\n' +
        '{"RecordID":6 ,"NamaPerusahaan":"DEXA MEDICA ","Actions":null},\n' +
        '{"RecordID":7 ,"NamaPerusahaan":"MONIX INDONESIA ","Actions":null}, \n' +
        '{"RecordID":8 ,"NamaPerusahaan":"PERUSAHAAN PERSEROAN (PERSERO) PT. INDONESIA FARMA TBK DISINGKAT PT. INDOFARMA (PERSERO) TBK","Actions":null},\n' +
        '{"RecordID":9 ,"NamaPerusahaan":"BIO FARMA (PERSERO)","Actions":null},\n'+
        '{"RecordID":10,"NamaPerusahaan":"PHAPROS TBK","Actions":null},\n'+
        '{"RecordID":11,"NamaPerusahaan":"KIMIA FARMA SUNGWUN PHARMACOPIA","Actions":null},\n'+
        '{"RecordID":12,"NamaPerusahaan":"METISKA FARMA","Actions":null},\n'+
        '{"RecordID":13,"NamaPerusahaan":"ETANA BIOTECHNOLOGIES INDONESIA","Actions":null},\n'	+
        '{"RecordID":14,"NamaPerusahaan":"KARYA DAYA SYAFARMASI","Actions":null},\n'+
        '{"RecordID":15,"NamaPerusahaan":"INDESSO AROMA","Actions":null},\n'+
        '{"RecordID":16,"NamaPerusahaan":"KALBE FARMA TBK.","Actions":null},\n'+
        '{"RecordID":17,"NamaPerusahaan":"BERKAH ALAM NUSANTARA","Actions":null}, \n' +
        '{"RecordID":18,"NamaPerusahaan":"NATRINDO SURYA PRIMA","Actions":null},\n'+
        '{"RecordID":19,"NamaPerusahaan":"NATURA AROMATIK NUSANTARA","Actions":null},\n'+
        '{"RecordID":20,"NamaPerusahaan":"NATURA LABORATORIA PRIMA","Actions":null},\n'+
        '{"RecordID":21,"NamaPerusahaan":"SARI ALAM SUKABUMI","Actions":null},\n'+
        '{"RecordID":22,"NamaPerusahaan":"PHYTOCHEMINDO REKSA","Actions":null},\n'+
        '{"RecordID":23,"NamaPerusahaan":"SINKONA INDONESIA LESTARI","Actions":null},\n'+
        '{"RecordID":24,"NamaPerusahaan":"KONIMEX","Actions":null},\n'+
        '{"RecordID":25,"NamaPerusahaan":"DAEWOONG INFION","Actions":null},\n' +
        '{"RecordID":26,"NamaPerusahaan":"MEIJI INDONESIAN PHARMACEUTICAL INDUSTRIES ","Actions":null},\n' +
        '{"RecordID":27,"NamaPerusahaan":"RIASIMA ABADI FARMA ","Actions":null},\n' +
        '{"RecordID":28,"NamaPerusahaan":"KALBIO GLOBAL MEDIKA ","Actions":null},\n' +
        '{"RecordID":29,"NamaPerusahaan":"MERSIFARMA TIRMAKU MERCUSANA ","Actions":null},\n' +
        '{"RecordID":30,"NamaPerusahaan":"DEXA MEDICA ","Actions":null},\n' +
        '{"RecordID":31,"NamaPerusahaan":"MONIX INDONESIA ","Actions":null}, \n' +
        '{"RecordID":32,"NamaPerusahaan":"PERUSAHAAN PERSEROAN (PERSERO) PT. INDONESIA FARMA TBK DISINGKAT PT. INDOFARMA (PERSERO) TBK","Actions":null},\n' +
        '{"RecordID":33,"NamaPerusahaan":"BIO FARMA (PERSERO)","Actions":null},\n'+
        '{"RecordID":34,"NamaPerusahaan":"PHAPROS TBK","Actions":null},\n'+
        '{"RecordID":35,"NamaPerusahaan":"KIMIA FARMA SUNGWUN PHARMACOPIA","Actions":null},\n'+
        '{"RecordID":36,"NamaPerusahaan":"METISKA FARMA","Actions":null},\n'+
        '{"RecordID":37,"NamaPerusahaan":"ETANA BIOTECHNOLOGIES INDONESIA","Actions":null},\n'	+
        '{"RecordID":38,"NamaPerusahaan":"KARYA DAYA SYAFARMASI","Actions":null},\n'+
        '{"RecordID":39,"NamaPerusahaan":"INDESSO AROMA","Actions":null},\n'+
        '{"RecordID":40,"NamaPerusahaan":"KALBE FARMA TBK.","Actions":null},\n'+
        '{"RecordID":41,"NamaPerusahaan":"BERKAH ALAM NUSANTARA","Actions":null}, \n' +
        '{"RecordID":42,"NamaPerusahaan":"NATRINDO SURYA PRIMA","Actions":null},\n'+
        '{"RecordID":43,"NamaPerusahaan":"NATURA AROMATIK NUSANTARA","Actions":null},\n'+
        '{"RecordID":44,"NamaPerusahaan":"NATURA LABORATORIA PRIMA","Actions":null},\n'+
        '{"RecordID":45,"NamaPerusahaan":"SARI ALAM SUKABUMI","Actions":null},\n'+
        '{"RecordID":46,"NamaPerusahaan":"PHYTOCHEMINDO REKSA","Actions":null},\n'+
        '{"RecordID":47,"NamaPerusahaan":"SINKONA INDONESIA LESTARI","Actions":null},\n'+
        '{"RecordID":48,"NamaPerusahaan":"KONIMEX","Actions":null}\n  ]');



        var datatable = $('#kt_datatable').KTDatatable({
            // datasource definition
            data: {
                type: 'local',
                source: dataJSONArray,
                pageSize: 10,
            },

            // layout definition
            layout: {
                scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
                // height: 450, // datatable's body's fixed height
                footer: false, // display/hide footer
            },

            // column sorting
            sortable: true,

            pagination: true,

            search: {
                input: $('#kt_datatable_search_query'),
                key: 'generalSearch',
            },

            // columns definition
            columns: [{
                field: 'RecordID',
                title: '#',
                sortable: false,
                width: 20,
                type: 'number',
                selector: true,
                textAlign: 'center',
            }, {
                field: 'NamaPerusahaan',
                title: 'Nama Perusahaan',
            }, {
                field: 'Actions',
                title: 'Actions',
                sortable: false,
                width: 125,
                overflow: 'visible',
                autoHide: false,
                template: function(response) {
                    return `<a href="/pelaporan-industri-cetak/`+response.userID+`" class="btn btn-sm btn-clean btn-icon" title="Detail Data" data-toggle="modal" data-target="#modalDetailData">
	                            <i class="flaticon2-printer"></i>
							</a>
						`;
                },
            }],
        });

        $('#kt_datatable_search_status').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Status');
        });

        $('#kt_datatable_search_type').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Type');
        });

        $('#kt_datatable_search_status, #kt_datatable_search_type').selectpicker();
  });
</script>
@endsection
