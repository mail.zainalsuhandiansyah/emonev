<ul class="nav nav-light-primary nav-pills nav-fill" id="myTab3" role="tablist">
    {{-- Begin tab tahun --}}
    <?php $x=1; ?>
    @foreach ($target as $key => $item_target)
        <li class="nav-item">
            <a class="nav-link {{ ($x == 1 ? 'active' :'') }}" id="home-tab-3" data-toggle="tab" href="#tabYear-{{ $item_target->year_id }}">
                <span class="nav-icon">
                    <i class="flaticon-calendar-2"></i>
                </span>
                <span class="nav-text font-size-h4 font-size-lg-h2">{{ $item_target->year_name }}</span>
            </a>
        </li>
    <?php $x++; ?>
    @endforeach
</ul>
<div class="tab-content mt-5" id="myTabContent3">
    <?php $x=1; ?>
    @foreach ($target as $key => $item_target)
        <div class="tab-pane fade {{ ($x == 1 ? 'show active' :'') }} " id="tabYear-{{ $item_target->year_id }}" role="tabpanel" aria-labelledby="home-tab-3">
            <ul class="nav nav-light-info nav-pills nav-fill" id="tabTw" role="tablist">
                    <?php $i=1; ?>
                @foreach ($triwulan as $keyTw => $itemTw)
                    <li class="nav-item">
                        <a class="nav-link {{ ($i == 1 ? 'active' :'') }}" id="tabTw_{{ $item_target->year_id }}_{{ $itemTw->triwulan_id }}" data-toggle="tab" href="#tabTw-{{ $item_target->year_id }}-{{ $itemTw->triwulan_id }}">
                            <span class="nav-icon">
                                <i class="flaticon-calendar-3"></i>
                            </span>
                            <span class="nav-text font-size-h4 font-size-lg-h2">{{ $itemTw->triwulan_description }}</span>
                        </a>
                    </li>
                    <?php $i++; ?>
                @endforeach
            </ul>
            <div class="tab-content mt-5" id="myContentTw">
                <?php $i=1; ?>
                @foreach ($triwulan as $keyTw => $itemTw)
                    <div class="tab-pane fade {{ ($i == 1 ? 'show active' :'') }}" id="tabTw-{{ $item_target->year_id }}-{{ $itemTw->triwulan_id }}" role="tabpanel" aria-labelledby="home-tab-3">
                        @foreach ($reportingDetail as $keyRdetail => $itemRdetail)
                                @if ($itemRdetail->year_id == $item_target->year_id && $itemRdetail->triwulan_id == $itemTw->triwulan_id )
                                    <div class="form-group">
                                        <span class="label label-xl label-warning label-pill label-inline mr-2">Tahapan :</span>
                                        <span class="label label-xl label-dark label-pill label-inline mr-2">{{ $itemRdetail->roadmap_title }}</span>

                                    </div>
                                    <div class="form-group">
                                        <table class="table table-bordered table-checkable" id="kt_datatable">
                                            <thead>
                                                <tr>
                                                    <th width="2%">No.</th>
                                                    <th width="75%">Uraian</th>
                                                    <th width="5%">Status</th>
                                                    <th width="15%">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="table_{{ $itemRdetail->reporting_detail_id }}_{{$itemRdetail->year_id}}_{{ $itemRdetail->triwulan_id }}">
                                                <?php $xx=1; ?>
                                                @foreach ($reportingSubDetail as $keyRSd => $itemRsd)
                                                    @if ( $itemRsd->reporting_detail_id ==  $itemRdetail->reporting_detail_id )

                                                        <tr>
                                                            <td>{{ $xx }}</td>
                                                            <td >{{ $itemRsd->roadmap_description }}</td>
                                                            <td >
                                                                @if ($itemRsd->status_id  == 1)
                                                                <span class="label label-xl label-warning label-pill label-inline mr-2">{{ $itemRsd->status_description }}</span>
                                                                @elseif( $itemRsd->status_id == 2)
                                                                <span class="label label-xl label-info label-pill label-inline mr-2">{{ $itemRsd->status_description }}</span>
                                                                @else
                                                                <span class="label label-xl label-success label-pill label-inline mr-2">{{ $itemRsd->status_description }}</span>
                                                                @endif


                                                                </td>
                                                            <td>
                                                                @if ($itemRsd->status_id  != 3)
                                                                <button type="button" class="btn btn-light-success" data-toggle="modal" data-target="#form_{{ $itemRsd->reporting_subdetail_id }}">
                                                                    <i class="flaticon2-chat-1"></i> Approval
                                                                </button>
                                                                @endif
                                                            </td>

                                                        </tr>
                                                        <tr>
                                                            <td colspan="4">
                                                                {{-- this according for chat --}}
                                                                @include('admin.pelaporan.evaluator.approve')
                                                                <div class="accordion accordion-toggle-arrow" id="accordion_{{ $itemRsd->reporting_subdetail_id }}">
                                                                    <div class="card">
                                                                        <div class="card-header">
                                                                            <div class="card-title collapsed" data-toggle="collapse" data-target="#collapse_{{ $itemRsd->reporting_subdetail_id }}">Laporan Perkembangan</div>
                                                                        </div>
                                                                        <div id="collapse_{{ $itemRsd->reporting_subdetail_id }}" class="collapse" data-parent="#accordion_{{ $itemRsd->reporting_subdetail_id }}">
                                                                            <div class="card-body">
                                                                                @include('admin.pelaporan.evaluator.chat')

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                {{-- End of Accordion for chat --}}
                                                            </td>
                                                        </tr>

                                                    @endif

                                                @endforeach

                                            </tbody>
                                        </table>
                                    </div>
                                @endif

                            @endforeach
                    </div>
                    <?php $i++; ?>
                @endforeach
            </div>
        </div>
    <?php $x++; ?>
    @endforeach
</div>

@section('scripts')
<script type="text/javascript">
   function SendChat(obj){
            console.log(obj.id);
            let _token   = $('meta[name="csrf-token"]').attr('content');
            // let isichat  = document.getElementById('isichat[800]');
            let isichat  = document.getElementById("isichat["+obj.id+"]").value;
            let reporting_id = $('input[name="reporting_id['+obj.id+']"]').val();
            let reporting_detail_id = $('input[name="reporting_detail_id['+obj.id+']"]').val();
            let reporting_subdetail_id = $('input[name="reporting_subdetail_id['+obj.id+']"]').val();

            console.log(reporting_id);console.log(reporting_detail_id);console.log(reporting_subdetail_id);

            $.ajax({
                url: "/admin/progress/getforum/" + reporting_id,
                type:"POST",
                data:{
                    _token: _token,
                    chat:isichat,
                    reporting_id:reporting_id,
                    reporting_detail_id:reporting_detail_id,
                    reporting_subdetail_id:reporting_subdetail_id,
                },
                success:function(response){
                    console.log(response);
                    if(response) {
                        // $('.success').text(response.success);
                        // $("#chatform["+obj.id+"]")[0].reset();
                        location.reload();
                    }
                },error:function(response){
                        console.log('response ',response)
                        if(response.status===419){
                            // Swal.fire('Warning ! ','Login Expired ! Please Relogin ','warning');
                            window.location.href = "{{ route('login')}}";
                        }else{
                            Swal.fire('Warning ! ','An error occurred, please try again later','warning');
                        }
                    }
            });

        }

</script>
@endsection
