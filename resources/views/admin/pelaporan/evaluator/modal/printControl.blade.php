
<div class="modal fade" id="modalPrintControl{{ $item->userID }}" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true" >
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Cetak Laporan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" id="form-print-{{ $item->userID }}" action="/admin/pelaporan-industri-cetak" enctype="multipart/form-data" target="_blank">
                    @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label>Pilih Tahun</label>
                        <input type="hidden" name="userID"  value="{{$item->userID}}">
                        <select class="form-control" id="year_id" name="year_id" required>
                            <option value="">- Silahkan Pilih -</option>
                            @foreach ($tahun as $a => $years)
                            <option value="{{ $years->year_id }}">{{ $years->year_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="reset" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Batal</button>
                    <button type="submit" class="btn btn-primary mr-2">Cetak</button>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
