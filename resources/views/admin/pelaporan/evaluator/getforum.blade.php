@extends('layout.admin.app')

@section('styles')
<link href=" {{ asset('css/pages/wizard/wizard-4.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

<div class="card">
    <div class="card-header">
        <h3 class="card-title">
            Data Master
            <input type="hidden" name="reportingID" id="reportingID" value="{{ $reportingID }}">
        </h3>
    </div>
 <!--begin::Form-->
    <form>
        <div class="card-body">
            <div class="form-group row">
                <table class="table">
                    <tbody>
                        <tr>
                            <td>Nama Perusahaan</td>
                            <td>:</td>
                            <td>{{ $namaPerusahaan }}</td>

                        </tr>
                        <tr>
                            <td>Nama Zat Aktif</td>
                            <td>:</td>
                            <td>{{ $zatActiveName }}</td>

                        </tr>
                        <tr>
                            <td>Tanggal Pelaporan</td>
                            <td>:</td>
                            <td>{{ $ReportingDate }}</td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </form>
</div>
<!-- Data Detail Reporting -->
<br>
<div class="card">
    <div class="card-header">
        <h3 class="card-title">
            Data Detail
        </h3>
    </div>
 <!--begin::Form-->

        <div class="card-body">
            @include('admin.pelaporan.evaluator.laporandetail')
        </div>

</div>


@endsection

@section('scripts')
<script src="{{ asset('js/pages/custom/wizard/wizard-4.js') }}"></script>

@endsection
