<div class="modal fade" id="form_{{ $itemRsd->reporting_subdetail_id }}" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <form method="POST" id="form-approval-{{ $itemRsd->reporting_subdetail_id }}" action="{{ url('/admin/pelaporan/approval') }}" enctype="multipart/form-data">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        @csrf
            <input type="hidden" name="reporting_id" value="{{$itemRsd->reporting_id}}" />
            <input type="hidden" name="reporting_detail_id" value="{{$itemRsd->reporting_detail_id}}" />
            <input type="hidden" name="reporting_subdetail_id" value="{{$itemRsd->reporting_subdetail_id}}" />
            <input type="hidden" name="_token2" id="_token2_[{{$itemRsd->reporting_subdetail_id}}]" value="{{ csrf_token() }}" />
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Form Approval</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">

                    <div class="card-body">
                     <div class="form-group mb-1">
                      <label for="exampleTextarea">Catatan :<span class="text-danger">*</span></label>

                      <textarea class="form-control" id="catatan-{{ $itemRsd->reporting_subdetail_id }}" name="catatan" rows="3" required></textarea>
                     </div>
                    </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
        </div>
        </form>
    </div>
</div>
