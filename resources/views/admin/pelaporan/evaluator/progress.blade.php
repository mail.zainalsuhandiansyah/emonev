@extends('layout.admin.app')

@section('styles')

@endsection

@section('content')

<div class="card card-custom gutter-b">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">Kemajuan Pelaporan
                <span class="d-block text-muted pt-2 font-size-sm"></span>
            </h3>
        </div>
        <div class="card-toolbar">
            <!--begin::Dropdown-->

        </div>
    </div>
    <div class="card-body">
        <!--begin: Search Form-->
        <!--begin::Search Form-->
        <div class="mb-7">
            <div class="row align-items-center">
                <div class="col-lg-9 col-xl-8">
                    <div class="row align-items-center">
                        <div class="col-md-4 my-2 my-md-0">
                            <div class="input-icon">
                                <input type="text" class="form-control" placeholder="Search..." id="kt_datatable_search_query" />
                                <span>
                                    <i class="flaticon2-search-1 text-muted"></i>
                                </span>
                            </div>
                        </div>
                        {{-- <div class="col-md-4 my-2 my-md-0">
                            <div class="d-flex align-items-center">
                                <label class="mr-3 mb-0 d-none d-md-block">Status:</label>
                                <select class="form-control" id="kt_datatable_search_status">
                                    <option value="">All</option>
                                    <option value="1">Pending</option>
                                    <option value="2">Delivered</option>
                                    <option value="3">Canceled</option>
                                    <option value="4">Success</option>
                                    <option value="5">Info</option>
                                    <option value="6">Danger</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4 my-2 my-md-0">
                            <div class="d-flex align-items-center">
                                <label class="mr-3 mb-0 d-none d-md-block">Type:</label>
                                <select class="form-control" id="kt_datatable_search_type">
                                    <option value="">All</option>
                                    <option value="1">Online</option>
                                    <option value="2">Retail</option>
                                    <option value="3">Direct</option>
                                </select>
                            </div>
                        </div> --}}
                    </div>
                </div>
                <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
                    <a href="#" class="btn btn-light-primary px-6 font-weight-bold">Search</a>
                </div>
            </div>
        </div>
        <!--end::Search Form-->
        <!--end: Search Form-->
        <!--begin: Datatable-->
        <div class="datatable datatable-bordered datatable-head-custom" id="kt_datatable"></div>
        <!--end: Datatable-->
    </div>
</div>

@endsection

@section('scripts')
<!-- <script src="{{ asset('js/pages/crud/ktdatatable/base/data-local.js') }}"></script> -->
<script>
    $(function () {
        // var dataJSONArray = JSON.parse('[{"RecordID":1,"namaPerusahaan":"DAEWOONG INFION","zatAktif":"Eritropoietin","tanggalPelaporan":"22 Januari 2021","statusPelaporan":"Di sepakati","action":null},\n' +
        // '{"RecordID":1,"namaPerusahaan":"MEIJI INDONESIAN PHARMACEUTICAL INDUSTRIES","zatAktif":"Eritropoietin","tanggalPelaporan":"23 Januari 2021","statusPelaporan":"Di sepakati","action":null},\n' +
        // '{"RecordID":1,"namaPerusahaan":"RIASIMA ABADI FARMA","zatAktif":"Eritropoietin","tanggalPelaporan":"28 Januari 2021","statusPelaporan":"Di sepakati","action":null}]');

        var data = {!!json_encode($pelaporan->toArray()) !!};

        var datatable = $('#kt_datatable').KTDatatable({
            // datasource definition
            data: {
                type: 'local',
                source: data,
                pageSize: 10,
            },

            // layout definition
            layout: {
                scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
                // height: 450, // datatable's body's fixed height
                footer: false, // display/hide footer
            },

            // column sorting
            sortable: true,

            pagination: true,

            search: {
                input: $('#kt_datatable_search_query'),
                key: 'generalSearch',
            },

            // columns definition
            columns: [{

                    field: 'namaPerusahaan',
                    title: 'Nama Perusahaan',
                },{
                    field: 'zat_active_name',
                    title: 'Bahan baku yang dikembangkan',
                },{
                    field: 'status',
                    title: 'Status Pelaporan',
                }, {
                    field: 'Actions',
                    title: 'Actions',
                    sortable: false,
                    width: 125,
                    overflow: 'visible',
                    autoHide: false,
                    template: function(response) {
                        return `<a href="{{ url('/admin/progress/getforum/') }}/`+ response.reporting_id +`"" class="btn btn-sm btn-clean btn-icon mr-2" title="Diskusi">
                                    <i class="fa fa-eye"></i>
                                </a>
                            `;
                    },
                }],
        });

        $('#kt_datatable_search_status').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Status');
        });

        $('#kt_datatable_search_type').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Type');
        });

        $('#kt_datatable_search_status, #kt_datatable_search_type').selectpicker();
  });
</script>
@endsection
