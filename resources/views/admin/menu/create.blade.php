@extends('layout.admin.app')

@section('styles')
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <!--begin::Card-->
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">Create Menu</h3>
                </div>
                <form method="POST" id="form-editor" action="{{ url('/admin/menu') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                        <div class=row>
                            <div class="form-group col-md-5">
                                <label for="title">Name <span class="text-danger">*</span></label>
                                <input type="text" class="form-control @error('menus_name') is-invalid @enderror" id="menus_name" name="menus_name" value="{{ old('menus_name') }}" placeholder="Enter Name" />
                            </div>
                            <div class="form-group col-md-7">
                                <label for="title">Slug <span class="text-danger">*</span></label>
                                <input type="text" class="form-control @error('menus_path') is-invalid @enderror" id="menus_path" name="menus_path" value="{{ old('menus_path') }}" placeholder="Enter Slug" />
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary mr-2">Submit</button>
                        <a href="{{ url('admin/menu') }}">
                            <button type="button" class="btn btn-secondary" aria-label="Close">Cancel</button>
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
@endsection