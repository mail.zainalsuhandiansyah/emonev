<div class="modal fade" id="modalTambahData" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" id="form-add" enctype="multipart/form-data" method="post" role="form">
                    @csrf
                    <div class="card-body">
                        <!-- <div class="form-group">
                            <label for="image">Image</label>
                            <div></div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input @error('image') is-invalid @enderror" id="image" name="image" value="{{ old('image') }}"/>
                                <label class="custom-file-label" for="image">Choose file</label>
                            </div>
                        </div> -->
                        <div class="form-group">
                            <label for="title">Title <span class="text-danger">*</span></label>
                            <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title" value="{{ old('title') }}" placeholder="Enter Title" />
                        </div>
                        <!-- <div class="form-group">
                            <label for="description">Description:</label> -->
                        <!-- <textarea rows="4" cols="4" class="form-control"></textarea> -->
                        <!-- <input type="text" class="form-control" placeholder="Description"> -->
                        <!-- <textarea name="description" id=""  class="form-control"></textarea> -->
                        <div class="form-group">
                            <textarea class="summernote @error('description') is-invalid @enderror" id="description" name="description">{{ old('description') }}</textarea>
                        </div>
                        <!-- </div> -->
                        <!-- <div class="form-group">
                        <label>Date</label>
                        <input type="date" names="date" class="form-control" placeholder=""/>
                    </div> -->
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary mr-2">Submit</button>
                        <button type="reset" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>