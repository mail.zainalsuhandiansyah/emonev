<html>

<head>
	@include('auth.head')
</head>

<body id="kt_body" class="header-fixed header-mobile-fixed subheader-enabled subheader-fixed aside-enabled aside-fixed aside-minimize-hoverable page-loading">
	<div class="d-flex flex-column flex-root">
		<div class="login login-1 login-signin-on d-flex flex-column flex-lg-row flex-column-fluid bg-white" id="kt_login">
			<div class="login-aside d-flex flex-column flex-row-auto" style="background-color: #F2C98A;">
				<div class="d-flex flex-column-auto flex-column pt-lg-40 pt-15">
					<a href="#" class="text-center mb-10">
						<img src="{{ asset('media/logos/emonev-new-logo.png') }}" class="max-h-90px" alt="" />
					</a>
					<h3 class="font-weight-bolder text-center font-size-h4 font-size-h1-lg" style="color: #986923;">
						<br />
					</h3>
				</div>
				<div class="aside-img d-flex flex-row-fluid bgi-no-repeat bgi-position-y-bottom bgi-position-x-center" style="background-image: url({{asset('media/svg/illustrations/login-visual-1.svg')}})"></div>
			</div>
			<div class="login-content flex-row-fluid d-flex flex-column justify-content-center position-relative overflow-hidden p-7 mx-auto">
				<div class="d-flex flex-column-fluid flex-center">
					<div class="login-form login-signin">
						<form class="form" novalidate="novalidate" id="kt_login_signin_form" method="post">
							@csrf
							<div class="pb-13 pt-lg-0 pt-5">
								<h3 class="font-weight-bolder text-dark font-size-h4 font-size-h1-lg">Welcome to EMONEV</h3>
							</div>
							<div class="alert alert-danger" id="responseDiv" style="display:none;">
								<button type="button" class="close" data-close="alert" id="clearMsg"><span aria-hidden="true">&times;</span></button>
								<span id="message"></span>
							</div>
							<div class="form-group">
								<label class="font-size-h6 font-weight-bolder text-dark">Username</label>
								<input class="form-control form-control-solid h-auto py-6 px-6 rounded-lg" type="text" id="username" name="username" autocomplete="off" />
							</div>
							<div class="form-group">
								<div class="d-flex justify-content-between mt-n5">
									<label class="font-size-h6 font-weight-bolder text-dark pt-5">Password</label>
									<a href="javascript:;" class="text-primary font-size-h6 font-weight-bolder text-hover-primary pt-5" id="kt_login_forgot">Forgot Password ?</a>
								</div>
								<input class="form-control form-control-solid h-auto py-6 px-6 rounded-lg" type="password" id="password" name="password" autocomplete="off" />
							</div>
							<div class="pb-lg-0 pb-5">
								<button type="button" class="btn btn-primary font-weight-bolder font-size-h6 px-8 py-4 my-3 mr-3" id="sigin_submit">Sign In</button>
								<!-- <a href="{{ url('/admin/dashboard') }}">
								</a> -->
								<!--<button type="button" class="btn btn-light-primary font-weight-bolder px-8 py-4 my-3 font-size-lg">
									<span class="svg-icon svg-icon-md">
										<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20" fill="none">
											<path d="M19.9895 10.1871C19.9895 9.36767 19.9214 8.76973 19.7742 8.14966H10.1992V11.848H15.8195C15.7062 12.7671 15.0943 14.1512 13.7346 15.0813L13.7155 15.2051L16.7429 17.4969L16.9527 17.5174C18.879 15.7789 19.9895 13.221 19.9895 10.1871Z" fill="#4285F4" />
											<path d="M10.1993 19.9313C12.9527 19.9313 15.2643 19.0454 16.9527 17.5174L13.7346 15.0813C12.8734 15.6682 11.7176 16.0779 10.1993 16.0779C7.50243 16.0779 5.21352 14.3395 4.39759 11.9366L4.27799 11.9466L1.13003 14.3273L1.08887 14.4391C2.76588 17.6945 6.21061 19.9313 10.1993 19.9313Z" fill="#34A853" />
											<path d="M4.39748 11.9366C4.18219 11.3166 4.05759 10.6521 4.05759 9.96565C4.05759 9.27909 4.18219 8.61473 4.38615 7.99466L4.38045 7.8626L1.19304 5.44366L1.08875 5.49214C0.397576 6.84305 0.000976562 8.36008 0.000976562 9.96565C0.000976562 11.5712 0.397576 13.0882 1.08875 14.4391L4.39748 11.9366Z" fill="#FBBC05" />
											<path d="M10.1993 3.85336C12.1142 3.85336 13.406 4.66168 14.1425 5.33717L17.0207 2.59107C15.253 0.985496 12.9527 0 10.1993 0C6.2106 0 2.76588 2.23672 1.08887 5.49214L4.38626 7.99466C5.21352 5.59183 7.50242 3.85336 10.1993 3.85336Z" fill="#EB4335" />
										</svg>
									</span>Sign in with Google</button>-->
							</div>
								<span class="text-muted font-weight-bold font-size-h4">New Here?
								<a href="javascript:;" id="kt_login_signup" class="text-primary font-weight-bolder">Create an Account Industry ?</a></span>
						</form>
					</div>
					<div class="login-form login-signup">
						<form class="form" novalidate="novalidate" id="kt_login_signup_form">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<div class="pb-13 pt-lg-0 pt-5">
								<h3 class="font-weight-bolder text-dark font-size-h4 font-size-h1-lg">Sign Up Industry</h3>
								<p class="text-muted font-weight-bold font-size-h4">Enter your details to create your accountasda</p>
							</div>
							<div class="alert alert-danger" id="responseDiv" style="display:none;">
								<button type="button" class="close" data-close="alert" id="clearMsg"><span aria-hidden="true">&times;</span></button>
								<span id="message"></span>
							</div>
							<div class="form-group">
								<input class="form-control form-control-solid h-auto py-6 px-6 rounded-lg font-size-h6" type="text" placeholder="Nama Perusahaan" name="company_name" id="company_name" autocomplete="off" />
							</div>
							<div class="form-group">
								<input class="form-control form-control-solid h-auto py-6 px-6 rounded-lg font-size-h6" type="text" placeholder="NPWP" name="company_npwp" id="company_npwp" autocomplete="off" />
							</div>
							<div class="form-group">
								<input class="form-control form-control-solid h-auto py-6 px-6 rounded-lg font-size-h6" type="text" placeholder="User E-licensing" name="company_users_elic" id="company_users_elic" autocomplete="off" />
							</div>
							<div class="form-group">
								<input class="form-control form-control-solid h-auto py-6 px-6 rounded-lg font-size-h6" type="password" placeholder="Password" name="company_password" id="company_password" autocomplete="off" />
							</div>
							<div class="form-group">
								<input class="form-control form-control-solid h-auto py-6 px-6 rounded-lg font-size-h6" type="password" placeholder="Confirm password" name="company_password_confirm" id="company_password_confirm" autocomplete="off" />
							</div>
							<div class="form-group d-flex flex-wrap pb-lg-0 pb-3">
								<button type="button" id="signup_submit" class="btn btn-primary font-weight-bolder font-size-h6 px-8 py-4 my-3 mr-4">Submit</button>
								<button type="button" id="kt_login_signup_cancel" class="btn btn-light-primary font-weight-bolder font-size-h6 px-8 py-4 my-3">Cancel</button>
							</div>
						</form>
					</div>
					<div class="login-form login-forgot">
						<form class="form" novalidate="novalidate" id="kt_login_forgot_form" method="post">
							@csrf
							<div class="pb-13 pt-lg-0 pt-5">
								<h3 class="font-weight-bolder text-dark font-size-h4 font-size-h1-lg">Forgotten Password ?</h3>
								<p class="text-muted font-weight-bold font-size-h4">Enter your email to reset your password</p>
							</div>
							<div class="alert alert-danger" id="responseDiv" style="display:none;">
								<button type="button" class="close" data-close="alert" id="clearMsg"><span aria-hidden="true">&times;</span></button>
								<span id="message"></span>
							</div>
							<div class="form-group">
								<input class="form-control form-control-solid h-auto py-6 px-6 rounded-lg font-size-h6" type="text" placeholder="Username" name="username" autocomplete="off" />
							</div>
							<div class="form-group">
								<input class="form-control form-control-solid h-auto py-6 px-6 rounded-lg font-size-h6" type="email" placeholder="Email" name="email" autocomplete="off" />
							</div>
							<div class="form-group d-flex flex-wrap pb-lg-0">
								<button type="button" id="button-submit-forgot" class="btn btn-primary font-weight-bolder font-size-h6 px-8 py-4 my-3 mr-4">Submit</button>
								<button type="button" id="kt_login_forgot_cancel" class="btn btn-light-primary font-weight-bolder font-size-h6 px-8 py-4 my-3">Cancel</button>
							</div>
						</form>
					</div>
				</div>
				<div class="d-flex justify-content-lg-start justify-content-center align-items-end py-7 py-lg-0">
					<div class="text-dark-50 font-size-lg font-weight-bolder mr-10">
						<span class="mr-1">2020©</span>
						<a href="http://keenthemes.com/metronic" target="_blank" class="text-dark-75 text-hover-primary">ExecutiveQ</a>
					</div>
					<!--<a href="#" class="text-primary font-weight-bolder font-size-lg">Terms</a>
						<a href="#" class="text-primary ml-5 font-weight-bolder font-size-lg">Plans</a>
						<a href="#" class="text-primary ml-5 font-weight-bolder font-size-lg">Contact Us</a>-->
				</div>
			</div>
		</div>
	</div>
	<!-- @include('auth.scripts') -->
	<script>
		"use strict";

		// Class Definition
		var KTLogin = function() {
			var _login;

			var _showForm = function(form) {
				var cls = 'login-' + form + '-on';
				var form = 'kt_login_' + form + '_form';

				_login.removeClass('login-forgot-on');
				_login.removeClass('login-signin-on');
				_login.removeClass('login-signup-on');

				_login.addClass(cls);

				KTUtil.animateClass(KTUtil.getById(form), 'animate__animated animate__backInUp');
			}

			var _handleSignInForm = function() {

				var validation;

				var Vpassword = $("#password","#kt_login_signin_form");

				Vpassword.keypress(function(e) {
					if (e.keyCode == 13) {
						e.preventDefault();
						$('#sigin_submit',"#kt_login_signin_form").trigger('click');
					}
				})
				// Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
				validation = FormValidation.formValidation(
					KTUtil.getById('kt_login_signin_form'), {
						fields: {
							username: {
								validators: {
									notEmpty: {
										message: 'Username is required'
									}
								}
							},
							password: {
								validators: {
									notEmpty: {
										message: 'Password is required'
									}
								}
							}
						},
						plugins: {
							trigger: new FormValidation.plugins.Trigger(),
							submitButton: new FormValidation.plugins.SubmitButton(),
							//defaultSubmit: new FormValidation.plugins.DefaultSubmit(), // Uncomment this line to enable normal button submit after form validation
							bootstrap: new FormValidation.plugins.Bootstrap()
						}
					}
				);

				$('#sigin_submit').on('click', function(e) {
					e.preventDefault();

					validation.validate().then(function(status) {
						if (status == 'Valid') {
							$("#sigin_submit").html('<i class="fas fa-spinner fa-pulse"></i>');
							$("#kt_login_signin_form :input").attr("readonly", true);
							console.log('ttter');
							$.ajax({
									url: 'login',
									method: 'POST',
									data: $("#kt_login_signin_form").serialize()
								}).done(function(response) {
									var resp = JSON.parse(response);
									var datauser = resp.data;
									console.log('resp ',resp)
									console.log('datauser ',datauser)
									$('#message', '#kt_login_signin_form').html(response.message);
									$('#logText').html('Sign In');
									if (resp.success == true) {
										// $('#responseDiv').removeClass('alert-danger').addClass('alert-success').show();
										// swal.fire({
										// 	text: "All is cool! Now you submit this form",
										// 	icon: "success",
										// 	buttonsStyling: false,
										// 	confirmButtonText: "Ok, got it!",
										// 	customClass: {
										// 		confirmButton: "btn font-weight-bold btn-light-primary"
										// 	}
										// }).then(function() {

										// });
										if(datauser.privileges_id=='1'){
											window.location.href = "{{ route('dashboardadmin')}}";
										}else if(datauser.privileges_id=='2'){
											window.location.href = "{{ route('dashboardevaluator')}}";
										}else if(datauser.privileges_id=='3'){
											window.location.href = "{{ route('dashboardindustri')}}";
										}else{
											window.location.href = "{{ route('dashboardadmin')}}";
										}
										$("#kt_login_signin_form :input").attr("readonly", false);
									} else {
										swal.fire({
											text: resp.message,
											icon: "error",
											buttonsStyling: false,
											confirmButtonText: "Ok, got it!",
											customClass: {
												confirmButton: "btn font-weight-bold btn-light-primary"
											}
										}).then(function() {
											KTUtil.scrollTop();
										});

										$('#responseDiv').removeClass('alert-success').addClass('alert-danger').show();
										$('#message').html("Opps, something wrong please try again or contact administrator");
										$("#kt_login_signin_form :input").attr("readonly", false);
										$("#sigin_submit").html('Sign In');
										$('#kt_login_signin_form')[0].reset();
									}
								})
								.fail(function(response) {
									$('#message').html("Opps, something wrong please try again or contact administrator");
									$('#responseDiv').removeClass('alert-danger').addClass('alert-danger').show();
									$('#kt_login_signin_form')[0].reset();
									$("#kt_login_signin_form :input").attr("readonly", false);
									$("#sigin_submit").html('Sign In');
								})
								.always(function(response) {
									var resp = JSON.parse(response);
									var datauser = resp.data;
									if(resp.success===false){
										Swal.fire('Warning !',resp.message,'warning');
									}else{
										// swal.fire({
										// 	text: "All is cool! Now you submit this form",
										// 	icon: "success",
										// 	buttonsStyling: false,
										// 	confirmButtonText: "Ok, got it!",
										// 	customClass: {
										// 		confirmButton: "btn font-weight-bold btn-light-primary"
										// 	}
										// }).then(function() {

										// });
										if(datauser.privileges_id=='1'){
											window.location.href = "{{ route('dashboardadmin')}}";
										}else if(datauser.privileges_id=='2'){
											window.location.href = "{{ route('dashboardevaluator')}}";
										}else if(datauser.privileges_id=='3'){
											window.location.href = "{{ route('dashboardindustri')}}";
										}else{
											window.location.href = "{{ route('dashboardadmin')}}";
										}
										$("#kt_login_signin_form :input").attr("readonly", false);
									}
								});;
						} else {
							swal.fire({
								text: "Sorry, looks like there are some errors detected, please try again.",
								icon: "error",
								buttonsStyling: false,
								confirmButtonText: "Ok, got it!",
								customClass: {
									confirmButton: "btn font-weight-bold btn-light-primary"
								}
							}).then(function() {
								KTUtil.scrollTop();
							});
						}
					});
				});

				// Handle forgot button
				$('#kt_login_forgot').on('click', function(e) {
					e.preventDefault();
					_showForm('forgot');
				});

				// Handle signup
				$('#kt_login_signup').on('click', function(e) {
					e.preventDefault();
					_showForm('signup');
				});
			}

			var _handleSignUpForm = function(e) {
				var validation;
				var form = KTUtil.getById('kt_login_signup_form');

				// Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
				validation = FormValidation.formValidation(
					form, {
						fields: {
							company_name: {
								validators: {
									notEmpty: {
										message: 'Nama Perusahaan is required'
									}
								}
							},
							company_npwp: {
								validators: {
									notEmpty: {
										message: 'NPWP is required'
									}
								}
							},
							company_users_elic: {
								validators: {
									notEmpty: {
										message: 'User E-licensing is required'
									}
								}
							},
							company_password: {
								validators: {
									notEmpty: {
										message: 'The password is required'
									}
								}
							},
							company_password_confirm: {
								validators: {
									notEmpty: {
										message: 'The password confirmation is required'
									},
									identical: {
										compare: function() {
											return form.querySelector('[name="company_password"]').value;
										},
										message: 'The password and its confirm are not the same'
									}
								}
							},
						},
						plugins: {
							trigger: new FormValidation.plugins.Trigger(),
							bootstrap: new FormValidation.plugins.Bootstrap()
						}
					}
				);

				$('#signup_submit').on('click', function(e) {
					e.preventDefault();

					validation.validate().then(function(status) {
						if (status == 'Valid') {
							$("#signup_submit").html('<i class="fas fa-spinner fa-pulse"></i>');
							$("#kt_login_signup_form :input").attr("readonly", true);
							console.log('ttter');

							$.ajax({
									url: 'register',
									method: 'POST',
									data: $("#kt_login_signup_form").serialize()
								}).done(function(response) {
									var resp = JSON.parse(response);
									var datauser = resp.data;
									console.log('resp ',resp)
									console.log('datauser ',datauser)
									$('#message', '#kt_login_signup_form').html(response.message);
									$('#logText').html('Submit');
									if (resp.success == true) {
										$('#responseDiv').removeClass('alert-danger').addClass('alert-success').show();
										swal.fire({
											text: "Register Successfully ! Please Login .",
											icon: "success",
											buttonsStyling: false,
											confirmButtonText: "Ok, got it!",
											customClass: {
												confirmButton: "btn font-weight-bold btn-light-primary"
											}
										}).then(function() {
											$("#kt_login_signup_form").trigger('reset');
											$("#kt_login_signup_form :input").attr("readonly", false);
											$("#responseDiv").hide();
											_showForm('signin');
										});
									} else {
										swal.fire({
											text: resp.message,
											icon: "error",
											buttonsStyling: false,
											confirmButtonText: "Ok, got it!",
											customClass: {
												confirmButton: "btn font-weight-bold btn-light-primary"
											}
										}).then(function() {
											KTUtil.scrollTop();
										});

										$('#responseDiv').removeClass('alert-success').addClass('alert-danger').show();
										$('#message').html("Opps, something wrong please try again or contact administrator");
										$("#kt_login_signup_form :input").attr("readonly", false);
										$("#signup_submit").html('Submit');
									}
								})
								.fail(function(response) {
									$('#message').html("Opps, something wrong please try again or contact administrator");
									$('#responseDiv').removeClass('alert-danger').addClass('alert-danger').show();
									$('#kt_login_signup_form')[0].reset();
									$("#kt_login_signup_form :input").attr("readonly", false);
									$("#signup_submit").html('Submit');
								})
								.always(function(response) {
									var resp = JSON.parse(response);
									var datauser = resp.data;
									if(resp.success===false){
										Swal.fire('Warning !',resp.message,'warning');
									}else{
										swal.fire({
											text: "Register Successfully ! Please Login .",
											icon: "success",
											buttonsStyling: false,
											confirmButtonText: "Ok, got it!",
											customClass: {
												confirmButton: "btn font-weight-bold btn-light-primary"
											}
										}).then(function() {
											$("#kt_login_signup_form").trigger('reset');
											$("#kt_login_signup_form :input").attr("readonly", false);
											$("#responseDiv").hide();
											_showForm('signin');
										});
									}
								});
						} else {
							swal.fire({
								text: "Sorry, looks like there are some errors detected, please try again.",
								icon: "error",
								buttonsStyling: false,
								confirmButtonText: "Ok, got it!",
								customClass: {
									confirmButton: "btn font-weight-bold btn-light-primary"
								}
							}).then(function() {
								KTUtil.scrollTop();
							});
						}
					});
				});

				// Handle cancel button
				$('#kt_login_signup_cancel').on('click', function(e) {
					e.preventDefault();

					_showForm('signin');
				});
			}

			var _handleForgotForm = function(e) {
				var validation;

				// Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
				validation = FormValidation.formValidation(
					KTUtil.getById('kt_login_forgot_form'), {
						fields: {
							email: {
								validators: {
									notEmpty: {
										message: 'Email address is required'
									},
									emailAddress: {
										message: 'The value is not a valid email address'
									}
								}
							}
						},
						plugins: {
							trigger: new FormValidation.plugins.Trigger(),
							bootstrap: new FormValidation.plugins.Bootstrap()
						}
					}
				);

				// Handle submit button
				$('#button-submit-forgot').on('click', function(e) {
					e.preventDefault();

					validation.validate().then(function(status) {
						if (status == 'Valid') {
							// Submit form

							$("#button-submit-forgot").html('<i class="fas fa-spinner fa-pulse"></i>');
							$("#kt_login_forgot_form :input").attr("readonly", true);
							console.log('ttter');

							$.ajax({
									url: 'forgot',
									method: 'POST',
									data: $("#kt_login_forgot_form").serialize()
								}).done(function(response) {
									var resp = JSON.parse(response);
									$('#message', '#kt_login_forgot_form').html(response.message);
									$('#logText').html('Submit');
									if (resp.success == true) {
										$('#responseDiv').removeClass('alert-danger').addClass('alert-success').show();
										swal.fire({
											text: resp.message,
											icon: "success",
											buttonsStyling: false,
											confirmButtonText: "Ok, got it!",
											customClass: {
												confirmButton: "btn font-weight-bold btn-light-primary"
											}
										}).then(function() {
											$("#kt_login_forgot_form").trigger('reset');
											$("#kt_login_forgot_form :input").attr("readonly", false);
											$("#responseDiv").hide();
											_showForm('signin');
										});
									} else {
										swal.fire({
											text: resp.message,
											icon: "error",
											buttonsStyling: false,
											confirmButtonText: "Ok, got it!",
											customClass: {
												confirmButton: "btn font-weight-bold btn-light-primary"
											}
										}).then(function() {
											KTUtil.scrollTop();
										});
									}

									$('#responseDiv','#kt_login_forgot_form').removeClass('alert-success').addClass('alert-danger').show();
									$('#message','#kt_login_forgot_form').html(resp.message);
									$("#kt_login_forgot_form :input").attr("readonly", false);
									$("#button-submit-forgot",'#kt_login_forgot_form').html('Submit');
								})
								.fail(function(response) {
									$('#message').html("Opps, something wrong please try again or contact administrator");
									$('#responseDiv').removeClass('alert-danger').addClass('alert-danger').show();
									$('#kt_login_forgot_form')[0].reset();
									$("#kt_login_forgot_form :input").attr("readonly", false);
									$("#button-submit-forgot").html('Submit');
								});

							KTUtil.scrollTop();
						} else {
							swal.fire({
								text: "Sorry, looks like there are some errors detected, please try again.",
								icon: "error",
								buttonsStyling: false,
								confirmButtonText: "Ok, got it!",
								customClass: {
									confirmButton: "btn font-weight-bold btn-light-primary"
								}
							}).then(function() {
								KTUtil.scrollTop();
							});
						}
					});
				});

				// Handle cancel button
				$('#kt_login_forgot_cancel').on('click', function(e) {
					e.preventDefault();

					_showForm('signin');
				});
			}

			// Public Functions
			return {
				// public functions
				init: function() {
					_login = $('#kt_login');

					_handleSignInForm();
					_handleSignUpForm();
					_handleForgotForm();
				}
			};
		}();

		// Class Initialization
		$(function() {
			KTLogin.init();
		});

	</script>
	<script>
		$(document).ajaxStart(function(){
			$.blockUI({ message: '<div style="padding:5px 0;">Please wait...</div>' ,css: { backgroundColor: '#fff', color: '#000', fontSize: '12px'} })
		}).ajaxStop($.unblockUI);
	</script>

</body>
</html>
