<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return redirect('/landing');
// });

Route::get('/', 'PagesController@landing');
Route::resource('/contact', 'ContactusController')->except([
    'edit'
]);

// ROUTE AUTH
Route::group(["prefix" => "auth"], function () {
    Route::namespace("Auth")->group(function () {
        // Route::resource('/login', 'LoginController')->except([
        //     'edit'
        // ]);
        Route::get('/', 'AuthController@showFormLogin')->name('login');
        Route::get('login', 'AuthController@showFormLogin')->name('login');
        Route::post('login', 'AuthController@login');
        Route::get('register', 'AuthController@showFormRegister')->name('register');
        Route::post('register', 'AuthController@register');
        Route::post('forgot', 'AuthController@forgot')->name('forgot');

        Route::group(['middleware' => 'auth'], function () {
            // Route::get('home', 'HomeController@index')->name('home');
            Route::get('logout', 'AuthController@logout')->name('logout');
        });
    });
});

// ROUTE INDUSTRI
Route::group(["prefix" => "admin", 'middleware' => ['auth', 'web']], function () {
    Route::namespace("Admin")->group(function () {
        Route::get('/dashboard', 'DashboardController@index')->name('dashboardadmin');
        Route::get('/dashboard-industri', 'DashboardindustriController@dashboardIndustri')->name('dashboardindustri');
        Route::post('/print-bbo', 'DashboardindustriController@printBBO');
        Route::get('/profile-industri', 'ProfileindustriController@profileindustri');
        Route::get('/profile-bahanbaku', 'ProfileindustriController@bahanbaku');
        Route::get('/pelaporan-industri', 'PelaporanindustriController@pelaporanIndustri')->name('listReport');
        // Route::get('/setslider', 'SettingController@settingslider');
        Route::get('/pelaporan-detail-getYear/{id?}', 'PelaporanindustriController@detail_pelaporan_getYear');
        Route::get('/pelaporan-detail-getTw/{yearid?}/{zatactiveid?}', 'PelaporanindustriController@detail_pelaporan_getTw')->name('pelaporan.get-tw');
        Route::get('/pelaporan/get-roadmap/{zatactiveid?}/{yearid?}/{twid?}', 'PelaporanindustriController@get_roadmap')->name('pelaporan.get-roadmap');
        Route::resource('masterBBOIndustri', MasterBBOIndustriController::class);
        Route::resource('masterBBO', MasterBBOController::class);
        Route::resource('masterBBOIndustri', MasterBBOIndustriController::class);
        Route::resource('roadmap', RoadmapController::class);
        Route::get('/generate-roadmap/{zatactiveid?}/{yearfrom?}/{yearthru?}', 'RoadmapController@generateHilirisasi')->name('generatehilirisasi');
        Route::post('/listBBOIndustri', 'MasterBBOIndustriController@listBBOIndustri');

        Route::resource('/changePassword', ChangePasswordController::class);
        Route::get('/pelaporan-detail/{id?}', 'PelaporanindustriController@getdetail')->name('pelaporan.detail');
        Route::post('/pelaporan-industri', 'PelaporanindustriController@store_answer')->name('simpan.jawaban');
        Route::post('/pelaporan/dropzonestore', 'PelaporanindustriController@DropZoneStore')->name('simpan.file');
        // Laporan
        Route::get('/pelaporan-progress-bahan-baku', 'PelaporanindustriController@getRoadmapbahanBaku')->name('report.bahanbaku');
        Route::get('/dashboard-get-progress', 'DashboardindustriController@get_progress')->name('report.get.progress');
    });
});

// ROUTE EVALUATOR
Route::group(["prefix" => "admin", 'middleware' => ['auth', 'web']], function () {
    Route::namespace("Admin")->group(function () {
        Route::get('/dashboard-evaluator', 'DashboardevaluatorController@dashboardEvaluator')->name('dashboardevaluator');
        Route::get('/daftar-industri', 'DaftarindustriController@index');
        Route::get('/progress', 'ProgressController@index');
        Route::get('/progress/getforum', 'ProgressController@getforum');
        Route::get('/cetak-bbo', 'CetakbboController@index');
        Route::get('/cetak-industri', 'CetakindustriController@index');
        Route::resource('/inquiry', 'ContactusController')->except([
            'edit'
        ]);
        Route::resource('/content', 'ContentController')->except([
            'edit'
        ]);
        Route::resource('registrasi-industri', RegistrasiIndustriController::class);
        Route::resource('privileges', PrivilegesController::class);
        Route::resource('users-management', UserManagementController::class);
        Route::resource('zat-active-group', ZatgroupController::class);
        Route::resource('zat-active', ZatActiveController::class);
        Route::resource('/menu', 'MenuController')->except([
            'edit'
        ]);
        Route::resource('registrasi-industri', RegistrasiIndustriController::class);
        Route::resource('privileges', PrivilegesController::class);
        Route::resource('users-management', UserManagementController::class);
        Route::resource('zat-active-group', ZatgroupController::class);
        Route::resource('zat-active', ZatActiveController::class);
        Route::resource('masterBBO', MasterBBOController::class);
        Route::post('/listBBO', 'ZatActiveController@listBBO');
        Route::post('/listZatActive', 'ZatActiveController@listZatActive');
        Route::post('/progress/getforum/{id?}', 'ProgressController@simpan_qna')->name('simpan.qna');
        Route::get('/progress/getforum/{id?}', 'ProgressController@getforum');
        Route::post('/pelaporan/approval', 'ProgressController@approve_langkah')->name('approval.langkah');
        // cetak laporan //
        // Route::post('/pelaporan-industri-cetak', 'CetakEvaluator@cetak1')->name('cetak1');
        Route::get('/pelaporan-industri-cetak2/{userID?}', 'CetakEvaluator@cetak2')->name('cetak.satu');
    });
});

// Setting Slider
Route::group(["prefix" => "admin", 'middleware' => ['auth', 'web']], function () {
    Route::namespace("Admin")->group(function () {

        Route::get('/setslider', 'SettingSliderController@index');

        Route::post('setslider', 'SettingSliderController@store')->name('store.image');
        Route::resource('/setslider', 'SettingSliderController')->except([
            'edit'
        ]);
        Route::get('/list', 'SettingSliderController@listslider');
    });
});

// Setting footer
Route::group(["prefix" => "admin", 'middleware' => ['auth', 'web']], function () {
    Route::namespace("Admin")->group(function () {

        Route::get('/setfooter', 'SettingFooterController@index');

        Route::post('setfooter', 'SettingFooterController@store')->name('store.footer');
        Route::resource('/setfooter', 'SettingFooterController')->except([
            'edit'
        ]);
    });
});

// Setting tahun
Route::group(["prefix" => "admin", 'middleware' => ['auth', 'web']], function () {
    Route::namespace("Admin")->group(function () {

        Route::get('/settahun', 'SettingtahunController@index');

        Route::post('settahun', 'SettingtahunController@store')->name('store.tahun');
        Route::resource('/settahun', 'SettingtahunController')->except([
            'edit'
        ]);
    });
});

Route::get('/{article}', 'ArticleController@index');


// Demo routes
Route::get('/datatables', 'PagesController@datatables');
Route::get('/ktdatatables', 'PagesController@ktDatatables');
Route::get('/select2', 'PagesController@select2');
Route::get('/jquerymask', 'PagesController@jQueryMask');
Route::get('/icons/custom-icons', 'PagesController@customIcons');
Route::get('/icons/flaticon', 'PagesController@flaticon');
Route::get('/icons/fontawesome', 'PagesController@fontawesome');
Route::get('/icons/lineawesome', 'PagesController@lineawesome');
Route::get('/icons/socicons', 'PagesController@socicons');
Route::get('/icons/svg', 'PagesController@svg');




// Quick search dummy route to display html elements in search dropdown (header search)
Route::get('/quick-search', 'PagesController@quickSearch')->name('quick-search');
