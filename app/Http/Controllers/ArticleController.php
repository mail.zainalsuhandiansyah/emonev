<?php

namespace App\Http\Controllers;
use App\Models\Admin\Content;
use App\Models\Settingfooter;
use Illuminate\Support\Str;

class ArticleController extends Controller
{
    public function index($article)
    {
        $title = Str::substr($article, 0, strrpos($article, "-"));
        $code = Str::substr($article, strrpos($article, "-") + 1);

//        check article
        $_article = Content::query()
            ->where("uuid", $code)
            ->firstOrFail();

        $content = Content::query()
            ->get();

        $footers = Settingfooter::query()
        ->take(1)
        ->orderByDesc("updated_at")
        ->get();

        if ($_article->slug !== $title)
            abort(404);

        return view('article', ["data" => $_article, "title" => $_article->title, "content" => $content, "footers" => $footers]);
    }
}
