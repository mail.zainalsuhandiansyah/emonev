<?php

namespace App\Http\Controllers;

use App\Models\Admin\Menu;
use Illuminate\Http\Request;
use App\Models\Contactus;
use Illuminate\Support\Facades\DB;

class ContactusController extends Controller
{
    public function index()
    {
        $page_title = 'Contact Us';
        $page_description = 'Some description for the page';

        $menu = Menu::query()
            ->get();

        return view('landing.contact', compact('page_title', 'page_description', 'menu'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'telp' => 'required',
            'address' => 'required',
        ]);

        $contact = new Contactus();
        $contact->name = $request->name;
        $contact->email = $request->email;
        $contact->telp = $request->telp;
        $contact->address = $request->address;
        $contact->save();

        return redirect('/contact');
    }
}
