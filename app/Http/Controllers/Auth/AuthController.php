<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Validator;
use Hash;
use Session;
use App\User;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    public function __construct()
    {
        $this->middleware('guest')->except('logout','changePassword');
    }

    public function showFormLogin()
    {
        // dd('test');
        $title = 'Login Emonev System';
        if (Auth::check()) { // true sekalian session field di users nanti bisa dipanggil via Auth
            $privileges_id = Auth::user()->privileges_id;
            if ($privileges_id == 3) {
                $redirect = redirect()->route('dashboardindustri');
            } else if ($privileges_id == 2) {
                $redirect = redirect()->route('dashboardevaluator');
            } else {
                $redirect = redirect()->route('dashboardadmin');
            }
            return $redirect;
        }
        return view('auth.login', compact('title'));
    }

    public function login(Request $request)
    {
        $data = $request->all();

        $rules = [
            'username' => 'required|username',
            'password' => 'required|string'
        ];

        $messages = [
            'username.required' => 'Username wajib diisi',
            'username.username' => 'Username tidak valid',
            'password.required' => 'Password wajib diisi',
            'password.string'   => 'Password harus berupa string'
        ];

        // $validator = Validator::make($request->all(), $rules, $messages);

        // if($validator->fails()){
        //     return redirect()->back()->withErrors($validator)->withInput($request->all);
        // }

        $data = [
            'username' => $request->input('username'),
            'password' => $request->input('password'),
        ];

        Auth::attempt($data);
        $password = Hash::make($request->password);

        // echo"<pre>";print_r($password);die();

        $datausers = DB::table('emonev_users')
            ->select('emonev_users.*')
            ->where('username',  $request->input('username'))
            // ->where('password' ,  $password)
            ->first();
        // echo"<pre>";print_r($datausers);die();
        if (Auth::check()) { // true sekalian session field di users nanti bisa dipanggil via Auth
            //Login Success
            // return redirect()->route('home');
            $insert['users_id']    = Auth::user()->users_id;
            $insert['privileges_id'] = Auth::user()->privileges_id;
            $insert['last_login'] = Carbon::now();

            DB::table('emonev_users_logs')->insert($insert);
            $return = array('success' => true, 'message' => 'Login Successfully !', 'data' => $datausers);
        } else { // false

            //Login Fail
            Session::flash('error', 'Email atau password salah');
            $return = array('success' => false, 'message' => 'Login Failed ! Please check your password and username ! ');
        }

        echo json_encode($return);
    }

    public function showFormRegister()
    {
        return view('register');
    }

    public function register(Request $request)
    {
        $data = $request->all();
        $bValid = true;
        $message = '';
        // return $data['password'];
        // mengambil data dari elicensing
        $a = DB::connection('mysql2')->table('t_user')
            ->join('m_trader', 't_user.trader_id', 'm_trader.trader_id')
            ->where('m_trader.npwp_ori', $data['company_npwp'])
            ->where('t_user.username', $data['company_users_elic'])
            ->get();

        if (Count($a) == 0) {
            $bValid = false;
            $message = 'Perusahaan Anda Tidak Terdaftar di Elicensing';
        }

        // jika datanya ada makan masukan ke tabel perusahaan
        $c = DB::table('emonev_company')->where('company_npwp', $data['company_npwp'])
            ->where('company_users_elic', $data['company_users_elic'])
            ->get();
        // validasi data nya sudah tersedia atau belum pada emonev bbo
        // jika ada akan ditolak registrasinya
        if (Count($c) != 0) {
            $bValid = false;
            $message = 'Perusahaan Anda Sudah Terdaftar Pada Sistem ini';
        }

        if ($bValid === true) {

            $password         = Hash::make($data['company_password']);
            $username          = $data['company_users_elic'];
            $name             = $data['company_name'];
            // return $password;
            $c = DB::connection('mysql2')->table('t_user')
                ->join('m_trader', 't_user.trader_id', 'm_trader.trader_id')
                ->where('m_trader.npwp_ori', $data['company_npwp'])
                ->where('t_user.username', $data['company_users_elic'])
                ->first();
            $d = DB::connection('mysql2')->table('t_if_izin')
                ->select('NO_IZIN', 'TGL_IZIN')
                ->where('trader_id',  $c->TRADER_ID)
                ->orderby('id', 'asc')
                ->first();

            $noIzinAwal     = isset($d->NO_IZIN) ? $d->NO_IZIN : '';
            $tglIzinAwal     = isset($d->TGL_IZIN) ? $d->TGL_IZIN : Carbon::now();;

            $d = DB::connection('mysql2')->table('t_if_izin')
                ->select('NO_IZIN', 'TGL_IZIN')
                ->where('trader_id',  $c->TRADER_ID)
                ->orderby('id', 'desc')
                ->first();

            $noIzinAkhir     = isset($d->NO_IZIN) ? $d->NO_IZIN : '';
            $tglIzinAkhir     = isset($d->TGL_IZIN) ? $d->TGL_IZIN : Carbon::now();;

            // dd($c);
            // INSERT TO CMS_USER
            $insert = [];
            $insert['fullname'] = $name;
            $insert['photo']     = 'uploads/img/user.png';
            $insert['username']    = $username;
            $insert['password']    = $password;
            $insert['email']     = $c->EMAIL;
            $insert['privileges_id'] = 3;
            $insert['created_at']    = Carbon::now();
            $insert['created_by']    = 1;
            $insert['status']   = 1;

            DB::table('emonev_users')->insert($insert);
            $a = DB::table('emonev_users')->orderby('users_id', 'desc')->first();
            $insert = [];
            $insert['company_users_elic'] = $username;
            $insert['company_name']        = $c->NAMA;
            $insert['company_npwp']        = $c->NPWP_ORI;
            $insert['company_address']    = $c->ALAMAT_FULL;
            $insert['company_phone']    = $c->TELP;
            $insert['company_fax']        = $c->FAX;
            $insert['company_email']    = $c->EMAIL;
            $insert['company_capital']    = $c->PERMODALAN;
            $insert['company_type']    = $c->JENIS_INDUSTRI_IF;
            $insert['permission_first_number']    = $noIzinAwal;
            $insert['permission_first_date'] = $tglIzinAwal;
            $insert['permission_final_number'] = $noIzinAkhir;
            $insert['permission_final_date'] = $tglIzinAkhir;
            $insert['users_id']    = $a->users_id;
            $insert['created_at'] = Carbon::now();
            $insert['created_by'] = $a->users_id;

            DB::table('emonev_company')->insert($insert);

            $return  = array('success' => true, 'message' => $message);
        } else {
            $return  = array('success' => false, 'message' => $message);
        }

        echo json_encode($return);
    }

    public function logout()
    {
        Auth::logout(); // menghapus session yang aktif
        Session::flush();
        // return redirect()->route('login');
        return redirect(\URL::previous());
    }

    public function forgot(Request $request){
        $data = $request->all();

        $url = url('');

        $bValid = true;
        $message = 'Opps, something wrong please try again or contact administrator';

        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:emonev_users'
        ]);
        
        if ($validator->fails()) {
            $bValid = false;
            $message = $validator->errors()->all();
        }

        if($bValid===true){

            $rand_string = Str::random(6);
            $password = \Hash::make($rand_string);

            DB::table('emonev_users')->where('email', $request->input('email'))
                                    ->where('username', $request->input('username'))
                                    ->update(['password' => $password]);

            $user = DB::table('emonev_users')
                            ->where('email',$request->input('email'))
                            ->first();
            $user->password = $rand_string;

            $to_name = $user->fullname;
            $to_email = $user->email;
            $data = array('name'=>$to_name, 'keterangan' => 'The password for your account on '.$url.' has successfully been changed.','new_pass'=>$rand_string);
            Mail::send('auth.reset', $data, function($message) use ($to_name, $to_email) {
                $message->to($to_email, $to_name)->subject('Reset Password');
                $message->from('emonevbbo@gmail.com','Reset Password ');
            });

            $return = array('success' => true, 'message' =>'Password telah di reset silahkan periksa email anda !');
        }else{
            $return = array('success' => false, 'message' =>$message);
        }
        echo json_encode($return);
    }

}
