<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Menu;
use App\Models\Contactus;
use Illuminate\Http\Request;
use League\CommonMark\Inline\Element\Code;

class ContactusController extends Controller
{
    public function index()
    {
        $content = Contactus::query()
            ->get();

        $menu = Menu::query()
            ->get();

        return view('admin.setting.inquiry', compact('content', 'menu'));
    }
}
