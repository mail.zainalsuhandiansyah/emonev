<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\Content;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ContentController extends Controller
{
    public function index()
    {
        $page_title = 'Content';
        $search = request("q", "");

        $article = Content::query()
            ->where(function ($query) use ($search) {
                if (!empty($search) || $search !== "")
                    $query->where("title", "LIKE", "%$search%");
            })
            ->get();

        return view('admin.content.index', compact('page_title', 'article'));
    }

    public function create()
    {
        return view('admin.content.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string',
            'description' => 'required',
            'image' => 'nullable|image|mimes:jpeg,png,jpg|max:512',
        ]);

        $imageName = null;
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = time() . '.' . $image->getClientOriginalExtension();

            $img = Image::make($image->getRealPath());
            $img->resize(null, 300, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->stream();

            Storage::disk('local')->put('public/images/article/' . $imageName, $img, 'public');
        }

        $article = new Content();
        $article->title = $request->title;
        $article->description = $request->get("description");
        $article->image = $imageName;
        $article->save();

        return redirect('/admin/content');
    }

    public function show($id)
    {
        $data = Content::query()->where('id', $id)->first();

        return view('admin.content.edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required|string',
            'description' => 'required',
            'image' => 'nullable|image|mimes:jpeg,png,jpg|max:512',
        ]);

        $article = Content::query()->where('id', $id)->first();

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = time() . '.' . $image->getClientOriginalExtension();

            $img = Image::make($image->getRealPath());
            $img->resize(null, 300, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->stream();

            Storage::disk('local')->put('public/images/article/' . $imageName, $img, 'public');

            $article->image = $imageName;
        }

        $article->title = $request->title;
        $article->description = $request->get("description");
        $article->save();

        return redirect('admin/content');
    }

    public function destroy($id)
    {
        $article = Content::query()->where('id', $id)->firstOrFail();
        $article->delete();

        return redirect('/admin/content');
    }

    protected function redirectURL($message)
    {
        return redirect('/admin/content')->with($message);
    }
}
