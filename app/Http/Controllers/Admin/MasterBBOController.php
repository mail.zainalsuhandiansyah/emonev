<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Models\MasterBBO;
use App\Models\Zatactive;
use Illuminate\Support\Facades\Hash;
use DataTables;
class MasterBBOController extends Controller
{
    public function index(){

        $traderId = Auth::user()->trader_id;
        $page_title = 'Master Bahan Baku Obat';
        $list = DB::connection('mysql3')->table('m_bahanobat_elic')
                                        ->select('OBAT_ID','CAS_NUMBER','NAMA_BAHAN') //HS_CODE,ORIGIN_COUNTRY
                                        // ->where('TRADER_ID' , $traderId)
                                        ->get();
        return view('admin.industri.masterBBO', compact('page_title','list'));
    }

    public function store(Request $request){
        $data = $request->all();
        $userId = Auth::user()->users_id;
        $bValid = true;
        $now = Carbon::now('utc')->toDateTimeString();
        $message = 'An error occurred, please try again later';
        
        $obatid = $data['obatid'];
        $array = explode(",",$obatid);
        $array = explode("'",$obatid);
        // $whereObatid = array();
        // var_dump($array);die();
        $listobat = DB::connection('mysql3')->table('m_bahanobat_elic')
                                        ->select('OBAT_ID','CAS_NUMBER','NAMA_BAHAN') //HS_CODE,ORIGIN_COUNTRY
                                        ->whereIn('OBAT_ID' , $array)
                                        ->get();


        if(Count($listobat) == 0){ 
            $bValid = false;
            $message = 'Data Tidak ditemukan !';
        }
        
        if($bValid===true){
            $allintests = [];
            for ($i=0; $i <Count($listobat) ; $i++) { 
                $intestcat = new Zatactive();
                $intestcat->obat_id = $listobat[$i]->OBAT_ID;
                $intestcat->zat_active_name = $listobat[$i]->NAMA_BAHAN;
                $intestcat->cas_number = $listobat[$i]->CAS_NUMBER;
                $intestcat->created_by = $userId;
                $allintests[] = $intestcat->attributesToArray();
            }
            
            $response = Zatactive::insert($allintests);
            if($response===true){
                $return  = array('success'=>true,'message'=>$message);
            }else{
                $return  = array('success'=>false,'message'=>$message);    
            }
        }else{
            $return  = array('success'=>false,'message'=>$message);
        }

        echo json_encode($return);
    }

}
