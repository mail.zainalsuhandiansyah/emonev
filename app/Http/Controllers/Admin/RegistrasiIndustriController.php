<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Models\Registrasiindustri;
use Illuminate\Support\Facades\Hash;
use DataTables;

class RegistrasiIndustriController extends Controller
{
    public function index()
    {
        $page_title = 'Registrasi Industri';
        $list = Registrasiindustri::select('company_id','company_users_elic','company_name','company_npwp','company_address')->get();
        return view('admin.industri.registrasi_industri', compact('page_title','list'));
    }

    public function store(Request $request){
        $data = $request->all();
        $bValid = true;
        $message = '';
        // return $data['password'];
        // mengambil data dari elicensing
        $a = DB::connection('mysql2')->table('t_user')
                                        ->join('m_trader' , 't_user.trader_id' , 'm_trader.trader_id')
                                        ->where('m_trader.npwp_ori' , $data['company_npwp'])
                                        ->where('t_user.username' , $data['company_users_elic'])
                                        ->get();
        
        if(Count($a) == 0){ 
            $bValid = false;
            $message = 'Perusahaan Anda Tidak Terdaftar di Elicensing';
        }

        // jika datanya ada makan masukan ke tabel perusahaan
        $c = DB::table('emonev_company')->where('company_npwp' , $data['company_npwp'])
                                    ->where('company_users_elic' , $data['company_users_elic'])
                                    ->get();
        // validasi data nya sudah tersedia atau belum pada emonev bbo
        // jika ada akan ditolak registrasinya
        if(Count($c) != 0){
            $bValid = false;
            $message = 'Perusahaan Anda Sudah Terdaftar Pada Sistem ini';
        }

        if($bValid===true){
            
            $password 		= Hash::make($data['company_password']);
            $username	  	= $data['company_users_elic'];
            $name 			= $data['company_name'];
                // return $password;
            $c = DB::connection('mysql2')->table('t_user')
                                        ->join('m_trader' , 't_user.trader_id' , 'm_trader.trader_id')
                                        ->where('m_trader.npwp_ori' , $data['company_npwp'])
                                        ->where('t_user.username' , $data['company_users_elic'])
                                        ->first();
            $d = DB::connection('mysql2')->table('t_if_izin')
                                            ->select('NO_IZIN' , 'TGL_IZIN')
                                            ->where('trader_id' ,  $c->TRADER_ID)
                                            ->orderby('id' , 'asc')
                                            ->first();
            
            $noIzinAwal 	= isset($d->NO_IZIN)?$d->NO_IZIN:'';
            $tglIzinAwal 	= isset($d->TGL_IZIN)?$d->TGL_IZIN:Carbon::now();

            $d = DB::connection('mysql2')->table('t_if_izin')
                                            ->select('NO_IZIN' , 'TGL_IZIN')
                                            ->where('trader_id' ,  $c->TRADER_ID)
                                            ->orderby('id' , 'desc')
                                            ->first();

            $noIzinAkhir 	= isset($d->NO_IZIN)?$d->NO_IZIN:'';
            $tglIzinAkhir 	= isset($d->TGL_IZIN)?$d->TGL_IZIN:Carbon::now();
                                            
            // dd($c);
            // INSERT TO CMS_USER
            $insert = [];
            $insert['fullname'] = $name;
            $insert['photo'] 	= 'uploads/img/user.png';
            $insert['username']	= $username;
            $insert['password']	= $password;
            $insert['email'] 	= $c->EMAIL;
            $insert['trader_id'] = $c->TRADER_ID;
            $insert['privileges_id'] = 3;
            $insert['created_at']	= Carbon::now();
            $insert['created_by']	= 1;
            $insert['status']   = 1;

            DB::table('emonev_users')->insert($insert);
            $a = DB::table('emonev_users')->orderby('users_id' , 'desc')->first();
            $insert = [];
            $insert['company_users_elic']= $username;
            $insert['company_name']		= $c->NAMA;
            $insert['company_npwp']		= $c->NPWP_ORI;
            $insert['company_address']	= $c->ALAMAT_FULL;
            $insert['company_phone']	= $c->TELP;
            $insert['company_fax']		= $c->FAX;
            $insert['company_email']	= $c->EMAIL;
            $insert['company_capital']	= $c->PERMODALAN;
            $insert['company_type']	= $c->JENIS_INDUSTRI_IF;
            $insert['permission_first_number']	= $noIzinAwal;
            $insert['permission_first_date'] = $tglIzinAwal;
            $insert['permission_final_number'] = $noIzinAkhir;
            $insert['permission_final_date'] = $tglIzinAkhir;
            $insert['users_id']	= $a->users_id;
            $insert['created_at'] = Carbon::now();
            $insert['created_by'] = $a->users_id;

            DB::table('emonev_company')->insert($insert);

            $return  = array('success'=>true,'message'=>$message);
        }else{
            $return  = array('success'=>false,'message'=>$message);
        }

        echo json_encode($return);
    }

    public function destroy($id){
        $action = Registrasiindustri::where('company_id', $id)->delete();
        $response = response();
        $return = array('success'=>true,'message'=>'Record has been deleted successfully!');
        echo json_encode($return);
    }

    public function show($id){
        $data = Registrasiindustri::where('company_id',$id)->first();
        echo json_encode($data);
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $message = 'An error occurred, please try again later';
        // echo"<pre>";print_r($data);die();
        if($data){
            DB::table('emonev_company')->where('company_id' , $data['company_id'])
                                     ->update([
                                            'company_name'=> $data['company_name'],
                                            'company_npwp'=> $data['company_npwp'],
                                            'company_address'=> $data['company_address'],
                                            'company_phone'=> $data['company_phone'],
                                            'company_fax'=> $data['company_fax'],
                                            'company_capital'=> $data['company_capital'],
                                            'company_type'=> $data['company_type'],
                                            'company_pic'=> $data['company_pic'],
                                            'company_pic_position'=> $data['company_pic_position'],
                                            'company_pic_phone'=> $data['company_pic_phone'],
                                            'company_pic_email'=> $data['company_pic_email'],
                                            'updated_at'=> Carbon::now()
                                        ]);

            $return = array('success'=>true,'message'=>'Data hass been updated!');
        }else{
            $return = array('success'=>false,'message'=>$message);
        }
        echo json_encode($return);
    }
}
