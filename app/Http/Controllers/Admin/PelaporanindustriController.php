<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pelaporan;
use App\Models\MasterBBO;
use App\Models\Zatactive;
use App\Models\Year;
use App\Models\Triwulan;
use App\Models\Roadmap;
use Auth;
use DB;
use Carbon\carbon;
use Illuminate\Support\Facades\Auth as FacadesAuth;

class PelaporanindustriController extends Controller
{
    public function pelaporanIndustri() // index
    {
        $data = [];
        $data['page_title'] = 'Pelaporan Progress';
        $data['sub_tittle']  = 'Bahan Baku';

        $this->kelola_bahan_baku();

        $data['pelaporan'] = Pelaporan::where('users_id', Auth::user()->users_id)->get();
        foreach ($data['pelaporan']  as $key => $value) {
            $this->transfer_from_roadmap(Auth::user()->users_id, $value->zat_active_id, $value->reporting_id);
        }
        // $data['triwulan'] = Triwulan::query()->get();

        return view('admin.pelaporan.industri.index', $data);
    }

    public function kelola_bahan_baku()
    {
        $MasterBBO = MasterBBO::where('users_id', Auth::user()->users_id)->get();

        foreach ($MasterBBO as $key => $value) {

            if (Pelaporan::where('users_id', $value->users_id)->where('zat_active_id', $value->zat_active_id)->doesntExist()) {
                $laporan = new Pelaporan;
                $laporan->users_id = $value->users_id;
                $laporan->zat_active_id = $value->zat_active_id;
                $laporan->zat_active_name = $value->zat_active_name;
                $laporan->created_by = Auth::user()->users_id;
                $laporan->save();
            }
        }
    }

    public function getdetail($id)
    {
        $data['page_title'] = 'Detail Pelaporan';
        $data['reportingID'] = $id;
        $pelaporan = Pelaporan::where('reporting_id', $id)->first();

        $data['zatActiveName'] = DB::table('emonev_reporting')->where('reporting_id', $id)->select('zat_active_id', 'zat_active_name')->first();
        $data['target']             = DB::table('emonev_target_hilirisasi')
            ->join('emonev_year', 'emonev_target_hilirisasi.year_id', 'emonev_year.year_id')
            ->select('emonev_year.year_name', 'emonev_year.year_id')
            ->where('users_id', Auth::user()->users_id)
            ->where('zat_active_id', $pelaporan->zat_active_id)
            ->groupby('emonev_year.year_id', 'emonev_year.year_name')
            ->get();
        $this->transfer_from_roadmap(Auth::user()->users_id, $pelaporan->zat_active_id, $pelaporan->reporting_id);

        $data['triwulan']           = DB::table('emonev_triwulan')->get();
        $data['reportingDetail']    = DB::table('emonev_reporting_detail')->where('reporting_id', $pelaporan->reporting_id)->get();
        $data['reportingSubDetail']    = DB::table('emonev_reporting_subdetail')->where('reporting_id', $pelaporan->reporting_id)->get();
        $data['qna']            = DB::table('emonev_reporting_qna')
            ->Join('emonev_users', 'emonev_reporting_qna.users_id', 'emonev_users.users_id')
            ->where('reporting_id', $pelaporan->reporting_id)->get();

        // transfer data from roadmap to reporting detail



        return view('admin.pelaporan.industri.detail.index', $data);
    }

    public function detail_pelaporan_getYear($id)
    {
        // dd($id);
        $a = Pelaporan::where('reporting_id', $id)->first();

        $data['year']     = Roadmap::Select('emonev_roadmap.year_id', 'emonev_year.year_name')
            ->join('emonev_year', 'emonev_year.year_id', 'emonev_roadmap.year_id')
            ->where('users_id', Auth::user()->users_id)
            ->where('emonev_roadmap.zat_active_id', $a->zat_active_id)
            ->groupby('emonev_roadmap.year_id', 'emonev_year.year_name')
            ->get();

        $data['zatactiveid'] = $a->zat_active_id;

        // $data['triwulan']     = Roadmap::Select('emonev_roadmap.triwulan_id', 'emonev_triwulan.triwulan_description')
        //     ->join('emonev_triwulan', 'emonev_triwulan.triwulan_id', 'emonev_roadmap.triwulan_id')
        //     ->where('users_id', Auth::user()->users_id)
        //     ->groupby('emonev_roadmap.triwulan_id', 'emonev_triwulan.triwulan_description')
        //     ->get();


        echo json_encode($data);
    }
    public function detail_pelaporan_getTw($yearid, $zatactiveid)
    {

        $data['triwulan']     = Roadmap::Select('emonev_roadmap.triwulan_id', 'emonev_triwulan.triwulan_description')
            ->join('emonev_triwulan', 'emonev_triwulan.triwulan_id', 'emonev_roadmap.triwulan_id')
            ->where('users_id', Auth::user()->users_id)
            ->where('emonev_roadmap.year_id', $yearid)
            ->where('emonev_roadmap.zat_active_id', $zatactiveid)
            ->groupby('emonev_roadmap.triwulan_id', 'emonev_triwulan.triwulan_description')
            ->get();


        echo json_encode($data);
    }

    public function get_roadmap($zatactiveid, $yearid, $twid)
    {
        $data = Roadmap::where('year_id', $yearid)
            ->where('triwulan_id', $twid)
            ->where('zat_active_id', $zatactiveid)->get();

        echo json_encode($data);
    }

    public function detail_pelaporan_getLangkah($zatactiveid, $yearid, $twid)
    {
        $roadmap = DB::table('emonev_roadmap')
            ->where('zat_active_id', $zatactiveid)
            ->where('year_id', $yearid)
            ->where('triwulan_id', $twid)
            ->where('users_id', Auth::user()->users_id)
            ->first();

        $data = DB::table('emonev_roadmap_detail')
            ->select('roadmap_detail_id', 'roadmap_description')
            ->where('roadmap_id', $roadmap->roadmap_id)
            ->orderby('roadmap_detail_id', 'asc')
            ->get();
        echo json_encode($data);
    }

    public function store_to_pelaporan($zatactiveid, $zatactivename,  $status, $userid)
    {
        if (Pelaporan::where('users_id', $userid)
            ->where('zat_active_id', $zatactiveid)
            ->doesntExist()
        ) {

            $laporan = new Pelaporan;
            $laporan->users_id          = $userid;
            $laporan->zat_active_id     = $zatactiveid;
            $laporan->zat_active_name   = $zatactivename;
            $laporan->status_id            =  $status; // baru
            $laporan->created_by        = $userid;
            $laporan->save();
        }
    }

    public function transfer_from_roadmap($userid, $zatActiveId, $reportingID)
    {
        $roadmap = DB::table('emonev_roadmap')
            ->where('users_id', $userid)
            ->where('zat_active_id', $zatActiveId)
            ->get();

        foreach ($roadmap as $key => $value) {

            $this->store_to_pelaporan($value->zat_active_id, $value->zat_active_name, 1, $value->users_id);
            if (DB::table('emonev_reporting_detail')->where('roadmap_id', $value->roadmap_id)->doesntExist()) {
                $insert = [];
                $insert['reporting_id']         = $reportingID;
                $insert['roadmap_id']           = $value->roadmap_id;
                $insert['year_id']              = $value->year_id;
                $insert['triwulan_id']          = $value->triwulan_id;
                $insert['roadmap_title']        = $value->roadmap_title;
                $insert['status_id']            = 1;
                $insert['created_at']           = $value->created_at;
                $insert['created_by']           = $value->created_by;

                DB::table('emonev_reporting_detail')->insert($insert);
            }

            $this->transfer_from_roadmap_detail($value->roadmap_id, $reportingID);
        }
    }

    public function transfer_from_roadmap_detail($roadmapID, $reportingID)
    {
        $reportingDetail = DB::table('emonev_reporting_detail')->where('roadmap_id', $roadmapID)->first();
        $roadmap_detail = DB::table('emonev_roadmap_detail')->where('roadmap_id', $roadmapID)->get();
        foreach ($roadmap_detail as $key => $value) {

            if (DB::table('emonev_reporting_subdetail')->where('roadmap_detail_id', $value->roadmap_detail_id)->doesntExist()) {
                $insert = [];
                $insert['reporting_id']         = $reportingID;
                $insert['reporting_detail_id']  = $reportingDetail->reporting_detail_id;
                $insert['roadmap_detail_id']    = $value->roadmap_detail_id;
                $insert['roadmap_description']  = $value->roadmap_description;
                $insert['status_id']             = 1;
                $insert['created_by']           = $value->created_by;
                $insert['created_at']              = $value->created_at;

                DB::table('emonev_reporting_subdetail')->insert($insert);
            }
        }
    }

    public function store_answer(Request $request)
    {
        $data = $request->all();
        // dd($data);
        if ($data) {

            $insert = [];
            $insert['reporting_id']             = $data['reporting_id'];
            $insert['reporting_detail_id']      = $data['reporting_detail_id'];
            $insert['reporting_subdetail_id']   = $data['reporting_subdetail_id'];
            $insert['users_id']                 = Auth::user()->users_id;
            $insert['qna']                      = 'Jawaban';
            $insert['qna_description']          = $data['chat'];
            $insert['qna_flag']                 = '2';
            $insert['created_by']                = Auth::user()->users_id;
            $insert['created_at']                = Carbon::now();

            DB::table('emonev_reporting_qna')->insert($insert);
        }


        return response()->json(['success' => 'Ajax request submitted successfully']);
    }

    public function getRoadmapbahanBaku()
    {
        $data = [];
        $data['bahan_baku'] = DB::table('emonev_material')->where('users_id', Auth::user()->users_id)->get();

        return view('admin.report.industri.roadmap_perbahanbaku.index', $data);
    }

    public function DropZoneStore(Request $request)
    {
        dd($request->all());
        return redirect()->back();
    }
}
