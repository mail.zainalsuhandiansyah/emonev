<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\Zatactivegroup;
use DataTables;

class ZatgroupController extends Controller
{
    public function index()
    {
        $page_title = 'Jenis Bahan Baku Obat';
        $list = Zatactivegroup::select('zat_group_id','zat_group_description','zat_group_order')->get();
        return view('admin.setting.zatactivegroup', compact('page_title','list'));
    }

    public function store(Request $request){
        $data = $request->all();
        $bValid = true;
        $message = 'An error occurred, please try again later';
        
        if($bValid===true){
            
            $privileges = new Zatactivegroup();
            $privileges->zat_group_description = $request->zat_group_description;
            $privileges->zat_group_order = $request->zat_group_order;
            $privileges->created_at = Carbon::now();
            $privileges->created_by = 1;
            $response = $privileges->save();
            if($response===true){
                $return  = array('success'=>true,'message'=>$message);
            }else{
                $return  = array('success'=>false,'message'=>$message);    
            }
        }else{
            $return  = array('success'=>false,'message'=>$message);
        }

        echo json_encode($return);
    }

    public function destroy($id){
        $action = Zatactivegroup::where('zat_group_id', $id)->delete();
        $response = response();
        $return = array('success'=>true,'message'=>'Record has been deleted successfully!');
        echo json_encode($return);
    }

    public function show($id){
        $data = Zatactivegroup::where('zat_group_id',$id)->first();
        echo json_encode($data);
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $message = 'An error occurred, please try again later';
        // echo"<pre>";print_r($data);die();
        if($data){
            DB::table('emonev_zat_group')->where('zat_group_id' , $data['zat_group_id'])
                                     ->update([
                                         'zat_group_description' => $data['zat_group_description'],
                                         'zat_group_order'=> $data['zat_group_order'],
                                         'updated_at'=> Carbon::now()
                                     ]);
            $return = array('success'=>true,'message'=>'Data hass been updated!');
        }else{
            $return = array('success'=>false,'message'=>$message);
        }
        echo json_encode($return);
    }
}
