<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use \PDF;
use DB;
use Auth;

class DashboardindustriController extends Controller
{
    public function dashboardIndustri(Request $request)
    {
        $data['page_title'] = 'Dashboard Industri';
        $data['tahun']      = DB::table('emonev_roadmap')
            ->select('emonev_year.year_id as yearID', 'emonev_year.year_name as yearName')
            ->join('emonev_year', 'emonev_roadmap.year_id', 'emonev_year.year_id')
            ->where('users_id', Auth::user()->users_id)
            ->groupby('emonev_year.year_id', 'emonev_year.year_name')
            ->get();

        $data['triwulan'] = DB::table('emonev_triwulan')
            ->get();

        $data['produk']  = DB::table('emonev_roadmap')
            ->select('zat_active_id', 'zat_active_name', 'year_id')
            ->where('users_id', Auth::user()->users_id)
            ->groupby('zat_active_id', 'zat_active_name', 'year_id')
            ->get();

        // dd($data);
        return view('admin.dashboard.industri.index', $data);
    }

    public function printBBO()
    {
        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('admin.report.printoutbbo');
        $pdf->setPaper('legal', 'landscape');
        return $pdf->stream('Laporan.pdf');
    }

    public function get_progress()
    {
        $a = DB::table('emonev_reporting_detail')->get();

        return response()->json($a, 200);
    }
}
