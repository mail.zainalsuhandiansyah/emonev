<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class ProfileindustriController extends Controller
{

    public function profileindustri()
    {
        $page_title = 'Profile Industri';
        $userId = Auth::user()->users_id;
        $listBBO = DB::table('emonev_material')
                    ->select('emonev_material.*')
                    ->where('emonev_material.users_id',$userId)
                    ->get();
        
        $profileCompany = DB::table('emonev_company as cp')
                    ->select('cp.*')
                    ->where('cp.users_id',$userId)
                    ->first();
        return view('admin.profile.industri.index', compact('page_title','listBBO','profileCompany'));
    }

    public function bahanbaku()
    {
        $page_title = 'Profile Bahan Baku';

        return view('admin.profile.industri.profile_bahanbaku', compact('page_title'));
    }

    public function profilebahanbaku()
    {
        $page_title = 'Profile Bahan Baku';
        // $page_title = '';
        return view('pages.profilebahanbaku', compact('page_title'));
    }
}
