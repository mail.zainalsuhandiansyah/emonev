<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Models\Zatactive;
use App\Models\MasterBBO;
use DataTables;

class MasterBBOIndustriController extends Controller
{
    public function index()
    {
        $userId = Auth::user()->users_id;
        $page_title = 'Master Bahan Baku Obat yang Dikembangkan';
        $jenisbb = DB::table('emonev_zat_group')
            ->select('emonev_zat_group.*')
            ->get();

        $bbo = DB::table('emonev_zat_active')
            ->join('emonev_zat_group', 'emonev_zat_active.zat_group_id', '=', 'emonev_zat_group.zat_group_id')
            ->select('emonev_zat_active.zat_active_name', 'emonev_zat_active.zat_active_id', 'emonev_zat_group.zat_group_description')
            ->get();

        $bboElic = DB::connection('mysql3')->table('m_bahanobat_elic')
            ->select('OBAT_ID', 'NAMA_BAHAN') //HS_CODE,ORIGIN_COUNTRY
            // ->where('TRADER_ID' , $traderId)
            ->get();
        $tahun = DB::table('emonev_year')
            ->select('*')
            ->where('year_active', '1')
            ->orderBy('year_name', 'ASC')
            ->get();
        $triwulan = DB::table('emonev_triwulan')
            ->select('*')
            ->get();

        return view('admin.industri.masterBBOIndustri', compact('page_title', 'jenisbb', 'bbo', 'bboElic', 'tahun', 'triwulan'));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $userId = Auth::user()->users_id;
        $bValid = true;
        $now = Carbon::now('utc')->toDateTimeString();
        $message = 'An error occurred, please try again later';

        $obatid = $data['obatid'];
        $array = explode(",", $obatid);
        $array = explode("'", $obatid);
        // $whereObatid = array();
        // var_dump($array);die();
        $listobat = DB::table('emonev_zat_active')
            ->select('zat_active_id', 'zat_active_name', 'cas_number') //HS_CODE,ORIGIN_COUNTRY
            ->whereIn('zat_active_id', $array)
            ->get();


        if (Count($listobat) == 0) {
            $bValid = false;
            $message = 'Data Tidak ditemukan !';
        }

        $exist = 0;
        $existMessage = 'Data Sudah Ada : <br>';
        $allintests = array();
        if(count($listobat)>0){
            //check ke table emonev_material
            for ($i=0; $i <Count($listobat) ; $i++) { 
                $check = DB::table('emonev_material')->where('users_id',$userId)->where('zat_active_id',$listobat[$i]->zat_active_id)->count();
                if($check>0){
                    $exist++;      
                    $existMessage .=' - '.$listobat[$i]->zat_active_name.' <br>';
                }else{
                    $allintests[] = array(
                        'zat_active_id'=>$listobat[$i]->zat_active_id,
                        'zat_active_name'=>$listobat[$i]->zat_active_name,
                        'cas_number'=>isset($listobat[$i]->cas_number)?$listobat[$i]->cas_number:'',
                        'users_id'=>$userId,
                        'created_by'=>$userId,
                        'created_at'=>$now
                    );
                }
            }
        }

        if($bValid===true){
            $response = true;
            if(count($allintests)>0){
                $response = MasterBBO::insert($allintests);
            }
            if($response===true){
                $return  = array('success'=>true,'message'=>$message,'existmessage'=>$existMessage);
            }else{
                $return  = array('success'=>false,'message'=>$message);    
            }
        } else {
            $return  = array('success' => false, 'message' => $message);
        }

        echo json_encode($return);
    }

    public function destroy($id)
    {
        $action = MasterBBO::where('material_id', $id)->delete();
        $response = response();
        $return = array('success' => true, 'message' => 'Record has been deleted successfully!');
        echo json_encode($return);
    }

    public function show($id)
    {
        $data = Zatactive::where('zat_active_id', $id)->first();
        echo json_encode($data);
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $userId = Auth::user()->users_id;
        $message = 'An error occurred, please try again later';
        // echo"<pre>";print_r($data);die();
        if ($data) {
            DB::table('emonev_zat_active')->where('zat_active_id', $data['zat_active_id'])
                ->update([
                    'zat_active_name' => $data['zat_active_name'],
                    'zat_group_id' => $data['zat_group_id'],
                    'zat_active_fornas' => $data['zat_active_fornas'],
                    'updated_at' => Carbon::now(),
                    'updated_by' => $userId
                ]);
            $return = array('success' => true, 'message' => 'Data hass been updated!');
        } else {
            $return = array('success' => false, 'message' => $message);
        }
        echo json_encode($return);
    }

    public function listBBOIndustri(Request $request)
    {
        $userId = Auth::user()->users_id;
        $data = $request->all();
        $keyword = isset($data['query']['generalSearch']) ? $data['query']['generalSearch'] : '';

        $sql = 'SELECT * FROM emonev_material a Join emonev_zat_active b on a.zat_active_id = b.zat_active_id join emonev_zat_group c on b.zat_group_id = c.zat_group_id where users_id = ' . $userId . '';
        if ($keyword != '') {
            $keyword = strtoupper(strtolower($keyword));
            $sql .= " AND (UPPER(zat_active_name) like '%" . $keyword . "%' or UPPER(cas_number) like '%" . $keyword . "%')";
        }
        $list = DB::select($sql);
        // $list = DB::table('emonev_material')
        //             ->select('emonev_material.*')
        //             ->where('emonev_material.users_id',$userId)
        //             ->get();
        $return = response()->json($list, 200);
        return $return;
    }
}
