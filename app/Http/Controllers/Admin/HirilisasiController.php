<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\Hirilisasi;
use App\Models\Zatactive;
use DataTables;

class HirilisasiController extends Controller
{

    public function index()
    {
        $page_title = 'Input Hirilisasi';

        // $list = Hirilisasi::select('hirilisasi_id','zat_active_id')->get();

        $list = DB::table('emonev_hirilisasi')
            ->join('emonev_zat_active', 'emonev_zat_active.zat_active_id', '=' , 'emonev_hirilisasi.zat_active_id')
            ->select('emonev_zat_active.zat_active_name')
            ->get();

            // $dYear = DB::table('emonev_target_bbo')
            //                 ->join('emonev_year','emonev_year.year_id','=','emonev_target_bbo.target_bbo_year')
            //                 ->select('emonev_year.year_id','emonev_year.year_name')
            //                 ->groupBy('emonev_year.year_id','emonev_year.year_name')
            //                 ->get();

        return view('admin.dashboard.evaluator.hirilisasi.index', compact('page_title','list'));
    }

    public function create()
    {
        $getzats = Zatactive::select('zat_active_id', 'zat_active_name')->get();
        return view('admin.dashboard.evaluator.hirilisasi.create', compact('getzats') );
    }

    public function show($id){
        $data = Hirilisasi::query()->where('hirilisasi_id', $id)->first();

        return view('admin.dashboard.evaluator.hirilisasi.edit', compact('data'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'zat_active_id' => 'required|string',
            // 'year_active' => 'required|string',
  
        ]);

        $hirilisasi = new Hirilisasi(); 
        $hirilisasi->zat_active_id = $request->zat_active_id;
        
        // $hirilisasi->created_by = "1";
   
        $hirilisasi->save();

        // dd("HIRILISASI", $hirilisasi);
        

        return redirect('/admin/hirilisasi');

    }

    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'year_name' => 'required|string',
            'year_active' => 'required|string',
            // 'tempat' => 'required|string',
            // 'kota' => 'required|string',
            // 'kodepos' => 'required|int',
            // 'nama_callcenter' => 'required|string',
            // 'notelp' => 'required|int',
            // 'image' => 'nullable|image|mimes:jpeg,png,jpg|max:512',
        ]);

        $hirilisasi = Hirilisasi::query()->where('id', $id)->first();

        // if ($request->hasFile('image')) {
        //     $image = $request->file('image');
        //     $imageName = time() . '.' . $image->getClientOriginalExtension();

        //     $img = Image::make($image->getRealPath());
        //     $img->resize(null, 300, function ($constraint) {
        //         $constraint->aspectRatio();
        //     });

        //     $img->stream();

        //     Storage::disk('local')->put('public/images/Tahun/' . $imageName, $img, 'public');

        //     $hirilisasi->image = $imageName;
        // }

        $hirilisasi->year_name = $request->year_name;
        $hirilisasi->year_active = $request->year_active;
        // $hirilisasi->tempat = $request->tempat;
        // $hirilisasi->kota = $request->kota;
        // $hirilisasi->kodepos = $request->kodepos;
        // $hirilisasi->nama_callcenter = $request->nama_callcenter;
        // $hirilisasi->notelp = $request->notelp;
        $hirilisasi->save();

        return redirect('admin/hirilisasi');
    }

    public function destroy($id){

        $hirilisasi = Hirilisasi::query()->where('id', $id)->firstOrFail();
        $hirilisasi->delete();

        return redirect('/admin/hirilisasi');
    }

    protected function redirectURL($message)
    {
        return redirect('/admin/hirilisasi')->with($message);
    }





}