<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Settingtahun;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use DataTables;
use Validator;
Use Response;
Use File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class SettingtahunController extends Controller
{

    // public function index()
    // {
    //     $page_title = 'Tahun';
    //     $list = Settingtahun::select('id','nama_perusahaan','alamat_perusahaan','kota','kodepos','notelp')->get();

    //     return view('admin.setting.settahun.index', compact('page_title', 'list'));
    // }
    public function index()
    {
        $page_title = 'Tahun';
        $search = request("q", "");

        $list = Settingtahun::query()
            ->where(function ($query) use ($search) {
                if (!empty($search) || $search !== "")
                    $query->where("year_name", "LIKE", "%$search%");
            })
            ->get();

            // var_dump($list);die();

        return view('admin.setting.settahun.index', compact('page_title', 'list'));
    }

    public function create()
    {
        return view('admin.setting.settahun.create');
    }

    public function show($id){
        $data = Settingtahun::query()->where('id', $id)->first();

        return view('admin.setting.settahun.edit', compact('data'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'year_name' => 'required|string',
            'year_active' => 'required|string',
            // 'tempat' => 'required|string',
            // 'kota' => 'required|string',
            // 'kodepos' => 'required|int',
            // 'nama_callcenter' => 'required|string',
            // 'notelp' => 'required|int',
            // 'image' => 'nullable|image|mimes:jpeg,png,jpg|max:512',
        ]);

        // $imageName = null;
        // if ($request->hasFile('image')) {
        //     $image = $request->file('image');
        //     $imageName = time() . '.' . $image->getClientOriginalExtension();

        //     $img = Image::make($image->getRealPath());
        //     $img->resize(null, 300, function ($constraint) {
        //         $constraint->aspectRatio();
        //     });

        //     $img->stream();

        //     Storage::disk('local')->put('public/images/Tahun/' . $imageName, $img, 'public');
        // }

        $settahun = new Settingtahun(); 
        $settahun->year_name = $request->year_name;
        $settahun->year_active = $request->year_active;
        $settahun->created_by = "1";
        // $settahun->kota = $request->kota;
        // $settahun->kodepos = $request->kodepos;
        // $settahun->nama_callcenter = $request->nama_callcenter;
        // $settahun->notelp = $request->notelp;
        // $settahun->image = $imageName;
        $settahun->save();

        return redirect('/admin/settahun');

    }

    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'year_name' => 'required|string',
            'year_active' => 'required|string',
            // 'tempat' => 'required|string',
            // 'kota' => 'required|string',
            // 'kodepos' => 'required|int',
            // 'nama_callcenter' => 'required|string',
            // 'notelp' => 'required|int',
            // 'image' => 'nullable|image|mimes:jpeg,png,jpg|max:512',
        ]);

        $settahun = Settingtahun::query()->where('id', $id)->first();

        // if ($request->hasFile('image')) {
        //     $image = $request->file('image');
        //     $imageName = time() . '.' . $image->getClientOriginalExtension();

        //     $img = Image::make($image->getRealPath());
        //     $img->resize(null, 300, function ($constraint) {
        //         $constraint->aspectRatio();
        //     });

        //     $img->stream();

        //     Storage::disk('local')->put('public/images/Tahun/' . $imageName, $img, 'public');

        //     $settahun->image = $imageName;
        // }

        $settahun->year_name = $request->year_name;
        $settahun->year_active = $request->year_active;
        // $settahun->tempat = $request->tempat;
        // $settahun->kota = $request->kota;
        // $settahun->kodepos = $request->kodepos;
        // $settahun->nama_callcenter = $request->nama_callcenter;
        // $settahun->notelp = $request->notelp;
        $settahun->save();

        return redirect('admin/settahun');
    }

    public function destroy($id){

        $settahun = Settingtahun::query()->where('id', $id)->firstOrFail();
        $settahun->delete();

        return redirect('/admin/settahun');
    }

    protected function redirectURL($message)
    {
        return redirect('/admin/settahun')->with($message);
    }
    
}
