<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use PDF;
use Auth;

class CetakEvaluator extends Controller
{
    public function cetak1(Request $request)
    {
        $data = $request->all();
        if ($data) {
            // dd($data);

            $data['reporting'] = DB::table('emonev_reporting')->where('users_id', $data['userID'])->get();
            $reportingData = [];
            foreach ($data['reporting'] as $key => $value) {
                $reportingData['reporID'][] = $value->reporting_id;
                $reportingData['zatactiveID'][] = $value->zat_active_id;
            }

            $data['tahapan'] = DB::table('emonev_reporting_detail')
                ->select('roadmap_title')
                ->whereIn('reporting_id', $reportingData['reporID'])
                ->where('emonev_reporting_detail.year_id', $data['year_id'])
                ->groupby('roadmap_title')
                ->orderby('reporting_detail_id', 'Asc')
                ->get();

            $data['progress'] = DB::table('emonev_reporting')
                ->select('emonev_reporting.reporting_id', 'emonev_reporting.zat_active_id', 'reporting_detail_id', 'emonev_reporting_detail.roadmap_title', 'emonev_reporting_detail.status_id')
                ->join('emonev_reporting_detail', 'emonev_reporting.reporting_id', 'emonev_reporting_detail.reporting_id')
                ->get();
            // dd($data);
        }

        $view = view('admin.report.progress-industri', $data)->render();
        $pdf = PDF::loadHTML($view)->setPaper('legal', 'landscape');

        return $pdf->stream();
    }

    public function cetak2($userID)
    {
        $data['perusahaan'] = DB::table('emonev_company')->where('users_id', $userID)->first();

        $data['tahun2'] = DB::table('emonev_roadmap')
            ->select('emonev_year.year_id as yearID', 'emonev_year.year_name as yearName')
            ->join('emonev_year', 'emonev_roadmap.year_id', 'emonev_year.year_id')
            ->where('users_id', $userID)
            ->groupby('emonev_year.year_id', 'emonev_year.year_name')
            ->get();

        $data['triwulan'] = DB::table('emonev_triwulan')
            ->get();

        $data['produk']  = DB::table('emonev_roadmap')
            ->select('zat_active_id', 'zat_active_name', 'year_id')
            ->where('users_id', $userID)
            ->groupby('zat_active_id', 'zat_active_name', 'year_id')
            ->get();

        $a = DB::table('emonev_reporting_detail')
            ->select('zat_active_id', 'emonev_reporting_detail.year_id as yearID', 'emonev_reporting_detail.triwulan_id as twID', 'emonev_reporting_detail.roadmap_title as roadmapTitle', 'emonev_reporting_detail.status_id')
            ->join('emonev_roadmap', 'emonev_reporting_detail.roadmap_id', 'emonev_roadmap.roadmap_id')
            ->get();

        $b = [];
        foreach ($a as $key => $value) {
            $b[$value->zat_active_id][$value->yearID][$value->twID][$key]['yearID']          = $value->yearID;
            $b[$value->zat_active_id][$value->yearID][$value->twID][$key]['twID']          = $value->twID;
            $b[$value->zat_active_id][$value->yearID][$value->twID][$key]['roadmapTitle']    = $value->roadmapTitle;
            $b[$value->zat_active_id][$value->yearID][$value->twID][$key]['statusID']       = $value->status_id;
            $b[$value->zat_active_id][$value->yearID][$value->twID][$key]['zatActiveID']    = $value->zat_active_id;
        }


        $data['progress'] = $b;

        $view = view('admin.report.progress-industri', $data)->render();
        $pdf = PDF::loadHTML($view)->setPaper('legal', 'landscape');

        return $pdf->stream();
    }

    public function cetak_roadmap1()
    {
        $data = [];
        // $data = DB::table('emonev_roadmap')
        //     ->select('emonev_target_hilirisasi.zat_active_id', 'emonev_zat_active.zat_active_name', DB::RAW('emonev_target_hilirisasi.year_id'))
        //     ->join('emonev_zat_active', 'emonev_roadmap.zat_active_id', 'emonev_zat_active.zat_active_id')
        //     ->join('emonev_zat_group', 'emonev_roadmap.zat_group_id', 'emonev_zat_group.zat_group_id')
        //     ->groupby('emonev_target_hilirisasi.zat_active_id', 'emonev_zat_active.zat_active_name')
        //     ->get();

        $q = DB::table('emonev_target_hilirisasi')
            ->select(
                'emonev_target_hilirisasi.zat_active_id',
                'emonev_zat_active.zat_active_name',
                'emonev_year.year_name',
                'emonev_year.year_id',
                DB::RAW('MAX(emonev_target_hilirisasi.year_id)'),
                'emonev_zat_group.zat_group_id'
            )
            ->join('emonev_zat_active', 'emonev_target_hilirisasi.zat_active_id', 'emonev_zat_active.zat_active_id')
            ->join('emonev_zat_group', 'emonev_zat_active.zat_group_id', 'emonev_zat_group.zat_group_id')
            ->join('emonev_year', 'emonev_target_hilirisasi.year_id', 'emonev_year.year_id')
            ->groupby('emonev_target_hilirisasi.zat_active_id', 'emonev_zat_active.zat_active_name', 'emonev_zat_group.zat_group_id', 'emonev_year.year_name', 'emonev_year.year_id')
            ->get();

        $b = [];
        foreach ($q as $key => $value) {
            $b[$value->year_id][$value->zat_group_id][$key]['zat_active_name'] = $value->zat_active_name;
        }
        // dd($b);
        $data['roadmap'] = $b;

        // dd($data);

        $data['tahun']  = DB::table('emonev_roadmap')
            ->select('emonev_roadmap.year_id', 'emonev_year.year_name')
            ->join('emonev_year', 'emonev_roadmap.year_id', 'emonev_year.year_id')
            ->groupby('emonev_roadmap.year_id', 'emonev_year.year_name')
            ->get();

        $data['GroupZat'] = DB::table('emonev_zat_group')->get();

        // dd($data);

        $view = view('admin.report.print-roadmap-all', $data)->render();
        $pdf = PDF::loadHTML($view)->setPaper('legal', 'landscape');

        return $pdf->stream();
    }

    public function cetak_roadmap2()
    {
        $data = [];
        // $data = DB::table('emonev_roadmap')
        //     ->select('emonev_target_hilirisasi.zat_active_id', 'emonev_zat_active.zat_active_name', DB::RAW('emonev_target_hilirisasi.year_id'))
        //     ->join('emonev_zat_active', 'emonev_roadmap.zat_active_id', 'emonev_zat_active.zat_active_id')
        //     ->join('emonev_zat_group', 'emonev_roadmap.zat_group_id', 'emonev_zat_group.zat_group_id')
        //     ->groupby('emonev_target_hilirisasi.zat_active_id', 'emonev_zat_active.zat_active_name')
        //     ->get();

        $q = DB::table('emonev_target_hilirisasi')
            ->select(
                'emonev_target_hilirisasi.zat_active_id',
                'emonev_zat_active.zat_active_name',
                'emonev_users.fullname',
                'emonev_year.year_name',
                'emonev_year.year_id',
                DB::RAW('MAX(emonev_target_hilirisasi.year_id)'),
                'emonev_zat_group.zat_group_id'
            )
            ->join('emonev_zat_active', 'emonev_target_hilirisasi.zat_active_id', 'emonev_zat_active.zat_active_id')
            ->join('emonev_zat_group', 'emonev_zat_active.zat_group_id', 'emonev_zat_group.zat_group_id')
            ->join('emonev_year', 'emonev_target_hilirisasi.year_id', 'emonev_year.year_id')
            ->join('emonev_users', 'emonev_target_hilirisasi.users_id', 'emonev_users.users_id')
            ->groupby(
                'emonev_target_hilirisasi.zat_active_id',
                'emonev_zat_active.zat_active_name',
                'emonev_zat_group.zat_group_id',
                'emonev_year.year_name',
                'emonev_year.year_id',
                'emonev_users.fullname'
            )
            ->get();

        // dd($q);
        $b = [];
        foreach ($q as $key => $value) {
            $b[$value->year_id][$value->zat_group_id][$key]['zat_active_name'] = $value->zat_active_name;
            $b[$value->year_id][$value->zat_group_id][$key]['userName'] = $value->fullname;
        }
        // dd($b);
        $data['roadmap'] = $b;

        // dd($data);

        $data['tahun']  = DB::table('emonev_roadmap')
            ->select('emonev_roadmap.year_id', 'emonev_year.year_name')
            ->join('emonev_year', 'emonev_roadmap.year_id', 'emonev_year.year_id')
            ->groupby('emonev_roadmap.year_id', 'emonev_year.year_name')
            ->get();

        $data['GroupZat'] = DB::table('emonev_zat_group')->get();

        // dd($data);

        $view = view('admin.report.print-roadmap-industri-all', $data)->render();
        $pdf = PDF::loadHTML($view)->setPaper('legal', 'landscape');

        return $pdf->stream();
    }


    public function get_cetak_roadmap3()
    {
        $data = [];
        $data['page_title'] = 'Cetak Roadmap Perindustri';
        $data['industri']   = DB::table('emonev_target_hilirisasi')
            ->select('emonev_target_hilirisasi.users_id', 'emonev_users.fullname')
            ->join('emonev_users', 'emonev_target_hilirisasi.users_id', 'emonev_users.users_id')
            ->groupby('emonev_target_hilirisasi.users_id', 'emonev_users.fullname')
            ->get();

        return view('admin.report.industri.roadmap-perindustri.index', $data);
    }

    public function cetak_roadmap3($usersID)
    {
        $data = [];
        // $data = DB::table('emonev_roadmap')
        //     ->select('emonev_target_hilirisasi.zat_active_id', 'emonev_zat_active.zat_active_name', DB::RAW('emonev_target_hilirisasi.year_id'))
        //     ->join('emonev_zat_active', 'emonev_roadmap.zat_active_id', 'emonev_zat_active.zat_active_id')
        //     ->join('emonev_zat_group', 'emonev_roadmap.zat_group_id', 'emonev_zat_group.zat_group_id')
        //     ->groupby('emonev_target_hilirisasi.zat_active_id', 'emonev_zat_active.zat_active_name')
        //     ->get();

        $q = DB::table('emonev_target_hilirisasi')
            ->select(
                'emonev_target_hilirisasi.zat_active_id',
                'emonev_zat_active.zat_active_name',
                'emonev_users.fullname',
                'emonev_year.year_name',
                'emonev_year.year_id',
                DB::RAW('MAX(emonev_target_hilirisasi.year_id)'),
                'emonev_zat_group.zat_group_id'
            )
            ->join('emonev_zat_active', 'emonev_target_hilirisasi.zat_active_id', 'emonev_zat_active.zat_active_id')
            ->join('emonev_zat_group', 'emonev_zat_active.zat_group_id', 'emonev_zat_group.zat_group_id')
            ->join('emonev_year', 'emonev_target_hilirisasi.year_id', 'emonev_year.year_id')
            ->join('emonev_users', 'emonev_target_hilirisasi.users_id', 'emonev_users.users_id')
            ->where('emonev_target_hilirisasi.users_id', $usersID)
            ->groupby(
                'emonev_target_hilirisasi.zat_active_id',
                'emonev_zat_active.zat_active_name',
                'emonev_zat_group.zat_group_id',
                'emonev_year.year_name',
                'emonev_year.year_id',
                'emonev_users.fullname'
            )
            ->get();

        // dd($q);
        $b = [];
        foreach ($q as $key => $value) {
            $b[$value->year_id][$value->zat_group_id][$key]['zat_active_name'] = $value->zat_active_name;
            $b[$value->year_id][$value->zat_group_id][$key]['userName'] = $value->fullname;
        }
        // dd($b);
        $data['roadmap'] = $b;

        // dd($data);

        $data['tahun']  = DB::table('emonev_roadmap')
            ->select('emonev_roadmap.year_id', 'emonev_year.year_name')
            ->join('emonev_year', 'emonev_roadmap.year_id', 'emonev_year.year_id')
            ->groupby('emonev_roadmap.year_id', 'emonev_year.year_name')
            ->get();

        $data['GroupZat'] = DB::table('emonev_zat_group')->get();
        $users = DB::table('emonev_users')->where('users_id', $usersID)->first();
        $data['nama_perusahaan'] = $users->fullname;

        // dd($data);

        $view = view('admin.report.print-roadmap-industri', $data)->render();
        $pdf = PDF::loadHTML($view)->setPaper('legal', 'landscape');

        return $pdf->stream();
    }
}
