<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Illuminate\Support\Facades\Auth as FacadesAuth;

class DashboardindustriController extends Controller
{
    public function dashboardIndustri(Request $request)
    {
        $data['page_title'] = 'Dashboard Industri';
        // $data['tahun'] = [];
        // $data['triwulan'] = [];
        // $data['produk'] = [];
        // $data['progress'] = [];

        $data['tahun2'] = DB::table('emonev_roadmap')
            ->select('emonev_year.year_id as yearID', 'emonev_year.year_name as yearName')
            ->join('emonev_year', 'emonev_roadmap.year_id', 'emonev_year.year_id')
            ->where('users_id', Auth::user()->users_id)
            ->groupby('emonev_year.year_id', 'emonev_year.year_name')
            ->get();

        $data['triwulan'] = DB::table('emonev_triwulan')
            ->get();

        $data['produk']  = DB::table('emonev_roadmap')
            ->select('zat_active_id', 'zat_active_name', 'year_id')
            ->where('users_id', Auth::user()->users_id)
            ->groupby('zat_active_id', 'zat_active_name', 'year_id')
            ->get();


        $a = DB::table('emonev_reporting_detail')
            ->select('zat_active_id', 'emonev_reporting_detail.year_id as yearID', 'emonev_reporting_detail.triwulan_id as twID', 'emonev_reporting_detail.roadmap_title as roadmapTitle', 'emonev_reporting_detail.status_id')
            ->join('emonev_roadmap', 'emonev_reporting_detail.roadmap_id', 'emonev_roadmap.roadmap_id')
            ->get();

        $b = [];
        foreach ($a as $key => $value) {
            $b[$value->zat_active_id][$value->yearID][$value->twID][$key]['yearID']          = $value->yearID;
            $b[$value->zat_active_id][$value->yearID][$value->twID][$key]['twID']          = $value->twID;
            $b[$value->zat_active_id][$value->yearID][$value->twID][$key]['roadmapTitle']    = $value->roadmapTitle;
            $b[$value->zat_active_id][$value->yearID][$value->twID][$key]['statusID']       = $value->status_id;
            $b[$value->zat_active_id][$value->yearID][$value->twID][$key]['zatActiveID']    = $value->zat_active_id;
        }


        $data['progress'] = $b;

        $data['company'] = DB::table('emonev_company')
            ->where('users_id', Auth::user()->users_id)
            ->get();

        $data['year'] = DB::table('emonev_target_hilirisasi')
            ->select('emonev_year.year_name as year', 'emonev_year.year_id')
            ->join('emonev_year', 'emonev_target_hilirisasi.year_id', 'emonev_year.year_id')
            ->where('users_id', Auth::user()->users_id)
            ->groupby('emonev_year.year_id', 'emonev_year.year_name')
            ->get();

        $data['group'] = DB::table('emonev_zat_group')->orderBy('zat_group_order', 'ASC')->get();

        $data['detail'] = DB::table('emonev_roadmap')
            ->join('emonev_zat_active', 'emonev_zat_active.zat_active_id', '=', 'emonev_roadmap.zat_active_id')
            ->select('emonev_zat_active.zat_active_id', 'emonev_zat_active.zat_active_name', 'emonev_zat_active.zat_group_id', 'emonev_roadmap.year_id')
            ->groupBy('emonev_zat_active.zat_active_id', 'emonev_zat_active.zat_active_name', 'emonev_zat_active.zat_group_id', 'emonev_roadmap.year_id')
            ->get();

        $data['roadmap'] = array(
            'year' => $data['year'],
            'group' => $data['group'],
            'detail' => $data['detail'],
        );


        // dd($data);
        return view('admin.dashboard.industri.index', $data);
    }

    public function printBBO()
    {
        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
            ->loadView('admin.report.printoutbbo');
        $pdf->setPaper('legal', 'landscape');
        return $pdf->stream('Laporan.pdf');
    }

    public function get_progress()
    {
        $a = DB::table('emonev_reporting_detail')
            ->select('zat_active_id', 'emonev_reporting_detail.year_id as yearID', 'emonev_reporting_detail.triwulan_id as twID', 'emonev_reporting_detail.roadmap_title as roadmapTitle', 'emonev_reporting_detail.status_id')
            ->join('emonev_roadmap', 'emonev_reporting_detail.roadmap_id', 'emonev_roadmap.roadmap_id')
            ->get();

        $b = [];
        foreach ($a as $key => $value) {
            $b[$value->yearID][$value->twID]['yearID']          = $value->yearID;
            $b[$value->yearID][$value->twID]['twID']            = $value->twID;
            $b[$value->yearID][$value->twID]['roadmapTitle']    = $value->roadmapTitle;
            $b[$value->yearID][$value->twID]['statusID']        = $value->status_id;
            $b[$value->yearID][$value->twID]['zatActiveID']     = $value->zat_active_id;
        }

        return response()->json($b, 200);
    }
}
