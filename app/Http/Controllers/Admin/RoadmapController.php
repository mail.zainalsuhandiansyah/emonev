<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Models\Roadmap;
use DataTables;

class RoadmapController extends Controller
{

    public function store(Request $request){
        $data = $request->all();
        $userId = Auth::user()->users_id;
        $now = Carbon::now('utc')->toDateTimeString();

        // echo"<pre>";print_r($data);die();

        $zat_active_id = $data['zat_active_id'];
        $start_year = $data['start_year'];
        $end_year = $data['end_year'];
        // [count_step] => 2
        // [current_yearname] => 2021
        // [current_yearid] => 2
        // [year_1] => 

        $getYear = DB::table('emonev_year')
                        ->select('*')
                        ->where('year_active',1)
                        ->whereBetween('year_id', [$start_year, $end_year])->get();

        $triwulan = DB::table('emonev_triwulan')
                    ->select('*')
                    ->get();

        $zatActive = DB::table('emonev_zat_active')
                            ->select('*')
                            ->where('zat_active_id',$zat_active_id)
                            ->first();

        $maxRoadmapId = DB::table('emonev_roadmap')->max('roadmap_id');
        if($maxRoadmapId===null){
            $maxRoadmapId = 1;
        }else{
            $maxRoadmapId = $maxRoadmapId+1;
        }

        $hilirisasi = array();
        $targetBBO = array();

        $roadmap = array();
        $roadmapDetail = array();
        $lastUserId = 0;

        $next = 0;
        // Loop tahun - target hilirisasi
        for ($y=0; $y <count($getYear) ; $y++) { 
            $eleyear = $getYear[$y];

            $targetBBO[] = array(
                'zat_active_id'=>$zat_active_id,
                'target_bbo_year'=>$eleyear->year_id,
                'created_by'=>$userId
            );

            //loop triwulan dalam setahun
            for ($w=0; $w <count($triwulan) ; $w++) { 
                $eletw = $triwulan[$w];

                // insert data hilirisasi 
                $hilirisasi[] = array(
                    'zat_active_id'=>$zat_active_id,
                    'users_id'=>$userId,
                    'year_id'=>$eleyear->year_id,
                    'triwulan_id'=>$eletw->triwulan_id,
                    'created_at'=>$now,
                    'created_by'=>$userId
                );

                //proses dan loop roadmap
                $postUraian = 'uraian_'.$eleyear->year_name.''.$eletw->triwulan_alias;
                $uraianYear = isset($data[$postUraian])?$data[$postUraian]:'';

                if($uraianYear!=''){
                    $lastUserId = 0;
                    for ($i=0; $i < count($uraianYear); $i++) { 
                        $roadmapId = $maxRoadmapId;
        
                        $roadmap[] = array(
                            'roadmap_id'=>$roadmapId,
                            'year_id'=>$eleyear->year_id,
                            'zat_active_id'=>$zat_active_id,
                            'zat_active_name'=>$zatActive->zat_active_name,
                            'users_id'=>$userId,
                            'created_at'=>$now,
                            'created_by'=>$userId,
                            'triwulan_id'=>$eletw->triwulan_id,
                            'roadmap_title'=>$uraianYear[$i]
                        );
                        
                        //proses dan loop roadmap detail
                        $postUraian = 'tahapUraian'.$eleyear->year_name.''.$eletw->triwulan_alias;
                        $tahapUraian = isset($data[$postUraian])?$data[$postUraian]:'';
                        
                        if($tahapUraian!=''){
                            $postDetailUraian = 'detail_uraian'.$eletw->triwulan_alias.''.$eleyear->year_name.''.$tahapUraian[$i];
        
                            $detailUraian = isset($data[$postDetailUraian])?$data[$postDetailUraian]:'';
                            if($detailUraian!=''){
        
                                for ($j=0; $j <count($detailUraian) ; $j++) { 
                                    if($detailUraian[$j]!=''){
                                        $roadmapDetail[] = array(
                                            'roadmap_id'=>$roadmapId,
                                            'roadmap_description'=>$detailUraian[$j],
                                            'created_at'=>$now,
                                            'created_by'=>$userId,
                                        );
                                    }
                                }
                            }
                        }//End proses dan loop roadmap detail
        
                        $lastUserId = $roadmapId;
                        $maxRoadmapId++;
                        $next++;
                    }
                } //End proses dan loop roadmap

            }//End loop Triwulan

        }//end loop tahun

        // echo"<pre>";
        // print_r($hilirisasi);
        // print_r($roadmap);
        // print_r($roadmapDetail);
        // die();
        
        $bValid = true;
        $message = 'An error occurred, please try again later';
        
        if($bValid===true){
            //Proses insert ke table hilirisasi dan roadmap
            if(count($hilirisasi)>0){
                //delete terlebih dahulu data sebelum insert
                $deleteHilirisasi = DB::table('emonev_target_hilirisasi')
                                        ->where('zat_active_id',$zat_active_id)
                                        ->where('users_id',$userId)
                                        ->whereBetween('year_id', [$start_year, $end_year])->delete();
                $response = DB::table('emonev_target_hilirisasi')->insert($hilirisasi);
            }

            if(count($roadmapDetail)>0){
                $sqlDetail = "DELETE FROM emonev_roadmap_detail where roadmap_id in (select roadmap_id from emonev_roadmap where users_id = '$userId' and zat_active_id = $zat_active_id and year_id between $start_year and $end_year)";
                $deleteRD = DB::delete($sqlDetail);

                $response = DB::table('emonev_roadmap_detail')->insert($roadmapDetail);
            }

            if(count($roadmap)>0){
                $sqlRoad = "DELETE FROM emonev_roadmap where users_id = '$userId' and zat_active_id = $zat_active_id and year_id between $start_year and $end_year";
                $deleteR = DB::delete($sqlRoad);
                $response = DB::table('emonev_roadmap')->insert($roadmap);
            }

            if(count($targetBBO)>0){
                $sqlRoad = "DELETE FROM emonev_target_bbo where created_by = '$userId' and zat_active_id = $zat_active_id and target_bbo_year between $start_year and $end_year";
                $deleteR = DB::delete($sqlRoad);
                $response = DB::table('emonev_target_bbo')->insert($targetBBO);
            }
            
            if($response===true){
                $return  = array('success'=>true,'message'=>$message);
            }else{
                $return  = array('success'=>false,'message'=>$message);    
            }
        }else{
            $return  = array('success'=>false,'message'=>$message);
        }

        echo json_encode($return);
        // return redirect('/privileges');
    }

    public function destroy($id){
        $action = Zatactive::where('zat_active_id', $id)->delete();
        $response = response();
        $return = array('success'=>true,'message'=>'Record has been deleted successfully!');
        echo json_encode($return);
    }

    public function show($id){
        $data = Zatactive::where('zat_active_id',$id)->first();
        echo json_encode($data);
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $updatedBy = Auth::user()->users_id;
        $message = 'An error occurred, please try again later';
        // echo"<pre>";print_r($data);die();
        if($data){
            DB::table('emonev_zat_active')->where('zat_active_id' , $data['zat_active_id'])
                                     ->update([
                                         'zat_active_name' => $data['zat_active_name'],
                                         'zat_group_id'=> $data['zat_group_id'],
                                         'zat_active_fornas'=> $data['zat_active_fornas'],
                                         'updated_at'=> Carbon::now(),
                                         'updated_by'=>$updatedBy
                                     ]);
            $return = array('success'=>true,'message'=>'Data hass been updated!');
        }else{
            $return = array('success'=>false,'message'=>$message);
        }
        echo json_encode($return);
    }

    public function generateHilirisasi($zatactiveid,$yearfrom,$yearthru){
        $usersId = Auth::user()->users_id;

        $getYear = DB::table('emonev_year')
                        ->select('*')
                        ->where('year_active',1)
                        ->whereBetween('year_id', [$yearfrom, $yearthru])->get();

        $triwulan = DB::table('emonev_triwulan')
                    ->select('*')
                    ->get();

        $stepHeader = '';
        $stepBody = '';
        $no=1;
        $count_step = 0;
        $current_yearid = 0;
        $current_yearname = '';
        foreach ($getYear as $key) {
            $count_step++;
            $Contentactive = '';
            if($key->year_id==$yearfrom){
                $current_yearid = $key->year_id;
                $current_yearname = $key->year_name;
            }
            if($no===1){
                $Contentactive = 'active';
            }else{
                $Contentactive = '';
            }
            $stepHeader .='<li class="nav-item" style="border:1px solid rgb(199 199 196);border-radius:5px;text-align:center;">
                                <a href="#year'.$key->year_name.'" data-toggle="tab" class="nav-link step" disabled="disabled" style="pointer-events: none;cursor: default;text-decoration: none;color: black;">
                                    <span class="desc"><i class="fa fa-calendar"></i> '.$key->year_name.' </span>
                                </a>
                            </li>';

            $stepBody .='<div class="tab-pane '.$Contentactive.'" id="year'.$key->year_name.'" data-id="'.$key->year_id.'">
                            <input type="hidden" name="year_'.$key->year_id.'" id="year_'.$key->year_id.'" data-id="'.$key->year_id.'"/>
                            <div class="example-preview">
                                <ul class="nav nav-tabs" id="myTab" role="tablist">';
                                            foreach($triwulan as $tr):
                                                if($tr->triwulan_alias==='tw1'){
                                                    $twActive='active';
                                                }else{
                                                    $twActive = ' ';
                                                }
                        $stepBody .=' <input type="hidden" data-id="'.$key->year_id.'" name="triwulanId[]" id="triwulanId" value="'.$tr->triwulan_id.'" >
                                        <li class="nav-item">
                                            <a class="nav-link '.$twActive.'" data-idx="'.$key->year_id.'" data-id="'.$tr->triwulan_alias.'" id="'.$tr->triwulan_alias.''.$key->year_name.'-tab" data-toggle="tab" href="#'.$tr->triwulan_alias.''.$key->year_name.'" aria-controls="'.$tr->triwulan_alias.''.$key->year_name.'">
                                                <span class="nav-icon">
                                                    <i class="flaticon2-layers-1"></i>
                                                </span>
                                                <span class="nav-text">'.$tr->triwulan_description.'</span>
                                            </a>
                                        </li>';

                                    endforeach;
                    $stepBody .=' </ul>
                                <div class="tab-content mt-5" id="myTabContent">';
                                        foreach($triwulan as $tr):
                                            if($tr->triwulan_alias==='tw1'){
                                                $active='show active';
                                            }else{
                                                $active = ' ';
                                            }
                        $stepBody .='<div class="tab-pane fade '.$active.'" id="'.$tr->triwulan_alias.''.$key->year_name.'" role="tabpanel" aria-labelledby="'.$tr->triwulan_alias.''.$key->year_name.'-tab" data-id="'.$key->year_id.'">
                                        <input type="hidden" name="tahap'.$tr->triwulan_alias.''.$key->year_name.'" id="tahap'.$tr->triwulan_alias.'" data-id="'.$key->year_id.'">
                                        <div class="row">
                                            <div class="offset-10 col-md-3">
                                                <button type="button" id="button-add-uraian'.$tr->triwulan_alias.'" data-idx="'.$key->year_id.'" data-id="'.$tr->triwulan_alias.'" data-yearname="'.$key->year_name.'" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Tambah</button>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <table class="table table-bordered" id="table_'.$tr->triwulan_alias.''.$key->year_name.'" data-id="'.$key->year_id.'">
                                                <thead>
                                                    <th style="width:85%;">Tahap Pengembangan</th>
                                                    <th style="width:15%;">Action</th>
                                                </thead>
                                                <tbody id="tbody_uraian'.$tr->triwulan_alias.''.$key->year_name.'">';
                                                    $getRoad = DB::table('emonev_roadmap')
                                                                    ->select('*')
                                                                    ->where('zat_active_id',$zatactiveid)
                                                                    ->where('year_id',$key->year_id)
                                                                    ->where('triwulan_id',$tr->triwulan_id)
                                                                    ->where('users_id',$usersId)
                                                                    ->get();

                                                    if(count($getRoad)>0){
                                                        for ($r=0; $r <count($getRoad) ; $r++) { 
                                                            $eleRoad = $getRoad[$r];
                                                            $stepBody .='<tr>
                                                                            <td>'.$eleRoad->roadmap_title.'
                                                                                <textarea style="display: none;" rows="5" cols="5" class="form-control" id="uraian" name="uraian_'.$key->year_name.''.$tr->triwulan_alias.'[]">'.$eleRoad->roadmap_title.'</textarea>
                                                                                <textarea style="display: none;" rows="5" cols="5" class="form-control" id="tahapUraian'.$key->year_name.''.$tr->triwulan_alias.'" name="tahapUraian'.$key->year_name.''.$tr->triwulan_alias.'[]">'.($r+1).'</textarea>
                                                                                <table id="table-detail-uraian-'.$key->year_name.''.$tr->triwulan_alias.'" style="width:100%;border:none !important;" data-idx="'.$key->year_id.'">
                                                                                    <tbody>';

                                                                                    $getRoadDetail = DB::table('emonev_roadmap_detail')
                                                                                                    ->select('*')
                                                                                                    ->where('roadmap_id',$eleRoad->roadmap_id)
                                                                                                    ->get();

                                                                                    if(count($getRoadDetail)>0){
                                                                                        for ($rd=0; $rd < count($getRoadDetail); $rd++) { 
                                                                                            $eleRd = $getRoadDetail[$rd];
                                                                                            $stepBody .='<tr>
                                                                                                            <td style="width:100%;border:none !important;">
                                                                                                                <input type="text" placeholder="Enter detail" class="form-control form-control-sm" name="detail_uraian'.$tr->triwulan_alias.''.$key->year_name.''.($r+1).'[]" id="detail_uraian" value="'.$eleRd->roadmap_description.'"/>
                                                                                                            </td>
                                                                                                        </tr>';  
                                                                                        }
                                                                                    }
                                                                                    
                                                                        $stepBody .='</tbody>
                                                                                </table>
                                                                            </td>
                                                                            <td align="center">
                                                                                <a href="javascript:;" class="btn btn-sm btn-success" data-id="'.$tr->triwulan_alias.'" data-idx="'.$key->year_id.'" data-yearname="'.$key->year_name.'" id="btn-detail'.$tr->triwulan_alias.''.$key->year_name.'" style="font-size:9px;padding:4px !important;"><i class="fa fa-list"></i> Detail</a>
                                                                            </td>
                                                                        </tr>';
                                                        }
                                                    }
                                    $stepBody .='</tbody>
                                            </table>
                                        </div>
                                    </div>';
                                        endforeach;
                    $stepBody .='</div>
                            </div>
                        </div>';
            $no++;
        }
        
        $return = array(
            'stepHeader'=>$stepHeader,
            'stepBody'=>$stepBody,
            'count_step'=>$count_step,
            'current_yearid'=>$current_yearid,
            'current_yearname'=>$current_yearname
        );
        echo json_encode($return);
    }
}
