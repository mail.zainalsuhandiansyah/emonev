<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Settingslider;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use DataTables;
use Validator;
Use Response;
Use File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class SettingSliderController extends Controller
{

    public function index()
    {
        $page_title = 'Slider';
        $search = request("q", "");

        $list = Settingslider::query()
            ->where(function ($query) use ($search) {
                if (!empty($search) || $search !== "")
                    $query->where("judul", "LIKE", "%$search%");
            })
            ->get();

        return view('admin.setting.setslider.index', compact('page_title', 'list'));
    }

    public function create()
    {
        return view('admin.setting.setslider.create');
    }

    public function show($id){
        $data = Settingslider::query()->where('id', $id)->first();

        return view('admin.setting.setslider.edit', compact('data'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'judul' => 'required|string',
            'image' => 'nullable|image|mimes:jpeg,png,jpg|max:512',
        ]);

        $imageName = null;
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = 'slide' . '-' . time() . '.' . $image->getClientOriginalExtension();

            $location = public_path('images/');
            $request->file('image')->move($location, $imageName);

            // $img = Image::make($image->getRealPath());
            // $img->resize(null, 300, function ($constraint) {
            //     $constraint->aspectRatio();
            // });

            // $img->stream();

            // Storage::disk('public')->put('public/images/slider/' . $imageName, $img, 'public');
        }

        $setslider = new Settingslider();
        $setslider->judul = $request->judul;
        $setslider->image = $imageName;
        $setslider->save();

        // return response()->json('Image uploaded successfully');
        return redirect('/admin/setslider');

        // =================

        // $this->validate($request, array(
        //     'judul'=>'required|max:225',
        //     'photo'=>'required|image|mimes:jpeg,png,jpg|max:512',
        //   ));
          
        //   if ($request->hasFile('image')) {
        //       $photo = $request->file('image');
        //       $filename = 'slide' . '-' . time() . '.' . $photo->getClientOriginalExtension();
        //       $location = public_path('images/');
        //       $request->file('image')->move($location, $filename);
              
        //       $slider->photo = $filename;
        //     }
            
        //     $slider = new Settingslider();
        //     $slider->judul = $request->input('judul');
        //     $slider->save();

        //   return redirect('/admin/setslider');
        
    }
    
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'judul' => 'required|string',
            'image' => 'nullable|image|mimes:jpeg,png,jpg|max:512',
        ]);

        $setslider = Settingslider::query()->where('id', $id)->first();

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = time() . '.' . $image->getClientOriginalExtension();

            $img = Image::make($image->getRealPath());
            $img->resize(null, 300, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->stream();

            Storage::disk('local')->put('public/images/footer/' . $imageName, $img, 'public');

            $setslider->image = $imageName;
        }

        $setslider->judul = $request->judul;
        $setslider->save();

        return redirect('admin/setslider');
    }

    public function destroy($id){

        $setslider = Settingslider::query()->where('id', $id)->firstOrFail();
        $setslider->delete();

        return redirect('/admin/setslider');
    }

    protected function redirectURL($message)
    {
        return redirect('/admin/setslider')->with($message);
    }
    

    public function listslider(){
        $files = Storage::allFiles('public');
        dd($files);
    }
    
}
