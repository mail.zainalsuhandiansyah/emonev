<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardevaluatorController extends Controller
{
    public function dashboardEvaluator()
    {
        $page_title = 'Dashboard Evaluator';

        //untuk menampilkan timeline
        $dYear = DB::table('emonev_target_bbo')
                            ->join('emonev_year','emonev_year.year_id','=','emonev_target_bbo.target_bbo_year')
                            ->select('emonev_year.year_id','emonev_year.year_name')
                            ->groupBy('emonev_year.year_id','emonev_year.year_name')
                            ->get();

        $dJenisBBO = DB::table('emonev_zat_group')->orderBy('zat_group_order','ASC')->get();

        $dDetailBBO = DB::table('emonev_target_bbo')
                                ->join('emonev_zat_active','emonev_zat_active.zat_active_id','=','emonev_target_bbo.zat_active_id')
                                ->select('emonev_zat_active.zat_active_id','emonev_zat_active.zat_active_name','emonev_zat_active.zat_group_id','emonev_target_bbo.target_bbo_year')
                                ->groupBy('emonev_zat_active.zat_active_id','emonev_zat_active.zat_active_name','emonev_zat_active.zat_group_id','emonev_target_bbo.target_bbo_year')
                                ->get();
        //end untuk menampilkan timeline

        $roadmap = array(
            'year'=>$dYear,
            'jenis'=>$dJenisBBO,
            'detail'=>$dDetailBBO,
        );

        $visitor = DB::table('view_visitor')->first();
        
        return view('admin.dashboard.evaluator.index', compact('page_title', 'roadmap', 'visitor'));
    }
}
