<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SettingFooter;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use DataTables;
use Validator;
Use Response;
Use File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class SettingFooterController extends Controller
{

    // public function index()
    // {
    //     $page_title = 'Footer';
    //     $list = Settingfooter::select('id','nama_perusahaan','alamat_perusahaan','kota','kodepos','notelp')->get();

    //     return view('admin.setting.setfooter.index', compact('page_title', 'list'));
    // }
    public function index()
    {
        $page_title = 'Footer';
        $search = request("q", "");

        $list = SettingFooter::query()
            ->where(function ($query) use ($search) {
                if (!empty($search) || $search !== "")
                    $query->where("nama_perusahaan", "LIKE", "%$search%");
            })
            ->get();

            // var_dump($list);die();

        return view('admin.setting.setfooter.index', compact('page_title', 'list'));
    }

    public function create()
    {
        return view('admin.setting.setfooter.create');
    }

    public function show($id){
        $data = SettingFooter::query()->where('id', $id)->first();

        return view('admin.setting.setfooter.edit', compact('data'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'nama_perusahaan' => 'required|string',
            'alamat_perusahaan' => 'required|string',
            'tempat' => 'required|string',
            'kota' => 'required|string',
            'kodepos' => 'required|int',
            'nama_callcenter' => 'required|string',
            'notelp' => 'required|int',
            'image' => 'nullable|image|mimes:jpeg,png,jpg|max:512',
        ]);

        $imageName = null;
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = time() . '.' . $image->getClientOriginalExtension();

            $img = Image::make($image->getRealPath());
            $img->resize(null, 300, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->stream();

            Storage::disk('local')->put('public/images/footer/' . $imageName, $img, 'public');
        }

        $setfooter = new Settingfooter();
        $setfooter->nama_perusahaan = $request->nama_perusahaan;
        $setfooter->alamat_perusahaan = $request->alamat_perusahaan;
        $setfooter->tempat = $request->tempat;
        $setfooter->kota = $request->kota;
        $setfooter->kodepos = $request->kodepos;
        $setfooter->nama_callcenter = $request->nama_callcenter;
        $setfooter->notelp = $request->notelp;
        $setfooter->image = $imageName;
        $setfooter->save();

        return redirect('/admin/setfooter');

    }

    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'nama_perusahaan' => 'required|string',
            'alamat_perusahaan' => 'required|string',
            'tempat' => 'required|string',
            'kota' => 'required|string',
            'kodepos' => 'required|int',
            'nama_callcenter' => 'required|string',
            'notelp' => 'required|int',
            'image' => 'nullable|image|mimes:jpeg,png,jpg|max:512',
        ]);

        $setfooter = Settingfooter::query()->where('id', $id)->first();

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = time() . '.' . $image->getClientOriginalExtension();

            $img = Image::make($image->getRealPath());
            $img->resize(null, 300, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->stream();

            Storage::disk('local')->put('public/images/footer/' . $imageName, $img, 'public');

            $setfooter->image = $imageName;
        }

        $setfooter->nama_perusahaan = $request->nama_perusahaan;
        $setfooter->alamat_perusahaan = $request->alamat_perusahaan;
        $setfooter->tempat = $request->tempat;
        $setfooter->kota = $request->kota;
        $setfooter->kodepos = $request->kodepos;
        $setfooter->nama_callcenter = $request->nama_callcenter;
        $setfooter->notelp = $request->notelp;
        $setfooter->save();

        return redirect('admin/setfooter');
    }

    public function destroy($id){

        $setfooter = Settingfooter::query()->where('id', $id)->firstOrFail();
        $setfooter->delete();

        return redirect('/admin/setfooter');
    }

    protected function redirectURL($message)
    {
        return redirect('/admin/setfooter')->with($message);
    }
    
}
