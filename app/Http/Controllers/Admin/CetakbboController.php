<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;


class CetakbboController extends Controller
{
    public function index()
    {

        $data['page_title'] = 'Cetak Pelaporan BBO';
        $data['bahan_baku'] = DB::table('emonev_reporting')
            ->join('emonev_users', 'emonev_reporting.users_id', 'emonev_users.users_id')
            ->select('emonev_users.users_id as userID', 'emonev_users.fullname as CompanyName')
            ->groupby('emonev_users.users_id', 'emonev_users.fullname')
            ->get();
        foreach ($data['bahan_baku'] as $key => $value) {
            $this->fill_reporting($value->userID);
            $this->fill_reporting_detail($value->userID);
        }
        $data['tahun'] = DB::table('emonev_year')->get();


        return view('admin.pelaporan.evaluator.laporanbbo', $data);
    }

    public function fill_reporting($userID)
    {
        $bahan_baku = DB::table('emonev_material')->get();
        foreach ($bahan_baku as $key => $value) {
            if (DB::table('emonev_reporting')->where('users_id', $userID)->where('zat_active_id', $value->zat_active_id)->doesntExist()) {
                $insert = [];
                $insert['users_id'] = $userID;
                $insert['zat_active_id'] = $value->zat_active_id;
                $insert['zat_active_name'] = $value->zat_active_name;
                $insert['created_by']   = $value->created_by;
                $insert['created_at']   = $value->created_at;

                DB::table('emoenv_reporting')->insert('$insert');
            }
        }
    }

    public function fill_reporting_detail($userID)
    {
        $reporting = DB::table('emonev_reporting')->where('users_id', $userID)->get();
        foreach ($reporting as $key => $value) {
            $roadmap = DB::table('emonev_roadmap')->where('users_id', $userID)->where('zat_active_id', $value->zat_active_id)->get();
            foreach ($roadmap as $key2 => $value2) {
                if (DB::table('emonev_reporting_detail')->where('roadmap_id', $value2->roadmap_id)->doesntExist()) {
                    $insert = [];
                    $insert['reporting_id'] = $value->reporting_id;
                    $insert['roadmap_id']   = $value2->roadmap_id;
                    $insert['year_id']      = $value2->year_id;
                    $insert['triwulan_id']  = $value2->triwulan_id;
                    $insert['roadmap_title'] = $value2->roadmap_title;
                    $insert['status_id']    = 1;
                    $insert['created_by']   = $value2->created_by;
                    $insert['created_at']   = $value->created_at;

                    DB::table('emonev_reporting_detail')->insert($insert);
                }
            }
        }
    }
}
