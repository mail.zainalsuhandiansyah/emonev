<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\Usermanagement;
use Illuminate\Support\Facades\Hash;
use DataTables;

class UserManagementController extends Controller
{
    public function index()
    {
        $page_title = 'User Management';
        $list = DB::table('emonev_users')
                    ->join('emonev_privileges', 'emonev_users.privileges_id', '=', 'emonev_privileges.privileges_id')
                    ->select('emonev_users.*', 'emonev_privileges.privileges_name')
                    ->get();
        $privileges = DB::table('emonev_privileges')
								->select('privileges_id' , 'privileges_name')
								->get();
        return view('admin.setting.usermanagement', compact('page_title','list','privileges'));
    }

    public function store(Request $request){
        $data = $request->all();
        $bValid = true;
        $message = 'An error occurred, please try again later';
        $password = Hash::make($request->password);

        if($bValid===true){
            
            $users = new Usermanagement();
            $users->username = $request->username;
            $users->password = $password;
            $users->fullname = $request->fullname;
            $users->email = $request->email;
            $users->status = $request->status;
            $users->photo = 'uploads/img/user.png';
            $users->privileges_id = $request->privileges_id;
            $users->created_at = Carbon::now();
            $users->created_by = 1;
            $response = $users->save();
            if($response===true){
                $return  = array('success'=>true,'message'=>$message);
            }else{
                $return  = array('success'=>false,'message'=>$message);    
            }
        }else{
            $return  = array('success'=>false,'message'=>$message);
        }

        echo json_encode($return);
        // return redirect('/privileges');
    }

    public function destroy($id){
        $action = Usermanagement::where('users_id', $id)->delete();
        $response = response();
        $return = array('success'=>true,'message'=>'Record has been deleted successfully!');
        echo json_encode($return);
    }

    public function show($id){
        $data = Usermanagement::where('users_id',$id)->first();
        echo json_encode($data);
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $message = 'An error occurred, please try again later';
        // echo"<pre>";print_r($data);die();
        $passwordOld = $data['password_old'];
        $password = $data['password'];
        $passwordnew = Hash::make($password);

        $email = $data['email'];
        if($email=='')$email='-';

        if($password==''){
            $passwordValue = $passwordOld;
        }else{
            $passwordValue = $passwordnew; 
        }

        if($data){
            DB::table('emonev_users')->where('users_id' , $data['users_id'])
                                     ->update([
                                         'username' => $data['username'],
                                         'password'=> $passwordValue,
                                         'fullname'=> $data['fullname'],
                                         'email'=> $email,
                                         'status'=> $data['status'],
                                         'privileges_id'=> $data['privileges_id'],
                                         'updated_at'=> Carbon::now()
                                     ]);
            $return = array('success'=>true,'message'=>'Data hass been updated!');
        }else{
            $return = array('success'=>false,'message'=>$message);
        }
        echo json_encode($return);
    }
}
