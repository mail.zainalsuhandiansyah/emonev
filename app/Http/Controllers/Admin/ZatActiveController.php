<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Models\Zatactive;
use DataTables;

class ZatActiveController extends Controller
{
    public function index()
    {
        $userId = Auth::user()->users_id;
        $privilegesId = Auth::user()->privileges_id;
        $page_title = 'Master Bahan Baku Obat yang Dikembangkan';
        $jenisbb = DB::table('emonev_zat_group')
                    ->select('emonev_zat_group.*')
                    ->get();
        return view('admin.setting.zatactive', compact('page_title','jenisbb'));
    }

    public function store(Request $request){
        $data = $request->all();
        // echo"<pre>";print_r($data);die();
        $userId = Auth::user()->users_id;
        $bValid = true;
        $message = 'An error occurred, please try again later';
        
        if($bValid===true){
            
            $zatactive = new Zatactive();
            $zatactive->zat_active_name = $data['zat_active_name'];
            $zatactive->zat_group_id = $data['zat_group_id'];
            $zatactive->zat_active_fornas = $data['zat_active_fornas'];
            $zatactive->created_at = Carbon::now();
            $zatactive->created_by = $userId;
            $response = $zatactive->save();
            if($response===true){
                $return  = array('success'=>true,'message'=>$message);
            }else{
                $return  = array('success'=>false,'message'=>$message);    
            }
        }else{
            $return  = array('success'=>false,'message'=>$message);
        }

        echo json_encode($return);
        // return redirect('/privileges');
    }

    public function destroy($id){
        $action = Zatactive::where('zat_active_id', $id)->delete();
        $response = response();
        $return = array('success'=>true,'message'=>'Record has been deleted successfully!');
        echo json_encode($return);
    }

    public function show($id){
        $data = Zatactive::where('zat_active_id',$id)->first();
        echo json_encode($data);
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $updatedBy = Auth::user()->users_id;
        $message = 'An error occurred, please try again later';
        // echo"<pre>";print_r($data);die();
        if($data){
            DB::table('emonev_zat_active')->where('zat_active_id' , $data['zat_active_id'])
                                     ->update([
                                         'zat_active_name' => $data['zat_active_name'],
                                         'zat_group_id'=> $data['zat_group_id'],
                                         'zat_active_fornas'=> $data['zat_active_fornas'],
                                         'updated_at'=> Carbon::now(),
                                         'updated_by'=>$updatedBy
                                     ]);
            $return = array('success'=>true,'message'=>'Data hass been updated!');
        }else{
            $return = array('success'=>false,'message'=>$message);
        }
        echo json_encode($return);
    }

    public function listZatActive(Request $request){
        $data = $request->all();
        $keyword = isset($data['query']['generalSearch'])?$data['query']['generalSearch']:'';

        $sql = 'SELECT 
                    a.*,b.zat_group_description 
                FROM emonev_zat_active a
                left join emonev_zat_group b on b.zat_group_id = a.zat_group_id';
        if($keyword!=''){
            $keyword = strtoupper(strtolower($keyword));
            $sql .=" WHERE (UPPER(a.zat_active_id) like '%".$keyword."%' or UPPER(a.zat_active_name) like '%".$keyword."%' or UPPER(b.zat_group_description) like '%".$keyword."%')";
        
        }    
        $list = DB::select($sql);
        $return= response()->json($list, 200);
        return $return;
    }

    public function listBBO(Request $request){

        $data = $request->all();
        $keyword = isset($data['query']['generalSearch'])?$data['query']['generalSearch']:'';

        $keyword = strtoupper(strtolower($keyword));

        $bboElic = DB::connection('mysql3')->table('m_bahanobat_elic')
                                            ->select('OBAT_ID','NAMA_BAHAN') //HS_CODE,ORIGIN_COUNTRY
                                            ->where(DB::raw('upper(OBAT_ID)'), 'like', '%' .$keyword. '%')->orWhere(DB::raw('UPPER(NAMA_BAHAN)'), 'like', '%' .$keyword. '%')
                                            ->get();
        $return= response()->json($bboElic, 200);
        return $return;
    }
}
