<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DaftarindustriController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function index()
    {
        $page_title = 'Daftar Industri';

        return view('admin.evaluator.daftar_industri', compact('page_title'));
    }
}
