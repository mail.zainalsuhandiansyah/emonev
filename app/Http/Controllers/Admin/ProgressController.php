<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Http\Controllers\Admin\PelaporanindustriController;
use Illuminate\Support\Facades\Auth;
use App\Models\Pelaporan;
use Carbon\Carbon;

class ProgressController extends Controller
{
    public function index()
    {
        $data['page_title'] = 'Pelaporan Progress';

        $Pelaporan = new PelaporanindustriController;

        $this->transfer_to_reporting();
        $this->transfer_to_reporting_detail();
        $this->transfer_to_reporting_subdetail();

        $data['pelaporan'] = DB::table('emonev_reporting')
            ->join('emonev_users', 'emonev_reporting.users_id', 'emonev_users.users_id')
            ->join('emonev_status', 'emonev_reporting.status_id', 'emonev_status.status_id')
            ->select(
                'emonev_reporting.reporting_id',
                'emonev_reporting.zat_active_id as zat_active_id',
                'emonev_reporting.zat_active_name as zat_active_name',
                'emonev_status.status_description as status',
                'emonev_users.fullname as namaPerusahaan',
                'emonev_users.users_id as idPerusahaan'
            )->get();

        return view('admin.pelaporan.evaluator.progress', $data);
    }

    public function getforum($pelaporanid)
    {
        $pelaporan = Pelaporan::where('reporting_id', $pelaporanid)->first();
        $data['reportingID'] = $pelaporan->reporting_id;
        $Perusahaan = DB::table('emonev_company')->where('users_id', $pelaporan->users_id)->first();

        $this->transfer_to_reporting_detail();
        $this->transfer_to_reporting_subdetail();


        $data['page_title']         = 'Pelaporan ' . $Perusahaan->company_name;
        $data['namaPerusahaan']     = $Perusahaan->company_name;
        $data['zatActiveName']      = $pelaporan->zat_active_name;
        $data['ReportingDate']       = $pelaporan->created_at;
        $data['target']             = DB::table('emonev_target_hilirisasi')
            ->join('emonev_year', 'emonev_target_hilirisasi.year_id', 'emonev_year.year_id')
            ->select('emonev_year.year_name', 'emonev_year.year_id')
            ->where('users_id', $pelaporan->users_id)
            ->where('zat_active_id', $pelaporan->zat_active_id)
            ->groupby('emonev_year.year_id', 'emonev_year.year_name')
            ->get();

        $data['triwulan']           = DB::table('emonev_triwulan')->get();
        $data['reporting']          = Pelaporan::where('reporting_id', $pelaporanid)->get();
        $data['reportingDetail']    = DB::table('emonev_reporting_detail')->where('reporting_id', $pelaporan->reporting_id)->get();
        $data['reportingSubDetail']    = DB::table('emonev_reporting_subdetail')
            ->join('emonev_status', 'emonev_reporting_subdetail.status_id', 'emonev_status.status_id')
            ->where('reporting_id', $pelaporan->reporting_id)->get();
        $data['qna']            = DB::table('emonev_reporting_qna')->Join('emonev_users', 'emonev_reporting_qna.users_id', 'emonev_users.users_id')
            ->where('reporting_id', $pelaporan->reporting_id)->get();






        return view('admin.pelaporan.evaluator.getforum', $data);
    }

    public function transfer_to_reporting()
    {
        $source = DB::table('emonev_material')->get();
        foreach ($source as $key => $value) {
            if (DB::table('emonev_reporting')->where('users_id', $value->users_id)->where('zat_active_id', $value->zat_active_id)->doesntExist()) {
                $laporan = new Pelaporan;
                $laporan->users_id          = $value->users_id;
                $laporan->zat_active_id     = $value->zat_active_id;
                $laporan->zat_active_name   = $value->zat_active_name;
                $laporan->status_id         = 1; // baru
                $laporan->created_by        = Auth::user()->users_id;
                $laporan->created_at        = Carbon::now();
                $laporan->save();
            }
        }
    }

    public function transfer_to_reporting_detail()
    {
        $data = DB::table('emonev_reporting')->get();
        foreach ($data as $key => $value) {
            $roadmap = DB::table('emonev_roadmap')->where('zat_active_id', $value->zat_active_id)->where('users_id', $value->users_id)->get();
            foreach ($roadmap as $key2 => $value2) {

                if (DB::table('emonev_reporting_detail')->where('reporting_id', $value->reporting_id)
                    ->where('roadmap_id', $value2->roadmap_id)
                    ->doesntExist()
                ) {
                    $insert = [];
                    $insert['reporting_id']         = $value->reporting_id;
                    $insert['roadmap_id']              = $value2->roadmap_id;
                    $insert['year_id']              = $value2->year_id;
                    $insert['triwulan_id']          = $value2->triwulan_id;
                    $insert['roadmap_title']        = $value2->roadmap_title;
                    $insert['created_by']           = Auth::user()->users_id;
                    $insert['created_at']           = Carbon::now();
                    DB::table('emonev_reporting_detail')->insert($insert);
                }
            }
        }
    }

    public function transfer_to_reporting_subdetail()
    {
        $data = DB::table('emonev_reporting_detail')->get();
        foreach ($data as $key => $value) {
            $roadmap = DB::table('emonev_roadmap_detail')->where('roadmap_id', $value->roadmap_id)->get();
            foreach ($roadmap as $key2 => $value2) {

                if (DB::table('emonev_reporting_subdetail')->where('reporting_detail_id', $value->reporting_detail_id)
                    ->where('roadmap_detail_id', $value2->roadmap_detail_id)
                    ->doesntExist()
                ) {
                    $insert = [];
                    $insert['reporting_id']         = $value->reporting_id;
                    $insert['reporting_detail_id']  = $value->reporting_detail_id;
                    $insert['roadmap_detail_id']    = $value2->roadmap_detail_id;
                    $insert['roadmap_description']  = $value2->roadmap_description;
                    $insert['created_by']           = Auth::user()->users_id;
                    $insert['created_at']           = Carbon::now();
                    DB::table('emonev_reporting_subdetail')->insert($insert);
                }
            }
        }
    }

    public function get_tahun($reportingID)
    {
        $pelaporan = Pelaporan::where('reporting_id', $reportingID)->first();
        $data = DB::table('emonev_target_hilirisasi')
            ->join('emonev_year', 'emonev_target_hilirisasi.year_id', 'emonev_year.year_id')
            ->select('emonev_year.year_name')
            ->where('users_id', $pelaporan->users_id)
            ->where('zat_active_id', $pelaporan->zat_active_id)
            ->groupby('emonev_year.year_id', 'emonev_year.year_name')
            ->get();

        echo json_encode($data);
    }

    public function get_tw()
    {
        $data = DB::table('emonev_triwulan')->get();
        echo json_encode($data);
    }


    public function get_langkah($reportingID)
    {
        $data = DB::table('emonev_reporting_subdetail')->where('reporting_id', $reportingID)->get();

        echo json_encode($data);
    }

    public function get_qna()
    {
    }

    public function simpan_qna(Request $request)
    {
        // dd('test1');
        $data = $request->all();
        // dd($data);
        if ($data) {

            $insert = [];
            $insert['reporting_id']             = $data['reporting_id'];
            $insert['reporting_detail_id']      = $data['reporting_detail_id'];
            $insert['reporting_subdetail_id']   = $data['reporting_subdetail_id'];
            $insert['users_id']                 = Auth::user()->users_id;
            $insert['qna']                      = 'Pertanyaan';
            $insert['qna_description']          = $data['chat'];
            $insert['qna_flag']                 = '1';
            $insert['created_by']                = Auth::user()->users_id;
            $insert['created_at']                = Carbon::now();
            DB::table('emonev_reporting_qna')->insert($insert);
        }

        return redirect()->back();
    }

    public function approve_langkah(Request $request)
    {
        $data = $request->all();
        if ($data) {
            if (DB::table('emonev_reporting_subdetail')
                ->where('reporting_id', $data['reporting_id'])
                ->where('reporting_detail_id', $data['reporting_detail_id'])
                ->where('reporting_subdetail_id', $data['reporting_subdetail_id'])
                ->exists()
            ) {

                DB::table('emonev_reporting_subdetail')
                    ->where('reporting_id', $data['reporting_id'])
                    ->where('reporting_detail_id', $data['reporting_detail_id'])
                    ->where('reporting_subdetail_id', $data['reporting_subdetail_id'])
                    ->update(['catatan' => $data['catatan'], 'status_id' => 3]);
            }

            return redirect()->back();
        }
    }
}
