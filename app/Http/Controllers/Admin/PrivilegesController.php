<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\Privileges;
use DataTables;

class PrivilegesController extends Controller
{
    public function index()
    {
        $page_title = 'Role Privileges';
        $list = Privileges::select('privileges_id','privileges_name','privileges_theme','privileges_flag')->get();
        return view('admin.setting.privileges', compact('page_title','list'));
    }

    public function store(Request $request){
        $data = $request->all();
        $bValid = true;
        $message = 'An error occurred, please try again later';
        
        if($bValid===true){
            
            $privileges = new Privileges();
            $privileges->privileges_name = $request->privileges_name;
            $privileges->privileges_theme = $request->privileges_theme;
            $privileges->privileges_flag = $request->privileges_flag;
            $privileges->created_at = Carbon::now();
            $privileges->created_by = 1;
            $response = $privileges->save();
            if($response===true){
                $return  = array('success'=>true,'message'=>$message);
            }else{
                $return  = array('success'=>false,'message'=>$message);    
            }
        }else{
            $return  = array('success'=>false,'message'=>$message);
        }

        echo json_encode($return);
        // return redirect('/privileges');
    }

    public function destroy($id){
        $action = Privileges::where('privileges_id', $id)->delete();
        $response = response();
        // return response()->json([
        //     'success' => 'Record has been deleted successfully!'
        // ]);
        $return = array('success'=>true,'message'=>'Record has been deleted successfully!');
        echo json_encode($return);
    }

    public function show($id){
        $data = Privileges::where('privileges_id',$id)->first();
        echo json_encode($data);
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $message = 'An error occurred, please try again later';
        // echo"<pre>";print_r($data);die();
        if($data){
            DB::table('emonev_privileges')->where('privileges_id' , $data['privileges_id'])
                                     ->update([
                                         'privileges_name' => $data['privileges_name'],
                                         'privileges_theme'=> $data['privileges_theme'],
                                         'privileges_flag'=> $data['privileges_flag'],
                                         'updated_at'=> Carbon::now()
                                     ]);
            $return = array('success'=>true,'message'=>'Data hass been updated!');
        }else{
            $return = array('success'=>false,'message'=>$message);
        }
        echo json_encode($return);
    }
}
