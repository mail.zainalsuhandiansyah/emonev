<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Models\Zatactive;
use DataTables;

class ChangePasswordController extends Controller
{    
    public function index()
    {
        $title = 'Change Password';
        return view('admin.setting.changePassword', compact('title'));
    }

    public function store(Request $request){
        $data = $request->all();
        $userId = Auth::user()->users_id;
        $now = Carbon::now('utc')->toDateTimeString();

        $bValid = true;
        $message = 'Opps, something wrong please try again or contact administrator';

        $new_password = $data['new_password'];
        $cNewpassword = $data['confirm_new_password'];

        $password = \Hash::make($cNewpassword);

        $response = DB::table('emonev_users')->where('users_id', $userId)
                                ->update(['password' => $password]);

        $return  = array('success'=>true);

        echo json_encode($return);
    }
}
