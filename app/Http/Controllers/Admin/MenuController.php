<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Admin\Menu;
use Carbon\Carbon;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_title = 'Menu';
        $search = request("q", "");

        $menu = Menu::query()
            ->get();

        return view('admin.menu.index', compact('page_title', 'menu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = "Create Menu";

        return view('admin.menu.create', compact('page_title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'menus_name' => 'required|string',
            'menus_path' => 'required',
        ]);

        $menu = new Menu();
        $menu->menus_name = $request->menus_name;
        $menu->menus_path = $request->menus_path;
        $menu->created_at = Carbon::now();
        $menu->save();

        return redirect('/admin/menu');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Menu::query()->where('id', $id)->first();

        return view('admin.menu.edit', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'menus_name' => 'required|string',
            'menus_path' => 'required',
        ]);

        $menu = Menu::query()->where('id', $id)->first();
        $menu->menus_name = $request->menus_name;
        $menu->menus_path = $request->menus_path;
        $menu->updated_at = Carbon::now();
        $menu->save();

        return redirect('/admin/menu');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $menu = Menu::query()->where('id', $id)->first();
        $menu->delete();

        return redirect('/admin/menu');
    }

    protected function redirectURL($message)
    {
        return redirect('/admin/menu')->with($message);
    }
}
