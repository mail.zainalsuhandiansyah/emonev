<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CetakindustriController extends Controller
{
    public function index()
    {
        $page_title = 'Cetak Industri';

        return view('admin.pelaporan.evaluator.laporanindustri', compact('page_title'));
    }
}
