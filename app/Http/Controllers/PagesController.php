<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin\Content;
use App\Models\Admin\Menu;
use App\Models\Settingfooter;
use App\Models\Settingslider;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Models\Visitor;

class PagesController extends Controller
{
    public function index()
    {
        $page_title = 'Dashboard';
        $page_description = 'Some description for the page';

        return view('pages.dashboard', compact('page_title', 'page_description'));
    }

    public function landing(Request $request)
    {
        $now = Carbon::now('utc')->toDateTimeString();
        $now = date('Y-m-d',strtotime($now));
        $page_title = 'Landing Page';
        $page_description = 'Some description for the page';

        $jmlF = DB::connection('mysql3')->table('m_trader')
                            ->select('OBAT_ID','NAMA_BAHAN')
                            ->where('JENIS_INDUSTRI_IF' , 1)
                            ->count();

        $jmlFBBO = DB::connection('mysql3')->table('m_trader')
                            ->select('OBAT_ID','NAMA_BAHAN')
                            ->where('JENIS_INDUSTRI_IEBA' , 1)
                            ->count();

        $jmlBBO = DB::table('emonev_zat_active')->count();
        // $jmlHilirisasi = DB::table('emonev_target_hilirisasi')->count();
        $jmlHilirisasi = DB::table('emonev_hirilisasi')->count();

        $visitor = DB::table('view_visitor')->first();
        
        $getjmlFs = DB::connection('mysql2')->table('m_trader')
                        ->select('NAMA')
                        ->where('JENIS_INDUSTRI_IF' , 1)
                        ->get();

        $getjmlFBBOs = DB::connection('mysql2')->table('m_trader')
                        ->select('NAMA')
                        ->where('JENIS_INDUSTRI_IEBA' , 1)
                        ->get();
        
        $getjmlBBOs = DB::table('emonev_zat_active')->get();

        $getjmlhills = DB::table('emonev_hirilisasi')
                    ->join('emonev_zat_active', 'emonev_zat_active.zat_active_id', '=' , 'emonev_hirilisasi.zat_active_id')
                    ->select('emonev_zat_active.zat_active_name')
                    ->get();

        
        // dd("DATA JMLF", $getjmlFs);
        
        // 'getjmlFs'=>$getjmlFs,
        $datas = array(
            'jmlF'=>$jmlF,
            'jmlFBBO'=>$jmlFBBO,
            'jmlBBO'=>$jmlBBO,
            'jmlHilirisasi'=>$jmlHilirisasi,
        );

        
        


        $footers = Settingfooter::select('nama_perusahaan','alamat_perusahaan','tempat','kota','kodepos','nama_callcenter','notelp','image')
                                    ->limit(1)
                                    ->get();

        $limit = request("limit", 4);
        $article = Content::query()
            ->orderByDesc("updated_at")
            ->paginate($limit);
        
        // $slider = Settingslider::select('judul','image')->get();

        // $limitslider = request("limit", 4);
        $slider = Settingslider::limit(4)->get();
            // ->orderByDesc("updated_at");
            // ->paginate($limitslider);

        $insertvisitor = DB::table('shetabit_visits')->insert([
            'ip' => $request->visitor()->ip(),
            'created_at' => Carbon::now(),
        ]);

        $menu = Menu::query()
            ->get();
        
        //untuk menampilkan timeline
        $dYear = DB::table('emonev_target_bbo')
                            ->join('emonev_year','emonev_year.year_id','=','emonev_target_bbo.target_bbo_year')
                            ->select('emonev_year.year_id','emonev_year.year_name')
                            ->groupBy('emonev_year.year_id','emonev_year.year_name')
                            ->get();

        $dJenisBBO = DB::table('emonev_zat_group')->orderBy('zat_group_order','ASC')->get();

        $dDetailBBO = DB::table('emonev_target_bbo')
                                ->join('emonev_zat_active','emonev_zat_active.zat_active_id','=','emonev_target_bbo.zat_active_id')
                                ->select('emonev_zat_active.zat_active_id','emonev_zat_active.zat_active_name','emonev_zat_active.zat_group_id','emonev_target_bbo.target_bbo_year')
                                ->groupBy('emonev_zat_active.zat_active_id','emonev_zat_active.zat_active_name','emonev_zat_active.zat_group_id','emonev_target_bbo.target_bbo_year')
                                ->get();
        //end untuk menampilkan timeline

        $roadmap = array(
            'year'=>$dYear,
            'jenis'=>$dJenisBBO,
            'detail'=>$dDetailBBO,
        );

        return view('landing.index', compact('page_title', 'page_description', 'article', 'footers', 'menu','datas','visitor','roadmap','slider','getjmlFs','getjmlFBBOs','getjmlBBOs','getjmlhills'));
    }

    public function contactus()
    {
        $page_title = 'Contact Us';
        $page_description = 'Some description for the page';

        return view('landing.contact', compact('page_title', 'page_description'));
    }

    /**
     * Demo methods below
     */

    // Datatables
    public function datatables()
    {
        $page_title = 'Datatables';
        $page_description = 'This is datatables test page';

        return view('pages.datatables', compact('page_title', 'page_description'));
    }

    // KTDatatables
    public function ktDatatables()
    {
        $page_title = 'KTDatatables';
        $page_description = 'This is KTdatatables test page';

        return view('pages.ktdatatables', compact('page_title', 'page_description'));
    }

    // Select2
    public function select2()
    {
        $page_title = 'Select 2';
        $page_description = 'This is Select2 test page';

        return view('pages.select2', compact('page_title', 'page_description'));
    }

    // jQuery-mask
    public function jQueryMask()
    {
        $page_title = 'jquery-mask';
        $page_description = 'This is jquery masks test page';

        return view('pages.jquery-mask', compact('page_title', 'page_description'));
    }

    // custom-icons
    public function customIcons()
    {
        $page_title = 'customIcons';
        $page_description = 'This is customIcons test page';

        return view('pages.icons.custom-icons', compact('page_title', 'page_description'));
    }

    // flaticon
    public function flaticon()
    {
        $page_title = 'flaticon';
        $page_description = 'This is flaticon test page';

        return view('pages.icons.flaticon', compact('page_title', 'page_description'));
    }

    // fontawesome
    public function fontawesome()
    {
        $page_title = 'fontawesome';
        $page_description = 'This is fontawesome test page';

        return view('pages.icons.fontawesome', compact('page_title', 'page_description'));
    }

    // lineawesome
    public function lineawesome()
    {
        $page_title = 'lineawesome';
        $page_description = 'This is lineawesome test page';

        return view('pages.icons.lineawesome', compact('page_title', 'page_description'));
    }

    // socicons
    public function socicons()
    {
        $page_title = 'socicons';
        $page_description = 'This is socicons test page';

        return view('pages.icons.socicons', compact('page_title', 'page_description'));
    }

    // svg
    public function svg()
    {
        $page_title = 'svg';
        $page_description = 'This is svg test page';

        return view('pages.icons.svg', compact('page_title', 'page_description'));
    }

    // Quicksearch Result
    public function quickSearch()
    {
        return view('layout.partials.extras._quick_search_result');
    }

    public function menu()
    {
        $menu = DB::table('vmenus')
            ->get();

        return view('layout.base._header', compact('menu'));
    }
}
