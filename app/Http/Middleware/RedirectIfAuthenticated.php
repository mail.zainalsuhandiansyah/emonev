<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  ...$guards
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ...$guards)
    {
        $guards = empty($guards) ? [null] : $guards;

        foreach ($guards as $guard) {
            if (Auth::guard($guard)->check()) {
                $privileges_id = Auth::user()->privileges_id;
                if ($privileges_id == 3) {
                    $redirect = redirect()->route('dashboardindustri');
                } else if ($privileges_id == 2) {
                    $redirect = redirect()->route('dashboardevaluator');
                } else {
                    $redirect = redirect()->route('dashboardadmin');
                }
                return $redirect;
                // return redirect(RouteServiceProvider::HOME);
            }
        }

        return $next($request);
    }
}
