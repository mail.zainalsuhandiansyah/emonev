<?php

namespace App\Http\View\Composers;

use Illuminate\View\View;
use Illuminate\Support\Facades\DB;

class MenuComposer
{
    public function compose(View $view)
    {
        $menu = DB::table('vmenus')
            ->get();

        $view->with('_header', $menu);
    }
}
