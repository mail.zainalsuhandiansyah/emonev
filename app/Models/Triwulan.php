<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Triwulan extends Model
{
    protected $table = 'emonev_triwulan';

    protected $fillable = [
        "triwulan_short",
        "triwulan_description"
    ];
}
