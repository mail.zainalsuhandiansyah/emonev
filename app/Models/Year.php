<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Year extends Model
{
    protected $table = 'emonev_year';

    protected $fillable = [
        "year_name",
        "year_active"
    ];

    public function roadmaps()
    {
        return $this->belongsToMany(Roadmap::class, 'year_id');
    }
}
