<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Roadmap extends Model
{
    protected $table = 'emonev_roadmap';

    protected $fillable = [
        "zat_active_id",
        "zat_active_name",
        "users_id",
        "year_id",
        "triwulan_id",
        "roadmap_title"
    ];

    public function Year()
    {
        return $this->belongsToMany(Year::class, 'year_id');
    }
}
