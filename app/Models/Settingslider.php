<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class Settingslider extends Model
{
    protected $table = 'emonev_slider';

    protected $primaryKey = 'id';

    protected $fillable = [
        "uuid",
        "judul",
        "image",
        "created_by",
        "created_at",
        "updated_by",
        "updated_at",
        "deleted_by",
        "deleted_at",

        
    ];
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->uuid = Str::uuid()->getHex()->toString();
        });
    }

    public function getSlugAttribute()
    {
        return Str::slug($this->judul, "-");
    }

    public function getImageUrlAttribute()
    {
        return Storage::url('public/images/slider/' . $this->image);
    }
}
