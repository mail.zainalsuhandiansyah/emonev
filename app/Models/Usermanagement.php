<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Usermanagement extends Model
{
    protected $table = 'emonev_users';

    protected $fillable = [
        "users_id",
        "username",
        "password",
        "fullname",
        "email",
        "photo",
        "privileges_id",
        "status",
        "created_by",
        "created_at",
        "updated_by",
        "updated_at",
        "deleted_by",
        "deleted_at",
    ];
}
