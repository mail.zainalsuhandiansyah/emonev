<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Registrasiindustri extends Model
{
    protected $table = 'emonev_company';

    protected $fillable = [
        "company_id",
        "users_id",
        "company_nib",
        "company_users_elic",
        "company_name",
        "company_npwp",
        "company_address",
        "company_phone",
        "company_fax",
        "company_email",
        "company_capital",
        "company_type",
        "permission_first_number",
        "permission_first_date",
        "permission_final_number",
        "permission_final_date",
        "company_pic",
        "company_pic_position",
        "company_pic_phone",
        "company_pic_email",
        "created_by",
        "created_at",
        "updated_by",
        "updated_at",
        "deleted_by",
        "deleted_at",
    ];
}
