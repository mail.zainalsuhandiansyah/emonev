<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Privileges extends Model
{
    protected $table = 'emonev_privileges';

    protected $fillable = [
        "privilege_id",
        "privilege_name",
        "privilege_theme",
        "privilege_flag",
        "created_by",
        "created_at",
        "updated_by",
        "updated_at",
        "deleted_by",
        "deleted_at",
    ];
}
