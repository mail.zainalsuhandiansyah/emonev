<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterBBO extends Model
{
    protected $table = 'emonev_material';

    protected $fillable = [
        "material_id",
        "zat_active_id",
        "cas_number",
        "zat_active_name",
        "users_id",
        "status_id",
        "created_by",
        "created_at",
        "updated_by",
        "updated_at",
        "deleted_by",
        "deleted_at",
    ];
}
