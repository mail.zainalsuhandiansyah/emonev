<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Hirilisasi extends Model
{
    protected $table = 'emonev_hirilisasi';

    protected $fillable = [
        "hirilisasi_id",
        "zat_active_id",
        "created_by",
        "created_at",
        "updated_by",
        "updated_at",
        "deleted_by",
        "deleted_at",
    ];

    // public function details()
    // {
    //     return $this->hasMany(PelaporanDetail::class);
    // }
}
