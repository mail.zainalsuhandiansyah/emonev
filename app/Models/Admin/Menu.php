<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = 'menu';

    protected $fillable = [
        "menus_name",
        "menus_path",
        "created_at",
        "updated_at",
        "created_by",
        "updated_by"
    ];
}
