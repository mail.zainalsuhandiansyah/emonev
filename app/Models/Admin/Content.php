<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class Content extends Model
{
    protected $table = 'emonev_article';

    protected $primaryKey = 'id';

    public $incrementing = false;

    protected $keyType = 'string';

    protected $fillable = [
        "uuid",
        "image",
        "title",
        "description",
    ];

    protected $hidden = [
        "description"
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->uuid = Str::uuid()->getHex()->toString();
        });
    }

    public function getSlugAttribute()
    {
        return Str::slug($this->title, "-");
    }

    public function getLimitTitleAttribute()
    {
        return Str::limit($this->title, 50);
    }

    public function getLimitContentAttribute()
    {
        $removed_html_tags = strip_tags($this->description);
        $explode_by_dot = explode(".", $removed_html_tags);

        return count($explode_by_dot) > 0 ? Str::limit($explode_by_dot[0], 500, "...") : null;
    }

    public function getImageUrlAttribute()
    {
        return Storage::url('images/article/' . $this->image);
    }
}
