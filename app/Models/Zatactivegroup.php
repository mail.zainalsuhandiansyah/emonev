<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Zatactivegroup extends Model
{
    protected $table = 'emonev_zat_group';

    protected $fillable = [
        "zat_group_id",
        "zat_group_description",
        "zat_group_order",
        "created_by",
        "created_at",
        "updated_by",
        "updated_at",
        "deleted_by",
        "deleted_at",
    ];
}
