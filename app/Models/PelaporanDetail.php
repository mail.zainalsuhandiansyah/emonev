<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PelaporanDetail extends Model
{
    protected $table = 'emonev_reporting_detail';

    protected $fillable = [
        "user_id",
        "zat_active",
        "zat-active_name",
        "created_by",
        "created_at",
        "updated_by",
        "updated_at",
        "deleted_by",
        "deleted_at",
    ];

    public function pelaporan()
    {
        return $this->belongsTo(Pelaporan::class);
    }
}
