<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Settingfooter extends Model
{
    protected $table = 'emonev_footer';

    protected $fillable = [
        "id",
        "nama_perusahaan",
        "tempat",
        "alamat_perusahaan",
        "kota",
        "kodepos",
        "nama_callcenter",
        "notelp",
        // "created_at",
        // "created_by",
        // "updated_by",
        // "updated_at",
        // "deleted_by",
        // "deleted_at",

        
    ];
}
