<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Visitor extends Model
{
    protected $table = 'shetabit_visits';

    protected $dateFormat = 'Y-m-d';

    protected $dates = ['driver_expiration', 'created_at', 'updated_at'];

    protected $casts = [
        'driver_expiration'     => 'date',
    ];

    protected $fillable = [
        'id',
        'method',
        'request',
        'url',
        'referer',
        'languages',
        'useragent',
        'headers',
        'device',
        'platform',
        'browser',
        'ip',
        'visitable_type',
        'visitable_id',
        'visitor_type',
        'visitor_id',
        'created_at',
        'updated_at'
    ];
}
