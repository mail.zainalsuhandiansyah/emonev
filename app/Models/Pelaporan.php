<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Pelaporan extends Model
{
    protected $table = 'emonev_reporting';

    protected $fillable = [
        "user_id",
        "zat_active",
        "zat-active_name",
        "created_by",
        "created_at",
        "updated_by",
        "updated_at",
        "deleted_by",
        "deleted_at",
    ];

    public function details()
    {
        return $this->hasMany(PelaporanDetail::class);
    }
}
