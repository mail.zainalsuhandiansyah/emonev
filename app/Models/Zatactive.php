<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Zatactive extends Model
{
    protected $table = 'emonev_zat_active';

    protected $fillable = [
        "zat_active_id",
        "zat_active_name",
        "zat_group_id",
        "zat_active_fornas",
        "zat_active_description",
        "chemical_form",
        "cas_number",
        "obat_id",
        "status_id",
        "created_by",
        "created_at",
        "updated_by",
        "updated_at",
        "deleted_by",
        "deleted_at",
    ];
}
