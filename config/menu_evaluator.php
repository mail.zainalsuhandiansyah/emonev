<?php
// Header menu
return [
    'items' => [
        [
            'section' => 'Evaluator',
        ],
        [
            'title' => 'Dashboard Evaluator',
            'root' => true,
            'icon' => 'media/svg/icons/Design/Layers.svg', // or can be 'flaticon-home' or any flaticon-*
            'page' => '/admin/dashboard-evaluator',
            'new-tab' => false,
        ],
        [
            'title' => 'Daftar Industri BBO',
            'root' => true,
            'icon' => 'media/svg/icons/Design/Layers.svg', // or can be 'flaticon-home' or any flaticon-*
            'page' => '/admin/daftar-industri',
            'new-tab' => false,
        ],
        [
            'title' => 'Pelaporan',
            'icon' => 'media/svg/icons/Design/Bucket.svg',
            'toggle' => 'click',
            'bullet' => 'dot',
            'root' => true,
            'submenu' => [
                [
                    'title' => 'Progress',
                    'page' => '/admin/progress',
                ],
            ]
        ],
        [
            'title' => 'Cetak',
            'icon' => 'media/svg/icons/Design/Bucket.svg',
            'bullet' => 'dot',
            'root' => true,
            'submenu' => [
                [
                    'title' => 'Cetak Pencapaian Bahan Baku Obat',
                    'page' => '/admin/cetak-bbo',
                ],
                [
                    'title' => 'Cetak Roadmap',
                    'page' => '/admin/pelaporan-roadmap-all',
                ],
                [
                    'title' => 'Cetak Roadmap industri',
                    'page' => '/admin/pelaporan-roadmap-all-industri',
                ],
                [
                    'title' => 'Cetak Roadmap Perindustri',
                    'page' => '/admin/pelaporan-roadmap-industri',
                ],
            ]
        ],
    ]

];
