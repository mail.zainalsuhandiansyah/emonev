<?php
// Header menu
return [
    'items' => [
        [
            'section' => 'Industri',
        ],
        [
            'title' => 'Dashboard',
            'root' => true,
            'icon' => 'media/svg/icons/Design/Layers.svg', // or can be 'flaticon-home' or any flaticon-*
            'page' => '/admin/dashboard-industri',
            'new-tab' => false,
        ],
        [
            'title' => 'Master',
            'icon' => 'media/svg/icons/Design/Bucket.svg',
            'bullet' => 'dot',
            'root' => true,
            'submenu' => [
                [
                    'title' => 'Master Bahan Baku',
                    'page' => '/admin/masterBBO',
                ],
                [
                    'title' => 'Master Bahan Baku yang Dikembangkan',
                    'page' => '/admin/masterBBOIndustri',
                ],
            ]
        ],
        [
            'title' => 'Profile',
            'icon' => 'media/svg/icons/Design/Bucket.svg',
            'toggle' => 'click',
            'bullet' => 'dot',
            'root' => true,
            'submenu' => [
                [
                    'title' => 'Profile Industri',
                    'page' => '/admin/profile-industri',
                ],
                [
                    'title' => 'Profile Bahan Baku',
                    'page' => '#',
                ],
            ]
        ],
        [
            'title' => 'Pelaporan',
            'icon' => 'media/svg/icons/Design/Bucket.svg',
            'toggle' => 'click',
            'bullet' => 'dot',
            'root' => true,
            'submenu' => [
                [
                    'title' => 'Progres',
                    'page' => '/admin/pelaporan-industri',
                ], [
                    'title' => 'Data Bahan Baku',
                    'page' => '/admin/pelaporan-industri',
                ],
            ]
        ],
        // [
        //     'title' => 'Cetak',
        //     'icon' => 'media/svg/icons/Design/Bucket.svg',
        //     'bullet' => 'dot',
        //     'root' => true,
        //     'submenu' => [
        //         [
        //             'title' => 'Cetak Roadmap Perindustri',
        //             'page' => '/admin/cetak-bbo',
        //         ], [
        //             'title' => 'Cetak Roadmap Perbahan baku',
        //             'page' => '/admin/pelaporan-progress-bahan-baku',
        //         ], [
        //             'title' => 'Cetak Pencapaian Bahan Baku Obat',
        //             'page' => '/admin/cetak-bbo',
        //         ],
        //         [
        //             'title' => 'Cetak Pencapaian Perindustri',
        //             'page' => '/admin/cetak-industri',
        //         ],
        //     ]
        // ],
    ]
];
